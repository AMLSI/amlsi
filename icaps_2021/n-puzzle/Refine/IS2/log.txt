# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial2.pddl
pddl/n-puzzle/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3068
x+ mean size : 14.3
x- mean size : 7.8780966
E+ size : 100
E- size : 39394
e+ mean size : 53.79
e- mean size : 35.195488
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 429.0
#States : 13
#Transitions : 25
Compression level : 33.0
PDDL Generation
Time : 2.241
Recall = 0.49
Precision = 1.0
Fscore automaton 0.6577181
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 429.0
#States : 13
#Transitions : 25
Compression level : 33.0
PDDL Generation
Time : 1.751
Recall = 0.49
Precision = 1.0
Fscore automaton 0.6577181
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 429.0
#States : 13
#Transitions : 25
Compression level : 33.0
PDDL Generation
Time : 2.15
Recall = 0.49
Precision = 1.0
Fscore automaton 0.6577181
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 429.0
#States : 13
#Transitions : 25
Compression level : 33.0
PDDL Generation
Time : 1.703
Recall = 0.49
Precision = 1.0
Fscore automaton 0.6577181
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 3454
x+ mean size : 15.233334
x- mean size : 8.237985
E+ size : 100
E- size : 36245
e+ mean size : 49.32
e- mean size : 33.418705
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 13
#Transitions : 25
Compression level : 35.153847
PDDL Generation
Time : 1.845
Recall = 0.72
Precision = 1.0
Fscore automaton 0.83720934
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 13
#Transitions : 25
Compression level : 35.153847
PDDL Generation
Time : 1.734
Recall = 0.72
Precision = 1.0
Fscore automaton 0.83720934
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 13
#Transitions : 25
Compression level : 35.153847
PDDL Generation
Time : 1.798
Recall = 0.72
Precision = 1.0
Fscore automaton 0.83720934
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 13
#Transitions : 25
Compression level : 35.153847
PDDL Generation
Time : 1.738
Recall = 0.72
Precision = 1.0
Fscore automaton 0.83720934
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 3707
x+ mean size : 15.833333
x- mean size : 8.56218
E+ size : 100
E- size : 34172
e+ mean size : 46.09
e- mean size : 32.290882
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 2.397
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 2.208
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 2.236
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 2.274
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 3490
x+ mean size : 16.0
x- mean size : 8.758452
E+ size : 100
E- size : 36102
e+ mean size : 49.0
e- mean size : 33.54753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 2.395
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 2.335
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 2.46
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 2.419
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 4
I+ size : 30
I- size : 3067
x+ mean size : 14.4
x- mean size : 8.155201
E+ size : 100
E- size : 33544
e+ mean size : 46.18
e- mean size : 32.959007
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 13
#Transitions : 25
Compression level : 33.23077
PDDL Generation
Time : 2.023
Recall = 0.65
Precision = 1.0
Fscore automaton 0.78787875
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 13
#Transitions : 25
Compression level : 33.23077
PDDL Generation
Time : 1.744
Recall = 0.65
Precision = 1.0
Fscore automaton 0.78787875
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 13
#Transitions : 25
Compression level : 33.23077
PDDL Generation
Time : 1.697
Recall = 0.65
Precision = 1.0
Fscore automaton 0.78787875
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 13
#Transitions : 25
Compression level : 33.23077
PDDL Generation
Time : 1.757
Recall = 0.65
Precision = 1.0
Fscore automaton 0.78787875
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
