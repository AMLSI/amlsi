# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial1.pddl
pddl/n-puzzle/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3300
x+ mean size : 15.166667
x- mean size : 8.36303
E+ size : 100
E- size : 34080
e+ mean size : 45.65
e- mean size : 32.42864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 2.79
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 2.495
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 2.283
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 2.122
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 3336
x+ mean size : 15.1
x- mean size : 8.536871
E+ size : 100
E- size : 38859
e+ mean size : 53.54
e- mean size : 34.17301
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 453.0
#States : 13
#Transitions : 24
Compression level : 34.846153
PDDL Generation
Time : 1.989
Recall = 0.42
Precision = 1.0
Fscore automaton 0.5915493
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 453.0
#States : 13
#Transitions : 24
Compression level : 34.846153
PDDL Generation
Time : 2.028
Recall = 0.42
Precision = 1.0
Fscore automaton 0.5915493
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 453.0
#States : 13
#Transitions : 24
Compression level : 34.846153
PDDL Generation
Time : 2.085
Recall = 0.42
Precision = 1.0
Fscore automaton 0.5915493
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 453.0
#States : 13
#Transitions : 24
Compression level : 34.846153
PDDL Generation
Time : 1.994
Recall = 0.42
Precision = 1.0
Fscore automaton 0.5915493
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 3391
x+ mean size : 14.933333
x- mean size : 8.311413
E+ size : 100
E- size : 33622
e+ mean size : 45.7
e- mean size : 32.101334
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 1.948
Recall = 0.52
Precision = 1.0
Fscore automaton 0.68421054
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 2.09
Recall = 0.52
Precision = 1.0
Fscore automaton 0.68421054
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 2.098
Recall = 0.52
Precision = 1.0
Fscore automaton 0.68421054
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 1.937
Recall = 0.52
Precision = 1.0
Fscore automaton 0.68421054
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 3333
x+ mean size : 15.233334
x- mean size : 8.219322
E+ size : 100
E- size : 41478
e+ mean size : 57.38
e- mean size : 36.68651
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 2.006
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 2.012
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 2.084
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 1.977
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 9.0661825E-4

RPNIR run : 4
I+ size : 30
I- size : 3351
x+ mean size : 15.266666
x- mean size : 8.220531
E+ size : 100
E- size : 34968
e+ mean size : 48.06
e- mean size : 33.030483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 12
#Transitions : 24
Compression level : 38.166668
PDDL Generation
Time : 2.063
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 12
#Transitions : 24
Compression level : 38.166668
PDDL Generation
Time : 1.877
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 12
#Transitions : 24
Compression level : 38.166668
PDDL Generation
Time : 1.905
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 12
#Transitions : 24
Compression level : 38.166668
PDDL Generation
Time : 1.989
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
