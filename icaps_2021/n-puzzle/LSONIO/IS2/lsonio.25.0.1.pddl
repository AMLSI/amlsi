(define (domain n-puzzle-typed)
(:requirements :strips :typing :negative-preconditions)
(:types
tile position - object
)
(:predicates
	(at ?x1 - tile ?x2 - position)
	(empty ?x1 - position)
	(neighbor ?x1 - position ?x2 - position)
)
(:action move
	:parameters (?x1 - tile ?x2 - position ?x3 - position )
	:precondition (and
	(not(at ?x1 ?x3)))
	:effect (and
	(at ?x1 ?x3))
)
)
