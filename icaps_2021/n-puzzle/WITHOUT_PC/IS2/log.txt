# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial2.pddl
pddl/n-puzzle/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 3068
x+ mean size : 14.3
x- mean size : 7.8780966
E+ size : 100
E- size : 39394
e+ mean size : 53.79
e- mean size : 35.195488
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 429.0
#States : 12
#Transitions : 24
Compression level : 35.75
PDDL Generation
Time : 25.296
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 429.0
#States : 12
#Transitions : 24
Compression level : 35.75
PDDL Generation
Time : 75.554
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 429.0
#States : 12
#Transitions : 24
Compression level : 35.75
PDDL Generation
Time : 22.529
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 429.0
#States : 12
#Transitions : 24
Compression level : 35.75
PDDL Generation
Time : 73.568
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 3454
x+ mean size : 15.233334
x- mean size : 8.237985
E+ size : 100
E- size : 36245
e+ mean size : 49.32
e- mean size : 33.418705
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 25.494
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 88.806
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 26.028
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 12
#Transitions : 24
Compression level : 38.083332
PDDL Generation
Time : 82.886
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 3707
x+ mean size : 15.833333
x- mean size : 8.56218
E+ size : 100
E- size : 34172
e+ mean size : 46.09
e- mean size : 32.290882
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 27.879
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 99.189
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 28.039
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 475.0
#States : 12
#Transitions : 24
Compression level : 39.583332
PDDL Generation
Time : 94.016
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 3490
x+ mean size : 16.0
x- mean size : 8.758452
E+ size : 100
E- size : 36102
e+ mean size : 49.0
e- mean size : 33.54753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 27.19
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 91.149
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 27.611
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 480.0
#States : 12
#Transitions : 24
Compression level : 40.0
PDDL Generation
Time : 89.625
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 3067
x+ mean size : 14.4
x- mean size : 8.155201
E+ size : 100
E- size : 33544
e+ mean size : 46.18
e- mean size : 32.959007
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 12
#Transitions : 24
Compression level : 36.0
PDDL Generation
Time : 22.89
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 12
#Transitions : 24
Compression level : 36.0
PDDL Generation
Time : 76.349
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 12
#Transitions : 24
Compression level : 36.0
PDDL Generation
Time : 22.633
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 12
#Transitions : 24
Compression level : 36.0
PDDL Generation
Time : 50.778
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
