(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(handempty))
	:effect (and
	(not(handempty)))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(ontable ?x1)))
	:effect (and
	(ontable ?x1))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(not(on ?x2 ?x1))
	(not(handempty)))
	:effect (and
	(on ?x2 ?x1)
	(handempty))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(on ?x1 ?x2))
	:effect (and
	(not(on ?x1 ?x2)))
)
)
