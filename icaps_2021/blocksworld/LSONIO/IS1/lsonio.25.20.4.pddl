(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(ontable ?x1)
	(holding ?x1))
	:effect (and
	(not(ontable ?x1))
	(not(holding ?x1)))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(ontable ?x1)))
	:effect (and
	(ontable ?x1))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(not(handempty)))
	:effect (and
	(handempty))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(holding ?x2))
	:effect (and
	(not(holding ?x2)))
)
)
