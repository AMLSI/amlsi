(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(not(handempty)))
	:effect (and
	(handempty))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(handempty)))
	:effect (and
	(handempty))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(not(holding ?x2)))
	:effect (and
	(holding ?x2))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(ontable ?x2)
	(not(holding ?x1)))
	:effect (and
	(not(ontable ?x2))
	(holding ?x1))
)
)
