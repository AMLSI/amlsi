(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(handempty))
	:effect (and
	(holding ?x1)
	(not(handempty)))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(holding ?x1)
	(not(handempty)))
	:effect (and
	(clear ?x1)
	(ontable ?x1)
	(not(holding ?x1))
	(handempty))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and)
	:effect (and
	(handempty)
	(on ?x1 ?x2))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(handempty)
	(on ?x1 ?x2))
	:effect (and
	(holding ?x1)
	(not(handempty))
	(not(on ?x1 ?x2)))
)
)
