(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(clear ?x1))
	:effect (and
	(not(clear ?x1))
	(holding ?x1))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(clear ?x1)))
	:effect (and
	(clear ?x1))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(holding ?x2))
	:effect (and
	(on ?x2 ?x1)
	(clear ?x1)
	(not(holding ?x2)))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(on ?x2 ?x1)
	(clear ?x1)
	(not(holding ?x2)))
	:effect (and
	(not(on ?x2 ?x1))
	(not(clear ?x1))
	(holding ?x2))
)
)
