(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(clear ?x1))
	:effect (and)
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and)
	:effect (and
	(clear ?x1)
	(ontable ?x1)
	(handempty))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and)
	:effect (and
	(clear ?x1))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(clear ?x1))
	:effect (and
	(not(clear ?x1)))
)
)
