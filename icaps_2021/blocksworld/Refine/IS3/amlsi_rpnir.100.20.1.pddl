(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and
	(ontable ?x1)
	(not(holding ?x1)))
	:effect (and
	(not(ontable ?x1))
	(holding ?x1))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(ontable ?x1))
	(holding ?x1)
	(not(handempty)))
	:effect (and
	(ontable ?x1)
	(not(holding ?x1))
	(handempty))
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(clear ?x2))
	:effect (and
	(clear ?x1)
	(not(clear ?x2))
	(handempty))
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(handempty))
	:effect (and
	(holding ?x1)
	(clear ?x2)
	(not(handempty)))
)
)
