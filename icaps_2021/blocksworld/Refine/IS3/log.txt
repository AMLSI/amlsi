# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial3.pddl
pddl/blocksworld/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 2095
x+ mean size : 13.833333
x- mean size : 8.172792
E+ size : 100
E- size : 23864
e+ mean size : 45.41
e- mean size : 32.710526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 4.012
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 3.194
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.3541667
Error Rate Precondition : 0.36785495
Error Rate Postcondition : 0.28341776
FSCORE : 0.0109974705
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 3.186
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 3.175
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.3263889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0058997045

RPNIR run : 1
I+ size : 30
I- size : 2272
x+ mean size : 15.4
x- mean size : 8.99912
E+ size : 100
E- size : 28025
e+ mean size : 54.08
e- mean size : 37.01067
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 4.784
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 4.903
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.2337963
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.006872853
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 4.841
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 4.64
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.16898148
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0075789476

RPNIR run : 2
I+ size : 30
I- size : 2258
x+ mean size : 14.533334
x- mean size : 8.347652
E+ size : 100
E- size : 24581
e+ mean size : 47.66
e- mean size : 33.426712
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 3.661
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 3.546
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.24537036
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.010152284
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 3.652
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 3.695
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21296297
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.03314917

RPNIR run : 3
I+ size : 30
I- size : 2143
x+ mean size : 14.466666
x- mean size : 8.271582
E+ size : 100
E- size : 26402
e+ mean size : 49.54
e- mean size : 35.303234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 3.575
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 3.632
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.27546296
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0028776978
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 3.699
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 3.539
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.30324078
Error Rate Precondition : 0.31003585
Error Rate Postcondition : 0.19747786
FSCORE : 0.009884353

RPNIR run : 4
I+ size : 30
I- size : 2262
x+ mean size : 15.3
x- mean size : 8.605659
E+ size : 100
E- size : 27580
e+ mean size : 52.15
e- mean size : 34.231472
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 3.891
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 4.115
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.3888889
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 4.013
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 4.063
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.25462964
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0059464816
