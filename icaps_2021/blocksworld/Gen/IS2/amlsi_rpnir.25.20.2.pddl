(define (domain blocks)
(:requirements :strips :typing :negative-preconditions)
(:types
block - object
)
(:predicates
	(clear ?x1 - block)
	(ontable ?x1 - block)
	(handempty)
	(holding ?x1 - block)
	(on ?x1 - block ?x2 - block)
)
(:action pick-up
	:parameters (?x1 - block )
	:precondition (and)
	:effect (and
	(holding ?x1))
)
(:action put-down
	:parameters (?x1 - block )
	:precondition (and
	(not(clear ?x1)))
	:effect (and)
)
(:action stack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and)
	:effect (and)
)
(:action unstack
	:parameters (?x1 - block ?x2 - block )
	:precondition (and
	(not(holding ?x2)))
	:effect (and
	(not(on ?x2 ?x1)))
)
)
