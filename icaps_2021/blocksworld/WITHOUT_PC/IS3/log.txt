# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial3.pddl
pddl/blocksworld/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 2095
x+ mean size : 13.833333
x- mean size : 8.172792
E+ size : 100
E- size : 23864
e+ mean size : 45.41
e- mean size : 32.710526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 32
#Transitions : 93
Compression level : 12.96875
PDDL Generation
Time : 11.525
Recall = 0.23
Precision = 0.046558704
Fscore automaton 0.07744108
Syntactical distance : 0.19907407
Error Rate Precondition : 0.24241434
Error Rate Postcondition : 0.3162111
FSCORE : 0.013422818

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 32
#Transitions : 93
Compression level : 12.96875
PDDL Generation
Time : 9.469
Recall = 0.23
Precision = 0.046558704
Fscore automaton 0.07744108
Syntactical distance : 0.35185188
Error Rate Precondition : 0.29480368
Error Rate Postcondition : 0.5349583
FSCORE : 0.00860215
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 32
#Transitions : 93
Compression level : 12.96875
PDDL Generation
Time : 9.104
Recall = 0.23
Precision = 0.046558704
Fscore automaton 0.07744108
Syntactical distance : 0.19907407
Error Rate Precondition : 0.24241434
Error Rate Postcondition : 0.3162111
FSCORE : 0.013422818

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 32
#Transitions : 93
Compression level : 12.96875
PDDL Generation
Time : 225.889
Recall = 0.23
Precision = 0.046558704
Fscore automaton 0.07744108
Syntactical distance : 0.11805556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.40241447

RPNI run : 1
I+ size : 30
I- size : 2272
x+ mean size : 15.4
x- mean size : 8.99912
E+ size : 100
E- size : 28025
e+ mean size : 54.08
e- mean size : 37.01067
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 42
Compression level : 21.0
PDDL Generation
Time : 68.913
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 42
Compression level : 21.0
PDDL Generation
Time : 427.144
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 42
Compression level : 21.0
PDDL Generation
Time : 69.722
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 42
Compression level : 21.0
PDDL Generation
Time : 346.904
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 2258
x+ mean size : 14.533334
x- mean size : 8.347652
E+ size : 100
E- size : 24581
e+ mean size : 47.66
e- mean size : 33.426712
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 26
#Transitions : 52
Compression level : 16.76923
PDDL Generation
Time : 66.633
Recall = 1.0
Precision = 0.78125
Fscore automaton 0.877193
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 26
#Transitions : 52
Compression level : 16.76923
PDDL Generation
Time : 357.381
Recall = 1.0
Precision = 0.78125
Fscore automaton 0.877193
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 26
#Transitions : 52
Compression level : 16.76923
PDDL Generation
Time : 67.519
Recall = 1.0
Precision = 0.78125
Fscore automaton 0.877193
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 26
#Transitions : 52
Compression level : 16.76923
PDDL Generation
Time : 330.69
Recall = 1.0
Precision = 0.78125
Fscore automaton 0.877193
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 2143
x+ mean size : 14.466666
x- mean size : 8.271582
E+ size : 100
E- size : 26402
e+ mean size : 49.54
e- mean size : 35.303234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 20
#Transitions : 39
Compression level : 21.7
PDDL Generation
Time : 3.407
Recall = 0.51
Precision = 0.30538923
Fscore automaton 0.38202247
Syntactical distance : 0.10185185
Error Rate Precondition : 0.044302177
Error Rate Postcondition : 0.30227986
FSCORE : 0.06993007

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 20
#Transitions : 39
Compression level : 21.7
PDDL Generation
Time : 297.885
Recall = 0.51
Precision = 0.30538923
Fscore automaton 0.38202247
Syntactical distance : 0.027777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.35906643
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 20
#Transitions : 39
Compression level : 21.7
PDDL Generation
Time : 3.383
Recall = 0.51
Precision = 0.30538923
Fscore automaton 0.38202247
Syntactical distance : 0.10185185
Error Rate Precondition : 0.044302177
Error Rate Postcondition : 0.30227986
FSCORE : 0.06993007

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 20
#Transitions : 39
Compression level : 21.7
PDDL Generation
Time : 3.359
Recall = 0.51
Precision = 0.30538923
Fscore automaton 0.38202247
Syntactical distance : 0.2638889
Error Rate Precondition : 0.14623837
Error Rate Postcondition : 0.299689
FSCORE : 0.011750881

RPNI run : 4
I+ size : 30
I- size : 2262
x+ mean size : 15.3
x- mean size : 8.605659
E+ size : 100
E- size : 27580
e+ mean size : 52.15
e- mean size : 34.231472
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 69.162
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 418.987
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 68.364
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 343.736
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
