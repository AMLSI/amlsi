# actions 11
# predicate 16
Initial state : pddl/NegVisitAll/initial_states/initial1.pddl
pddl/NegVisitAll/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1637
x+ mean size : 14.833333
x- mean size : 8.544289
E+ size : 100
E- size : 20653
e+ mean size : 49.8
e- mean size : 36.10507
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 53
#Transitions : 84
Compression level : 8.396227
PDDL Generation
Time : 47.703
Recall = 0.27
Precision = 0.13432837
Fscore automaton 0.179402
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 53
#Transitions : 84
Compression level : 8.396227
PDDL Generation
Time : 40.413
Recall = 0.27
Precision = 0.13432837
Fscore automaton 0.179402
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 53
#Transitions : 84
Compression level : 8.396227
PDDL Generation
Time : 38.934
Recall = 0.27
Precision = 0.13432837
Fscore automaton 0.179402
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 53
#Transitions : 84
Compression level : 8.396227
PDDL Generation
Time : 42.965
Recall = 0.27
Precision = 0.13432837
Fscore automaton 0.179402
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1620
x+ mean size : 15.3
x- mean size : 8.671605
E+ size : 100
E- size : 23577
e+ mean size : 56.43
e- mean size : 37.923824
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 48
#Transitions : 80
Compression level : 9.5625
PDDL Generation
Time : 44.723
Recall = 0.28
Precision = 0.10108303
Fscore automaton 0.14854111
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 48
#Transitions : 80
Compression level : 9.5625
PDDL Generation
Time : 44.355
Recall = 0.28
Precision = 0.10108303
Fscore automaton 0.14854111
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 48
#Transitions : 80
Compression level : 9.5625
PDDL Generation
Time : 39.385
Recall = 0.28
Precision = 0.10108303
Fscore automaton 0.14854111
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 48
#Transitions : 80
Compression level : 9.5625
PDDL Generation
Time : 43.003
Recall = 0.28
Precision = 0.10108303
Fscore automaton 0.14854111
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1608
x+ mean size : 14.633333
x- mean size : 8.478234
E+ size : 100
E- size : 22303
e+ mean size : 54.11
e- mean size : 35.990627
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 52
#Transitions : 82
Compression level : 8.442307
PDDL Generation
Time : 39.785
Recall = 0.29
Precision = 0.085798815
Fscore automaton 0.13242008
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 52
#Transitions : 82
Compression level : 8.442307
PDDL Generation
Time : 37.732
Recall = 0.29
Precision = 0.085798815
Fscore automaton 0.13242008
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 52
#Transitions : 82
Compression level : 8.442307
PDDL Generation
Time : 36.146
Recall = 0.29
Precision = 0.085798815
Fscore automaton 0.13242008
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 52
#Transitions : 82
Compression level : 8.442307
PDDL Generation
Time : 43.723
Recall = 0.29
Precision = 0.085798815
Fscore automaton 0.13242008
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1586
x+ mean size : 14.7
x- mean size : 8.778058
E+ size : 100
E- size : 20301
e+ mean size : 49.65
e- mean size : 35.77602
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 43
#Transitions : 73
Compression level : 10.255814
PDDL Generation
Time : 41.791
Recall = 0.5
Precision = 0.08116883
Fscore automaton 0.1396648
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 43
#Transitions : 73
Compression level : 10.255814
PDDL Generation
Time : 42.074
Recall = 0.5
Precision = 0.08116883
Fscore automaton 0.1396648
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 43
#Transitions : 73
Compression level : 10.255814
PDDL Generation
Time : 36.54
Recall = 0.5
Precision = 0.08116883
Fscore automaton 0.1396648
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 43
#Transitions : 73
Compression level : 10.255814
PDDL Generation
Time : 35.369
Recall = 0.5
Precision = 0.08116883
Fscore automaton 0.1396648
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1583
x+ mean size : 15.0
x- mean size : 8.469362
E+ size : 100
E- size : 20805
e+ mean size : 50.94
e- mean size : 35.990772
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 35.195
Recall = 0.66
Precision = 0.077738516
Fscore automaton 0.13909377
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 36.765
Recall = 0.66
Precision = 0.077738516
Fscore automaton 0.13909377
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 35.705
Recall = 0.66
Precision = 0.077738516
Fscore automaton 0.13909377
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 40.409
Recall = 0.66
Precision = 0.077738516
Fscore automaton 0.13909377
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
