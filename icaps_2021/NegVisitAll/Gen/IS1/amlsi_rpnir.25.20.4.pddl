(define (domain grid-visit-all)
(:requirements :strips :typing :negative-preconditions)
(:types
place - object
)
(:predicates
	(at-robot ?x1 - place)
	(visited ?x1 - place)
	(connected ?x1 - place ?x2 - place)
)
(:action move
	:parameters (?x1 - place ?x2 - place )
	:precondition (and
	(connected ?x1 ?x2))
	:effect (and
	(not(at-robot ?x1)))
)
(:action visit
	:parameters (?x1 - place )
	:precondition (and
	(at-robot ?x1))
	:effect (and)
)
)
