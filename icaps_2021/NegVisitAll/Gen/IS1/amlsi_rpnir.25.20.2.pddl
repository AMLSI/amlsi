(define (domain grid-visit-all)
(:requirements :strips :typing :negative-preconditions)
(:types
place - object
)
(:predicates
	(at-robot ?x1 - place)
	(visited ?x1 - place)
	(connected ?x1 - place ?x2 - place)
)
(:action move
	:parameters (?x1 - place ?x2 - place )
	:precondition (and
	(connected ?x1 ?x2))
	:effect (and
	(at-robot ?x2))
)
(:action visit
	:parameters (?x1 - place )
	:precondition (and
	(not(visited ?x1)))
	:effect (and)
)
)
