# actions 9
# predicate 13
Initial state : pddl/NegVisitAll/initial_states/initial3.pddl
pddl/NegVisitAll/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 1274
x+ mean size : 15.0
x- mean size : 8.771585
E+ size : 100
E- size : 16565
e+ mean size : 50.28
e- mean size : 35.80543
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
1724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.16260162

*** Noise = 20.0% ***
1724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.27324688
Error Rate Postcondition : 0.051512618
FSCORE : 0.0

RPNI-R run : 1
I+ size : 30
I- size : 1329
x+ mean size : 15.266666
x- mean size : 9.3927765
E+ size : 100
E- size : 16588
e+ mean size : 49.09
e- mean size : 34.914818
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1787 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.005319149

*** Noise = 20.0% ***
1787 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.008053692
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1787 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.16420361

*** Noise = 20.0% ***
1787 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
visited:?x2
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.19444445
Error Rate Precondition : 0.07768018
Error Rate Postcondition : 0.40279394
FSCORE : 0.0

RPNI-R run : 2
I+ size : 30
I- size : 1354
x+ mean size : 15.4
x- mean size : 9.19424
E+ size : 100
E- size : 15488
e+ mean size : 47.66
e- mean size : 35.090843
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1816 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01025641

*** Noise = 20.0% ***
1816 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.30555555
Error Rate Precondition : 0.07763323
Error Rate Postcondition : 0.0
FSCORE : 0.005347593
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1816 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.14803849

*** Noise = 20.0% ***
1816 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.19444445
Error Rate Precondition : 0.39587426
Error Rate Postcondition : 0.08393364
FSCORE : 0.007858546

RPNI-R run : 3
I+ size : 30
I- size : 1390
x+ mean size : 15.433333
x- mean size : 9.5474825
E+ size : 100
E- size : 16307
e+ mean size : 49.86
e- mean size : 36.10063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1852 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01734104

*** Noise = 20.0% ***
1852 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01734104
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1852 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.18132366

*** Noise = 20.0% ***
1852 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.25
Error Rate Precondition : 0.43351385
Error Rate Postcondition : 0.066486165
FSCORE : 0.018181818

RPNI-R run : 4
I+ size : 30
I- size : 1268
x+ mean size : 14.766666
x- mean size : 8.644321
E+ size : 100
E- size : 17160
e+ mean size : 51.96
e- mean size : 36.774475
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1709 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.016483517

*** Noise = 20.0% ***
1709 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.020725388
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1709 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.16260162

*** Noise = 20.0% ***
1709 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.3611111
Error Rate Precondition : 0.59708214
Error Rate Postcondition : 0.07758505
FSCORE : 0.006309148
