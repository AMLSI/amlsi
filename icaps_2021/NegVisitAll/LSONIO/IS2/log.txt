# actions 6
# predicate 9
Initial state : pddl/NegVisitAll/initial_states/initial2.pddl
pddl/NegVisitAll/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 900
x+ mean size : 14.866667
x- mean size : 8.868889
E+ size : 100
E- size : 13047
e+ mean size : 54.2
e- mean size : 35.577526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1341 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0067114094

*** Noise = 20.0% ***
1341 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
at-robot:?x1
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.051784463
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1341 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.3058104

*** Noise = 20.0% ***
1341 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.3058104

RPNI-R run : 1
I+ size : 30
I- size : 892
x+ mean size : 15.166667
x- mean size : 9.257848
E+ size : 100
E- size : 11545
e+ mean size : 49.27
e- mean size : 34.478302
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1327 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.012903227

*** Noise = 20.0% ***
1327 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0137299765
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1327 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.2857143

*** Noise = 20.0% ***
1327 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.19444445
Error Rate Precondition : 0.029125229
Error Rate Postcondition : 0.0
FSCORE : 0.008583692

RPNI-R run : 2
I+ size : 30
I- size : 883
x+ mean size : 14.133333
x- mean size : 8.725934
E+ size : 100
E- size : 12597
e+ mean size : 52.18
e- mean size : 34.76288
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1287 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.014150944

*** Noise = 20.0% ***
1287 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.22222224
Error Rate Precondition : 0.8901878
Error Rate Postcondition : 0.054043695
FSCORE : 0.0096852295
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1287 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.2853067

*** Noise = 20.0% ***
1287 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
at-robot:?x1
Syntactical distance : 0.1388889
Error Rate Precondition : 0.3023498
Error Rate Postcondition : 0.0372974
FSCORE : 0.039215688

RPNI-R run : 3
I+ size : 30
I- size : 865
x+ mean size : 14.333333
x- mean size : 8.894797
E+ size : 100
E- size : 12390
e+ mean size : 52.3
e- mean size : 35.445522
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1283 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0125

*** Noise = 20.0% ***
1283 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0125
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1283 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.27855152

*** Noise = 20.0% ***
1283 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
visited:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.019192588
Error Rate Postcondition : 0.30774322
FSCORE : 0.005988024

RPNI-R run : 4
I+ size : 30
I- size : 946
x+ mean size : 15.7
x- mean size : 8.866808
E+ size : 100
E- size : 10665
e+ mean size : 46.65
e- mean size : 33.12958
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1374 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004761905

*** Noise = 20.0% ***
1374 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004761905
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1374 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
at-robot:?x1
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.2770083

*** Noise = 20.0% ***
1374 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robot ?x1), (at-robot ?x2), (visited ?x1), (visited ?x2), (connected ?x1 ?x2)]
[(at-robot ?x1), (visited ?x1)]
Syntactical distance : 0.22222224
Error Rate Precondition : 0.62728155
Error Rate Postcondition : 0.35141176
FSCORE : 0.0
