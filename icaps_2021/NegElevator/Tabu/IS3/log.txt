# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial3.pddl
pddl/NegElevator/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1049
x+ mean size : 15.5
x- mean size : 8.769304
E+ size : 100
E- size : 13146
e+ mean size : 52.3
e- mean size : 35.592422
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 39.25
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 36.476
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 33.65
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 33.512
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1031
x+ mean size : 15.333333
x- mean size : 8.636275
E+ size : 100
E- size : 11995
e+ mean size : 47.51
e- mean size : 35.519382
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 34.365
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 36.811
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 33.143
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 33.119
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1022
x+ mean size : 14.6
x- mean size : 8.460861
E+ size : 100
E- size : 13935
e+ mean size : 54.49
e- mean size : 38.91353
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 33.183
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 35.049
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 31.693
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 31.848
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1086
x+ mean size : 15.0
x- mean size : 8.776243
E+ size : 100
E- size : 13148
e+ mean size : 50.71
e- mean size : 35.437557
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 36.472
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 37.415
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 35.228
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 34.579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1014
x+ mean size : 14.833333
x- mean size : 8.325444
E+ size : 100
E- size : 12410
e+ mean size : 49.6
e- mean size : 34.17325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 33.123
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 33.898
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 31.436
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 31.714
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
