# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial1.pddl
pddl/NegElevator/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 1095
x+ mean size : 14.633333
x- mean size : 8.162557
E+ size : 100
E- size : 13821
e+ mean size : 52.99
e- mean size : 34.13248
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 36.226
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 68.971
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 18.591
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 52.996
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 1133
x+ mean size : 14.833333
x- mean size : 8.496028
E+ size : 100
E- size : 12315
e+ mean size : 47.98
e- mean size : 34.52075
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 40
Compression level : 21.190475
PDDL Generation
Time : 35.263
Recall = 0.52
Precision = 0.15476191
Fscore automaton 0.23853211
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 40
Compression level : 21.190475
PDDL Generation
Time : 4.527
Recall = 0.52
Precision = 0.15476191
Fscore automaton 0.23853211
Syntactical distance : 0.13333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.48096064
FSCORE : 0.022087246
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 40
Compression level : 21.190475
PDDL Generation
Time : 20.067
Recall = 0.52
Precision = 0.15476191
Fscore automaton 0.23853211
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 40
Compression level : 21.190475
PDDL Generation
Time : 65.043
Recall = 0.52
Precision = 0.15476191
Fscore automaton 0.23853211
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 1094
x+ mean size : 15.566667
x- mean size : 8.624314
E+ size : 100
E- size : 12076
e+ mean size : 47.19
e- mean size : 34.12852
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 22
#Transitions : 39
Compression level : 21.227272
PDDL Generation
Time : 35.488
Recall = 0.78
Precision = 0.16738197
Fscore automaton 0.27561837
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 22
#Transitions : 39
Compression level : 21.227272
PDDL Generation
Time : 54.58
Recall = 0.78
Precision = 0.16738197
Fscore automaton 0.27561837
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 22
#Transitions : 39
Compression level : 21.227272
PDDL Generation
Time : 19.861
Recall = 0.78
Precision = 0.16738197
Fscore automaton 0.27561837
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 22
#Transitions : 39
Compression level : 21.227272
PDDL Generation
Time : 65.016
Recall = 0.78
Precision = 0.16738197
Fscore automaton 0.27561837
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 1044
x+ mean size : 15.333333
x- mean size : 9.1341
E+ size : 100
E- size : 13375
e+ mean size : 51.54
e- mean size : 36.472973
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 20
#Transitions : 38
Compression level : 23.0
PDDL Generation
Time : 21.459
Recall = 0.62
Precision = 0.11742424
Fscore automaton 0.19745222
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 20
#Transitions : 38
Compression level : 23.0
PDDL Generation
Time : 48.386
Recall = 0.62
Precision = 0.11742424
Fscore automaton 0.19745222
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 20
#Transitions : 38
Compression level : 23.0
PDDL Generation
Time : 3.732
Recall = 0.62
Precision = 0.11742424
Fscore automaton 0.19745222
Syntactical distance : 0.05
Error Rate Precondition : 0.010807053
Error Rate Postcondition : 0.016900573
FSCORE : 0.18181819

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 20
#Transitions : 38
Compression level : 23.0
PDDL Generation
Time : 38.268
Recall = 0.62
Precision = 0.11742424
Fscore automaton 0.19745222
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 1014
x+ mean size : 15.266666
x- mean size : 8.962524
E+ size : 100
E- size : 13207
e+ mean size : 50.94
e- mean size : 35.884457
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 29
#Transitions : 53
Compression level : 15.793103
PDDL Generation
Time : 40.303
Recall = 0.71
Precision = 0.13271028
Fscore automaton 0.22362205
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 29
#Transitions : 53
Compression level : 15.793103
PDDL Generation
Time : 56.122
Recall = 0.71
Precision = 0.13271028
Fscore automaton 0.22362205
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 29
#Transitions : 53
Compression level : 15.793103
PDDL Generation
Time : 21.59
Recall = 0.71
Precision = 0.13271028
Fscore automaton 0.22362205
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 29
#Transitions : 53
Compression level : 15.793103
PDDL Generation
Time : 52.638
Recall = 0.71
Precision = 0.13271028
Fscore automaton 0.22362205
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
