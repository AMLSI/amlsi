(define (domain miconic)
(:requirements :strips :typing :negative-preconditions)
(:types
floor passenger - object
)
(:predicates
	(lift-at ?x1 - floor)
	(boarded ?x1 - passenger)
	(served ?x1 - passenger)
	(above ?x1 - floor ?x2 - floor)
	(origin ?x1 - passenger ?x2 - floor)
	(destin ?x1 - passenger ?x2 - floor)
)
(:action board
	:parameters (?x1 - floor ?x2 - passenger )
	:precondition (and
	(origin ?x2 ?x1))
	:effect (and
	(lift-at ?x1))
)
(:action depart
	:parameters (?x1 - floor ?x2 - passenger )
	:precondition (and
	(destin ?x2 ?x1))
	:effect (and
	(not(lift-at ?x1)))
)
(:action up
	:parameters (?x1 - floor ?x2 - floor )
	:precondition (and
	(above ?x1 ?x2))
	:effect (and)
)
(:action down
	:parameters (?x1 - floor ?x2 - floor )
	:precondition (and
	(above ?x2 ?x1))
	:effect (and)
)
)
