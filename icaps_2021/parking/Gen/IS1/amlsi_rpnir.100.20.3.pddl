(define (domain parking)
(:requirements :strips :typing :negative-preconditions)
(:types
car curb - object
)
(:predicates
	(car-clear ?x1 - car)
	(curb-clear ?x1 - curb)
	(at-curb-num ?x1 - car ?x2 - curb)
	(at-curb ?x1 - car)
	(behind-car ?x1 - car ?x2 - car)
)
(:action move-curb-to-curb
	:parameters (?x1 - car ?x2 - curb ?x3 - curb )
	:precondition (and)
	:effect (and)
)
(:action move-curb-to-car
	:parameters (?x1 - car ?x2 - curb ?x3 - car )
	:precondition (and)
	:effect (and)
)
(:action move-car-to-curb
	:parameters (?x1 - car ?x2 - car ?x3 - curb )
	:precondition (and)
	:effect (and
	(at-curb ?x1))
)
(:action move-car-to-car
	:parameters (?x1 - car ?x2 - car ?x3 - car )
	:precondition (and
	(behind-car ?x1 ?x2)
	(not(behind-car ?x1 ?x3))
	(car-clear ?x3)
	(at-curb ?x2)
	(not(behind-car ?x3 ?x1))
	(not(behind-car ?x2 ?x3))
	(not(behind-car ?x2 ?x1)))
	:effect (and)
)
)
