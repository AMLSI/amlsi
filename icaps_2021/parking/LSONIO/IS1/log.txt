# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial1.pddl
pddl/parking/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 5794
x+ mean size : 14.7
x- mean size : 8.358475
E+ size : 100
E- size : 69720
e+ mean size : 52.15
e- mean size : 33.316868
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
6235 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
Syntactical distance : 0.24537037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0019970045

*** Noise = 20.0% ***
6235 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x3
Syntactical distance : 0.25694448
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 8.726003E-4
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
6235 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0698812

*** Noise = 20.0% ***
6235 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x1
behind-car:?x2:?x1
Syntactical distance : 0.23148149
Error Rate Precondition : 0.06745198
Error Rate Postcondition : 0.2154862
FSCORE : 0.0025773195

RPNI-R run : 1
I+ size : 30
I- size : 6105
x+ mean size : 15.233334
x- mean size : 7.9408684
E+ size : 100
E- size : 73782
e+ mean size : 54.99
e- mean size : 35.674988
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
6562 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.23842593
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0018993352

*** Noise = 20.0% ***
6562 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
behind-car:?x1:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
car-clear:?x1
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.28935185
Error Rate Precondition : 0.31274083
Error Rate Postcondition : 0.31646985
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
6562 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.067317404

*** Noise = 20.0% ***
6562 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
curb-clear:?x2
car-clear:?x3
behind-car:?x1:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
behind-car:?x2:?x3
Syntactical distance : 0.30555558
Error Rate Precondition : 0.358651
Error Rate Postcondition : 0.30491066
FSCORE : 0.0

RPNI-R run : 2
I+ size : 30
I- size : 6259
x+ mean size : 15.5
x- mean size : 8.653459
E+ size : 100
E- size : 62917
e+ mean size : 46.68
e- mean size : 32.31421
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
6724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.24768518
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0059391237

*** Noise = 20.0% ***
6724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
car-clear:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
at-curb:?x2
Syntactical distance : 0.26851854
Error Rate Precondition : 0.18724981
Error Rate Postcondition : 0.3841364
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
6724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.07607455

*** Noise = 20.0% ***
6724 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
at-curb-num:?x2:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
at-curb:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
at-curb:?x3
Syntactical distance : 0.2824074
Error Rate Precondition : 0.123291016
Error Rate Postcondition : 0.31414795
FSCORE : 0.004514673

RPNI-R run : 3
I+ size : 30
I- size : 6089
x+ mean size : 15.0
x- mean size : 8.27952
E+ size : 100
E- size : 70826
e+ mean size : 52.64
e- mean size : 34.61439
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
6539 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x3
Syntactical distance : 0.23842593
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0023076925

*** Noise = 20.0% ***
6539 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x3
Syntactical distance : 0.28703701
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.17819148
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
6539 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.06702413

*** Noise = 20.0% ***
6539 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x1
at-curb:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
Syntactical distance : 0.22916667
Error Rate Precondition : 0.068894655
Error Rate Postcondition : 0.3042521
FSCORE : 0.0

RPNI-R run : 4
I+ size : 30
I- size : 5502
x+ mean size : 13.633333
x- mean size : 7.757361
E+ size : 100
E- size : 63547
e+ mean size : 47.59
e- mean size : 32.401325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
5911 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.22453704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
5911 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
at-curb:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
Syntactical distance : 0.3587963
Error Rate Precondition : 0.20768347
Error Rate Postcondition : 0.57363325
FSCORE : 0.0024539877
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
5911 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.076452605

*** Noise = 20.0% ***
5911 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
at-curb:?x1
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
behind-car:?x3:?x1
car-clear:?x3
Syntactical distance : 0.23611112
Error Rate Precondition : 0.33103096
Error Rate Postcondition : 0.4389017
FSCORE : 0.0
