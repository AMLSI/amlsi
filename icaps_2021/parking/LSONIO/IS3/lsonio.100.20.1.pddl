(define (domain parking)
(:requirements :strips :typing :negative-preconditions)
(:types
car curb - object
)
(:predicates
	(car-clear ?x1 - car)
	(curb-clear ?x1 - curb)
	(at-curb-num ?x1 - car ?x2 - curb)
	(at-curb ?x1 - car)
	(behind-car ?x1 - car ?x2 - car)
)
(:action move-curb-to-curb
	:parameters (?x1 - car ?x2 - curb ?x3 - curb )
	:precondition (and
	(curb-clear ?x3)
	(at-curb-num ?x1 ?x2)
	(at-curb ?x1))
	:effect (and
	(not(curb-clear ?x3))
	(not(at-curb-num ?x1 ?x2))
	(not(at-curb ?x1)))
)
(:action move-curb-to-car
	:parameters (?x1 - car ?x2 - curb ?x3 - car )
	:precondition (and
	(not(behind-car ?x1 ?x3))
	(at-curb-num ?x1 ?x2))
	:effect (and
	(behind-car ?x1 ?x3)
	(not(at-curb-num ?x1 ?x2)))
)
(:action move-car-to-curb
	:parameters (?x1 - car ?x2 - car ?x3 - curb )
	:precondition (and
	(car-clear ?x1)
	(not(at-curb ?x1)))
	:effect (and
	(not(car-clear ?x1))
	(at-curb ?x1))
)
(:action move-car-to-car
	:parameters (?x1 - car ?x2 - car ?x3 - car )
	:precondition (and
	(not(behind-car ?x1 ?x2))
	(not(behind-car ?x3 ?x1)))
	:effect (and
	(behind-car ?x1 ?x2)
	(behind-car ?x3 ?x1))
)
)
