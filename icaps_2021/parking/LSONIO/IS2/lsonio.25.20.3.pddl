(define (domain parking)
(:requirements :strips :typing :negative-preconditions)
(:types
car curb - object
)
(:predicates
	(car-clear ?x1 - car)
	(curb-clear ?x1 - curb)
	(at-curb-num ?x1 - car ?x2 - curb)
	(at-curb ?x1 - car)
	(behind-car ?x1 - car ?x2 - car)
)
(:action move-curb-to-curb
	:parameters (?x1 - car ?x2 - curb ?x3 - curb )
	:precondition (and
	(not(at-curb-num ?x1 ?x3)))
	:effect (and
	(at-curb-num ?x1 ?x3))
)
(:action move-curb-to-car
	:parameters (?x1 - car ?x2 - curb ?x3 - car )
	:precondition (and
	(not(curb-clear ?x2)))
	:effect (and
	(curb-clear ?x2))
)
(:action move-car-to-curb
	:parameters (?x1 - car ?x2 - car ?x3 - curb )
	:precondition (and
	(at-curb-num ?x2 ?x3))
	:effect (and
	(not(at-curb-num ?x2 ?x3)))
)
(:action move-car-to-car
	:parameters (?x1 - car ?x2 - car ?x3 - car )
	:precondition (and
	(not(at-curb ?x1)))
	:effect (and
	(at-curb ?x1))
)
)
