# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial2.pddl
pddl/parking/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 5570
x+ mean size : 15.1
x- mean size : 8.598025
E+ size : 100
E- size : 64214
e+ mean size : 50.98
e- mean size : 33.534462
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
6023 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x3
Syntactical distance : 0.24074073
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0014705881

*** Noise = 20.0% ***
6023 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
at-curb:?x2
car-clear:?x3
Syntactical distance : 0.26851854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.2822891
FSCORE : 0.0013490724
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
6023 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.049346164

*** Noise = 20.0% ***
6023 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
at-curb:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
at-curb:?x2
car-clear:?x3
Syntactical distance : 0.23148148
Error Rate Precondition : 0.08236377
Error Rate Postcondition : 0.3301124
FSCORE : 0.0

RPNI-R run : 1
I+ size : 30
I- size : 5354
x+ mean size : 14.533334
x- mean size : 8.282032
E+ size : 100
E- size : 63818
e+ mean size : 51.26
e- mean size : 34.81048
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
5790 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.24768518
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0033707863

*** Noise = 20.0% ***
5790 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
at-curb:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
car-clear:?x3
Syntactical distance : 0.24305557
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.14209828
FSCORE : 0.0033613446
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
5790 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.051177073

*** Noise = 20.0% ***
5790 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
behind-car:?x1:?x3
Syntactical distance : 0.2546296
Error Rate Precondition : 0.12321622
Error Rate Postcondition : 0.12410196
FSCORE : 0.0034752388

RPNI-R run : 2
I+ size : 30
I- size : 5344
x+ mean size : 15.066667
x- mean size : 8.426646
E+ size : 100
E- size : 64570
e+ mean size : 51.99
e- mean size : 35.06142
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
5796 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.25694445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
5796 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
at-curb:?x3
Syntactical distance : 0.2685185
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.2535103
FSCORE : 0.0014440434
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
5796 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.04853191

*** Noise = 20.0% ***
5796 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb-num:?x2:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.26851854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.38257357
FSCORE : 0.0025348542

RPNI-R run : 3
I+ size : 30
I- size : 5410
x+ mean size : 14.433333
x- mean size : 8.132532
E+ size : 100
E- size : 63620
e+ mean size : 49.61
e- mean size : 33.275887
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
5843 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.27083334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0033333332

*** Noise = 20.0% ***
5843 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
at-curb-num:?x3:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.31250003
Error Rate Precondition : 0.2529732
Error Rate Postcondition : 0.24410401
FSCORE : 0.0013586957
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
5843 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.050671395

*** Noise = 20.0% ***
5843 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
car-clear:?x3
at-curb:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
Syntactical distance : 0.20601851
Error Rate Precondition : 0.16703637
Error Rate Postcondition : 0.24675673
FSCORE : 0.009661836

RPNI-R run : 4
I+ size : 30
I- size : 5405
x+ mean size : 15.166667
x- mean size : 8.551526
E+ size : 100
E- size : 62702
e+ mean size : 48.98
e- mean size : 34.47332
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
5860 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.2800926
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.002643754

*** Noise = 20.0% ***
5860 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
car-clear:?x1
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
at-curb:?x2
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
Syntactical distance : 0.30324075
Error Rate Precondition : 0.14525075
Error Rate Postcondition : 0.43193898
FSCORE : 0.003081664
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
5860 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
at-curb-num:?x2:?x1
car-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x2:?x1
car-clear:?x3
Syntactical distance : 0.06481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.051612902

*** Noise = 20.0% ***
5860 actions in training set
Train Perceptron
Extract rules
Combine rules
[(car-clear ?x1), (curb-clear ?x3), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (at-curb-num ?x1 ?x3), (at-curb ?x1)]
at-curb-num:?x2:?x1
curb-clear:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x2), (curb-clear ?x2), (car-clear ?x3), (at-curb-num ?x3 ?x2), (at-curb ?x3), (behind-car ?x1 ?x3), (at-curb ?x1), (behind-car ?x3 ?x1)]
at-curb:?x1
car-clear:?x3
at-curb:?x3
behind-car:?x1:?x3
[(car-clear ?x1), (at-curb-num ?x1 ?x3), (curb-clear ?x3), (car-clear ?x2), (at-curb-num ?x2 ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (behind-car ?x2 ?x1)]
behind-car:?x2:?x1
[(car-clear ?x1), (car-clear ?x2), (car-clear ?x3), (at-curb ?x2), (behind-car ?x1 ?x2), (at-curb ?x1), (at-curb ?x3), (behind-car ?x1 ?x3), (behind-car ?x2 ?x1), (behind-car ?x2 ?x3), (behind-car ?x3 ?x1), (behind-car ?x3 ?x2)]
behind-car:?x1:?x3
behind-car:?x2:?x3
Syntactical distance : 0.2800926
Error Rate Precondition : 0.3115892
Error Rate Postcondition : 0.23297267
FSCORE : 0.010152285
