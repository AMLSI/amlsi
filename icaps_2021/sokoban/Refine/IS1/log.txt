# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial1.pddl
pddl/sokoban/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 4138
x+ mean size : 15.1
x- mean size : 8.158772
E+ size : 100
E- size : 45992
e+ mean size : 50.64
e- mean size : 33.55131
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 453.0
#States : 9
#Transitions : 14
Compression level : 50.333332
PDDL Generation
Time : 1.567
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 453.0
#States : 9
#Transitions : 14
Compression level : 50.333332
PDDL Generation
Time : 1.295
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.17222223
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0010384216
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 453.0
#States : 9
#Transitions : 14
Compression level : 50.333332
PDDL Generation
Time : 1.207
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 453.0
#States : 9
#Transitions : 14
Compression level : 50.333332
PDDL Generation
Time : 1.175
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 4049
x+ mean size : 15.0
x- mean size : 8.431958
E+ size : 100
E- size : 45719
e+ mean size : 49.53
e- mean size : 32.957634
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.192
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.18
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.16111112
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004819277
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.25
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.2
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 4295
x+ mean size : 15.0
x- mean size : 8.366472
E+ size : 100
E- size : 49679
e+ mean size : 53.87
e- mean size : 34.829586
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.253
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.198
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.17777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.223
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 9
#Transitions : 14
Compression level : 50.0
PDDL Generation
Time : 1.285
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 4220
x+ mean size : 15.4
x- mean size : 8.451422
E+ size : 100
E- size : 42718
e+ mean size : 47.37
e- mean size : 33.856102
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 9
#Transitions : 14
Compression level : 51.333332
PDDL Generation
Time : 1.296
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 9
#Transitions : 14
Compression level : 51.333332
PDDL Generation
Time : 1.288
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.2
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 9
#Transitions : 14
Compression level : 51.333332
PDDL Generation
Time : 1.276
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 9
#Transitions : 14
Compression level : 51.333332
PDDL Generation
Time : 1.299
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 4
I+ size : 30
I- size : 3935
x+ mean size : 14.966666
x- mean size : 8.01906
E+ size : 100
E- size : 48496
e+ mean size : 53.82
e- mean size : 33.019382
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 449.0
#States : 9
#Transitions : 14
Compression level : 49.88889
PDDL Generation
Time : 1.11
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 449.0
#States : 9
#Transitions : 14
Compression level : 49.88889
PDDL Generation
Time : 1.08
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0030674846
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 449.0
#States : 9
#Transitions : 14
Compression level : 49.88889
PDDL Generation
Time : 1.192
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 449.0
#States : 9
#Transitions : 14
Compression level : 49.88889
PDDL Generation
Time : 1.126
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
