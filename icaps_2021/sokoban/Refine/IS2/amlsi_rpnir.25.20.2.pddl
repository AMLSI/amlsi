(define (domain sokoban-typed)
(:requirements :strips :typing :negative-preconditions)
(:types
box loc dir - object
)
(:predicates
	(clear ?x1 - loc)
	(at-robot ?x1 - loc)
	(at ?x1 - box ?x2 - loc)
	(adjacent ?x1 - loc ?x2 - loc ?x3 - dir)
)
(:action move
	:parameters (?x1 - loc ?x2 - loc ?x3 - dir )
	:precondition (and
	(adjacent ?x1 ?x2 ?x3))
	:effect (and)
)
(:action push
	:parameters (?x1 - loc ?x2 - loc ?x3 - loc ?x4 - dir ?x5 - box )
	:precondition (and
	(clear ?x3)
	(adjacent ?x1 ?x2 ?x4)
	(adjacent ?x2 ?x3 ?x4)
	(at ?x5 ?x2))
	:effect (and)
)
)
