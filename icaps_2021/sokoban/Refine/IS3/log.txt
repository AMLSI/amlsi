# actions 28
# predicate 44
Initial state : pddl/sokoban/initial_states/initial3.pddl
pddl/sokoban/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3605
x+ mean size : 15.4
x- mean size : 8.387517
E+ size : 100
E- size : 37116
e+ mean size : 48.26
e- mean size : 33.27231
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 36
#Transitions : 74
Compression level : 12.833333
PDDL Generation
Time : 8.961
Recall = 0.24
Precision = 0.3809524
Fscore automaton 0.29447854
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 36
#Transitions : 74
Compression level : 12.833333
PDDL Generation
Time : 7.444
Recall = 0.24
Precision = 0.3809524
Fscore automaton 0.29447854
Syntactical distance : 0.18888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 36
#Transitions : 74
Compression level : 12.833333
PDDL Generation
Time : 7.536
Recall = 0.24
Precision = 0.3809524
Fscore automaton 0.29447854
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 36
#Transitions : 74
Compression level : 12.833333
PDDL Generation
Time : 7.757
Recall = 0.24
Precision = 0.3809524
Fscore automaton 0.29447854
Syntactical distance : 0.15
Error Rate Precondition : 0.029594446
Error Rate Postcondition : 0.0
FSCORE : 0.002870985

RPNIR run : 1
I+ size : 30
I- size : 3443
x+ mean size : 14.8
x- mean size : 7.6587276
E+ size : 100
E- size : 35399
e+ mean size : 45.92
e- mean size : 33.380943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 32
#Transitions : 69
Compression level : 13.875
PDDL Generation
Time : 6.491
Recall = 0.27
Precision = 0.3970588
Fscore automaton 0.32142857
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 32
#Transitions : 69
Compression level : 13.875
PDDL Generation
Time : 6.16
Recall = 0.27
Precision = 0.3970588
Fscore automaton 0.32142857
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 32
#Transitions : 69
Compression level : 13.875
PDDL Generation
Time : 6.07
Recall = 0.27
Precision = 0.3970588
Fscore automaton 0.32142857
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 32
#Transitions : 69
Compression level : 13.875
PDDL Generation
Time : 6.023
Recall = 0.27
Precision = 0.3970588
Fscore automaton 0.32142857
Syntactical distance : 0.18888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 3511
x+ mean size : 14.166667
x- mean size : 7.604671
E+ size : 100
E- size : 34470
e+ mean size : 44.86
e- mean size : 32.198753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 425.0
#States : 29
#Transitions : 61
Compression level : 14.655172
PDDL Generation
Time : 4.421
Recall = 0.25
Precision = 0.3968254
Fscore automaton 0.30674845
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 425.0
#States : 29
#Transitions : 61
Compression level : 14.655172
PDDL Generation
Time : 4.381
Recall = 0.25
Precision = 0.3968254
Fscore automaton 0.30674845
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 425.0
#States : 29
#Transitions : 61
Compression level : 14.655172
PDDL Generation
Time : 4.45
Recall = 0.25
Precision = 0.3968254
Fscore automaton 0.30674845
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 425.0
#States : 29
#Transitions : 61
Compression level : 14.655172
PDDL Generation
Time : 4.384
Recall = 0.25
Precision = 0.3968254
Fscore automaton 0.30674845
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 3301
x+ mean size : 14.233334
x- mean size : 7.62375
E+ size : 100
E- size : 38332
e+ mean size : 49.28
e- mean size : 33.330795
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 427.0
#States : 38
#Transitions : 80
Compression level : 11.236842
PDDL Generation
Time : 7.232
Recall = 0.2
Precision = 0.21276596
Fscore automaton 0.20618556
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 427.0
#States : 38
#Transitions : 80
Compression level : 11.236842
PDDL Generation
Time : 7.256
Recall = 0.2
Precision = 0.21276596
Fscore automaton 0.20618556
Syntactical distance : 0.2
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 427.0
#States : 38
#Transitions : 80
Compression level : 11.236842
PDDL Generation
Time : 7.269
Recall = 0.2
Precision = 0.21276596
Fscore automaton 0.20618556
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 427.0
#States : 38
#Transitions : 80
Compression level : 11.236842
PDDL Generation
Time : 7.378
Recall = 0.2
Precision = 0.21276596
Fscore automaton 0.20618556
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0054198774

RPNIR run : 4
I+ size : 30
I- size : 3850
x+ mean size : 16.033333
x- mean size : 9.046494
E+ size : 100
E- size : 38080
e+ mean size : 49.4
e- mean size : 32.35478
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 481.0
#States : 28
#Transitions : 60
Compression level : 17.178572
PDDL Generation
Time : 5.334
Recall = 0.23
Precision = 0.35384616
Fscore automaton 0.2787879
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 481.0
#States : 28
#Transitions : 60
Compression level : 17.178572
PDDL Generation
Time : 5.509
Recall = 0.23
Precision = 0.35384616
Fscore automaton 0.2787879
Syntactical distance : 0.2
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 481.0
#States : 28
#Transitions : 60
Compression level : 17.178572
PDDL Generation
Time : 5.522
Recall = 0.23
Precision = 0.35384616
Fscore automaton 0.2787879
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 481.0
#States : 28
#Transitions : 60
Compression level : 17.178572
PDDL Generation
Time : 5.543
Recall = 0.23
Precision = 0.35384616
Fscore automaton 0.2787879
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
