# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
pddl/sokoban/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3333
x+ mean size : 15.066667
x- mean size : 8.40834
E+ size : 100
E- size : 36679
e+ mean size : 49.78
e- mean size : 34.099567
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 22
#Transitions : 39
Compression level : 20.545454
PDDL Generation
Time : 4.82
Recall = 0.88
Precision = 1.0
Fscore automaton 0.9361702
Syntactical distance : 0.15
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01980198

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 22
#Transitions : 39
Compression level : 20.545454
PDDL Generation
Time : 4.592
Recall = 0.88
Precision = 1.0
Fscore automaton 0.9361702
Syntactical distance : 0.2
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 22
#Transitions : 39
Compression level : 20.545454
PDDL Generation
Time : 3.533
Recall = 0.88
Precision = 1.0
Fscore automaton 0.9361702
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 22
#Transitions : 39
Compression level : 20.545454
PDDL Generation
Time : 3.849
Recall = 0.88
Precision = 1.0
Fscore automaton 0.9361702
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 3222
x+ mean size : 14.7
x- mean size : 8.145872
E+ size : 100
E- size : 38391
e+ mean size : 53.03
e- mean size : 35.329636
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 22
#Transitions : 41
Compression level : 20.045454
PDDL Generation
Time : 4.195
Recall = 0.46
Precision = 0.53488374
Fscore automaton 0.49462366
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 22
#Transitions : 41
Compression level : 20.045454
PDDL Generation
Time : 4.19
Recall = 0.46
Precision = 0.53488374
Fscore automaton 0.49462366
Syntactical distance : 0.2
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 22
#Transitions : 41
Compression level : 20.045454
PDDL Generation
Time : 4.12
Recall = 0.46
Precision = 0.53488374
Fscore automaton 0.49462366
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 22
#Transitions : 41
Compression level : 20.045454
PDDL Generation
Time : 4.09
Recall = 0.46
Precision = 0.53488374
Fscore automaton 0.49462366
Syntactical distance : 0.18888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 3105
x+ mean size : 14.433333
x- mean size : 7.9433174
E+ size : 100
E- size : 37988
e+ mean size : 52.54
e- mean size : 34.1864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 22
#Transitions : 41
Compression level : 19.681818
PDDL Generation
Time : 4.039
Recall = 0.79
Precision = 0.9404762
Fscore automaton 0.8586957
Syntactical distance : 0.15
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 22
#Transitions : 41
Compression level : 19.681818
PDDL Generation
Time : 3.85
Recall = 0.79
Precision = 0.9404762
Fscore automaton 0.8586957
Syntactical distance : 0.18888889
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 22
#Transitions : 41
Compression level : 19.681818
PDDL Generation
Time : 3.826
Recall = 0.79
Precision = 0.9404762
Fscore automaton 0.8586957
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 22
#Transitions : 41
Compression level : 19.681818
PDDL Generation
Time : 3.97
Recall = 0.79
Precision = 0.9404762
Fscore automaton 0.8586957
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 3236
x+ mean size : 14.9
x- mean size : 8.333127
E+ size : 100
E- size : 38594
e+ mean size : 53.41
e- mean size : 35.54537
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 14
#Transitions : 26
Compression level : 31.928572
PDDL Generation
Time : 3.01
Recall = 0.84
Precision = 0.9882353
Fscore automaton 0.9081081
Syntactical distance : 0.15
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 14
#Transitions : 26
Compression level : 31.928572
PDDL Generation
Time : 3.111
Recall = 0.84
Precision = 0.9882353
Fscore automaton 0.9081081
Syntactical distance : 0.15555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 14
#Transitions : 26
Compression level : 31.928572
PDDL Generation
Time : 3.002
Recall = 0.84
Precision = 0.9882353
Fscore automaton 0.9081081
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 14
#Transitions : 26
Compression level : 31.928572
PDDL Generation
Time : 3.2
Recall = 0.84
Precision = 0.9882353
Fscore automaton 0.9081081
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 4
I+ size : 30
I- size : 3311
x+ mean size : 15.533334
x- mean size : 8.608578
E+ size : 100
E- size : 39317
e+ mean size : 54.09
e- mean size : 35.744335
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 466.0
#States : 21
#Transitions : 38
Compression level : 22.190475
PDDL Generation
Time : 4.249
Recall = 0.83
Precision = 1.0
Fscore automaton 0.90710384
Syntactical distance : 0.15
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01980198

*** Noise = 20.0% ***
#Observed states : 466.0
#States : 21
#Transitions : 38
Compression level : 22.190475
PDDL Generation
Time : 3.976
Recall = 0.83
Precision = 1.0
Fscore automaton 0.90710384
Syntactical distance : 0.17777778
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 466.0
#States : 21
#Transitions : 38
Compression level : 22.190475
PDDL Generation
Time : 3.985
Recall = 0.83
Precision = 1.0
Fscore automaton 0.90710384
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 466.0
#States : 21
#Transitions : 38
Compression level : 22.190475
PDDL Generation
Time : 3.939
Recall = 0.83
Precision = 1.0
Fscore automaton 0.90710384
Syntactical distance : 0.18888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0064102565
