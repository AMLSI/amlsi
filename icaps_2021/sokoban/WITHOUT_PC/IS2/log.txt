# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
pddl/sokoban/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 3333
x+ mean size : 15.066667
x- mean size : 8.40834
E+ size : 100
E- size : 36679
e+ mean size : 49.78
e- mean size : 34.099567
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 21
#Transitions : 46
Compression level : 21.52381
PDDL Generation
Time : 237.315
Recall = 0.92
Precision = 0.77310926
Fscore automaton 0.8401827
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 21
#Transitions : 46
Compression level : 21.52381
PDDL Generation
Time : 275.542
Recall = 0.92
Precision = 0.77310926
Fscore automaton 0.8401827
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 21
#Transitions : 46
Compression level : 21.52381
PDDL Generation
Time : 205.195
Recall = 0.92
Precision = 0.77310926
Fscore automaton 0.8401827
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 21
#Transitions : 46
Compression level : 21.52381
PDDL Generation
Time : 377.861
Recall = 0.92
Precision = 0.77310926
Fscore automaton 0.8401827
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 3222
x+ mean size : 14.7
x- mean size : 8.145872
E+ size : 100
E- size : 38391
e+ mean size : 53.03
e- mean size : 35.329636
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 20
#Transitions : 40
Compression level : 22.05
PDDL Generation
Time : 80.723
Recall = 0.48
Precision = 0.5217391
Fscore automaton 0.5
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 20
#Transitions : 40
Compression level : 22.05
PDDL Generation
Time : 337.772
Recall = 0.48
Precision = 0.5217391
Fscore automaton 0.5
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.2680965
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 20
#Transitions : 40
Compression level : 22.05
PDDL Generation
Time : 80.634
Recall = 0.48
Precision = 0.5217391
Fscore automaton 0.5
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 20
#Transitions : 40
Compression level : 22.05
PDDL Generation
Time : 288.618
Recall = 0.48
Precision = 0.5217391
Fscore automaton 0.5
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 3105
x+ mean size : 14.433333
x- mean size : 7.9433174
E+ size : 100
E- size : 37988
e+ mean size : 52.54
e- mean size : 34.1864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 21
#Transitions : 45
Compression level : 20.619047
PDDL Generation
Time : 77.45
Recall = 0.79
Precision = 0.7181818
Fscore automaton 0.7523809
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 21
#Transitions : 45
Compression level : 20.619047
PDDL Generation
Time : 283.046
Recall = 0.79
Precision = 0.7181818
Fscore automaton 0.7523809
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.26666668
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 21
#Transitions : 45
Compression level : 20.619047
PDDL Generation
Time : 76.814
Recall = 0.79
Precision = 0.7181818
Fscore automaton 0.7523809
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 21
#Transitions : 45
Compression level : 20.619047
PDDL Generation
Time : 314.166
Recall = 0.79
Precision = 0.7181818
Fscore automaton 0.7523809
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 3236
x+ mean size : 14.9
x- mean size : 8.333127
E+ size : 100
E- size : 38594
e+ mean size : 53.41
e- mean size : 35.54537
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 12
#Transitions : 24
Compression level : 37.25
PDDL Generation
Time : 233.178
Recall = 0.85
Precision = 0.9444444
Fscore automaton 0.8947368
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 12
#Transitions : 24
Compression level : 37.25
PDDL Generation
Time : 240.602
Recall = 0.85
Precision = 0.9444444
Fscore automaton 0.8947368
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 12
#Transitions : 24
Compression level : 37.25
PDDL Generation
Time : 207.659
Recall = 0.85
Precision = 0.9444444
Fscore automaton 0.8947368
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 12
#Transitions : 24
Compression level : 37.25
PDDL Generation
Time : 267.128
Recall = 0.85
Precision = 0.9444444
Fscore automaton 0.8947368
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 3311
x+ mean size : 15.533334
x- mean size : 8.608578
E+ size : 100
E- size : 39317
e+ mean size : 54.09
e- mean size : 35.744335
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 466.0
#States : 20
#Transitions : 42
Compression level : 23.3
PDDL Generation
Time : 195.456
Recall = 0.8
Precision = 0.71428573
Fscore automaton 0.754717
Syntactical distance : 0.06666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 466.0
#States : 20
#Transitions : 42
Compression level : 23.3
PDDL Generation
Time : 317.664
Recall = 0.8
Precision = 0.71428573
Fscore automaton 0.754717
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.2621232
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 466.0
#States : 20
#Transitions : 42
Compression level : 23.3
PDDL Generation
Time : 76.876
Recall = 0.8
Precision = 0.71428573
Fscore automaton 0.754717
Syntactical distance : 0.06666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 466.0
#States : 20
#Transitions : 42
Compression level : 23.3
PDDL Generation
Time : 342.859
Recall = 0.8
Precision = 0.71428573
Fscore automaton 0.754717
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
