# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
pddl/sokoban/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 4138
x+ mean size : 15.1
x- mean size : 8.158772
E+ size : 100
E- size : 45992
e+ mean size : 50.64
e- mean size : 33.55131
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
4582 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.22222222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.00247117

*** Noise = 20.0% ***
4582 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at:?x2:?x5
Syntactical distance : 0.21111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0012262415
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
4582 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.07777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.095831335

*** Noise = 20.0% ***
4582 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
clear:?x1
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.1388889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.03159295
FSCORE : 0.013333334

RPNI-R run : 1
I+ size : 30
I- size : 4049
x+ mean size : 15.0
x- mean size : 8.431958
E+ size : 100
E- size : 45719
e+ mean size : 49.53
e- mean size : 32.957634
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
4499 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
Syntactical distance : 0.18333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.010204082

*** Noise = 20.0% ***
4499 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x2
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.3277778
Error Rate Precondition : 0.9618413
Error Rate Postcondition : 1.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
4499 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.07777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.09615385

*** Noise = 20.0% ***
4499 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
clear:?x1
at-robot:?x1
Syntactical distance : 0.17777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.019079346
FSCORE : 0.021978023

RPNI-R run : 2
I+ size : 30
I- size : 4295
x+ mean size : 15.0
x- mean size : 8.366472
E+ size : 100
E- size : 49679
e+ mean size : 53.87
e- mean size : 34.829586
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
4702 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
clear:?x3
Syntactical distance : 0.15555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.008833922

*** Noise = 20.0% ***
4702 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at:?x1:?x5
Syntactical distance : 0.25555557
Error Rate Precondition : 0.034713197
Error Rate Postcondition : 0.0
FSCORE : 0.0014630578
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
4702 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.07777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.08845644

*** Noise = 20.0% ***
4702 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
Syntactical distance : 0.23333333
Error Rate Precondition : 0.032459643
Error Rate Postcondition : 0.032459643
FSCORE : 0.0015686273


RPNI-R run : 3
I+ size : 30
I- size : 4220
x+ mean size : 15.4
x- mean size : 8.451422
E+ size : 100
E- size : 42718
e+ mean size : 47.37
e- mean size : 33.856102
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.19444445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0095877275

*** Noise = 20.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.22222222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0028680686
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.07777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.10303967

*** Noise = 20.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
Syntactical distance : 0.23333333
Error Rate Precondition : 0.032459643
Error Rate Postcondition : 0.032459643
FSCORE : 0.0015686273

RPNI-R run : 4
I+ size : 30
I- size : 4220
x+ mean size : 15.4
x- mean size : 8.451422
E+ size : 100
E- size : 42718
e+ mean size : 47.37
e- mean size : 33.856102
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.19444445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0095877275

*** Noise = 20.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
Syntactical distance : 0.22222222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0028680686
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
at-robot:?x1
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
clear:?x3
at:?x2:?x5
Syntactical distance : 0.07777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.10303967

*** Noise = 20.0% ***
4682 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (adjacent ?x1 ?x2 ?x3)]
[(clear ?x2), (at-robot ?x1), (at-robot ?x2), (clear ?x1), (clear ?x3), (at-robot ?x3), (at ?x5 ?x2), (at ?x5 ?x3), (at ?x5 ?x1), (adjacent ?x1 ?x2 ?x4), (adjacent ?x2 ?x3 ?x4)]
at-robot:?x1
Syntactical distance : 0.23333333
Error Rate Precondition : 0.032459643
Error Rate Postcondition : 0.032459643
FSCORE : 0.0015686273
