(define (domain sokoban-typed)
(:requirements :strips :typing :negative-preconditions)
(:types
box loc dir - object
)
(:predicates
	(clear ?x1 - loc)
	(at-robot ?x1 - loc)
	(at ?x1 - box ?x2 - loc)
	(adjacent ?x1 - loc ?x2 - loc ?x3 - dir)
)
(:action move
	:parameters (?x1 - loc ?x2 - loc ?x3 - dir )
	:precondition (and
	(not(at-robot ?x2))
	(at-robot ?x1))
	:effect (and
	(at-robot ?x2)
	(not(at-robot ?x1)))
)
(:action push
	:parameters (?x1 - loc ?x2 - loc ?x3 - loc ?x4 - dir ?x5 - box )
	:precondition (and
	(clear ?x1)
	(at-robot ?x1))
	:effect (and
	(not(clear ?x1))
	(not(at-robot ?x1)))
)
)
