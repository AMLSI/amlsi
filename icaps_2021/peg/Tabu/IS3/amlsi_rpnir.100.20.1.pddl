(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(in-line ?x1 ?x2 ?x3)
	(not(last-visited ?x2))
	(not(last-visited ?x1))
	(in-line ?x3 ?x2 ?x1)
	(occupied ?x1)
	(not(free ?x2))
	(move-ended)
	(occupied ?x2)
	(not(free ?x1))
	(not(occupied ?x3))
	(free ?x3))
	:effect (and
	(not(occupied ?x1))
	(free ?x2)
	(not(move-ended))
	(not(occupied ?x2))
	(free ?x1))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(in-line ?x1 ?x2 ?x3)
	(not(last-visited ?x2))
	(in-line ?x3 ?x2 ?x1)
	(not(occupied ?x1))
	(not(free ?x2))
	(not(move-ended))
	(occupied ?x2)
	(not(occupied ?x3))
	(free ?x3))
	:effect (and
	(free ?x2)
	(not(occupied ?x2))
	(free ?x1))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(not(move-ended))
	(free ?x1))
	:effect (and
	(occupied ?x1)
	(move-ended)
	(not(free ?x1)))
)
)
