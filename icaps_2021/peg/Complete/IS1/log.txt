# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial1.pddl
pddl/peg/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3836
x+ mean size : 7.5333333
x- mean size : 5.6543274
E+ size : 100
E- size : 12235
e+ mean size : 7.13
e- mean size : 5.3328156
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 518.007
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 565.678
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.0625
Error Rate Precondition : 0.0119794635
Error Rate Postcondition : 0.0
FSCORE : 0.9795918
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 136.059
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 834.136
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 3770
x+ mean size : 7.2
x- mean size : 5.2973475
E+ size : 100
E- size : 12555
e+ mean size : 7.14
e- mean size : 5.3444047
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 628.717
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 796.349
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 128.96
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 912.631
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 3814
x+ mean size : 7.233333
x- mean size : 5.3314104
E+ size : 100
E- size : 12372
e+ mean size : 7.1
e- mean size : 5.3195925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 372.48
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 676.468
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 129.533
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 1217.188
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 3861
x+ mean size : 7.366667
x- mean size : 5.449106
E+ size : 100
E- size : 12369
e+ mean size : 7.07
e- mean size : 5.346107
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 642.403
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 786.326
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.014547414
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 131.671
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 645.894
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 4171
x+ mean size : 7.4666667
x- mean size : 5.463198
E+ size : 100
E- size : 11867
e+ mean size : 7.04
e- mean size : 5.374568
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 396.226
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 1.307
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.24305554
Error Rate Precondition : 0.11072744
Error Rate Postcondition : 0.15649325
FSCORE : 0.14285715
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 139.245
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 862.209
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
