# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial1.pddl
pddl/peg/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 3836
x+ mean size : 7.5333333
x- mean size : 5.6543274
E+ size : 100
E- size : 12235
e+ mean size : 7.13
e- mean size : 5.3328156
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
2943 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.19444443
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.045

*** Noise = 20.0% ***
2943 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
Syntactical distance : 0.27083334
Error Rate Precondition : 0.45301542
Error Rate Postcondition : 0.0
FSCORE : 0.02037278
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
2943 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
last-visited:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.013888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
2943 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
last-visited:?x1
occupied:?x2
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.104166664
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.011235954

RPNI-R run : 1
I+ size : 30
I- size : 3770
x+ mean size : 7.2
x- mean size : 5.2973475
E+ size : 100
E- size : 12555
e+ mean size : 7.14
e- mean size : 5.3444047
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
2883 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x2
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
Syntactical distance : 0.19444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
2883 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
Syntactical distance : 0.25
Error Rate Precondition : 0.3106424
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
2883 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
last-visited:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.013888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
2883 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
last-visited:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.09027778
Error Rate Precondition : 0.015831819
Error Rate Postcondition : 0.0757851
FSCORE : 0.10840108

RPNI-R run : 2
I+ size : 30
I- size : 3814
x+ mean size : 7.233333
x- mean size : 5.3314104
E+ size : 100
E- size : 12372
e+ mean size : 7.1
e- mean size : 5.3195925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
2927 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.02444988

*** Noise = 20.0% ***
2927 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.18055557
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.002069679
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
2927 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
last-visited:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.013888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
2927 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
last-visited:?x1
last-visited:?x2
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
occupied:?x1
Syntactical distance : 0.22916667
Error Rate Precondition : 0.045161292
Error Rate Postcondition : 0.102580644
FSCORE : 0.014925374

RPNI-R run : 3
I+ size : 30
I- size : 3861
x+ mean size : 7.366667
x- mean size : 5.449106
E+ size : 100
E- size : 12369
e+ mean size : 7.07
e- mean size : 5.346107
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
2967 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.19444443
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.007722007

*** Noise = 20.0% ***
2967 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x2
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
occupied:?x1
Syntactical distance : 0.30555555
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.3299389
FSCORE : 0.010218978
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
2967 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.104166664
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.6928104

*** Noise = 20.0% ***
2967 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
in-line:?x2:?x1:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.111111104
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.09116098
FSCORE : 0.15686274

RPNI-R run : 4
I+ size : 30
I- size : 4171
x+ mean size : 7.4666667
x- mean size : 5.463198
E+ size : 100
E- size : 11867
e+ mean size : 7.04
e- mean size : 5.374568
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3270 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
Syntactical distance : 0.21527779
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0034013605

*** Noise = 20.0% ***
3270 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.22916667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.005509642
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3270 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
occupied:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x1
last-visited:?x1
occupied:?x2
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
last-visited:?x1
Syntactical distance : 0.013888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3270 actions in training set
Train Perceptron
Extract rules
Combine rules
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
occupied:?x2
[(move-ended), (occupied ?x1), (occupied ?x2), (free ?x3), (free ?x1), (free ?x2), (occupied ?x3), (last-visited ?x3), (last-visited ?x1), (last-visited ?x2), (in-line ?x1 ?x2 ?x3), (in-line ?x3 ?x2 ?x1)]
move-ended
free:?x3
[(move-ended), (occupied ?x1), (free ?x1), (last-visited ?x1)]
free:?x1
last-visited:?x1
Syntactical distance : 0.22916667
Error Rate Precondition : 0.2863599
Error Rate Postcondition : 0.0
FSCORE : 0.005427409
