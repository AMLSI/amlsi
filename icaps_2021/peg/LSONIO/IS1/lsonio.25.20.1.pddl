(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(free ?x3))
	:effect (and
	(last-visited ?x3)
	(not(free ?x3)))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(in-line ?x3 ?x2 ?x1))
	:effect (and)
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(not(last-visited ?x1)))
	:effect (and)
)
)
