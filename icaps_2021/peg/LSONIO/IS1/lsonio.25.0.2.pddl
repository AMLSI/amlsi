(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(occupied ?x1)
	(not(occupied ?x3)))
	:effect (and
	(last-visited ?x3)
	(not(occupied ?x1))
	(occupied ?x3))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(occupied ?x1))
	:effect (and
	(not(occupied ?x1)))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(last-visited ?x1))
	:effect (and
	(not(last-visited ?x1)))
)
)
