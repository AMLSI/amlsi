(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(move-ended)
	(occupied ?x2))
	:effect (and
	(not(move-ended))
	(not(occupied ?x2)))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(occupied ?x3)))
	:effect (and
	(occupied ?x3))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(not(occupied ?x1)))
	:effect (and
	(occupied ?x1))
)
)
