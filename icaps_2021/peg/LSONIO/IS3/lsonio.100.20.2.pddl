(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(not(last-visited ?x2))
	(occupied ?x1)
	(not(free ?x2))
	(not(free ?x1)))
	:effect (and
	(last-visited ?x3)
	(last-visited ?x2)
	(not(occupied ?x1))
	(free ?x2)
	(free ?x1))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(last-visited ?x1)
	(not(free ?x1))
	(free ?x3))
	:effect (and
	(last-visited ?x3)
	(not(last-visited ?x1))
	(free ?x1)
	(not(free ?x3)))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(not(free ?x1)))
	:effect (and
	(free ?x1))
)
)
