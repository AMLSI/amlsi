(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(not(last-visited ?x3))
	(not(free ?x2))
	(move-ended)
	(free ?x3))
	:effect (and
	(last-visited ?x3)
	(free ?x2)
	(not(move-ended))
	(not(free ?x3)))
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(free ?x3))
	:effect (and
	(not(free ?x3)))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(not(move-ended)))
	:effect (and
	(move-ended))
)
)
