(define (domain pegsolitaire-sequential)
(:requirements :strips :typing :negative-preconditions)
(:types
location - object
)
(:predicates
	(move-ended)
	(occupied ?x1 - location)
	(free ?x1 - location)
	(last-visited ?x1 - location)
	(in-line ?x1 - location ?x2 - location ?x3 - location)
)
(:action jump-new-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(in-line ?x1 ?x2 ?x3)
	(not(last-visited ?x1))
	(in-line ?x3 ?x2 ?x1)
	(occupied ?x1)
	(not(free ?x2))
	(occupied ?x2)
	(free ?x3))
	:effect (and)
)
(:action jump-continue-move
	:parameters (?x1 - location ?x2 - location ?x3 - location )
	:precondition (and
	(in-line ?x1 ?x2 ?x3)
	(not(last-visited ?x2))
	(last-visited ?x1)
	(in-line ?x3 ?x2 ?x1)
	(not(occupied ?x1))
	(not(occupied ?x3))
	(free ?x3))
	:effect (and
	(not(free ?x3)))
)
(:action end-move
	:parameters (?x1 - location )
	:precondition (and
	(last-visited ?x1)
	(occupied ?x1))
	:effect (and
	(not(last-visited ?x1)))
)
)
