# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial2.pddl
pddl/peg/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 3369
x+ mean size : 5.8333335
x- mean size : 4.602256
E+ size : 100
E- size : 10705
e+ mean size : 5.69
e- mean size : 4.5860815
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 383.696
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 250.238
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.111111104
Error Rate Precondition : 0.013863216
Error Rate Postcondition : 0.0076982295
FSCORE : 0.88888896
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 95.067
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 505.377
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 3282
x+ mean size : 5.733333
x- mean size : 4.518586
E+ size : 100
E- size : 10225
e+ mean size : 5.47
e- mean size : 4.4870415
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 172.0
#States : 17
#Transitions : 21
Compression level : 10.117647
PDDL Generation
Time : 357.999
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 172.0
#States : 17
#Transitions : 21
Compression level : 10.117647
PDDL Generation
Time : 452.326
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.13194445
Error Rate Precondition : 0.0075266026
Error Rate Postcondition : 0.012647187
FSCORE : 0.729927
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 172.0
#States : 17
#Transitions : 21
Compression level : 10.117647
PDDL Generation
Time : 90.534
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 172.0
#States : 17
#Transitions : 21
Compression level : 10.117647
PDDL Generation
Time : 405.897
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010316613
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 3168
x+ mean size : 5.766667
x- mean size : 4.6887627
E+ size : 100
E- size : 10626
e+ mean size : 5.6
e- mean size : 4.4980235
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 173.0
#States : 17
#Transitions : 21
Compression level : 10.176471
PDDL Generation
Time : 304.443
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 173.0
#States : 17
#Transitions : 21
Compression level : 10.176471
PDDL Generation
Time : 442.613
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.083333336
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.062459756
FSCORE : 0.88105726
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 173.0
#States : 17
#Transitions : 21
Compression level : 10.176471
PDDL Generation
Time : 91.026
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 173.0
#States : 17
#Transitions : 21
Compression level : 10.176471
PDDL Generation
Time : 576.152
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.125
Error Rate Precondition : 0.062236782
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 3358
x+ mean size : 5.7
x- mean size : 4.556879
E+ size : 100
E- size : 10512
e+ mean size : 5.61
e- mean size : 4.513889
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 171.0
#States : 17
#Transitions : 20
Compression level : 10.058824
PDDL Generation
Time : 294.972
Recall = 0.89
Precision = 1.0
Fscore automaton 0.9417989
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 171.0
#States : 17
#Transitions : 20
Compression level : 10.058824
PDDL Generation
Time : 371.06
Recall = 0.89
Precision = 1.0
Fscore automaton 0.9417989
Syntactical distance : 0.09722223
Error Rate Precondition : 0.008077544
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 171.0
#States : 17
#Transitions : 20
Compression level : 10.058824
PDDL Generation
Time : 93.543
Recall = 0.89
Precision = 1.0
Fscore automaton 0.9417989
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 171.0
#States : 17
#Transitions : 20
Compression level : 10.058824
PDDL Generation
Time : 457.993
Recall = 0.89
Precision = 1.0
Fscore automaton 0.9417989
Syntactical distance : 0.16666667
Error Rate Precondition : 0.07941972
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 3441
x+ mean size : 5.8333335
x- mean size : 4.6004066
E+ size : 100
E- size : 10803
e+ mean size : 5.75
e- mean size : 4.5760436
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 289.771
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.083333336
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.8700565

*** Noise = 20.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 342.828
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.15972222
Error Rate Precondition : 0.06989247
Error Rate Postcondition : 0.008
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 96.74
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 175.0
#States : 17
#Transitions : 21
Compression level : 10.294118
PDDL Generation
Time : 514.052
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
