# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial1.pddl
pddl/peg/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 3836
x+ mean size : 7.5333333
x- mean size : 5.6543274
E+ size : 100
E- size : 12235
e+ mean size : 7.13
e- mean size : 5.3328156
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 21
#Transitions : 28
Compression level : 10.761905
PDDL Generation
Time : 629.266
Recall = 1.0
Precision = 0.8333333
Fscore automaton 0.90909094
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 21
#Transitions : 28
Compression level : 10.761905
PDDL Generation
Time : 602.205
Recall = 1.0
Precision = 0.8333333
Fscore automaton 0.90909094
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 21
#Transitions : 28
Compression level : 10.761905
PDDL Generation
Time : 449.578
Recall = 1.0
Precision = 0.8333333
Fscore automaton 0.90909094
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 21
#Transitions : 28
Compression level : 10.761905
PDDL Generation
Time : 1037.791
Recall = 1.0
Precision = 0.8333333
Fscore automaton 0.90909094
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 3770
x+ mean size : 7.2
x- mean size : 5.2973475
E+ size : 100
E- size : 12555
e+ mean size : 7.14
e- mean size : 5.3444047
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 590.199
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 749.461
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 121.72
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 861.9
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 3814
x+ mean size : 7.233333
x- mean size : 5.3314104
E+ size : 100
E- size : 12372
e+ mean size : 7.1
e- mean size : 5.3195925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 20
#Transitions : 27
Compression level : 10.85
PDDL Generation
Time : 1.52
Recall = 1.0
Precision = 0.9009009
Fscore automaton 0.94786733
Syntactical distance : 0.15277778
Error Rate Precondition : 0.056243367
Error Rate Postcondition : 0.17785844
FSCORE : 0.109826595

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 20
#Transitions : 27
Compression level : 10.85
PDDL Generation
Time : 520.351
Recall = 1.0
Precision = 0.9009009
Fscore automaton 0.94786733
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 20
#Transitions : 27
Compression level : 10.85
PDDL Generation
Time : 1.559
Recall = 1.0
Precision = 0.9009009
Fscore automaton 0.94786733
Syntactical distance : 0.09027778
Error Rate Precondition : 0.056243367
Error Rate Postcondition : 0.102030195
FSCORE : 0.35964912

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 20
#Transitions : 27
Compression level : 10.85
PDDL Generation
Time : 554.571
Recall = 1.0
Precision = 0.9009009
Fscore automaton 0.94786733
Syntactical distance : 0.15277778
Error Rate Precondition : 0.074672826
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 3861
x+ mean size : 7.366667
x- mean size : 5.449106
E+ size : 100
E- size : 12369
e+ mean size : 7.07
e- mean size : 5.346107
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 602.941
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 735.418
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.014547414
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 123.879
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 605.724
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 4171
x+ mean size : 7.4666667
x- mean size : 5.463198
E+ size : 100
E- size : 11867
e+ mean size : 7.04
e- mean size : 5.374568
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 21
#Transitions : 27
Compression level : 10.666667
PDDL Generation
Time : 379.143
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 21
#Transitions : 27
Compression level : 10.666667
PDDL Generation
Time : 1.497
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.24305554
Error Rate Precondition : 0.11072744
Error Rate Postcondition : 0.15649325
FSCORE : 0.14285715
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 21
#Transitions : 27
Compression level : 10.666667
PDDL Generation
Time : 134.722
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 21
#Transitions : 27
Compression level : 10.666667
PDDL Generation
Time : 826.836
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
