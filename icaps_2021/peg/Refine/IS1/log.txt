# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial1.pddl
pddl/peg/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3836
x+ mean size : 7.5333333
x- mean size : 5.6543274
E+ size : 100
E- size : 12235
e+ mean size : 7.13
e- mean size : 5.3328156
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 0.592
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.125
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.15584417

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 0.244
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.17361112
Error Rate Precondition : 0.023764618
Error Rate Postcondition : 0.0
FSCORE : 0.024590163
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 0.276
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 226.0
#States : 22
#Transitions : 28
Compression level : 10.272727
PDDL Generation
Time : 0.361
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.22916667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0026525198

RPNIR run : 1
I+ size : 30
I- size : 3770
x+ mean size : 7.2
x- mean size : 5.2973475
E+ size : 100
E- size : 12555
e+ mean size : 7.14
e- mean size : 5.3444047
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 0.21
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.104166664
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.35643563

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 0.203
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.17361112
Error Rate Precondition : 0.019973805
Error Rate Postcondition : 0.0
FSCORE : 0.018126888
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 0.211
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 216.0
#States : 21
#Transitions : 27
Compression level : 10.285714
PDDL Generation
Time : 0.41
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.21527779
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.022928111

RPNIR run : 2
I+ size : 30
I- size : 3814
x+ mean size : 7.233333
x- mean size : 5.3314104
E+ size : 100
E- size : 12372
e+ mean size : 7.1
e- mean size : 5.3195925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 0.305
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.104166664
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.34408602

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 0.209
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.16666667
Error Rate Precondition : 0.020710059
Error Rate Postcondition : 0.0
FSCORE : 0.0040983604
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 0.212
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 217.0
#States : 21
#Transitions : 27
Compression level : 10.333333
PDDL Generation
Time : 0.21
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.2638889
Error Rate Precondition : 0.056179777
Error Rate Postcondition : 0.17857143
FSCORE : 0.0043010754

RPNIR run : 3
I+ size : 30
I- size : 3861
x+ mean size : 7.366667
x- mean size : 5.449106
E+ size : 100
E- size : 12369
e+ mean size : 7.07
e- mean size : 5.346107
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 0.194
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.11805556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.14184396

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 0.195
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.18055557
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.047149125
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 0.373
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 221.0
#States : 21
#Transitions : 27
Compression level : 10.523809
PDDL Generation
Time : 0.388
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.15972222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.09920634

RPNIR run : 4
I+ size : 30
I- size : 4171
x+ mean size : 7.4666667
x- mean size : 5.463198
E+ size : 100
E- size : 11867
e+ mean size : 7.04
e- mean size : 5.374568
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 0.416
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.09722222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.3333333

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 0.214
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.24305554
Error Rate Precondition : 0.11072744
Error Rate Postcondition : 0.15649325
FSCORE : 0.14285715
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 0.196
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 224.0
#States : 22
#Transitions : 28
Compression level : 10.181818
PDDL Generation
Time : 0.201
Recall = 0.96
Precision = 1.0
Fscore automaton 0.9795918
Syntactical distance : 0.26388893
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
