(define (domain zeno-travel)
(:requirements :strips :typing :negative-preconditions)
(:types
flevel aircraft person city - object
)
(:predicates
	(at ?x1 - person ?x2 - city)
	(at_aircraft ?x1 - aircraft ?x2 - city)
	(in ?x1 - person ?x2 - aircraft)
	(fuel-level ?x1 - aircraft ?x2 - flevel)
	(next ?x1 - flevel ?x2 - flevel)
)
(:action board
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(in ?x1 ?x2))
	:effect (and
	(not(in ?x1 ?x2)))
)
(:action debark
	:parameters (?x1 - person ?x2 - aircraft ?x3 - city )
	:precondition (and
	(not(in ?x1 ?x2)))
	:effect (and
	(in ?x1 ?x2))
)
(:action fly
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel )
	:precondition (and
	(not(at_aircraft ?x1 ?x3))
	(at_aircraft ?x1 ?x2))
	:effect (and
	(at_aircraft ?x1 ?x3)
	(not(at_aircraft ?x1 ?x2)))
)
(:action zoom
	:parameters (?x1 - aircraft ?x2 - city ?x3 - city ?x4 - flevel ?x5 - flevel ?x6 - flevel )
	:precondition (and
	(not(at_aircraft ?x1 ?x3))
	(at_aircraft ?x1 ?x2))
	:effect (and
	(at_aircraft ?x1 ?x3)
	(not(at_aircraft ?x1 ?x2)))
)
(:action refuel
	:parameters (?x1 - aircraft ?x2 - city ?x3 - flevel ?x4 - flevel )
	:precondition (and
	(not(fuel-level ?x1 ?x4)))
	:effect (and
	(fuel-level ?x1 ?x4))
)
)
