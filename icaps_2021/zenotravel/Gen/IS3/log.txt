# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial3.pddl
pddl/zenotravel/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1570
x+ mean size : 15.3
x- mean size : 8.529936
E+ size : 100
E- size : 18040
e+ mean size : 50.34
e- mean size : 34.25909
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 8.064
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0820202
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01980198

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 6.502
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.25414142
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.011235954
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 6.821
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 6.157
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.2420202
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.002980626

RPNIR run : 1
I+ size : 30
I- size : 1681
x+ mean size : 15.333333
x- mean size : 8.553837
E+ size : 100
E- size : 17577
e+ mean size : 48.92
e- mean size : 33.027992
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 5.558
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.11757575
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 5.642
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.2420202
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.013513514
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 5.623
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 5.513
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.289697
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 1613
x+ mean size : 15.4
x- mean size : 8.4488535
E+ size : 100
E- size : 17611
e+ mean size : 49.36
e- mean size : 34.260406
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 4.531
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.09818182
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.01980198

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 4.628
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.219798
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.025706943
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 4.791
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 4.59
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.16929293
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.11046427
FSCORE : 0.018656718

RPNIR run : 3
I+ size : 30
I- size : 1722
x+ mean size : 15.366667
x- mean size : 8.901858
E+ size : 100
E- size : 16153
e+ mean size : 43.94
e- mean size : 31.124063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 5.523
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.12646464
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 5.52
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.25919193
Error Rate Precondition : 0.12935966
Error Rate Postcondition : 0.15953574
FSCORE : 0.003533569
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 5.449
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 5.496
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.27030307
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004842615

RPNIR run : 4
I+ size : 30
I- size : 1554
x+ mean size : 15.0
x- mean size : 8.027027
E+ size : 100
E- size : 17531
e+ mean size : 49.96
e- mean size : 34.22172
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 6.979
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.1610101
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 6.833
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.24424243
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.009411765
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 6.895
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 6.963
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.29252526
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
