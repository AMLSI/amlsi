# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial1.pddl
pddl/zenotravel/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1731
x+ mean size : 15.233334
x- mean size : 8.679376
E+ size : 100
E- size : 18048
e+ mean size : 49.88
e- mean size : 32.92287
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 21
#Transitions : 47
Compression level : 21.761906
PDDL Generation
Time : 6.514
Recall = 0.36
Precision = 0.46753246
Fscore automaton 0.40677965
Syntactical distance : 0.012121213
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.04291845

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 21
#Transitions : 47
Compression level : 21.761906
PDDL Generation
Time : 5.925
Recall = 0.36
Precision = 0.46753246
Fscore automaton 0.40677965
Syntactical distance : 0.16141415
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0070671374
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 21
#Transitions : 47
Compression level : 21.761906
PDDL Generation
Time : 5.444
Recall = 0.36
Precision = 0.46753246
Fscore automaton 0.40677965
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 21
#Transitions : 47
Compression level : 21.761906
PDDL Generation
Time : 5.534
Recall = 0.36
Precision = 0.46753246
Fscore automaton 0.40677965
Syntactical distance : 0.19474748
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 1485
x+ mean size : 14.266666
x- mean size : 7.870707
E+ size : 100
E- size : 18520
e+ mean size : 52.05
e- mean size : 35.036446
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 428.0
#States : 39
#Transitions : 83
Compression level : 10.974359
PDDL Generation
Time : 9.466
Recall = 0.15
Precision = 0.27272728
Fscore automaton 0.1935484
Syntactical distance : 0.0060606063
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.03610108

*** Noise = 20.0% ***
#Observed states : 428.0
#States : 39
#Transitions : 83
Compression level : 10.974359
PDDL Generation
Time : 9.763
Recall = 0.15
Precision = 0.27272728
Fscore automaton 0.1935484
Syntactical distance : 0.25636366
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0152091235
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 428.0
#States : 39
#Transitions : 83
Compression level : 10.974359
PDDL Generation
Time : 9.737
Recall = 0.15
Precision = 0.27272728
Fscore automaton 0.1935484
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 428.0
#States : 39
#Transitions : 83
Compression level : 10.974359
PDDL Generation
Time : 9.197
Recall = 0.15
Precision = 0.27272728
Fscore automaton 0.1935484
Syntactical distance : 0.25313133
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0077881613

RPNIR run : 2
I+ size : 30
I- size : 1526
x+ mean size : 15.166667
x- mean size : 8.3066845
E+ size : 100
E- size : 16750
e+ mean size : 46.59
e- mean size : 33.32591
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 22
#Transitions : 47
Compression level : 20.681818
PDDL Generation
Time : 4.058
Recall = 0.38
Precision = 0.5
Fscore automaton 0.4318182
Syntactical distance : 0.012121213
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.09649123

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 22
#Transitions : 47
Compression level : 20.681818
PDDL Generation
Time : 4.286
Recall = 0.38
Precision = 0.5
Fscore automaton 0.4318182
Syntactical distance : 0.27757576
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.021739129
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 22
#Transitions : 47
Compression level : 20.681818
PDDL Generation
Time : 4.468
Recall = 0.38
Precision = 0.5
Fscore automaton 0.4318182
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 22
#Transitions : 47
Compression level : 20.681818
PDDL Generation
Time : 4.28
Recall = 0.38
Precision = 0.5
Fscore automaton 0.4318182
Syntactical distance : 0.3008081
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 1761
x+ mean size : 16.1
x- mean size : 8.535491
E+ size : 100
E- size : 19065
e+ mean size : 52.28
e- mean size : 35.3562
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 26
#Transitions : 61
Compression level : 18.576923
PDDL Generation
Time : 7.699
Recall = 0.28
Precision = 0.33734939
Fscore automaton 0.30601093
Syntactical distance : 0.0060606063
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.13852814

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 26
#Transitions : 61
Compression level : 18.576923
PDDL Generation
Time : 7.394
Recall = 0.28
Precision = 0.33734939
Fscore automaton 0.30601093
Syntactical distance : 0.14363638
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.016507098
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 26
#Transitions : 61
Compression level : 18.576923
PDDL Generation
Time : 7.38
Recall = 0.28
Precision = 0.33734939
Fscore automaton 0.30601093
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 26
#Transitions : 61
Compression level : 18.576923
PDDL Generation
Time : 7.265
Recall = 0.28
Precision = 0.33734939
Fscore automaton 0.30601093
Syntactical distance : 0.24707071
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0076238886

RPNIR run : 4
I+ size : 30
I- size : 1535
x+ mean size : 14.9
x- mean size : 8.428013
E+ size : 100
E- size : 16834
e+ mean size : 46.84
e- mean size : 33.108234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 26
#Transitions : 60
Compression level : 17.192308
PDDL Generation
Time : 6.77
Recall = 0.33
Precision = 0.2820513
Fscore automaton 0.30414748
Syntactical distance : 0.012121213
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.112068966

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 26
#Transitions : 60
Compression level : 17.192308
PDDL Generation
Time : 6.734
Recall = 0.33
Precision = 0.2820513
Fscore automaton 0.30414748
Syntactical distance : 0.30424243
Error Rate Precondition : 0.08925366
Error Rate Postcondition : 0.08654564
FSCORE : 0.016474465
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 26
#Transitions : 60
Compression level : 17.192308
PDDL Generation
Time : 6.731
Recall = 0.33
Precision = 0.2820513
Fscore automaton 0.30414748
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 26
#Transitions : 60
Compression level : 17.192308
PDDL Generation
Time : 6.691
Recall = 0.33
Precision = 0.2820513
Fscore automaton 0.30414748
Syntactical distance : 0.2109091
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.030303031
