# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial1.pddl
pddl/zenotravel/domain.pddl

RPNI run : 0
I+ size : 30
I- size : 1731
x+ mean size : 15.233334
x- mean size : 8.679376
E+ size : 100
E- size : 18048
e+ mean size : 49.88
e- mean size : 32.92287
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 29
#Transitions : 74
Compression level : 15.75862
PDDL Generation
Time : 11.564
Recall = 0.16
Precision = 0.125
Fscore automaton 0.14035088
Syntactical distance : 0.07595959
Error Rate Precondition : 0.021264423
Error Rate Postcondition : 0.0
FSCORE : 0.058252428

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 29
#Transitions : 74
Compression level : 15.75862
PDDL Generation
Time : 9.703
Recall = 0.16
Precision = 0.125
Fscore automaton 0.14035088
Syntactical distance : 0.14101009
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.1725743
FSCORE : 0.009864365
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 29
#Transitions : 74
Compression level : 15.75862
PDDL Generation
Time : 9.683
Recall = 0.16
Precision = 0.125
Fscore automaton 0.14035088
Syntactical distance : 0.0820202
Error Rate Precondition : 0.021264423
Error Rate Postcondition : 0.02622758
FSCORE : 0.058252428

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 29
#Transitions : 74
Compression level : 15.75862
PDDL Generation
Time : 159.536
Recall = 0.16
Precision = 0.125
Fscore automaton 0.14035088
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 1
I+ size : 30
I- size : 1485
x+ mean size : 14.266666
x- mean size : 7.870707
E+ size : 100
E- size : 18520
e+ mean size : 52.05
e- mean size : 35.036446
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 428.0
#States : 34
#Transitions : 86
Compression level : 12.588235
PDDL Generation
Time : 10.883
Recall = 0.18
Precision = 0.13043478
Fscore automaton 0.15126051
Syntactical distance : 0.10424242
Error Rate Precondition : 0.020144414
Error Rate Postcondition : 0.09284584
FSCORE : 0.03508772

*** Noise = 20.0% ***
#Observed states : 428.0
#States : 34
#Transitions : 86
Compression level : 12.588235
PDDL Generation
Time : 110.921
Recall = 0.18
Precision = 0.13043478
Fscore automaton 0.15126051
Syntactical distance : 0.085454546
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.020689655
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 428.0
#States : 34
#Transitions : 86
Compression level : 12.588235
PDDL Generation
Time : 11.417
Recall = 0.18
Precision = 0.13043478
Fscore automaton 0.15126051
Syntactical distance : 0.10424242
Error Rate Precondition : 0.020144414
Error Rate Postcondition : 0.09284584
FSCORE : 0.03508772

*** Noise = 20.0% ***
#Observed states : 428.0
#States : 34
#Transitions : 86
Compression level : 12.588235
PDDL Generation
Time : 140.277
Recall = 0.18
Precision = 0.13043478
Fscore automaton 0.15126051
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 2
I+ size : 30
I- size : 1526
x+ mean size : 15.166667
x- mean size : 8.3066845
E+ size : 100
E- size : 16750
e+ mean size : 46.59
e- mean size : 33.32591
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 28
#Transitions : 72
Compression level : 16.25
PDDL Generation
Time : 7.301
Recall = 0.26
Precision = 0.13197969
Fscore automaton 0.17508416
Syntactical distance : 0.108686864
Error Rate Precondition : 0.021473018
Error Rate Postcondition : 0.13969293
FSCORE : 0.03100775

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 28
#Transitions : 72
Compression level : 16.25
PDDL Generation
Time : 201.689
Recall = 0.26
Precision = 0.13197969
Fscore automaton 0.17508416
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 28
#Transitions : 72
Compression level : 16.25
PDDL Generation
Time : 7.104
Recall = 0.26
Precision = 0.13197969
Fscore automaton 0.17508416
Syntactical distance : 0.108686864
Error Rate Precondition : 0.021473018
Error Rate Postcondition : 0.13969293
FSCORE : 0.03100775

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 28
#Transitions : 72
Compression level : 16.25
PDDL Generation
Time : 144.585
Recall = 0.26
Precision = 0.13197969
Fscore automaton 0.17508416
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 3
I+ size : 30
I- size : 1761
x+ mean size : 16.1
x- mean size : 8.535491
E+ size : 100
E- size : 19065
e+ mean size : 52.28
e- mean size : 35.3562
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 34
#Transitions : 88
Compression level : 14.205882
PDDL Generation
Time : 11.966
Recall = 0.16
Precision = 0.09142857
Fscore automaton 0.11636363
Syntactical distance : 0.11313132
Error Rate Precondition : 0.112945266
Error Rate Postcondition : 0.2293326
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 34
#Transitions : 88
Compression level : 14.205882
PDDL Generation
Time : 183.105
Recall = 0.16
Precision = 0.09142857
Fscore automaton 0.11636363
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 34
#Transitions : 88
Compression level : 14.205882
PDDL Generation
Time : 12.271
Recall = 0.16
Precision = 0.09142857
Fscore automaton 0.11636363
Syntactical distance : 0.11919192
Error Rate Precondition : 0.112945266
Error Rate Postcondition : 0.25168344
FSCORE : 0.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 34
#Transitions : 88
Compression level : 14.205882
PDDL Generation
Time : 171.643
Recall = 0.16
Precision = 0.09142857
Fscore automaton 0.11636363
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNI run : 4
I+ size : 30
I- size : 1535
x+ mean size : 14.9
x- mean size : 8.428013
E+ size : 100
E- size : 16834
e+ mean size : 46.84
e- mean size : 33.108234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 29
#Transitions : 79
Compression level : 15.413794
PDDL Generation
Time : 8.984
Recall = 0.19
Precision = 0.12258065
Fscore automaton 0.14901961
Syntactical distance : 0.1810101
Error Rate Precondition : 0.033564523
Error Rate Postcondition : 0.11515229
FSCORE : 0.08620689

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 29
#Transitions : 79
Compression level : 15.413794
PDDL Generation
Time : 8.848
Recall = 0.19
Precision = 0.12258065
Fscore automaton 0.14901961
Syntactical distance : 0.28868687
Error Rate Precondition : 0.16702779
Error Rate Postcondition : 0.2899674
FSCORE : 0.017857142
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 29
#Transitions : 79
Compression level : 15.413794
PDDL Generation
Time : 9.033
Recall = 0.19
Precision = 0.12258065
Fscore automaton 0.14901961
Syntactical distance : 0.1870707
Error Rate Precondition : 0.033564523
Error Rate Postcondition : 0.13910598
FSCORE : 0.08620689

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 29
#Transitions : 79
Compression level : 15.413794
PDDL Generation
Time : 9.095
Recall = 0.19
Precision = 0.12258065
Fscore automaton 0.14901961
Syntactical distance : 0.3181818
Error Rate Precondition : 0.09596179
Error Rate Postcondition : 0.18948689
FSCORE : 0.009975062
