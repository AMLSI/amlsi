# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial3.pddl
pddl/zenotravel/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1570
x+ mean size : 15.3
x- mean size : 8.529936
E+ size : 100
E- size : 18040
e+ mean size : 50.34
e- mean size : 34.25909
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 90.654
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 193.986
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 43.165
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 26
#Transitions : 59
Compression level : 17.653847
PDDL Generation
Time : 164.704
Recall = 0.31
Precision = 0.46969697
Fscore automaton 0.37349397
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1681
x+ mean size : 15.333333
x- mean size : 8.553837
E+ size : 100
E- size : 17577
e+ mean size : 48.92
e- mean size : 33.027992
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 88.215
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 147.763
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 44.265
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 23
#Transitions : 52
Compression level : 20.0
PDDL Generation
Time : 188.461
Recall = 0.36
Precision = 0.42857143
Fscore automaton 0.39130437
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1613
x+ mean size : 15.4
x- mean size : 8.4488535
E+ size : 100
E- size : 17611
e+ mean size : 49.36
e- mean size : 34.260406
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 80.532
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 211.329
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 41.201
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 21
#Transitions : 45
Compression level : 22.0
PDDL Generation
Time : 154.457
Recall = 0.35
Precision = 0.7291667
Fscore automaton 0.472973
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1722
x+ mean size : 15.366667
x- mean size : 8.901858
E+ size : 100
E- size : 16153
e+ mean size : 43.94
e- mean size : 31.124063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 95.575
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 262.024
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 46.543
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 461.0
#States : 24
#Transitions : 50
Compression level : 19.208334
PDDL Generation
Time : 190.904
Recall = 0.33
Precision = 0.64705884
Fscore automaton 0.43708608
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1554
x+ mean size : 15.0
x- mean size : 8.027027
E+ size : 100
E- size : 17531
e+ mean size : 49.96
e- mean size : 34.22172
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 79.395
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 251.911
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 41.064
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 30
#Transitions : 65
Compression level : 15.0
PDDL Generation
Time : 190.768
Recall = 0.12
Precision = 0.38709676
Fscore automaton 0.1832061
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
