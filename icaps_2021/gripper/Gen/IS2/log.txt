# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial2.pddl
pddl/gripper/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1162
x+ mean size : 15.266666
x- mean size : 8.3356285
E+ size : 100
E- size : 13381
e+ mean size : 50.93
e- mean size : 33.544205
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 2.767
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 2.483
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.41666666
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 2.038
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 2.105
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.4722222
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 1173
x+ mean size : 15.466666
x- mean size : 7.87127
E+ size : 100
E- size : 13206
e+ mean size : 50.99
e- mean size : 33.505756
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 2.198
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 2.141
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.41666666
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 2.029
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 2.094
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.4722222
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 2
I+ size : 30
I- size : 1209
x+ mean size : 16.133333
x- mean size : 8.370554
E+ size : 100
E- size : 14356
e+ mean size : 55.0
e- mean size : 34.82704
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 2.062
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 2.029
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 2.074
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 2.072
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.4722222
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 1215
x+ mean size : 15.233334
x- mean size : 8.138271
E+ size : 100
E- size : 13079
e+ mean size : 52.55
e- mean size : 33.167904
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 2.033
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 1.963
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888893
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 1.832
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 1.875
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.4722222
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 4
I+ size : 30
I- size : 1193
x+ mean size : 14.433333
x- mean size : 7.7728415
E+ size : 100
E- size : 14051
e+ mean size : 54.55
e- mean size : 34.400967
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 1.715
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 1.617
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.38888887
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 1.776
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 1.602
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.4722222
Error Rate Precondition : NaN
Error Rate Postcondition : NaN
FSCORE : 0.0
