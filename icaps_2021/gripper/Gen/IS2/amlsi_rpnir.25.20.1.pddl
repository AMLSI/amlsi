(define (domain gripper-typed)
(:requirements :strips :typing :negative-preconditions)
(:types
ball room gripper - object
)
(:predicates
	(at-robby ?x1 - room)
	(at ?x1 - ball ?x2 - room)
	(free ?x1 - gripper)
	(carry ?x1 - ball ?x2 - gripper)
)
(:action move
	:parameters (?x1 - room ?x2 - room )
	:precondition (and)
	:effect (and)
)
(:action pick
	:parameters (?x1 - ball ?x2 - room ?x3 - gripper )
	:precondition (and
	(at-robby ?x2)
	(not(carry ?x1 ?x3)))
	:effect (and)
)
(:action drop
	:parameters (?x1 - ball ?x2 - room ?x3 - gripper )
	:precondition (and)
	:effect (and
	(free ?x3))
)
)
