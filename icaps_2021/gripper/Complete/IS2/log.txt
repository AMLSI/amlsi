# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial2.pddl
pddl/gripper/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1162
x+ mean size : 15.266666
x- mean size : 8.3356285
E+ size : 100
E- size : 13381
e+ mean size : 50.93
e- mean size : 33.544205
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 10.084
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 29.417
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 9.091
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 8
#Transitions : 16
Compression level : 57.25
PDDL Generation
Time : 29.976
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1173
x+ mean size : 15.466666
x- mean size : 7.87127
E+ size : 100
E- size : 13206
e+ mean size : 50.99
e- mean size : 33.505756
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 8.728
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 28.272
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 8.818
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 8
#Transitions : 16
Compression level : 58.0
PDDL Generation
Time : 30.368
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1209
x+ mean size : 16.133333
x- mean size : 8.370554
E+ size : 100
E- size : 14356
e+ mean size : 55.0
e- mean size : 34.82704
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 9.182
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 28.574
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 9.082
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 484.0
#States : 8
#Transitions : 16
Compression level : 60.5
PDDL Generation
Time : 32.18
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1215
x+ mean size : 15.233334
x- mean size : 8.138271
E+ size : 100
E- size : 13079
e+ mean size : 52.55
e- mean size : 33.167904
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 8.693
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 30.045
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 8.702
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 8
#Transitions : 16
Compression level : 57.125
PDDL Generation
Time : 30.686
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1193
x+ mean size : 14.433333
x- mean size : 7.7728415
E+ size : 100
E- size : 14051
e+ mean size : 54.55
e- mean size : 34.400967
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 8.397
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 23.207
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 8.15
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 8
#Transitions : 16
Compression level : 54.125
PDDL Generation
Time : 27.863
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
