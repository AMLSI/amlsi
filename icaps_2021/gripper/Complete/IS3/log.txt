# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial3.pddl
pddl/gripper/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1122
x+ mean size : 15.066667
x- mean size : 8.582888
E+ size : 100
E- size : 12625
e+ mean size : 49.27
e- mean size : 33.25798
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 8
#Transitions : 16
Compression level : 56.5
PDDL Generation
Time : 14.415
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 8
#Transitions : 16
Compression level : 56.5
PDDL Generation
Time : 34.701
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 8
#Transitions : 16
Compression level : 56.5
PDDL Generation
Time : 11.579
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 8
#Transitions : 16
Compression level : 56.5
PDDL Generation
Time : 40.879
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1096
x+ mean size : 14.733334
x- mean size : 7.9562044
E+ size : 100
E- size : 11560
e+ mean size : 45.11
e- mean size : 30.669464
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 442.0
#States : 8
#Transitions : 16
Compression level : 55.25
PDDL Generation
Time : 11.438
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 442.0
#States : 8
#Transitions : 16
Compression level : 55.25
PDDL Generation
Time : 35.842
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 442.0
#States : 8
#Transitions : 16
Compression level : 55.25
PDDL Generation
Time : 10.733
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 442.0
#States : 8
#Transitions : 16
Compression level : 55.25
PDDL Generation
Time : 38.855
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1320
x+ mean size : 15.566667
x- mean size : 8.743182
E+ size : 100
E- size : 12677
e+ mean size : 50.0
e- mean size : 33.4957
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 8
#Transitions : 16
Compression level : 58.375
PDDL Generation
Time : 12.959
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 8
#Transitions : 16
Compression level : 58.375
PDDL Generation
Time : 36.248
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 8
#Transitions : 16
Compression level : 58.375
PDDL Generation
Time : 13.158
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 8
#Transitions : 16
Compression level : 58.375
PDDL Generation
Time : 47.895
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1075
x+ mean size : 15.0
x- mean size : 8.105116
E+ size : 100
E- size : 13000
e+ mean size : 51.98
e- mean size : 35.798153
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 8
#Transitions : 16
Compression level : 56.25
PDDL Generation
Time : 11.148
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 8
#Transitions : 16
Compression level : 56.25
PDDL Generation
Time : 35.414
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 8
#Transitions : 16
Compression level : 56.25
PDDL Generation
Time : 11.002
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 8
#Transitions : 16
Compression level : 56.25
PDDL Generation
Time : 39.451
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1148
x+ mean size : 15.166667
x- mean size : 8.337979
E+ size : 100
E- size : 12196
e+ mean size : 47.69
e- mean size : 32.163662
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 8
#Transitions : 16
Compression level : 56.875
PDDL Generation
Time : 11.29
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 8
#Transitions : 16
Compression level : 56.875
PDDL Generation
Time : 22.901
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 8
#Transitions : 16
Compression level : 56.875
PDDL Generation
Time : 11.552
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 8
#Transitions : 16
Compression level : 56.875
PDDL Generation
Time : 40.682
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
