# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial3.pddl
pddl/gripper/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1122
x+ mean size : 15.066667
x- mean size : 8.582888
E+ size : 100
E- size : 12625
e+ mean size : 49.27
e- mean size : 33.25798
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 25.631
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 22.686
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 21.597
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 22.214
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1096
x+ mean size : 14.733334
x- mean size : 7.9562044
E+ size : 100
E- size : 11560
e+ mean size : 45.11
e- mean size : 30.669464
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 21.373
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 20.547
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 20.701
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 21.373
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1320
x+ mean size : 15.566667
x- mean size : 8.743182
E+ size : 100
E- size : 12677
e+ mean size : 50.0
e- mean size : 33.4957
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 25.26
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 25.682
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 24.992
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 25.906
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1075
x+ mean size : 15.0
x- mean size : 8.105116
E+ size : 100
E- size : 13000
e+ mean size : 51.98
e- mean size : 35.798153
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 20.902
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 19.883
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 19.404
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 20.341
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1148
x+ mean size : 15.166667
x- mean size : 8.337979
E+ size : 100
E- size : 12196
e+ mean size : 47.69
e- mean size : 32.163662
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 24.414
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 23.984
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 21.629
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time : 22.213
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
