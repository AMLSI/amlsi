# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial1.pddl
pddl/gripper/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 1034
x+ mean size : 14.2
x- mean size : 7.9700193
E+ size : 100
E- size : 13100
e+ mean size : 50.86
e- mean size : 34.45649
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1460 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.30555555
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.021917809

*** Noise = 20.0% ***
1460 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.25
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.028368795
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1460 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.11983224

*** Noise = 20.0% ***
1460 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.22222222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.019417476

RPNI-R run : 1
I+ size : 30
I- size : 1089
x+ mean size : 14.766666
x- mean size : 8.185492
E+ size : 100
E- size : 12472
e+ mean size : 49.79
e- mean size : 33.434895
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1532 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.2777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
1532 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.25
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1532 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.12330455

*** Noise = 20.0% ***
1532 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.11111111
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.061068702

RPNI-R run : 2
I+ size : 30
I- size : 1333
x+ mean size : 16.366667
x- mean size : 9.185296
E+ size : 100
E- size : 12226
e+ mean size : 48.27
e- mean size : 34.716095
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1824 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0137299765

*** Noise = 20.0% ***
1824 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.24176507
Error Rate Postcondition : 0.0
FSCORE : 0.017595306
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1824 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.124455504

*** Noise = 20.0% ***
1824 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.38888893
Error Rate Precondition : 0.20353687
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNI-R run : 3
I+ size : 30
I- size : 1120
x+ mean size : 14.233334
x- mean size : 8.339286
E+ size : 100
E- size : 12988
e+ mean size : 52.01
e- mean size : 34.179474
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1547 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004385965

*** Noise = 20.0% ***
1547 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004878049
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1547 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.12048194

*** Noise = 20.0% ***
1547 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at-robby:?x2
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.38888893
Error Rate Precondition : 0.20103407
Error Rate Postcondition : 0.20909368
FSCORE : 0.0051020407

RPNI-R run : 4
I+ size : 30
I- size : 1232
x+ mean size : 15.866667
x- mean size : 8.847403
E+ size : 100
E- size : 13188
e+ mean size : 50.96
e- mean size : 33.273506
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1708 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.2777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
1708 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0038610038
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1708 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.11869435

*** Noise = 20.0% ***
1708 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.16666667
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
