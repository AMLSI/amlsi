# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial2.pddl
pddl/gripper/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 1162
x+ mean size : 15.266666
x- mean size : 8.3356285
E+ size : 100
E- size : 13381
e+ mean size : 50.93
e- mean size : 33.544205
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1620 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.22222222
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.038729664

*** Noise = 20.0% ***
1620 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.41666666
Error Rate Precondition : 0.23561752
Error Rate Postcondition : 0.0
FSCORE : 0.007952286
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1620 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.11389522

*** Noise = 20.0% ***
1620 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.19444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.010033445

RPNI-R run : 1
I+ size : 30
I- size : 1173
x+ mean size : 15.466666
x- mean size : 7.87127
E+ size : 100
E- size : 13206
e+ mean size : 50.99
e- mean size : 33.505756
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1637 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.020920502

*** Noise = 20.0% ***
1637 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at-robby:?x2
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.24416552
FSCORE : 0.015936254
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1637 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.1150748

*** Noise = 20.0% ***
1637 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.22222222
Error Rate Precondition : 0.22073287
Error Rate Postcondition : 0.11236835
FSCORE : 0.07692308

RPNI-R run : 2
I+ size : 30
I- size : 1209
x+ mean size : 16.133333
x- mean size : 8.370554
E+ size : 100
E- size : 14356
e+ mean size : 55.0
e- mean size : 34.82704
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1693 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.2777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.008048289

*** Noise = 20.0% ***
1693 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.33333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0034364264
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1693 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.10683761

*** Noise = 20.0% ***
1693 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.30555555
Error Rate Precondition : 0.29741567
Error Rate Postcondition : 0.20090525
FSCORE : 0.01980198

RPNI-R run : 3
I+ size : 30
I- size : 1215
x+ mean size : 15.233334
x- mean size : 8.138271
E+ size : 100
E- size : 13079
e+ mean size : 52.55
e- mean size : 33.167904
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1672 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.25
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0054495917

*** Noise = 20.0% ***
1672 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.2777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.009153318
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1672 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.11641444

*** Noise = 20.0% ***
1672 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.19444446
Error Rate Precondition : 0.1226451
Error Rate Postcondition : 0.0
FSCORE : 0.025559105

RPNI-R run : 4
I+ size : 30
I- size : 1193
x+ mean size : 14.433333
x- mean size : 7.7728415
E+ size : 100
E- size : 14051
e+ mean size : 54.55
e- mean size : 34.400967
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
1626 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.25
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

*** Noise = 20.0% ***
1626 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
Syntactical distance : 0.30555555
Error Rate Precondition : 0.17414832
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
1626 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.110192835

*** Noise = 20.0% ***
1626 actions in training set
Train Perceptron
Extract rules
Combine rules
[(at-robby ?x1), (at-robby ?x2)]
at-robby:?x1
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
at:?x2:?x1
free:?x3
[(at-robby ?x2), (at ?x1 ?x2), (free ?x3), (carry ?x1 ?x3)]
carry:?x3:?x1
at-robby:?x2
Syntactical distance : 0.083333336
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.099956
FSCORE : 0.01619433
