(define (domain hanoi)
(:requirements :strips :typing :negative-preconditions)
(:types
disk case - object
)
(:predicates
	(clear_disk ?x1 - disk)
	(on_case ?x1 - disk ?x2 - case)
	(handempty)
	(clear_case ?x1 - case)
	(holding ?x1 - disk)
	(on_disk ?x1 - disk ?x2 - disk)
	(bigger ?x1 - disk ?x2 - disk)
)
(:action pick_up
	:parameters (?x1 - disk ?x2 - case )
	:precondition (and)
	:effect (and
	(holding ?x1))
)
(:action unstack
	:parameters (?x1 - disk ?x2 - disk )
	:precondition (and
	(bigger ?x2 ?x1))
	:effect (and
	(clear_disk ?x2)
	(holding ?x1))
)
(:action put_down
	:parameters (?x1 - disk ?x2 - case )
	:precondition (and)
	:effect (and)
)
(:action stack
	:parameters (?x1 - disk ?x2 - disk )
	:precondition (and
	(clear_disk ?x2)
	(holding ?x1)
	(bigger ?x2 ?x1))
	:effect (and
	(clear_disk ?x1)
	(not(clear_disk ?x2))
	(not(holding ?x1))
	(handempty))
)
)
