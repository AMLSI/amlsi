# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial2.pddl
pddl/hanoi/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3024
x+ mean size : 14.8
x- mean size : 8.147156
E+ size : 100
E- size : 32200
e+ mean size : 48.67
e- mean size : 31.983448
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 4.821
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 3.766
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.3703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 3.794
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 3.738
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.29814816
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0

RPNIR run : 1
I+ size : 30
I- size : 3143
x+ mean size : 15.433333
x- mean size : 8.366847
E+ size : 100
E- size : 33397
e+ mean size : 48.46
e- mean size : 32.682068
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 5.661
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 5.631
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.3351852
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.006153846
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 5.74
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 5.71
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.36111113
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.006153846

RPNIR run : 2
I+ size : 30
I- size : 2677
x+ mean size : 14.4
x- mean size : 8.09675
E+ size : 100
E- size : 34497
e+ mean size : 49.49
e- mean size : 34.050526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 4.811
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 4.591
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.32222223
Error Rate Precondition : 0.17284666
Error Rate Postcondition : 0.101634376
FSCORE : 0.0075483094
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 4.51
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 4.49
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.39814815
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 3
I+ size : 30
I- size : 3179
x+ mean size : 16.1
x- mean size : 8.530355
E+ size : 100
E- size : 38440
e+ mean size : 55.47
e- mean size : 35.973362
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 4.861
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 4.501
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.3703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.014440433
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 4.513
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 4.529
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.39814815
Error Rate Precondition : 0.0
Error Rate Postcondition : NaN
FSCORE : 0.0

RPNIR run : 4
I+ size : 30
I- size : 2850
x+ mean size : 14.4
x- mean size : 7.9045615
E+ size : 100
E- size : 32687
e+ mean size : 48.33
e- mean size : 32.898155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 3.755
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 3.766
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.34444448
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0017064846
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 3.932
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 3.855
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.3888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
