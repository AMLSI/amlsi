(define (domain hanoi)
(:requirements :strips :typing :negative-preconditions)
(:types
disk case - object
)
(:predicates
	(clear_disk ?x1 - disk)
	(on_case ?x1 - disk ?x2 - case)
	(handempty)
	(clear_case ?x1 - case)
	(holding ?x1 - disk)
	(on_disk ?x1 - disk ?x2 - disk)
	(bigger ?x1 - disk ?x2 - disk)
)
(:action pick_up
	:parameters (?x1 - disk ?x2 - case )
	:precondition (and)
	:effect (and)
)
(:action unstack
	:parameters (?x1 - disk ?x2 - disk )
	:precondition (and
	(bigger ?x2 ?x1))
	:effect (and)
)
(:action put_down
	:parameters (?x1 - disk ?x2 - case )
	:precondition (and)
	:effect (and)
)
(:action stack
	:parameters (?x1 - disk ?x2 - disk )
	:precondition (and
	(bigger ?x2 ?x1))
	:effect (and)
)
)
