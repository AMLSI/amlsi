# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial2.pddl
pddl/hanoi/domain.pddl

RPNI-R run : 0
I+ size : 30
I- size : 3024
x+ mean size : 14.8
x- mean size : 8.147156
E+ size : 100
E- size : 32200
e+ mean size : 48.67
e- mean size : 31.983448
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3468 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_disk:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.3037037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.003964321

*** Noise = 20.0% ***
3468 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
Syntactical distance : 0.31481484
Error Rate Precondition : 0.1292876
Error Rate Postcondition : 0.1292876
FSCORE : 0.019002374
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3468 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
clear_disk:?x2
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3468 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
holding:?x2
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x2
Syntactical distance : 0.2888889
Error Rate Precondition : 0.23999125
Error Rate Postcondition : 0.17545395
FSCORE : 0.0041753654

RPNI-R run : 1
I+ size : 30
I- size : 3143
x+ mean size : 15.433333
x- mean size : 8.366847
E+ size : 100
E- size : 33397
e+ mean size : 48.46
e- mean size : 32.682068
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3606 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.2851852
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.002805049

*** Noise = 20.0% ***
3606 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_disk:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.3037037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3606 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
clear_disk:?x2
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3606 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.23888889
Error Rate Precondition : 0.11024299
Error Rate Postcondition : 0.11024299
FSCORE : 0.0

RPNI-R run : 2
I+ size : 30
I- size : 2677
x+ mean size : 14.4
x- mean size : 8.09675
E+ size : 100
E- size : 34497
e+ mean size : 49.49
e- mean size : 34.050526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3109 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.32962963
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.004237288

*** Noise = 20.0% ***
3109 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x2
Syntactical distance : 0.2777778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0037243946
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3109 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
clear_disk:?x2
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3109 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.038989328
FSCORE : 0.13084112

RPNI-R run : 3
I+ size : 30
I- size : 3179
x+ mean size : 16.1
x- mean size : 8.530355
E+ size : 100
E- size : 38440
e+ mean size : 55.47
e- mean size : 35.973362
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3662 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.32037038
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0034965035

*** Noise = 20.0% ***
3662 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
Syntactical distance : 0.2925926
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0046457606
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3662 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
clear_disk:?x2
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3662 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
Syntactical distance : 0.2
Error Rate Precondition : 0.09994833
Error Rate Postcondition : 0.16051438
FSCORE : 0.024390243

RPNI-R run : 4
I+ size : 30
I- size : 2850
x+ mean size : 14.4
x- mean size : 7.9045615
E+ size : 100
E- size : 32687
e+ mean size : 48.33
e- mean size : 32.898155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
3282 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.32037038
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0029629632

*** Noise = 20.0% ***
3282 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
clear_disk:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
Syntactical distance : 0.33888888
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.2694982
FSCORE : 0.0020491802
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
3282 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
handempty
clear_disk:?x1
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
clear_disk:?x1
on_disk:?x2:?x1
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
clear_disk:?x2
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
3282 actions in training set
Train Perceptron
Extract rules
Combine rules
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
on_case:?x2:?x1
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
handempty
[(clear_disk ?x1), (on_case ?x1 ?x2), (handempty), (clear_case ?x2), (holding ?x1)]
holding:?x1
clear_case:?x2
[(clear_disk ?x2), (handempty), (holding ?x2), (clear_disk ?x1), (holding ?x1), (on_disk ?x1 ?x2), (bigger ?x2 ?x1)]
holding:?x1
Syntactical distance : 0.16851853
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.054802734
FSCORE : 0.0067114094
