# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial1.pddl
pddl/hanoi/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3258
x+ mean size : 14.566667
x- mean size : 7.7099447
E+ size : 100
E- size : 34079
e+ mean size : 49.56
e- mean size : 33.640396
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 278.692
Syntactical distance : 0.27037036
Error Rate Precondition : 0.107419565
Error Rate Postcondition : 0.2775382
FSCORE : 0.16064256

*** Noise = 20.0% ***
Time : 299.694
Syntactical distance : 0.27037036
Error Rate Precondition : 0.107419565
Error Rate Postcondition : 0.2775382
FSCORE : 0.16064256
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 315.939
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25316456

*** Noise = 20.0% ***
Time : 303.996
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25316456

RPNIR run : 1
I+ size : 30
I- size : 3162
x+ mean size : 14.833333
x- mean size : 7.823213
E+ size : 100
E- size : 35986
e+ mean size : 51.82
e- mean size : 33.58595
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 188.814
Syntactical distance : 0.32037038
Error Rate Precondition : 0.1706062
Error Rate Postcondition : 0.22211497
FSCORE : 0.027906977

*** Noise = 20.0% ***
Time : 176.373
Syntactical distance : 0.32037038
Error Rate Precondition : 0.17886749
Error Rate Postcondition : 0.22211497
FSCORE : 0.028129395
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 271.305
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25094104

*** Noise = 20.0% ***
Time : 289.552
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25094104

RPNIR run : 2
I+ size : 30
I- size : 3195
x+ mean size : 15.4
x- mean size : 8.167762
E+ size : 100
E- size : 37976
e+ mean size : 53.86
e- mean size : 34.943832
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 316.641
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.24783146

*** Noise = 20.0% ***
Time : 302.569
Syntactical distance : 0.1462963
Error Rate Precondition : 0.059739884
Error Rate Postcondition : 0.0
FSCORE : 0.06722689
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 280.1
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.24783146

*** Noise = 20.0% ***
Time : 308.575
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.24783146

RPNIR run : 3
I+ size : 30
I- size : 3160
x+ mean size : 15.466666
x- mean size : 8.2376585
E+ size : 100
E- size : 37695
e+ mean size : 53.91
e- mean size : 35.721794
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 213.088
Syntactical distance : 0.32037038
Error Rate Precondition : 0.17204258
Error Rate Postcondition : 0.21977337
FSCORE : 0.031746034

*** Noise = 20.0% ***
Time : 286.528
Syntactical distance : 0.22037038
Error Rate Precondition : 0.08211679
Error Rate Postcondition : 0.13071167
FSCORE : 0.030835118
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 333.426
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.23148149

*** Noise = 20.0% ***
Time : 203.793
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.02296337

RPNIR run : 4
I+ size : 30
I- size : 2972
x+ mean size : 14.266666
x- mean size : 7.7311573
E+ size : 100
E- size : 33159
e+ mean size : 47.68
e- mean size : 33.301456
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 234.094
Syntactical distance : 0.27037036
Error Rate Precondition : 0.10726975
Error Rate Postcondition : 0.27727377
FSCORE : 0.16597511

*** Noise = 20.0% ***
Time : 198.495
Syntactical distance : 0.32037038
Error Rate Precondition : 0.17958754
Error Rate Postcondition : 0.2201878
FSCORE : 0.033970278
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 249.44
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25608197

*** Noise = 20.0% ***
Time : 246.605
Syntactical distance : 0.08703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.25608197
