# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial3.pddl
pddl/hanoi/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3014
x+ mean size : 14.866667
x- mean size : 8.545786
E+ size : 100
E- size : 33107
e+ mean size : 49.13
e- mean size : 32.896305
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 264.335
Syntactical distance : 0.18333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.053356286

*** Noise = 20.0% ***
Time : 245.206
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.063816205
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 242.716
Syntactical distance : 0.21111111
Error Rate Precondition : 0.038760412
Error Rate Postcondition : 0.0
FSCORE : 0.045025416

*** Noise = 20.0% ***
Time : 312.703
Syntactical distance : 0.18333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.053356286

RPNIR run : 1
I+ size : 30
I- size : 2974
x+ mean size : 14.333333
x- mean size : 8.115333
E+ size : 100
E- size : 37215
e+ mean size : 55.09
e- mean size : 35.114094
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 274.0
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.058979653

*** Noise = 20.0% ***
Time : 213.913
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.058979653
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 241.225
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.040865388

*** Noise = 20.0% ***
Time : 248.018
Syntactical distance : 0.16481481
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.047864854

RPNIR run : 2
I+ size : 30
I- size : 3145
x+ mean size : 15.066667
x- mean size : 8.776789
E+ size : 100
E- size : 34186
e+ mean size : 50.21
e- mean size : 33.80232
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 306.092
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0622665

*** Noise = 20.0% ***
Time : 298.104
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0622665
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 241.165
Syntactical distance : 0.22037038
Error Rate Precondition : 0.03941918
Error Rate Postcondition : 0.0
FSCORE : 0.052301254

*** Noise = 20.0% ***
Time : 312.025
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.0622665

RPNIR run : 3
I+ size : 30
I- size : 2749
x+ mean size : 14.066667
x- mean size : 8.328847
E+ size : 100
E- size : 33142
e+ mean size : 48.15
e- mean size : 33.94388
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 219.414
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.064516135

*** Noise = 20.0% ***
Time : 266.948
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.064516135
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 226.633
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.05335874

*** Noise = 20.0% ***
Time : 227.237
Syntactical distance : 0.18333334
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.05645161

RPNIR run : 4
I+ size : 30
I- size : 2667
x+ mean size : 13.8
x- mean size : 8.049494
E+ size : 100
E- size : 33944
e+ mean size : 49.84
e- mean size : 32.84831
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time : 256.732
Syntactical distance : 0.19259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.06487188

*** Noise = 20.0% ***
Time : 185.723
Syntactical distance : 0.22037038
Error Rate Precondition : 0.037919927
Error Rate Postcondition : 0.0
FSCORE : 0.053777892
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time : 200.9
Syntactical distance : 0.21111111
Error Rate Precondition : 0.037919927
Error Rate Postcondition : 0.0
FSCORE : 0.046587925

*** Noise = 20.0% ***
Time : 261.911
Syntactical distance : 0.13703704
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.10378827
