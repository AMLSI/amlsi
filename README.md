# IncrAMLSI : Incremental Action Model Learning with State machine Interaction

## Description

IncrAMLSI is an interactive action model learner using interactions with state machine.

## Libraries

* PDDL4J 3.7.2
* fast_downward 19.06
* VAL

## Compilation

	make compile

## Usage

### Name
IncrAMLSI - Incremental Action Model Learning with System Interaction

### Synopsis
	java -jar build/amlsi.jar [options]

### Main Classes
* main.RUN  : Experiment for the AMLSI algorithm

### Option
* - [withoutRefinement, withoutTabu] : The tested scenario
* -n, -name [string] : The planning domain's name.
* -t, -type [RPNI | RPNIR] : the regular grammar induction algorithm used.
* -d, -directory [directory] : The directory where al logs and models are saved.
* -o, -domain [file]: The file declaring the planning domain to encapsulate the blackbox.
* -i, -initial [file] : The file declaring initial state.
* -r, -run [integer] : The number of runs for the experimentation.
* -properties [file] : The properties file.

## Documentations
	make javadoc
The javadoc is generated in the repository "./javadoc".

## Experimental Setup - ICTAI 2021

### Domains used for the experiment
All domains tested in the experiment are present in the "./pddl" folder. For example, for the Gripper domain, the folder and subfolders are organized as follows :

```
pddl/gripper
│   domain.pddl #The planning domain
└───initial_states #Folder with all initial states declarations
│   │   initial1.pddl #The first initial state
│   │   initial2.pddl #The second initial state
│   │   initial3.pddl #The third initial state
└───problem #Folder with all planning problems declarations
    │   problems.txt #A text file with links to all planning problems
    │   p01.pddl #The first planning problem
    │   p02.pddl #The second planning problem
    │   ...
    │   p20.pddl #The 20th planning problem
```

### Scripts for experiments
* ./script_experimentation/ICTAI_2021 domain domain_name save_directory : Test all scenarii for the domaine "domain" and save results in the directory "save_directory".
* ./script_experimentation/ICTAI_2021_accuracy domain save_directory : Compute the accuracy.
* log_reader/ICTAI_21.ipynb : The jupyter script reading log files

 ### Example of experiments command

To experiment the AMLSI algorithm for the domain Gripper with five runs on the first initial state, you must execute the following commands :

	make compile
	./script_experimentation/ICTAI_21 gripper gripper-typed ICTAI_2021
	./script_experimentation/ICTAI_21_accuracy gripper ICTAI_2021

### Log files

All log files for the experiment are present in the repository experiment. Log fles for the domain Gripper are in the repository "experiment/gripper_amlsi" and is organized as follows
```
ICTAI_21/gripper
└───-AMLSI #The BASE scenario
└──────IS1 #The first initial state
│   │   │   log.txt #The log file containing all metrics results and benchmark specifications
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl #The planning domain learnt where ${f} is the level of observability, ${n} is the level of noise and ${r} is he current run
└──────IS2 #The second initial state
│   │   │   log.txt
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────IS3 #The third initial state
│   │   │   log.txt
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────planning_results.txt #File containing accuracy results
└─── INCR #The incremental scenario
└──────IS1 #The first initial state
│   │   │   log.txt #The log file containing all metrics results and benchmark specifications
│   │   │   amlsi_rpni.${f}.${n}.${r}.pddl #The planning domain learnt where ${f} is the level of observability, ${n} is the level of noise and ${r} is he current run
└──────IS2 #The second initial state
│   │   │   log.txt
│   │   │   amlsi_rpni.${f}.${n}.${r}.pddl
└──────IS3 #The third initial state
│   │   │   log.txt
│   │   │   amlsi_rpni.${f}.${n}.${r}.pddl
└──────planning_results.txt #File containing accuracy results
```
## Additionnal Experimental Setup - ICTAI 2021

### Scripts for experiments
* ./script_experimentation/ICTAI_2021_additionnal domain domain_name save_directory : Test all scenarii for the domaine "domain" and save results in the directory "save_directory".
* ./script_experimentation/ICTAI_2021_accuracy_additionnal domain save_directory : Compute the accuracy.
* log_reader/ICTAI_21_additionnal.ipynb : The jupyter script reading log files


### Log files

All log files for the experiment are present in the repository experiment. Log fles for the domain Gripper are in the repository "experiment/gripper_amlsi" and is organized as follows
```
ICTAI_21_additionnal/gripper
└───-NON_CONVERGENT #The non-convergent scenario
└──────IS1 #The first initial state
│   │   │   log.txt #The log file containing all metrics results and benchmark specifications
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────IS2 #The second initial state
│   │   │   log.txt
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────IS3 #The third initial state
│   │   │   log.txt
│   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└─── CONVERGENT #The incremental scenario
└──────DELTA2 # The scenario where the convergence criteria is 2
└──────────IS1 #The first initial state
│   │   │   │   log.txt #The log file containing all metrics results and benchmark specifications
│   │   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────────IS2 #The second initial state
│   │   │   │   log.txt
│   │   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────────IS3 #The third initial state
│   │   │   │   log.txt
│   │   │   │   amlsi_rpnir.${f}.${n}.${r}.pddl
└──────────planning_results.txt #File containing accuracy results
```
