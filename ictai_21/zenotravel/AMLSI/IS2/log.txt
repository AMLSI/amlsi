# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial2.pddl
pddl/zenotravel/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1683
x+ mean size : 15.166667
x- mean size : 8.499702
E+ size : 100
E- size : 19962
e+ mean size : 56.11
e- mean size : 35.450104
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 20
#Transitions : 46
Compression level : 22.75
PDDL Generation
Time : 141.655
Recall = 0.25
Precision = 0.5102041
Fscore automaton 0.33557045
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 20
#Transitions : 46
Compression level : 22.75
PDDL Generation
Time : 346.096
Recall = 0.25
Precision = 0.5102041
Fscore automaton 0.33557045
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 20
#Transitions : 46
Compression level : 22.75
PDDL Generation
Time : 67.217
Recall = 0.25
Precision = 0.5102041
Fscore automaton 0.33557045
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 20
#Transitions : 46
Compression level : 22.75
PDDL Generation
Time : 274.812
Recall = 0.25
Precision = 0.5102041
Fscore automaton 0.33557045
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1570
x+ mean size : 14.333333
x- mean size : 7.9012737
E+ size : 100
E- size : 20089
e+ mean size : 55.57
e- mean size : 35.590572
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 430.0
#States : 21
#Transitions : 43
Compression level : 20.47619
PDDL Generation
Time : 129.012
Recall = 0.24
Precision = 0.35820895
Fscore automaton 0.28742513
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 430.0
#States : 21
#Transitions : 43
Compression level : 20.47619
PDDL Generation
Time : 188.646
Recall = 0.24
Precision = 0.35820895
Fscore automaton 0.28742513
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 430.0
#States : 21
#Transitions : 43
Compression level : 20.47619
PDDL Generation
Time : 58.898
Recall = 0.24
Precision = 0.35820895
Fscore automaton 0.28742513
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 430.0
#States : 21
#Transitions : 43
Compression level : 20.47619
PDDL Generation
Time : 127.578
Recall = 0.24
Precision = 0.35820895
Fscore automaton 0.28742513
Syntactical distance : 0.025454545
Error Rate Precondition : 0.020800373
Error Rate Postcondition : 0.0
FSCORE : 0.33009708

RPNIR run : 2
I+ size : 30
I- size : 1483
x+ mean size : 14.466666
x- mean size : 8.095752
E+ size : 100
E- size : 16564
e+ mean size : 45.56
e- mean size : 32.300953
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 18
#Transitions : 43
Compression level : 24.11111
PDDL Generation
Time : 120.828
Recall = 0.34
Precision = 0.5
Fscore automaton 0.40476188
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 18
#Transitions : 43
Compression level : 24.11111
PDDL Generation
Time : 283.203
Recall = 0.34
Precision = 0.5
Fscore automaton 0.40476188
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 18
#Transitions : 43
Compression level : 24.11111
PDDL Generation
Time : 54.402
Recall = 0.34
Precision = 0.5
Fscore automaton 0.40476188
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 18
#Transitions : 43
Compression level : 24.11111
PDDL Generation
Time : 215.499
Recall = 0.34
Precision = 0.5
Fscore automaton 0.40476188
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1768
x+ mean size : 15.3
x- mean size : 8.212104
E+ size : 100
E- size : 17741
e+ mean size : 49.68
e- mean size : 33.48915
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 47
Compression level : 20.863636
PDDL Generation
Time : 133.276
Recall = 0.46
Precision = 0.6571429
Fscore automaton 0.5411765
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 47
Compression level : 20.863636
PDDL Generation
Time : 301.379
Recall = 0.46
Precision = 0.6571429
Fscore automaton 0.5411765
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 47
Compression level : 20.863636
PDDL Generation
Time : 66.805
Recall = 0.46
Precision = 0.6571429
Fscore automaton 0.5411765
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 47
Compression level : 20.863636
PDDL Generation
Time : 298.749
Recall = 0.46
Precision = 0.6571429
Fscore automaton 0.5411765
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1789
x+ mean size : 15.233334
x- mean size : 8.609279
E+ size : 100
E- size : 17269
e+ mean size : 47.74
e- mean size : 34.36441
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 22
#Transitions : 46
Compression level : 20.772728
PDDL Generation
Time : 142.313
Recall = 0.3
Precision = 0.8333333
Fscore automaton 0.44117647
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 22
#Transitions : 46
Compression level : 20.772728
PDDL Generation
Time : 263.071
Recall = 0.3
Precision = 0.8333333
Fscore automaton 0.44117647
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 22
#Transitions : 46
Compression level : 20.772728
PDDL Generation
Time : 68.115
Recall = 0.3
Precision = 0.8333333
Fscore automaton 0.44117647
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 22
#Transitions : 46
Compression level : 20.772728
PDDL Generation
Time : 278.876
Recall = 0.3
Precision = 0.8333333
Fscore automaton 0.44117647
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
