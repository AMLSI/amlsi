# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial1.pddl
pddl/parking/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 5794
x+ mean size : 14.7
x- mean size : 8.358475
E+ size : 100
E- size : 69720
e+ mean size : 52.15
e- mean size : 33.316868
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 49
#Transitions : 128
Compression level : 9.0
PDDL Generation
Time : 753.094
Recall = 0.05
Precision = 0.13157895
Fscore automaton 0.07246377
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 49
#Transitions : 128
Compression level : 9.0
PDDL Generation
Time : 2883.66
Recall = 0.05
Precision = 0.13157895
Fscore automaton 0.07246377
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 441.0
#States : 49
#Transitions : 128
Compression level : 9.0
PDDL Generation
Time : 304.186
Recall = 0.05
Precision = 0.13157895
Fscore automaton 0.07246377
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 441.0
#States : 49
#Transitions : 128
Compression level : 9.0
PDDL Generation
Time : 1822.174
Recall = 0.05
Precision = 0.13157895
Fscore automaton 0.07246377
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 6105
x+ mean size : 15.233334
x- mean size : 7.9408684
E+ size : 100
E- size : 73782
e+ mean size : 54.99
e- mean size : 35.674988
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 55
#Transitions : 130
Compression level : 8.309091
PDDL Generation
Time : 308.311
Recall = 0.07
Precision = 0.33333334
Fscore automaton 0.11570248
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 55
#Transitions : 130
Compression level : 8.309091
PDDL Generation
Time : 2322.819
Recall = 0.07
Precision = 0.33333334
Fscore automaton 0.11570248
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 457.0
#States : 55
#Transitions : 130
Compression level : 8.309091
PDDL Generation
Time : 698.972
Recall = 0.07
Precision = 0.33333334
Fscore automaton 0.11570248
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 457.0
#States : 55
#Transitions : 130
Compression level : 8.309091
PDDL Generation
Time : 1967.059
Recall = 0.07
Precision = 0.33333334
Fscore automaton 0.11570248
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 6259
x+ mean size : 15.5
x- mean size : 8.653459
E+ size : 100
E- size : 62917
e+ mean size : 46.68
e- mean size : 32.31421
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 465.0
#States : 63
#Transitions : 155
Compression level : 7.3809524
PDDL Generation
Time : 819.633
Recall = 0.1
Precision = 0.27027026
Fscore automaton 0.1459854
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 465.0
#States : 63
#Transitions : 155
Compression level : 7.3809524
PDDL Generation
Time : 2154.501
Recall = 0.1
Precision = 0.27027026
Fscore automaton 0.1459854
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 465.0
#States : 63
#Transitions : 155
Compression level : 7.3809524
PDDL Generation
Time : 755.213
Recall = 0.1
Precision = 0.27027026
Fscore automaton 0.1459854
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 465.0
#States : 63
#Transitions : 155
Compression level : 7.3809524
PDDL Generation
Time : 1633.294
Recall = 0.1
Precision = 0.27027026
Fscore automaton 0.1459854
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 6089
x+ mean size : 15.0
x- mean size : 8.27952
E+ size : 100
E- size : 70826
e+ mean size : 52.64
e- mean size : 34.61439
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 55
#Transitions : 127
Compression level : 8.181818
PDDL Generation
Time : 703.45
Recall = 0.09
Precision = 0.33333334
Fscore automaton 0.14173229
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 55
#Transitions : 127
Compression level : 8.181818
PDDL Generation
Time : 1874.602
Recall = 0.09
Precision = 0.33333334
Fscore automaton 0.14173229
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 55
#Transitions : 127
Compression level : 8.181818
PDDL Generation
Time : 700.878
Recall = 0.09
Precision = 0.33333334
Fscore automaton 0.14173229
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 55
#Transitions : 127
Compression level : 8.181818
PDDL Generation
Time : 1915.67
Recall = 0.09
Precision = 0.33333334
Fscore automaton 0.14173229
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 5502
x+ mean size : 13.633333
x- mean size : 7.757361
E+ size : 100
E- size : 63547
e+ mean size : 47.59
e- mean size : 32.401325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 409.0
#States : 54
#Transitions : 126
Compression level : 7.5740743
PDDL Generation
Time : 578.625
Recall = 0.07
Precision = 0.3043478
Fscore automaton 0.11382114
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 409.0
#States : 54
#Transitions : 126
Compression level : 7.5740743
PDDL Generation
Time : 1967.367
Recall = 0.07
Precision = 0.3043478
Fscore automaton 0.11382114
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 409.0
#States : 54
#Transitions : 126
Compression level : 7.5740743
PDDL Generation
Time : 603.696
Recall = 0.07
Precision = 0.3043478
Fscore automaton 0.11382114
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 409.0
#States : 54
#Transitions : 126
Compression level : 7.5740743
PDDL Generation
Time : 1596.37
Recall = 0.07
Precision = 0.3043478
Fscore automaton 0.11382114
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
