# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial2.pddl
Delta=10
pddl/parking/domain.pddl
E+ size : 100
E- size : 64214
e+ mean size : 50.98
e- mean size : 33.534462
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2071
Time : 1061.407
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 31
Size I+ 31
Size I- 5164
Time : 22190.303
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2137
Time : 1003.679
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2330
Time : 1324.563
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 63818
e+ mean size : 51.26
e- mean size : 34.81048
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 3290
Time : 3233.989
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 38
Size I+ 38
Size I- 6350
Time : 18909.75
false 
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1945
Time : 825.728
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2125
Time : 1604.081
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 64570
e+ mean size : 51.99
e- mean size : 35.06142
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 19
Size I+ 19
Size I- 2951
Time : 2868.118
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 30
Size I+ 30
Size I- 4933
Time : 16346.425
false 
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1959
Time : 743.428
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 2557
Time : 1753.92
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 63620
e+ mean size : 49.61
e- mean size : 33.275887
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 22
Size I+ 22
Size I- 3907
Time : 4652.922
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 29
Size I+ 29
Size I- 5091
Time : 14840.116
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1848
Time : 853.532
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 2300
Time : 1679.78
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 62702
e+ mean size : 48.98
e- mean size : 34.47332
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 24
Size I+ 24
Size I- 4389
Time : 5971.836
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 36
Size I+ 36
Size I- 6274
Time : 18050.348
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2148
Time : 929.966
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 2148
Time : 1099.792
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
