# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial1.pddl
pddl/NegElevator/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1095
x+ mean size : 14.633333
x- mean size : 8.162557
E+ size : 100
E- size : 13821
e+ mean size : 52.99
e- mean size : 34.13248
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 26.371
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 48.152
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 14.786
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 439.0
#States : 19
#Transitions : 35
Compression level : 23.105263
PDDL Generation
Time : 40.013
Recall = 0.91
Precision = 0.14
Fscore automaton 0.24266668
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1133
x+ mean size : 14.833333
x- mean size : 8.496028
E+ size : 100
E- size : 12315
e+ mean size : 47.98
e- mean size : 34.52075
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 39
Compression level : 21.190475
PDDL Generation
Time : 27.639
Recall = 0.54
Precision = 0.163142
Fscore automaton 0.25058004
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 39
Compression level : 21.190475
PDDL Generation
Time : 35.033
Recall = 0.54
Precision = 0.163142
Fscore automaton 0.25058004
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 39
Compression level : 21.190475
PDDL Generation
Time : 15.909
Recall = 0.54
Precision = 0.163142
Fscore automaton 0.25058004
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 21
#Transitions : 39
Compression level : 21.190475
PDDL Generation
Time : 50.055
Recall = 0.54
Precision = 0.163142
Fscore automaton 0.25058004
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1094
x+ mean size : 15.566667
x- mean size : 8.624314
E+ size : 100
E- size : 12076
e+ mean size : 47.19
e- mean size : 34.12852
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 23
#Transitions : 40
Compression level : 20.304348
PDDL Generation
Time : 28.684
Recall = 0.76
Precision = 0.16666667
Fscore automaton 0.2733813
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 23
#Transitions : 40
Compression level : 20.304348
PDDL Generation
Time : 44.313
Recall = 0.76
Precision = 0.16666667
Fscore automaton 0.2733813
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 23
#Transitions : 40
Compression level : 20.304348
PDDL Generation
Time : 16.77
Recall = 0.76
Precision = 0.16666667
Fscore automaton 0.2733813
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 23
#Transitions : 40
Compression level : 20.304348
PDDL Generation
Time : 52.845
Recall = 0.76
Precision = 0.16666667
Fscore automaton 0.2733813
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1044
x+ mean size : 15.333333
x- mean size : 9.1341
E+ size : 100
E- size : 13375
e+ mean size : 51.54
e- mean size : 36.472973
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 22
#Transitions : 40
Compression level : 20.90909
PDDL Generation
Time : 37.795
Recall = 0.89
Precision = 0.11454312
Fscore automaton 0.20296466
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 22
#Transitions : 40
Compression level : 20.90909
PDDL Generation
Time : 38.933
Recall = 0.89
Precision = 0.11454312
Fscore automaton 0.20296466
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 22
#Transitions : 40
Compression level : 20.90909
PDDL Generation
Time : 16.173
Recall = 0.89
Precision = 0.11454312
Fscore automaton 0.20296466
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 22
#Transitions : 40
Compression level : 20.90909
PDDL Generation
Time : 42.572
Recall = 0.89
Precision = 0.11454312
Fscore automaton 0.20296466
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1014
x+ mean size : 15.266666
x- mean size : 8.962524
E+ size : 100
E- size : 13207
e+ mean size : 50.94
e- mean size : 35.884457
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 30
#Transitions : 53
Compression level : 15.266666
PDDL Generation
Time : 32.779
Recall = 0.71
Precision = 0.13575526
Fscore automaton 0.22792938
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 30
#Transitions : 53
Compression level : 15.266666
PDDL Generation
Time : 46.596
Recall = 0.71
Precision = 0.13575526
Fscore automaton 0.22792938
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 30
#Transitions : 53
Compression level : 15.266666
PDDL Generation
Time : 19.587
Recall = 0.71
Precision = 0.13575526
Fscore automaton 0.22792938
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 30
#Transitions : 53
Compression level : 15.266666
PDDL Generation
Time : 42.068
Recall = 0.71
Precision = 0.13575526
Fscore automaton 0.22792938
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
