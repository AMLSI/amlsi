# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial2.pddl
pddl/hanoi/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3024
x+ mean size : 14.8
x- mean size : 8.147156
E+ size : 100
E- size : 32200
e+ mean size : 48.67
e- mean size : 31.983448
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 86.534
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 595.238
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 83.624
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 31
#Transitions : 58
Compression level : 14.32258
PDDL Generation
Time : 499.26
Recall = 0.25
Precision = 0.64102566
Fscore automaton 0.35971224
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 3143
x+ mean size : 15.433333
x- mean size : 8.366847
E+ size : 100
E- size : 33397
e+ mean size : 48.46
e- mean size : 32.682068
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 90.516
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 548.75
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 90.946
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 37
#Transitions : 66
Compression level : 12.513514
PDDL Generation
Time : 517.673
Recall = 0.27
Precision = 0.675
Fscore automaton 0.3857143
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 2677
x+ mean size : 14.4
x- mean size : 8.09675
E+ size : 100
E- size : 34497
e+ mean size : 49.49
e- mean size : 34.050526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 74.393
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 279.421
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.053703707
Error Rate Precondition : 0.03333085
Error Rate Postcondition : 0.037522476
FSCORE : 0.6666667
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 71.91
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 36
#Transitions : 65
Compression level : 12.0
PDDL Generation
Time : 491.949
Recall = 0.23
Precision = 0.51111114
Fscore automaton 0.3172414
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 3179
x+ mean size : 16.1
x- mean size : 8.530355
E+ size : 100
E- size : 38440
e+ mean size : 55.47
e- mean size : 35.973362
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 86.34
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 609.848
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 86.647
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 483.0
#States : 27
#Transitions : 53
Compression level : 17.88889
PDDL Generation
Time : 638.119
Recall = 0.21
Precision = 0.3181818
Fscore automaton 0.25301203
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 2850
x+ mean size : 14.4
x- mean size : 7.9045615
E+ size : 100
E- size : 32687
e+ mean size : 48.33
e- mean size : 32.898155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 73.86
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 450.5
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 74.428
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 432.0
#States : 29
#Transitions : 58
Compression level : 14.896552
PDDL Generation
Time : 504.841
Recall = 0.33
Precision = 0.37931034
Fscore automaton 0.3529412
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
