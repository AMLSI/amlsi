# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial2.pddl
pddl/blocksworld/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 2602
x+ mean size : 15.4
x- mean size : 8.697156
E+ size : 100
E- size : 28162
e+ mean size : 51.43
e- mean size : 33.46783
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 41
Compression level : 21.0
PDDL Generation
Time : 80.673
Recall = 0.39
Precision = 0.8666667
Fscore automaton 0.537931
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 41
Compression level : 21.0
PDDL Generation
Time : 392.267
Recall = 0.39
Precision = 0.8666667
Fscore automaton 0.537931
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 41
Compression level : 21.0
PDDL Generation
Time : 77.598
Recall = 0.39
Precision = 0.8666667
Fscore automaton 0.537931
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 22
#Transitions : 41
Compression level : 21.0
PDDL Generation
Time : 404.644
Recall = 0.39
Precision = 0.8666667
Fscore automaton 0.537931
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 2603
x+ mean size : 14.966666
x- mean size : 7.7041874
E+ size : 100
E- size : 26562
e+ mean size : 48.06
e- mean size : 33.24343
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 449.0
#States : 21
#Transitions : 39
Compression level : 21.380953
PDDL Generation
Time : 72.475
Recall = 0.42
Precision = 0.75
Fscore automaton 0.53846157
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 449.0
#States : 21
#Transitions : 39
Compression level : 21.380953
PDDL Generation
Time : 335.306
Recall = 0.42
Precision = 0.75
Fscore automaton 0.53846157
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 449.0
#States : 21
#Transitions : 39
Compression level : 21.380953
PDDL Generation
Time : 72.538
Recall = 0.42
Precision = 0.75
Fscore automaton 0.53846157
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 449.0
#States : 21
#Transitions : 39
Compression level : 21.380953
PDDL Generation
Time : 569.495
Recall = 0.42
Precision = 0.75
Fscore automaton 0.53846157
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 2515
x+ mean size : 14.8
x- mean size : 7.862823
E+ size : 100
E- size : 23479
e+ mean size : 42.52
e- mean size : 31.012947
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 25
#Transitions : 44
Compression level : 17.76
PDDL Generation
Time : 72.215
Recall = 0.29
Precision = 0.5471698
Fscore automaton 0.37908494
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 25
#Transitions : 44
Compression level : 17.76
PDDL Generation
Time : 116.173
Recall = 0.29
Precision = 0.5471698
Fscore automaton 0.37908494
Syntactical distance : 0.037037037
Error Rate Precondition : 0.124055184
Error Rate Postcondition : 0.0
FSCORE : 0.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 25
#Transitions : 44
Compression level : 17.76
PDDL Generation
Time : 72.659
Recall = 0.29
Precision = 0.5471698
Fscore automaton 0.37908494
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 25
#Transitions : 44
Compression level : 17.76
PDDL Generation
Time : 395.394
Recall = 0.29
Precision = 0.5471698
Fscore automaton 0.37908494
Syntactical distance : 0.048611112
Error Rate Precondition : 0.045903903
Error Rate Postcondition : 0.061486837
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 2477
x+ mean size : 14.433333
x- mean size : 7.269681
E+ size : 100
E- size : 26740
e+ mean size : 49.62
e- mean size : 32.771614
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 20
#Transitions : 40
Compression level : 21.65
PDDL Generation
Time : 67.292
Recall = 0.27
Precision = 0.29032257
Fscore automaton 0.27979276
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 20
#Transitions : 40
Compression level : 21.65
PDDL Generation
Time : 402.783
Recall = 0.27
Precision = 0.29032257
Fscore automaton 0.27979276
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 433.0
#States : 20
#Transitions : 40
Compression level : 21.65
PDDL Generation
Time : 67.463
Recall = 0.27
Precision = 0.29032257
Fscore automaton 0.27979276
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 433.0
#States : 20
#Transitions : 40
Compression level : 21.65
PDDL Generation
Time : 314.609
Recall = 0.27
Precision = 0.29032257
Fscore automaton 0.27979276
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 2776
x+ mean size : 14.9
x- mean size : 8.102666
E+ size : 100
E- size : 26871
e+ mean size : 49.04
e- mean size : 31.585018
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 21
#Transitions : 38
Compression level : 21.285715
PDDL Generation
Time : 77.794
Recall = 0.4
Precision = 0.41666666
Fscore automaton 0.40816328
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 21
#Transitions : 38
Compression level : 21.285715
PDDL Generation
Time : 384.92
Recall = 0.4
Precision = 0.41666666
Fscore automaton 0.40816328
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 447.0
#States : 21
#Transitions : 38
Compression level : 21.285715
PDDL Generation
Time : 77.681
Recall = 0.4
Precision = 0.41666666
Fscore automaton 0.40816328
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 447.0
#States : 21
#Transitions : 38
Compression level : 21.285715
PDDL Generation
Time : 464.863
Recall = 0.4
Precision = 0.41666666
Fscore automaton 0.40816328
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
