# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial3.pddl
pddl/blocksworld/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 2095
x+ mean size : 13.833333
x- mean size : 8.172792
E+ size : 100
E- size : 23864
e+ mean size : 45.41
e- mean size : 32.710526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 47.232
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 252.042
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0462963
Error Rate Precondition : 0.06041574
Error Rate Postcondition : 0.076702565
FSCORE : 0.22962113
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 44.87
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 415.0
#States : 23
#Transitions : 44
Compression level : 18.043478
PDDL Generation
Time : 238.826
Recall = 0.43
Precision = 0.5
Fscore automaton 0.4623656
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 2272
x+ mean size : 15.4
x- mean size : 8.99912
E+ size : 100
E- size : 28025
e+ mean size : 54.08
e- mean size : 37.01067
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 52.51
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 315.64
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 52.407
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 24
#Transitions : 45
Compression level : 19.25
PDDL Generation
Time : 249.239
Recall = 0.57
Precision = 1.0
Fscore automaton 0.7261147
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 2258
x+ mean size : 14.533334
x- mean size : 8.347652
E+ size : 100
E- size : 24581
e+ mean size : 47.66
e- mean size : 33.426712
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 47.204
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 254.219
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 47.057
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 436.0
#States : 22
#Transitions : 42
Compression level : 19.818182
PDDL Generation
Time : 253.861
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 2143
x+ mean size : 14.466666
x- mean size : 8.271582
E+ size : 100
E- size : 26402
e+ mean size : 49.54
e- mean size : 35.303234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 45.492
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 267.243
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 45.382
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 434.0
#States : 25
#Transitions : 51
Compression level : 17.36
PDDL Generation
Time : 277.824
Recall = 0.59
Precision = 0.28921568
Fscore automaton 0.3881579
Syntactical distance : 0.0462963
Error Rate Precondition : 0.08100657
Error Rate Postcondition : 0.057662174
FSCORE : 0.19379845

RPNIR run : 4
I+ size : 30
I- size : 2262
x+ mean size : 15.3
x- mean size : 8.605659
E+ size : 100
E- size : 27580
e+ mean size : 52.15
e- mean size : 34.231472
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 48.996
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 299.13
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 48.725
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 459.0
#States : 22
#Transitions : 42
Compression level : 20.863636
PDDL Generation
Time : 247.64
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
