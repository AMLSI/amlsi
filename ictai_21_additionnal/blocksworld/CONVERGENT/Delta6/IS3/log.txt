# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial3.pddl
Delta=6
pddl/blocksworld/domain.pddl
E+ size : 100
E- size : 23864
e+ mean size : 45.41
e- mean size : 32.710526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 487
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 1150
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.06481481
Error Rate Precondition : 0.08757388
Error Rate Postcondition : 0.06233049
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 503
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 637
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 28025
e+ mean size : 54.08
e- mean size : 37.01067
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 1116
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 1345
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 453
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 587
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 24581
e+ mean size : 47.66
e- mean size : 33.426712
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 657
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 819
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.083333336
Error Rate Precondition : 0.11978095
Error Rate Postcondition : 0.12638704
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 461
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 1362
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 26402
e+ mean size : 49.54
e- mean size : 35.303234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 486
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 90
Size I+ 90
Size I- 5126
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 608
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 1057
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 27580
e+ mean size : 52.15
e- mean size : 34.231472
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 569
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 23
Size I+ 23
Size I- 1430
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 623
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 27
Size I+ 27
Size I- 1799
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
