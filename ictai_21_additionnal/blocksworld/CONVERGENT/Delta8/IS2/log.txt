# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial2.pddl
Delta=8
pddl/blocksworld/domain.pddl
E+ size : 100
E- size : 28162
e+ mean size : 51.43
e- mean size : 33.46783
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 660
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 34
Size I+ 34
Size I- 1756
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 670
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 707
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 26562
e+ mean size : 48.06
e- mean size : 33.24343
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 727
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 1139
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 630
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 630
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 23479
e+ mean size : 42.52
e- mean size : 31.012947
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 714
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 42
Size I+ 42
Size I- 2393
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 625
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 625
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 26740
e+ mean size : 49.62
e- mean size : 32.771614
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 690
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 690
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 523
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 22
Size I+ 22
Size I- 1248
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 26871
e+ mean size : 49.04
e- mean size : 31.585018
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 641
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 907
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 548
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 548
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
