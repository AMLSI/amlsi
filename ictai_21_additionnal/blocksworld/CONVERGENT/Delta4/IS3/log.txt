# actions 18
# predicate 16
Initial state : pddl/blocksworld/initial_states/initial3.pddl
Delta=4
pddl/blocksworld/domain.pddl
E+ size : 100
E- size : 23864
e+ mean size : 45.41
e- mean size : 32.710526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 355
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 646
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.074074075
Error Rate Precondition : 0.12644129
Error Rate Postcondition : 0.06233049
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 415
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 503
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 28025
e+ mean size : 54.08
e- mean size : 37.01067
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 390
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 1253
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 333
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 453
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 24581
e+ mean size : 47.66
e- mean size : 33.426712
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 563
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 657
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.083333336
Error Rate Precondition : 0.11978095
Error Rate Postcondition : 0.12638704
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 338
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 1250
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 26402
e+ mean size : 49.54
e- mean size : 35.303234
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 400
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 88
Size I+ 88
Size I- 5039
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 475
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 987
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 27580
e+ mean size : 52.15
e- mean size : 34.231472
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 472
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 878
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 472
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 1664
Tested domain: experiment/blocksworld_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
