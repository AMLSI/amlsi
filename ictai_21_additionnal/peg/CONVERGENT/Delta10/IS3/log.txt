# actions 52
# predicate 57
Initial state : pddl/peg/initial_states/initial3.pddl
Delta=10
pddl/peg/domain.pddl
E+ size : 100
E- size : 19977
e+ mean size : 7.92
e- mean size : 6.0419483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 2923
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 39
Size I+ 39
Size I- 4563
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.11805555
Error Rate Precondition : 0.061769616
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2225
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 24
Size I+ 24
Size I- 3959
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 20531
e+ mean size : 7.86
e- mean size : 6.0698943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 2844
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 2260
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.111111104
Error Rate Precondition : 0.05733375
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 2309
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 30
Size I+ 30
Size I- 4607
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 20292
e+ mean size : 7.81
e- mean size : 6.057609
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 17
Size I+ 17
Size I- 2954
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 40
Size I+ 40
Size I- 5296
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1945
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 2670
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 20344
e+ mean size : 7.68
e- mean size : 6.0107155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 3348
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 39
Size I+ 39
Size I- 5775
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.11805555
Error Rate Precondition : 0.058027077
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2076
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 2245
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 22230
e+ mean size : 8.17
e- mean size : 6.113045
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 23
Size I+ 23
Size I- 3410
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 75
Size I+ 75
Size I- 7223
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1733
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1966
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010762976
0.99
FSCORE : 0.99
