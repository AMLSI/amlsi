# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial1.pddl
Delta=6
pddl/peg/domain.pddl
E+ size : 100
E- size : 12235
e+ mean size : 7.13
e- mean size : 5.3328156
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 700
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.8
FSCORE : 0.88888896

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 958
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 682
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1028
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12555
e+ mean size : 7.14
e- mean size : 5.3444047
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 939
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 939
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 647
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 676
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.73
FSCORE : 0.75647676
E+ size : 100
E- size : 12372
e+ mean size : 7.1
e- mean size : 5.3195925
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 934
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 1268
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 555
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 939
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12369
e+ mean size : 7.07
e- mean size : 5.346107
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 1113
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 1516
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.09722223
Error Rate Precondition : 0.031243972
Error Rate Postcondition : 0.013202934
0.53
FSCORE : 0.6928104
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 814
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 1283
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 11867
e+ mean size : 7.04
e- mean size : 5.374568
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 940
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 1153
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 596
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 1260
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
