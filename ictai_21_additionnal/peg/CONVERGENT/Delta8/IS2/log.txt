# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial2.pddl
Delta=8
pddl/peg/domain.pddl
E+ size : 100
E- size : 10705
e+ mean size : 5.69
e- mean size : 4.5860815
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 28
Size I+ 28
Size I- 884
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 900
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.19444443
Error Rate Precondition : 0.08118675
Error Rate Postcondition : 0.0070646415
0.8
FSCORE : 0.88888896
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 713
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 802
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 10225
e+ mean size : 5.47
e- mean size : 4.4870415
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 750
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 900
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 651
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 724
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010316613
1.0
FSCORE : 1.0
E+ size : 100
E- size : 10626
e+ mean size : 5.6
e- mean size : 4.4980235
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 21
Size I+ 21
Size I- 835
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 900
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.18055557
Error Rate Precondition : 0.06901476
Error Rate Postcondition : 0.0
0.76
FSCORE : 0.8636364
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 703
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 900
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.07638889
Error Rate Precondition : 0.005988024
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 10512
e+ mean size : 5.61
e- mean size : 4.513889
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 836
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.09027778
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 21
Size I+ 21
Size I- 850
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.13194445
Error Rate Precondition : 0.07423359
Error Rate Postcondition : 0.011009174
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 523
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 523
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 10803
e+ mean size : 5.75
e- mean size : 4.5760436
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 771
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 23
Size I+ 23
Size I- 778
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.13194443
Error Rate Precondition : 0.06818182
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 622
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 900
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.00862069
1.0
FSCORE : 1.0
