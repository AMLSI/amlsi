# actions 52
# predicate 57
Initial state : pddl/peg/initial_states/initial3.pddl
Delta=2
pddl/peg/domain.pddl
E+ size : 100
E- size : 19977
e+ mean size : 7.92
e- mean size : 6.0419483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 844
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 844
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.011175899
0.97
FSCORE : 0.9107982
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 694
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 849
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.97
FSCORE : 0.9847716
E+ size : 100
E- size : 20531
e+ mean size : 7.86
e- mean size : 6.0698943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 1031
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 1031
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.111111104
Error Rate Precondition : 0.05733375
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 697
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.125
Error Rate Precondition : 0.0073382254
Error Rate Postcondition : 0.0
0.61
FSCORE : 0.757764

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 697
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.125
Error Rate Precondition : 0.0072847684
Error Rate Postcondition : 0.0
0.61
FSCORE : 0.757764
E+ size : 100
E- size : 20292
e+ mean size : 7.81
e- mean size : 6.057609
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 1101
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.96
FSCORE : 0.9795918

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 2551
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 803
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 1618
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 20344
e+ mean size : 7.68
e- mean size : 6.0107155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 514
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.99
FSCORE : 0.9124424

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 1484
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.1736111
Error Rate Precondition : 0.06913073
Error Rate Postcondition : 0.030726258
0.97
FSCORE : 0.9847716
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 564
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 564
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.99
FSCORE : 0.99497485
E+ size : 100
E- size : 22230
e+ mean size : 8.17
e- mean size : 6.113045
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 489
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.125
Error Rate Precondition : 0.0072242734
Error Rate Postcondition : 0.0
0.59
FSCORE : 0.7421384

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 2657
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 521
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 653
Tested domain: experiment/peg_amlsi_convergent2/passive/Delta2/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010762976
0.99
FSCORE : 0.99
