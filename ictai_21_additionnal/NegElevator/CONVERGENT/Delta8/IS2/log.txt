# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial2.pddl
Delta=8
pddl/NegElevator/domain.pddl
E+ size : 100
E- size : 13233
e+ mean size : 51.68
e- mean size : 36.82491
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 457
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 628
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 258
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 258
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12995
e+ mean size : 50.49
e- mean size : 35.05333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 17
Size I+ 17
Size I- 476
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 476
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 268
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 349
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13148
e+ mean size : 51.15
e- mean size : 35.614616
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 330
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 48
Size I+ 48
Size I- 1228
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 257
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 393
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13577
e+ mean size : 52.17
e- mean size : 36.00648
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 260
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 312
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 270
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 270
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13918
e+ mean size : 53.62
e- mean size : 36.94561
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 484
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 597
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 285
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 285
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta8/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
