# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial1.pddl
Delta=4
pddl/NegElevator/domain.pddl
E+ size : 100
E- size : 13821
e+ mean size : 52.99
e- mean size : 34.13248
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 216
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 216
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 207
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 229
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12315
e+ mean size : 47.98
e- mean size : 34.52075
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 198
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 227
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 169
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 169
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12076
e+ mean size : 47.19
e- mean size : 34.12852
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 258
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 258
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 169
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 292
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13375
e+ mean size : 51.54
e- mean size : 36.472973
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 185
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 253
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 173
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 173
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13207
e+ mean size : 50.94
e- mean size : 35.884457
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 140
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 0.18957347

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 379
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.083333336
Error Rate Precondition : 0.010493985
Error Rate Postcondition : 0.034517158
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 211
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 211
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
