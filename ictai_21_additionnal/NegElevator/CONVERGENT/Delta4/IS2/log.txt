# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial2.pddl
Delta=4
pddl/NegElevator/domain.pddl
E+ size : 100
E- size : 13233
e+ mean size : 51.68
e- mean size : 36.82491
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 238
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.033333335
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.017879948
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 226
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.116666675
Error Rate Precondition : 0.01981948
Error Rate Postcondition : 0.033910174
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 163
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 163
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12995
e+ mean size : 50.49
e- mean size : 35.05333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 184
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 184
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 167
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 202
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13148
e+ mean size : 51.15
e- mean size : 35.614616
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 231
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 554
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 121
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 158
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13577
e+ mean size : 52.17
e- mean size : 36.00648
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 178
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 260
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 160
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 160
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13918
e+ mean size : 53.62
e- mean size : 36.94561
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 376
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 495
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 147
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 147
Tested domain: experiment/NegElevator_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
