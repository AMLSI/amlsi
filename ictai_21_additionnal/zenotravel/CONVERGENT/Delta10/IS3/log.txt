# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial3.pddl
Delta=10
pddl/zenotravel/domain.pddl
E+ size : 100
E- size : 18040
e+ mean size : 50.34
e- mean size : 34.25909
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 519
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 37
Size I+ 37
Size I- 1535
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 440
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 21
Size I+ 21
Size I- 809
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17577
e+ mean size : 48.92
e- mean size : 33.027992
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 679
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 38
Size I+ 38
Size I- 1672
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 490
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 580
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17611
e+ mean size : 49.36
e- mean size : 34.260406
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 723
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 33
Size I+ 33
Size I- 1621
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 556
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 829
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 16153
e+ mean size : 43.94
e- mean size : 31.124063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 599
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 799
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 495
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 555
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17531
e+ mean size : 49.96
e- mean size : 34.22172
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 870
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 927
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 488
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 677
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
