# actions 14
# predicate 10
Initial state : pddl/zenotravel/initial_states/initial3.pddl
Delta=6
pddl/zenotravel/domain.pddl
E+ size : 100
E- size : 18040
e+ mean size : 50.34
e- mean size : 34.25909
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 342
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 33
Size I+ 33
Size I- 1412
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 260
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 668
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17577
e+ mean size : 48.92
e- mean size : 33.027992
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 483
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 788
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 302
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 388
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17611
e+ mean size : 49.36
e- mean size : 34.260406
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 524
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 29
Size I+ 29
Size I- 1448
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 335
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 695
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 16153
e+ mean size : 43.94
e- mean size : 31.124063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 473
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 569
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 280
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 407
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 17531
e+ mean size : 49.96
e- mean size : 34.22172
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 679
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 783
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 339
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 546
Tested domain: experiment/zenotravel_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
