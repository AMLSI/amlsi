# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial3.pddl
Delta=4
pddl/gripper/domain.pddl
E+ size : 100
E- size : 12625
e+ mean size : 49.27
e- mean size : 33.25798
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 202
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 302
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 183
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 209
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 11560
e+ mean size : 45.11
e- mean size : 30.669464
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 172
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 172
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 211
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 211
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12677
e+ mean size : 50.0
e- mean size : 33.4957
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 161
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 161
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 164
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 164
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 13000
e+ mean size : 51.98
e- mean size : 35.798153
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 201
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 201
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 146
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 146
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 12196
e+ mean size : 47.69
e- mean size : 32.163662
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 191
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 239
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 160
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 160
Tested domain: experiment/gripper_amlsi_convergent2/passive/Delta4/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
