# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial1.pddl
Delta=6
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 34080
e+ mean size : 45.65
e- mean size : 32.42864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 613
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 613
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 695
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 695
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38859
e+ mean size : 53.54
e- mean size : 34.17301
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 639
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 639
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 535
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 535
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33622
e+ mean size : 45.7
e- mean size : 32.101334
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 627
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 627
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 664
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 664
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 41478
e+ mean size : 57.38
e- mean size : 36.68651
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 681
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 681
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 748
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 748
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34968
e+ mean size : 48.06
e- mean size : 33.030483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 709
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 709
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 650
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 650
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
