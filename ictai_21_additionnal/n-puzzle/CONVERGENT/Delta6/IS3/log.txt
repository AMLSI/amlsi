# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial3.pddl
Delta=6
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 37252
e+ mean size : 49.84
e- mean size : 34.824387
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 654
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 654
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 625
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 625
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 37556
e+ mean size : 51.6
e- mean size : 34.7062
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 637
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 637
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 669
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 669
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 37363
e+ mean size : 50.91
e- mean size : 34.296764
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 620
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 620
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 622
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 622
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38355
e+ mean size : 52.48
e- mean size : 35.88656
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 841
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 841
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 773
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 773
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 36211
e+ mean size : 48.92
e- mean size : 34.156445
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 754
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 754
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 849
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 849
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
