# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial2.pddl
Delta=6
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 39394
e+ mean size : 53.79
e- mean size : 35.195488
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 694
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 694
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 680
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 680
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 36245
e+ mean size : 49.32
e- mean size : 33.418705
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 715
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 715
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 667
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 667
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34172
e+ mean size : 46.09
e- mean size : 32.290882
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 716
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 716
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 814
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 814
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 36102
e+ mean size : 49.0
e- mean size : 33.54753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 686
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 686
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 733
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 733
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33544
e+ mean size : 46.18
e- mean size : 32.959007
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 641
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 641
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 688
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 688
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
