# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial2.pddl
Delta=4
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 39394
e+ mean size : 53.79
e- mean size : 35.195488
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 507
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 507
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 474
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 474
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 36245
e+ mean size : 49.32
e- mean size : 33.418705
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 515
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 515
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 512
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 512
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34172
e+ mean size : 46.09
e- mean size : 32.290882
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 562
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 562
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 633
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 633
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 36102
e+ mean size : 49.0
e- mean size : 33.54753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 496
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 496
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 590
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 590
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33544
e+ mean size : 46.18
e- mean size : 32.959007
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 471
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 471
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 508
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 508
Tested domain: experiment/n-puzzle_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
