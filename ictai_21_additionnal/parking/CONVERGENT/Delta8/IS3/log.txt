# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial3.pddl
Delta=8
pddl/parking/domain.pddl
E+ size : 100
E- size : 64266
e+ mean size : 51.12
e- mean size : 32.577053
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 1561
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 37
Size I+ 37
Size I- 6651
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.067129634
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1348
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1631
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62039
e+ mean size : 48.87
e- mean size : 34.41809
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 3076
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 37
Size I+ 37
Size I- 6168
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1667
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1905
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 59254
e+ mean size : 46.13
e- mean size : 31.88551
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 3021
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 3021
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.08796296
Error Rate Precondition : 0.056857426
Error Rate Postcondition : 0.08937624
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1550
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 2340
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 65462
e+ mean size : 52.31
e- mean size : 35.93804
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 3271
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 33
Size I+ 33
Size I- 5542
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1577
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 2707
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 67512
e+ mean size : 53.57
e- mean size : 35.69034
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 2084
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 29
Size I+ 29
Size I- 5081
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.07175926
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1606
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1831
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
