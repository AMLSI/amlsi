# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial2.pddl
Delta=10
pddl/parking/domain.pddl
E+ size : 100
E- size : 64214
e+ mean size : 50.98
e- mean size : 33.534462
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.044
Iteration 11
Size I+ 11
Size I- 2071
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 31
Size I+ 31
Size I- 5164
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.009
Iteration 11
Size I+ 11
Size I- 2137
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 12
Size I+ 12
Size I- 2330
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63818
e+ mean size : 51.26
e- mean size : 34.81048
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.012
Iteration 20
Size I+ 20
Size I- 3290
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 38
Size I+ 38
Size I- 6350
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.009
Iteration 11
Size I+ 11
Size I- 1945
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.008
Iteration 12
Size I+ 12
Size I- 2125
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 64570
e+ mean size : 51.99
e- mean size : 35.06142
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.009
Iteration 19
Size I+ 19
Size I- 2951
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 30
Size I+ 30
Size I- 4933
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.01
Iteration 11
Size I+ 11
Size I- 1959
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 13
Size I+ 13
Size I- 2257
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63620
e+ mean size : 49.61
e- mean size : 33.275887
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.021
Iteration 22
Size I+ 22
Size I- 3907
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.021
Iteration 22
Size I+ 22
Size I- 3907
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.02
Iteration 11
Size I+ 11
Size I- 1848
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.021
Iteration 14
Size I+ 14
Size I- 2300
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62702
e+ mean size : 48.98
e- mean size : 34.47332
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.018
Iteration 24
Size I+ 24
Size I- 4389
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.017
Iteration 30
Size I+ 30
Size I- 5468
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.018
Iteration 11
Size I+ 11
Size I- 2148
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.018
Iteration 11
Size I+ 11
Size I- 2148
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
