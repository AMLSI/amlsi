# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial3.pddl
Delta=10
pddl/parking/domain.pddl
E+ size : 100
E- size : 64266
e+ mean size : 51.12
e- mean size : 32.577053
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.042
Iteration 12
Size I+ 12
Size I- 1810
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 39
Size I+ 39
Size I- 6907
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.067129634
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.009
Iteration 11
Size I+ 11
Size I- 1631
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.009
Iteration 13
Size I+ 13
Size I- 1980
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62039
e+ mean size : 48.87
e- mean size : 34.41809
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.005
Iteration 45
Size I+ 45
Size I- 7456
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.005
Iteration 47
Size I+ 47
Size I- 7747
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.005
Iteration 11
Size I+ 11
Size I- 1905
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.005
Iteration 13
Size I+ 13
Size I- 2402
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 59254
e+ mean size : 46.13
e- mean size : 31.88551
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.008
Iteration 22
Size I+ 22
Size I- 3341
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.008
Iteration 26
Size I+ 26
Size I- 3995
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.008
Iteration 11
Size I+ 11
Size I- 2000
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.008
Iteration 15
Size I+ 15
Size I- 2582
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 65462
e+ mean size : 52.31
e- mean size : 35.93804
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.023
Iteration 20
Size I+ 20
Size I- 3603
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.022
Iteration 35
Size I+ 35
Size I- 5952
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.022
Iteration 11
Size I+ 11
Size I- 2026
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.022
Iteration 17
Size I+ 17
Size I- 3118
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 67512
e+ mean size : 53.57
e- mean size : 35.69034
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.017
Iteration 15
Size I+ 15
Size I- 2477
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.017
Iteration 31
Size I+ 31
Size I- 5473
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.07175926
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Time RPNI : 0.017
Iteration 11
Size I+ 11
Size I- 2037
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Time RPNI : 0.017
Iteration 12
Size I+ 12
Size I- 2129
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
