# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial1.pddl
Delta=10
pddl/parking/domain.pddl
E+ size : 100
E- size : 69720
e+ mean size : 52.15
e- mean size : 33.316868
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 2415
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 4467
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2179
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 2554
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 73782
e+ mean size : 54.99
e- mean size : 35.674988
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 17
Size I+ 17
Size I- 2425
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 50
Size I+ 50
Size I- 8183
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1911
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2027
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62917
e+ mean size : 46.68
e- mean size : 32.31421
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 33
Size I+ 33
Size I- 5440
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 29
Size I+ 29
Size I- 4926
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2131
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 3589
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 70826
e+ mean size : 52.64
e- mean size : 34.61439
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 3558
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 22
Size I+ 22
Size I- 4159
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2020
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2203
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63547
e+ mean size : 47.59
e- mean size : 32.401325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 2727
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 23
Size I+ 23
Size I- 4896
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2059
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2168
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta10/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
