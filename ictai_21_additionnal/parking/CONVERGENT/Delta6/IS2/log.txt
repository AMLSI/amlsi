# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial2.pddl
Delta=6
pddl/parking/domain.pddl
E+ size : 100
E- size : 64214
e+ mean size : 50.98
e- mean size : 33.534462
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1244
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 27
Size I+ 27
Size I- 4487
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1416
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 1597
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63818
e+ mean size : 51.26
e- mean size : 34.81048
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 2558
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 27
Size I+ 27
Size I- 4720
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.067129634
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1316
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 1458
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 64570
e+ mean size : 51.99
e- mean size : 35.06142
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 2306
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 4281
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1155
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 1502
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63620
e+ mean size : 49.61
e- mean size : 33.275887
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1483
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 2939
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1275
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1767
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62702
e+ mean size : 48.98
e- mean size : 34.47332
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 3623
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 4747
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1383
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 1383
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta6/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
