# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial1.pddl
Delta=2
pddl/parking/domain.pddl
E+ size : 100
E- size : 69720
e+ mean size : 52.15
e- mean size : 33.316868
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 639
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 3049
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 715
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 1110
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 73782
e+ mean size : 54.99
e- mean size : 35.674988
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1104
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.067129634
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1873
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 493
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 732
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 62917
e+ mean size : 46.68
e- mean size : 32.31421
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 1132
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2109
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 668
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 954
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 70826
e+ mean size : 52.64
e- mean size : 34.61439
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 975
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1965
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 631
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 782
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 63547
e+ mean size : 47.59
e- mean size : 32.401325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 735
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 3218
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 596
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 837
Tested domain: experiment/parking_amlsi_convergent2/passive/Delta2/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
