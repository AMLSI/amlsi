# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial3.pddl
Delta=8
pddl/hanoi/domain.pddl
E+ size : 100
E- size : 33107
e+ mean size : 49.13
e- mean size : 32.896305
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1026
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 1259
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 998
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 1389
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 37215
e+ mean size : 55.09
e- mean size : 35.114094
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 1128
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 42
Size I+ 42
Size I- 3294
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 684
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 684
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34186
e+ mean size : 50.21
e- mean size : 33.80232
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 901
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 1377
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 817
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 958
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33142
e+ mean size : 48.15
e- mean size : 33.94388
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1001
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 1457
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1097
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 1470
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33944
e+ mean size : 49.84
e- mean size : 32.84831
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1346
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 37
Size I+ 37
Size I- 3077
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 934
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 934
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta8/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
