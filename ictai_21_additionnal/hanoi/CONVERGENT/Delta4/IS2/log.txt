# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial2.pddl
Delta=4
pddl/hanoi/domain.pddl
E+ size : 100
E- size : 32200
e+ mean size : 48.67
e- mean size : 31.983448
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 486
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 1252
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 520
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 520
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33397
e+ mean size : 48.46
e- mean size : 32.682068
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 544
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 1883
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 500
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 500
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34497
e+ mean size : 49.49
e- mean size : 34.050526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 431
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 728
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 543
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 543
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38440
e+ mean size : 55.47
e- mean size : 35.973362
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 605
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 605
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 549
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 775
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 32687
e+ mean size : 48.33
e- mean size : 32.898155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 695
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 958
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 541
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 716
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta4/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
