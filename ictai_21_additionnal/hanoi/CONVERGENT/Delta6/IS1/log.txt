# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial1.pddl
Delta=6
pddl/hanoi/domain.pddl
E+ size : 100
E- size : 34079
e+ mean size : 49.56
e- mean size : 33.640396
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 811
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 1189
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 458
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 458
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 35986
e+ mean size : 51.82
e- mean size : 33.58595
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 500
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 1326
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 582
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 582
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 37976
e+ mean size : 53.86
e- mean size : 34.943832
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 909
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 21
Size I+ 21
Size I- 1718
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 507
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 561
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 37695
e+ mean size : 53.91
e- mean size : 35.721794
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 813
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1006
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 759
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1264
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 33159
e+ mean size : 47.68
e- mean size : 33.301456
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 670
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 1404
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 589
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 589
Tested domain: experiment/hanoi_amlsi_convergent2/passive/Delta6/IS1/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
