# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
Delta=2
pddl/sokoban/domain.pddl
E+ size : 100
E- size : 36679
e+ mean size : 49.78
e- mean size : 34.099567
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 438
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 543
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 326
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 437
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010018128
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38391
e+ mean size : 53.03
e- mean size : 35.329636
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 484
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.8
FSCORE : 0.88888896

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 619
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010678105
0.8
FSCORE : 0.88888896
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 245
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 582
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.8
FSCORE : 0.88888896
E+ size : 100
E- size : 37988
e+ mean size : 52.54
e- mean size : 34.1864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 487
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 7846
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.10555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0066691367
1.0
FSCORE : 0.6389776
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 380
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 495
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38594
e+ mean size : 53.41
e- mean size : 35.54537
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 322
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.85
FSCORE : 0.9189189

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 466
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.009890308
0.85
FSCORE : 0.9189189
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 327
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 327
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.009890308
0.85
FSCORE : 0.9189189
E+ size : 100
E- size : 39317
e+ mean size : 54.09
e- mean size : 35.744335
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 511
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.83
FSCORE : 0.90710384

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 758
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
0.83
FSCORE : 0.90710384
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 334
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 517
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta2/IS2/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
