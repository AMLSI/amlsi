# actions 28
# predicate 44
Initial state : pddl/sokoban/initial_states/initial3.pddl
Delta=10
pddl/sokoban/domain.pddl
E+ size : 100
E- size : 37116
e+ mean size : 48.26
e- mean size : 33.27231
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1549
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 8593
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.0.pddl
Syntactical distance : 0.10555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010596546
1.0
FSCORE : 0.6289308
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1194
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 1466
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.0.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 35399
e+ mean size : 45.92
e- mean size : 33.380943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1082
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.1.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 7945
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.1.pddl
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 0.33557045
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1158
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.1.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1210
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.1.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 34470
e+ mean size : 44.86
e- mean size : 32.198753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 1633
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 8013
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.2.pddl
Syntactical distance : 0.094444446
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 0.31595576
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 1243
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 1428
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.2.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38332
e+ mean size : 49.28
e- mean size : 33.330795
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 17
Size I+ 17
Size I- 1831
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.3.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 84
Size I+ 84
Size I- 7040
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.3.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 985
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.3.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1032
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.3.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
E+ size : 100
E- size : 38080
e+ mean size : 49.4
e- mean size : 32.35478
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1401
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.0.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 60
Size I+ 60
Size I- 5089
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.25.20.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1209
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.0.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 1929
Tested domain: experiment/sokoban_amlsi_convergent2/passive/Delta10/IS3/amlsi_rpnir.100.20.4.pddl
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
1.0
FSCORE : 1.0
