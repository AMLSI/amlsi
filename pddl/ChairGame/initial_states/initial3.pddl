(define (problem P15E1S30)
  (:domain ChairGame)
(:objects
 P1  P2  P3  P4 P5 - person
 NO1  - empty )
(:init
(before P1 P2)
(before P2 P3)
(before P3 P4)
(before P4 NO1)
(before NO1 P5)
(before P5 P1)
)
(:goal ())
)
