begin_version
3
end_version
begin_metric
1
end_metric
33
begin_variable
var0
-1
2
Atom active-jump(pos-0-0, pos-0-1, pos-0-2)
NegatedAtom active-jump(pos-0-0, pos-0-1, pos-0-2)
end_variable
begin_variable
var1
-1
2
Atom active-jump(pos-0-0, pos-1-0, pos-2-0)
NegatedAtom active-jump(pos-0-0, pos-1-0, pos-2-0)
end_variable
begin_variable
var2
-1
2
Atom active-jump(pos-0-1, pos-1-1, pos-2-1)
NegatedAtom active-jump(pos-0-1, pos-1-1, pos-2-1)
end_variable
begin_variable
var3
-1
2
Atom active-jump(pos-0-2, pos-0-1, pos-0-0)
NegatedAtom active-jump(pos-0-2, pos-0-1, pos-0-0)
end_variable
begin_variable
var4
-1
2
Atom active-jump(pos-0-2, pos-1-2, pos-2-2)
NegatedAtom active-jump(pos-0-2, pos-1-2, pos-2-2)
end_variable
begin_variable
var5
-1
2
Atom active-jump(pos-1-0, pos-1-1, pos-1-2)
NegatedAtom active-jump(pos-1-0, pos-1-1, pos-1-2)
end_variable
begin_variable
var6
-1
2
Atom active-jump(pos-1-2, pos-1-1, pos-1-0)
NegatedAtom active-jump(pos-1-2, pos-1-1, pos-1-0)
end_variable
begin_variable
var7
-1
2
Atom active-jump(pos-2-0, pos-1-0, pos-0-0)
NegatedAtom active-jump(pos-2-0, pos-1-0, pos-0-0)
end_variable
begin_variable
var8
-1
2
Atom active-jump(pos-2-0, pos-2-1, pos-2-2)
NegatedAtom active-jump(pos-2-0, pos-2-1, pos-2-2)
end_variable
begin_variable
var9
-1
2
Atom active-jump(pos-2-1, pos-1-1, pos-0-1)
NegatedAtom active-jump(pos-2-1, pos-1-1, pos-0-1)
end_variable
begin_variable
var10
-1
2
Atom active-jump(pos-2-2, pos-1-2, pos-0-2)
NegatedAtom active-jump(pos-2-2, pos-1-2, pos-0-2)
end_variable
begin_variable
var11
-1
2
Atom active-jump(pos-2-2, pos-2-1, pos-2-0)
NegatedAtom active-jump(pos-2-2, pos-2-1, pos-2-0)
end_variable
begin_variable
var12
-1
2
Atom emptystack()
NegatedAtom emptystack()
end_variable
begin_variable
var13
-1
2
Atom free(pos-0-0)
NegatedAtom free(pos-0-0)
end_variable
begin_variable
var14
-1
2
Atom free(pos-0-1)
NegatedAtom free(pos-0-1)
end_variable
begin_variable
var15
-1
2
Atom free(pos-0-2)
NegatedAtom free(pos-0-2)
end_variable
begin_variable
var16
-1
2
Atom free(pos-1-0)
NegatedAtom free(pos-1-0)
end_variable
begin_variable
var17
-1
2
Atom free(pos-1-1)
NegatedAtom free(pos-1-1)
end_variable
begin_variable
var18
-1
2
Atom free(pos-1-2)
NegatedAtom free(pos-1-2)
end_variable
begin_variable
var19
-1
2
Atom free(pos-2-0)
NegatedAtom free(pos-2-0)
end_variable
begin_variable
var20
-1
2
Atom free(pos-2-1)
NegatedAtom free(pos-2-1)
end_variable
begin_variable
var21
-1
2
Atom free(pos-2-2)
NegatedAtom free(pos-2-2)
end_variable
begin_variable
var22
-1
2
Atom occupied(pos-0-0)
NegatedAtom occupied(pos-0-0)
end_variable
begin_variable
var23
-1
2
Atom occupied(pos-0-1)
NegatedAtom occupied(pos-0-1)
end_variable
begin_variable
var24
-1
2
Atom occupied(pos-0-2)
NegatedAtom occupied(pos-0-2)
end_variable
begin_variable
var25
-1
2
Atom occupied(pos-1-0)
NegatedAtom occupied(pos-1-0)
end_variable
begin_variable
var26
-1
2
Atom occupied(pos-1-1)
NegatedAtom occupied(pos-1-1)
end_variable
begin_variable
var27
-1
2
Atom occupied(pos-1-2)
NegatedAtom occupied(pos-1-2)
end_variable
begin_variable
var28
-1
2
Atom occupied(pos-2-0)
NegatedAtom occupied(pos-2-0)
end_variable
begin_variable
var29
-1
2
Atom occupied(pos-2-1)
NegatedAtom occupied(pos-2-1)
end_variable
begin_variable
var30
-1
2
Atom occupied(pos-2-2)
NegatedAtom occupied(pos-2-2)
end_variable
begin_variable
var31
-1
2
Atom stack1()
NegatedAtom stack1()
end_variable
begin_variable
var32
-1
2
Atom stack2()
NegatedAtom stack2()
end_variable
0
begin_state
1
1
1
1
1
1
1
1
1
1
1
1
0
0
1
1
0
1
1
0
1
0
1
0
0
1
0
0
1
0
1
1
1
end_state
begin_goal
9
12 0
13 0
14 0
15 0
16 0
17 0
18 0
19 0
20 0
end_goal
24
begin_operator
end-jump pos-0-0 pos-0-1 pos-0-2
0
8
0 0 0 1
1 31 0 12 -1 0
0 13 -1 0
0 14 -1 0
0 24 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-0-0 pos-1-0 pos-2-0
0
8
0 1 0 1
1 31 0 12 -1 0
0 13 -1 0
0 16 -1 0
0 28 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-0-1 pos-1-1 pos-2-1
0
8
0 2 0 1
1 31 0 12 -1 0
0 14 -1 0
0 17 -1 0
0 29 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-0-2 pos-0-1 pos-0-0
0
8
0 3 0 1
1 31 0 12 -1 0
0 14 -1 0
0 15 -1 0
0 22 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-0-2 pos-1-2 pos-2-2
0
8
0 4 0 1
1 31 0 12 -1 0
0 15 -1 0
0 18 -1 0
0 30 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-1-0 pos-1-1 pos-1-2
0
8
0 5 0 1
1 31 0 12 -1 0
0 16 -1 0
0 17 -1 0
0 27 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-1-2 pos-1-1 pos-1-0
0
8
0 6 0 1
1 31 0 12 -1 0
0 17 -1 0
0 18 -1 0
0 25 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-2-0 pos-1-0 pos-0-0
0
8
0 7 0 1
1 31 0 12 -1 0
0 16 -1 0
0 19 -1 0
0 22 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-2-0 pos-2-1 pos-2-2
0
8
0 8 0 1
1 31 0 12 -1 0
0 19 -1 0
0 20 -1 0
0 30 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-2-1 pos-1-1 pos-0-1
0
8
0 9 0 1
1 31 0 12 -1 0
0 17 -1 0
0 20 -1 0
0 23 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-2-2 pos-1-2 pos-0-2
0
8
0 10 0 1
1 31 0 12 -1 0
0 18 -1 0
0 21 -1 0
0 24 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
end-jump pos-2-2 pos-2-1 pos-2-0
0
8
0 11 0 1
1 31 0 12 -1 0
0 20 -1 0
0 21 -1 0
0 28 -1 0
1 32 0 31 -1 0
1 32 1 31 -1 1
0 32 -1 1
1
end_operator
begin_operator
start-jump pos-0-0 pos-0-1 pos-0-2
0
8
0 0 1 0
0 12 -1 1
0 15 0 1
0 22 0 1
0 23 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-0-0 pos-1-0 pos-2-0
0
8
0 1 1 0
0 12 -1 1
0 19 0 1
0 22 0 1
0 25 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-0-1 pos-1-1 pos-2-1
0
8
0 2 1 0
0 12 -1 1
0 20 0 1
0 23 0 1
0 26 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-0-2 pos-0-1 pos-0-0
0
8
0 3 1 0
0 12 -1 1
0 13 0 1
0 23 0 1
0 24 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-0-2 pos-1-2 pos-2-2
0
8
0 4 1 0
0 12 -1 1
0 21 0 1
0 24 0 1
0 27 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-1-0 pos-1-1 pos-1-2
0
8
0 5 1 0
0 12 -1 1
0 18 0 1
0 25 0 1
0 26 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-1-2 pos-1-1 pos-1-0
0
8
0 6 1 0
0 12 -1 1
0 16 0 1
0 26 0 1
0 27 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-2-0 pos-1-0 pos-0-0
0
8
0 7 1 0
0 12 -1 1
0 13 0 1
0 25 0 1
0 28 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-2-0 pos-2-1 pos-2-2
0
8
0 8 1 0
0 12 -1 1
0 21 0 1
0 28 0 1
0 29 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-2-1 pos-1-1 pos-0-1
0
8
0 9 1 0
0 12 -1 1
0 14 0 1
0 26 0 1
0 29 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-2-2 pos-1-2 pos-0-2
0
8
0 10 1 0
0 12 -1 1
0 15 0 1
0 27 0 1
0 30 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
begin_operator
start-jump pos-2-2 pos-2-1 pos-2-0
0
8
0 11 1 0
0 12 -1 1
0 19 0 1
0 29 0 1
0 30 0 1
1 12 0 31 -1 0
1 12 1 31 -1 1
1 31 0 32 1 0
10000
end_operator
0
