(define (problem init)
(:domain deliver)
(:objects 
	n0 n1 n2 n3 n4 - node 
)
(:init
 (at n0)

 (to_deliver n0)
 (to_deliver n1)
 (to_deliver n2)
 (to_deliver n3)
 (to_deliver n4)
 
 (connected n0 n1)
 (connected n1 n3)
 (connected n3 n2)
 (connected n3 n0)
 (connected n2 n4)
 (connected n4 n1)
)
(:goal ()
)
)
