(define (domain DELIVER)
	(:requirements :strips :typing)
	(:types node)
	(:predicates
		(at ?n - node)
        (to_deliver ?n - node)
		(delivered ?n - node)
		(connected ?n1 - node ?n2 - node)
	)

	(:action move_to
		 :parameters (?n1 - node ?n2 - node)
	     :precondition (and (at ?n1) (connected ?n1 ?n2))
	     :effect
	     	 (and
             (not (at ?n1))
             (at ?n2)
		     )
	)

(:action deliver
		 :parameters (?n1 - node)
	     :precondition (and (at ?n1) (to_deliver ?n1))
	     :effect
	     	 (and
             (not (to_deliver ?n1))
             (delivered ?n1)
		     )
	)
)