(define (problem DELIVER-1)
(:domain DELIVER)
(:objects n1 n2 n3 n4 n5 - node)
(:INIT
    (at n1)
    (connected n1 n2)
    (connected n2 n3)
    (connected n3 n4)
    (connected n4 n5)
    (to_deliver n5)
)
(:goal (AND (delivered n5) ))
)