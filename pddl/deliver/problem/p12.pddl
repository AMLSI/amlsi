(define (problem DELIVER-12)
(:domain DELIVER)
(:objects n1 n2 n3 n4 n5 n6 n7 n8 n9 n10 n11 n12 n13 n14 n15 n16 n17 - node)
(:INIT
    (at n1)
    (connected n1 n1)
    (connected n1 n2)
    (connected n1 n3)
    (connected n1 n4)
    (connected n1 n5)
    (connected n1 n6)
    (connected n1 n7)
    (connected n1 n8)
    (connected n1 n9)
    (connected n1 n10)
    (connected n1 n11)
    (connected n1 n12)
    (connected n1 n13)
    (connected n1 n14)
    (connected n1 n15)
    (connected n1 n16)
    (connected n1 n17)
    (connected n2 n1)
    (connected n2 n2)
    (connected n2 n3)
    (connected n2 n4)
    (connected n2 n5)
    (connected n2 n6)
    (connected n2 n7)
    (connected n2 n8)
    (connected n2 n9)
    (connected n2 n10)
    (connected n2 n11)
    (connected n2 n12)
    (connected n2 n13)
    (connected n2 n14)
    (connected n2 n15)
    (connected n2 n16)
    (connected n2 n17)
    (connected n3 n1)
    (connected n3 n2)
    (connected n3 n3)
    (connected n3 n4)
    (connected n3 n5)
    (connected n3 n6)
    (connected n3 n7)
    (connected n3 n8)
    (connected n3 n9)
    (connected n3 n10)
    (connected n3 n11)
    (connected n3 n12)
    (connected n3 n13)
    (connected n3 n14)
    (connected n3 n15)
    (connected n3 n16)
    (connected n3 n17)
    (connected n4 n1)
    (connected n4 n2)
    (connected n4 n3)
    (connected n4 n4)
    (connected n4 n5)
    (connected n4 n6)
    (connected n4 n7)
    (connected n4 n8)
    (connected n4 n9)
    (connected n4 n10)
    (connected n4 n11)
    (connected n4 n12)
    (connected n4 n13)
    (connected n4 n14)
    (connected n4 n15)
    (connected n4 n16)
    (connected n4 n17)
    (connected n5 n1)
    (connected n5 n2)
    (connected n5 n3)
    (connected n5 n4)
    (connected n5 n5)
    (connected n5 n6)
    (connected n5 n7)
    (connected n5 n8)
    (connected n5 n9)
    (connected n5 n10)
    (connected n5 n11)
    (connected n5 n12)
    (connected n5 n13)
    (connected n5 n14)
    (connected n5 n15)
    (connected n5 n16)
    (connected n5 n17)
    (connected n6 n1)
    (connected n6 n2)
    (connected n6 n3)
    (connected n6 n4)
    (connected n6 n5)
    (connected n6 n6)
    (connected n6 n7)
    (connected n6 n8)
    (connected n6 n9)
    (connected n6 n10)
    (connected n6 n11)
    (connected n6 n12)
    (connected n6 n13)
    (connected n6 n14)
    (connected n6 n15)
    (connected n6 n16)
    (connected n6 n17)
    (connected n7 n1)
    (connected n7 n2)
    (connected n7 n3)
    (connected n7 n4)
    (connected n7 n5)
    (connected n7 n6)
    (connected n7 n7)
    (connected n7 n8)
    (connected n7 n9)
    (connected n7 n10)
    (connected n7 n11)
    (connected n7 n12)
    (connected n7 n13)
    (connected n7 n14)
    (connected n7 n15)
    (connected n7 n16)
    (connected n7 n17)
    (connected n8 n1)
    (connected n8 n2)
    (connected n8 n3)
    (connected n8 n4)
    (connected n8 n5)
    (connected n8 n6)
    (connected n8 n7)
    (connected n8 n8)
    (connected n8 n9)
    (connected n8 n10)
    (connected n8 n11)
    (connected n8 n12)
    (connected n8 n13)
    (connected n8 n14)
    (connected n8 n15)
    (connected n8 n16)
    (connected n8 n17)
    (connected n9 n1)
    (connected n9 n2)
    (connected n9 n3)
    (connected n9 n4)
    (connected n9 n5)
    (connected n9 n6)
    (connected n9 n7)
    (connected n9 n8)
    (connected n9 n9)
    (connected n9 n10)
    (connected n9 n11)
    (connected n9 n12)
    (connected n9 n13)
    (connected n9 n14)
    (connected n9 n15)
    (connected n9 n16)
    (connected n9 n17)
    (connected n10 n1)
    (connected n10 n2)
    (connected n10 n3)
    (connected n10 n4)
    (connected n10 n5)
    (connected n10 n6)
    (connected n10 n7)
    (connected n10 n8)
    (connected n10 n9)
    (connected n10 n10)
    (connected n10 n11)
    (connected n10 n12)
    (connected n10 n13)
    (connected n10 n14)
    (connected n10 n15)
    (connected n10 n16)
    (connected n10 n17)
    (connected n11 n1)
    (connected n11 n2)
    (connected n11 n3)
    (connected n11 n4)
    (connected n11 n5)
    (connected n11 n6)
    (connected n11 n7)
    (connected n11 n8)
    (connected n11 n9)
    (connected n11 n10)
    (connected n11 n11)
    (connected n11 n12)
    (connected n11 n13)
    (connected n11 n14)
    (connected n11 n15)
    (connected n11 n16)
    (connected n11 n17)
    (connected n12 n1)
    (connected n12 n2)
    (connected n12 n3)
    (connected n12 n4)
    (connected n12 n5)
    (connected n12 n6)
    (connected n12 n7)
    (connected n12 n8)
    (connected n12 n9)
    (connected n12 n10)
    (connected n12 n11)
    (connected n12 n12)
    (connected n12 n13)
    (connected n12 n14)
    (connected n12 n15)
    (connected n12 n16)
    (connected n12 n17)
    (connected n13 n1)
    (connected n13 n2)
    (connected n13 n3)
    (connected n13 n4)
    (connected n13 n5)
    (connected n13 n6)
    (connected n13 n7)
    (connected n13 n8)
    (connected n13 n9)
    (connected n13 n10)
    (connected n13 n11)
    (connected n13 n12)
    (connected n13 n13)
    (connected n13 n14)
    (connected n13 n15)
    (connected n13 n16)
    (connected n13 n17)
    (connected n14 n1)
    (connected n14 n2)
    (connected n14 n3)
    (connected n14 n4)
    (connected n14 n5)
    (connected n14 n6)
    (connected n14 n7)
    (connected n14 n8)
    (connected n14 n9)
    (connected n14 n10)
    (connected n14 n11)
    (connected n14 n12)
    (connected n14 n13)
    (connected n14 n14)
    (connected n14 n15)
    (connected n14 n16)
    (connected n14 n17)
    (connected n15 n1)
    (connected n15 n2)
    (connected n15 n3)
    (connected n15 n4)
    (connected n15 n5)
    (connected n15 n6)
    (connected n15 n7)
    (connected n15 n8)
    (connected n15 n9)
    (connected n15 n10)
    (connected n15 n11)
    (connected n15 n12)
    (connected n15 n13)
    (connected n15 n14)
    (connected n15 n15)
    (connected n15 n16)
    (connected n15 n17)
    (connected n16 n1)
    (connected n16 n2)
    (connected n16 n3)
    (connected n16 n4)
    (connected n16 n5)
    (connected n16 n6)
    (connected n16 n7)
    (connected n16 n8)
    (connected n16 n9)
    (connected n16 n10)
    (connected n16 n11)
    (connected n16 n12)
    (connected n16 n13)
    (connected n16 n14)
    (connected n16 n15)
    (connected n16 n16)
    (connected n16 n17)
    (connected n17 n1)
    (connected n17 n2)
    (connected n17 n3)
    (connected n17 n4)
    (connected n17 n5)
    (connected n17 n6)
    (connected n17 n7)
    (connected n17 n8)
    (connected n17 n9)
    (connected n17 n10)
    (connected n17 n11)
    (connected n17 n12)
    (connected n17 n13)
    (connected n17 n14)
    (connected n17 n15)
    (connected n17 n16)
    (connected n17 n17)
    (to_deliver n1)
    (to_deliver n2)
    (to_deliver n3)
    (to_deliver n4)
    (to_deliver n5)
    (to_deliver n6)
    (to_deliver n7)
    (to_deliver n8)
    (to_deliver n9)
    (to_deliver n10)
    (to_deliver n11)
    (to_deliver n12)
    (to_deliver n13)
    (to_deliver n14)
    (to_deliver n15)
    (to_deliver n16)
    (to_deliver n17)
)
(:goal (AND
       (delivered n1)
       (delivered n2)
       (delivered n3)
       (delivered n4)
       (delivered n5)
       (delivered n6)
       (delivered n7)
       (delivered n8)
       (delivered n9)
       (delivered n10)
       (delivered n11)
       (delivered n12)
       (delivered n13)
       (delivered n14)
       (delivered n15)
       (delivered n16)
       (delivered n17)
       )
    )
)