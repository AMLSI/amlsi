(define (problem roverprob7692) (:domain Rover)
(:objects
	general - Lander
	colour - Mode
	rover  - Rover
	store - Store
	waypoint0 waypoint1 - Waypoint
	camera - Camera
	objective - Objective
	)
(:init
	(at rover waypoint0)
	
	(at_lander general waypoint0)
	(at_lander general waypoint1)

	(equipped_for_soil_analysis rover)
        (equipped_for_rock_analysis rover)
        (equipped_for_imaging rover)
        (empty store)

	(visible waypoint0 waypoint1)
	(visible waypoint1 waypoint0)
)

(:goal (and)
)
)
