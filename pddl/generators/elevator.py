import numpy as np

class Elevator:
    def __init__(self, nbFloor=2, nbPassager=1, file_="instance.pddl"):
        self.nbFloor=nbFloor
        self.nbPassager=nbPassager
        self.file_=file_

    def objects(self):
        self.objects = ""
        for i in range(self.nbFloor):
            self.objects += ("floor_%d - floor\n" % (i))
        for i in range(self.nbPassager):
            self.objects += ("passager_%d - passager\n" % (i))

    def above(self):
        self.above = ""
        for i in range(self.nbFloor-1):
            self.above += ("\t(above floor_%d floor%d)\n" % (i, (i+1)))

    def originAndDestin(self):
        self.origin = ""
        self.destin = ""
        for i in range(self.nbPassager):
            origin = np.random.randint(low=0, high=nbFloor)
            destin = origin
            while origin == destin:
                destin = np.random.randint(low=0, high=nbFloor)
            self.origin += ("\t(origin passager_%d floor_%d)\n" % (i, origin))
            self.destin += ("(destin passager_%d floor_%d)" % (i, destin))
            

    def generate(self):
        self.board = np.zeros((self.size, self.size))
        #print()
        
        for i in range(self.board.shape[0]):
            self.board[i] = 1
        self.board[0,0]=0
        self.board[self.size-1,self.size-1]=0
        print(self.board)
        self.objects()
        self.inline()
        self.free()
        self.occupied()

        file_content = "(define (problem is)\n(:domain miconic)\n(:objects\n"
        file_content += self.objects
        file_content += ")\n"
        file_content+="(:init\n(move-ended)"
        file_content += self.above
        file_content += self.origin
        file_content += self.destin
        file_content += ")\n"
        file_content += "(:goal ())\n)"
        f = open(self.file_, "w")
        f.write(file_content)
        f.close()

for i in range(1,6):
    e = Elevator(nbFloor=i+1, nbPassager=i, file_=("scale%d.pddl" % i))
    p.generate()
