class Generator:
    def __init__(self, file_name, domain_name, args, ID=""):
        self.file_name=file_name
        self.domain_name=domain_name
        self.args=args
        self.ID=ID

    def generate(self):
        file_content=("(define (problem initial_state_%s_%s)\n" % (self.domaine_name, self.ID))
        file_content=("(:domain %s)\n" % (self.domaine_name))
        file_content="(:objects\n"
        for line in args["objects"]:
            file_content=("\t%s"%(line))
        file_content=")\n(:init\n"
        for props in args["init"]:
            for line in props:
                file_content=("\t%s"%(line))
        
