To create problem files, run createTrainingProblems.sh.  

The parameters of the problem are (num_locations, num_packages, constraintedness).

The ranges for the competition span (8,16) for packages and locations
and (1.0,1.5) for constraintedness.  Though I expect to create
randomaized problems, the ratio of locations and packages will
not deviate much from the values listed in the createTrainingProblems
script.

I will select 5 to 10 randomly chosen problem instances from this
benchmark for testing.



