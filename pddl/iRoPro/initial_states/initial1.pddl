(define (problem study-task3-swap)
(:domain iRoPro)
(:objects
	posA posB - position
	c - cube
	b - base
	r - roof)
(:init
	(clear c)
	(flat c)
	(thin c)
	(stackable c posA)
	(stackable c posB)
	(stackable c b)
	(not (stackable c r))
	
	(not(clear b))
	(flat b)
	(not(thin b))
	(stackable b posA)
	(stackable b posB)
	(not (stackable b r))
	(not (stackable b c))

	(clear r)
	(thin r)
	(not(flat r))
	(stackable r posA)
	(stackable r posB)
	(stackable r b)
	(stackable r c)

	(not (clear posB))
	(not (clear posA))
	(on c b)
	(on b posB)
	(on r posA)

)
(:goal (and(on r posB) (on c posA)) )
)
