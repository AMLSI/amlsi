#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 18:38:26 2020

@author: maxence
"""

from tabulate import tabulate
from math import sqrt

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Observability")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

def mean(l):
    if len(l) == 0:
        return 0

    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

#
def std(l):
    ll = []
    N=0
    for x in l:
        ll.append(float(x))
        N+=1
    mean_ = mean(l)
    sum_=0
    for x in ll:
        sum_+=((x - mean_)*(x - mean_))
    return float(sqrt((1/N)*sum_))

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def read_log_domain(domain, directory):
    print("Benchmar Info")
    reader = Reader(domain, method="AMLSI", directory=directory)
    reader.print_benchmark_info()
    print("\n\nAlgorithm : AMLSI")
    reader = Reader(domain, method="AMLSI", directory=directory)
    reader.print_domain_results()

class Reader:
    def __init__(self, domain, method="AMLSI",directory="./"):

        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.metrics_domain = ["fscore",
                               "distance",
                               "solve",
                               "accuracy",
                               "time"]
        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression"]

        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]

        self.data_domain = {}
        self.data_domain_is1 = {}
        self.data_domain_is2 = {}
        self.data_domain_is3 = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            self.data_domain_is1[metric] = {}
            self.data_domain_is2[metric] = {}
            self.data_domain_is3[metric] = {}
            for f in {100,25}:
                self.data_domain[metric][f] = {}
                self.data_domain_is1[metric][f] = {}
                self.data_domain_is2[metric][f] = {}
                self.data_domain_is3[metric][f] = {}
                for n in {0,20}:
                    self.data_domain[metric][f][n] = []
                    self.data_domain_is1[metric][f][n] = []
                    self.data_domain_is2[metric][f][n] = []
                    self.data_domain_is3[metric][f][n] = []

        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = []

        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []

    def read_log_domain(self):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s%s/%s/IS%d/log.txt" %
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:4] == "Time":
                        self.data_domain["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue
        file_ = ("%s%s/%s/planning_results.txt" %
                 (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise].append(float(tab[0])/n)
                else:
                    continue


    def read_log_automaton(self):
        for initial in [1,2,3]:
            file_ = ("%s%s/%s/IS%d/log.txt" %
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:16] == "#Observed states":
                    self.data_automaton["observations"].append(line[18:])
                elif line[:7] == "#States":
                    self.data_automaton["states"].append(line[9:])
                elif line[:12] == "#Transitions":
                    self.data_automaton["transitions"].append(line[14:])
                elif line[:17] == "Compression level":
                    self.data_automaton["compression"].append(line[19:])
                elif line[:6] == "Recall":
                    self.data_automaton["recall"].append(line[9:])
                elif line[:9] == "Precision":
                    self.data_automaton["precision"].append(line[12:])
                elif line[:16] == "Fscore automaton":
                    self.data_automaton["fscore"].append(line[17:])
                else:
                    continue
    def print_automaton_results(self):
        self.read_log_automaton()
        heads = ["fscore","R","P","#States","#Obs","#Transitions","Compression level"]
        res = []
        res_ = []
        for metrics in self.metrics_automaton:
            if(metrics == "states" or metrics == "observations" or metrics == "transitions" or metrics == "compression"):
                res_.append(mean(self.data_automaton[metrics]))
            else:
                res_.append(mean(self.data_automaton[metrics])*100)
        res.append(res_)
        print(tabulate(res, headers=heads))

    def print_domain_results(self):
        self.read_log_domain()
        heads = ["Fluents", "Noise","fscore","dist","solve","accuracy","time"]
        res = []
        for f in [100,25]:
            for noise in [0,20]:
                res_ = []
                res_.append(f)
                res_.append(noise)
                for metrics in self.metrics_domain:
                    if(metrics == "time"):
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]]))
                    else:
                        # print(metrics)
                        # print(self.data_domain[metrics])
                        res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]])*100)
                    #print(metrics)
                    #print(res_)
                res.append(res_)

        print(tabulate(res, headers=heads))

    def read_log_benchmark(self):
        for initial in [1,2,3]:
            file_ = ("%s%s/%s/IS%d/log.txt" %
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:7] == "I+ size":
                    self.data_benchmark["I+"].append(line[9:])
                elif line[:7] == "I- size":
                    self.data_benchmark["I-"].append(line[9:])
                elif line[:12] == "x+ mean size":
                    self.data_benchmark["i+"].append(line[14:])
                elif line[:12] == "x- mean size":
                    self.data_benchmark["i-"].append(line[14:])
                elif line[:7] == "E+ size":
                    self.data_benchmark["E+"].append(line[9:])
                elif line[:7] == "E- size":
                    self.data_benchmark["E-"].append(line[9:])
                elif line[:12] == "e+ mean size":
                    self.data_benchmark["e+"].append(line[14:])
                elif line[:12] == "e- mean size":
                    self.data_benchmark["e-"].append(line[14:])
                else:
                    continue

    def print_benchmark_info(self):
        self.read_log_benchmark()
        res = []
        res_ = []
        for info in self.benchmark_info:
            res_.append(mean(self.data_benchmark[info]))

        res.append(res_)
        print(tabulate(res, headers=self.benchmark_info))

    def read_log_domain_split_is(self):
        #Read log files
        for param in [[1, self.data_domain_is1],[2, self.data_domain_is2],[3, self.data_domain_is3]]:
            initial = param[0]
            data = param[1]
            file_ = ("%s%s/%s/IS%d/log.txt" %
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:4] == "Time":
                        data["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:6] == "FSCORE":
                        data["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        data["precondition"][fluent][noise].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        data["effect"][fluent][noise].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        data["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue

        file_ = ("%s%s/%s/planning_results.txt" %
                 (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        datas = {1:self.data_domain_is1,
                 2:self.data_domain_is2,
                 3:self.data_domain_is3}
        n = 0
        for line in raws:
            if line[:14] == "Initial states":
                data=datas[int(line[15:])]
                fluent=-1
            elif line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    data["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    data["accuracy"][fluent][noise].append(float(tab[0])/n)
                    continue


    def print_std_split(self):
        self.read_log_domain_split_is()
        heads = ["Fluents", "Noise","fscore","prec","eff","dist","solved","accuracy"]
        res_ = {}
        res=[]
        for p in [[1, self.data_domain_is1],[2, self.data_domain_is2],[3, self.data_domain_is3]]:
            initial = p[0]
            data = p[1]
            res_[initial]={}
            for f in [100, 25]:
                res_[initial][f] = {}
                for n in [0,20]:
                    res_[initial][f][n] = {}
                    for metrics in self.metrics_domain:
                        if(metrics == "time"):
                            continue
                        else:
                            res_[initial][f][n][metrics] = mean(data[metrics][f][n])*100

        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res2 = []
            res2.append(param[0])
            res2.append(param[1])
            tmp = []
            for metric in self.metrics_domain:
                if(metric == "time"):
                    continue
                else:
                    tmp = []
                    for initial in [1,2,3]:
                        tmp.append(res_[initial][param[0]][param[1]][metric])
                    res2.append(std(tmp))
            res.append(res2)
        print(tabulate(res, headers=heads))
