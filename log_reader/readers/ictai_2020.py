#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 23 18:38:26 2020

@author: maxence
"""

from tabulate import tabulate
from math import sqrt
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Observability")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

def mean(l):
    if len(l) == 0:
        return 0
    
    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

#
def std(l):
    ll = []
    N=0
    for x in l:
        ll.append(float(x))
        N+=1
    mean_ = mean(l)
    sum_=0    
    for x in ll:
        sum_+=((x - mean_)*(x - mean_))
    return float(sqrt((1/N)*sum_))

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws
def read_log_domain(domain, directory, repsave):
    print("Benchmar Info")
    reader = Reader(domain, method="WITH_PC_Gen", directory=directory)
    reader.print_benchmark_info()
    print("Automaton - RPNI")
    reader = Reader(domain, method="WITHOUT_PC_Gen", directory=directory)
    reader.print_automaton_results()
    print("Automaton - RPNIR")
    reader = Reader(domain, method="WITH_PC_Gen", directory=directory)
    reader.print_automaton_results()
    colors = {"WITHOUT_PC_Gen":"b", "WITHOUT_PC_Refine":"g", "WITH_PC_Gen":"r", "WITH_PC_Refine":"k"}
    markers = {"WITHOUT_PC_Gen":"o-", "WITHOUT_PC_Refine":"^-", "WITH_PC_Gen":"s-", "WITH_PC_Refine":"d-"}
    legend = {"WITHOUT_PC_Gen":"Base", "WITHOUT_PC_Refine":"Base + Refine", "WITH_PC_Gen":"Base + PC", "WITH_PC_Refine":"Base + PC + Refine"}
    fig = plt.figure(num=None, figsize=(20, 4), dpi=80, facecolor='w', edgecolor='k')
    gs = gridspec.GridSpec(1,3)
    distance = plt.subplot(gs[0, 0])
    fscoreD = plt.subplot(gs[0, 1])
    acc = plt.subplot(gs[0, 2])
    legends1 = {"fscore":[], "distance":[], "accuracy":[]}
    legends2 = {"fscore":[], "distance":[], "accuracy":[]}
    data={"distance":{}, "fscore":{}, "accuracy":{}}
    for grammar in ["WITHOUT_PC", "WITH_PC"]:
        for scenar in ["Gen", "Refine"]:
            m=("%s_%s") % (grammar, scenar)
            reader = Reader(domain, method=m, directory=directory)
            tmp=reader.to_plot()
            for met in ["distance", "fscore", "accuracy"]:
                data[met][m]=tmp[met]
    x=[10,20,30,40,50,60,70,80,90,100]
    for grammar in ["WITHOUT_PC", "WITH_PC"]:
        for scenar in ["Gen", "Refine"]:
            gen=("%s_%s") % (grammar, scenar)
            l1=plot(distance, x, data["distance"][gen], "Syntactical Distance",m=markers[gen], c=colors[gen])
            l2=plot(fscoreD, x, data["fscore"][gen], "FScore",m=markers[gen], c=colors[gen])
            l3=plot(acc, x, data["accuracy"][gen], "Accuracy",m=markers[gen], c=colors[gen])
            legends1["fscore"].append(l2)
            legends1["distance"].append(l1)
            legends1["accuracy"].append(l3)
            legends2["fscore"].append(legend[gen])
            legends2["distance"].append(legend[gen])
            legends2["accuracy"].append(legend[gen])
    fscoreD.legend(legends1["fscore"], legends2["fscore"])
    distance.legend(legends1["distance"], legends2["distance"])
    acc.legend(legends1["accuracy"], legends2["accuracy"])
    fig.savefig("%s%s_results.pdf" % (repsave, domain))
    
class Reader:
    def __init__(self, domain, method="",directory="./"):
        
        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:10, 10:20, 20:30, 30:40, 40:50, 50:60, 60:70, 70:80, 80:90, 90:100, 100:10}
        self.noise = {-1:0}
        self.metrics_domain = ["fscore",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "time"]
        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression"]
        
        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]
        
        self.data_domain = {}
        self.data_domain_is1 = {}
        self.data_domain_is2 = {}
        self.data_domain_is3 = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            self.data_domain_is1[metric] = {}
            self.data_domain_is2[metric] = {}
            self.data_domain_is3[metric] = {}
            for f in {10,20,30,40,50,60,70,80,90,100}:
                self.data_domain[metric][f] = {}
                self.data_domain_is1[metric][f] = {}
                self.data_domain_is2[metric][f] = {}
                self.data_domain_is3[metric][f] = {}
                for n in {0}:
                    self.data_domain[metric][f][n] = []
                    self.data_domain_is1[metric][f][n] = []
                    self.data_domain_is2[metric][f][n] = []
                    self.data_domain_is3[metric][f][n] = []
                    
        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = []
            
        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        
    def read_log_domain(self):
        #Read log files
        for initial in [1,2,3]: 
            file_ = ("%s%s/%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:4] == "Time":
                        self.data_domain["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        self.data_domain["precondition"][fluent][noise].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        self.data_domain["effect"][fluent][noise].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue
        file_ = ("%s%s/%s/planning_results.txt" % 
                 (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = 0
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise].append(float(tab[0])/n)
                else:
                    continue
                
            
    def read_log_automaton(self):
        for initial in [1,2,3]: 
            file_ = ("%s%s/%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:16] == "#Observed states":
                    self.data_automaton["observations"].append(line[18:])
                elif line[:7] == "#States":
                    self.data_automaton["states"].append(line[9:])
                elif line[:12] == "#Transitions":
                    self.data_automaton["transitions"].append(line[14:])
                elif line[:17] == "Compression level":
                    self.data_automaton["compression"].append(line[19:])
                elif line[:6] == "Recall":
                    self.data_automaton["recall"].append(line[9:])
                elif line[:9] == "Precision":
                    self.data_automaton["precision"].append(line[12:])
                elif line[:16] == "Fscore automaton":
                    self.data_automaton["fscore"].append(line[17:])
                else:
                    continue
    def print_automaton_results(self):
        self.read_log_automaton()
        heads = ["fscore","R","P","#States","#Obs","#Transitions","Compression level"]
        res = []
        res_ = []
        for metrics in self.metrics_automaton:
            if(metrics == "states" or metrics == "observations" or metrics == "transitions" or metrics == "compression"):
                res_.append(mean(self.data_automaton[metrics]))
            else:
                res_.append(mean(self.data_automaton[metrics])*100)
        res.append(res_)
        print(tabulate(res, headers=heads))
        
    def read_log_benchmark(self):
        for initial in [1,2,3]: 
            file_ = ("%s%s/%s/IS%d/log.txt" % 
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            for line in raws:
                #print(self.data_automaton)
                if line[:7] == "I+ size":
                    self.data_benchmark["I+"].append(line[9:])
                elif line[:7] == "I- size":
                    self.data_benchmark["I-"].append(line[9:])
                elif line[:12] == "x+ mean size":
                    self.data_benchmark["i+"].append(line[14:])
                elif line[:12] == "x- mean size":
                    self.data_benchmark["i-"].append(line[14:])
                elif line[:7] == "E+ size":
                    self.data_benchmark["E+"].append(line[9:])
                elif line[:7] == "E- size":
                    self.data_benchmark["E-"].append(line[9:])
                elif line[:12] == "e+ mean size":
                    self.data_benchmark["e+"].append(line[14:])
                elif line[:12] == "e- mean size":
                    self.data_benchmark["e-"].append(line[14:])
                else:
                    continue
                
    def print_benchmark_info(self):
        self.read_log_benchmark()
        res = []
        res_ = []
        for info in self.benchmark_info:
            res_.append(mean(self.data_benchmark[info]))
            
        res.append(res_)
        print(tabulate(res, headers=self.benchmark_info))
    
    def to_plot(self):
        self.read_log_domain()
        metrics=["distance", "fscore", "accuracy"]
        to_plot={}
        for m in metrics:
            to_plot[m] = []
            for fluent in [10,20,30,40,50,60,70,80,90,100]:
                to_plot[m].append(100*mean(self.data_domain[m][fluent][0]))
        return to_plot