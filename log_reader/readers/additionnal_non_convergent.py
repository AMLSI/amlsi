#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 19 10:06:42 2020

@author: maxence
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Grand Maxence
"""

from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
from math import sqrt
T=30

def mean(l):
    if len(l) == 0:
        return 0

    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

#
def std(l):
    ll = []
    N=0
    for x in l:
        ll.append(float(x))
        N+=1
    mean_ = mean(l)
    sum_=0
    for x in ll:
        sum_+=((x - mean_)*(x - mean_))
    return float(sqrt((1/N)*sum_))

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def read_results(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Overall results")
    reader.print_domain_results()
    print("\nStandard deviation between configurations")
    reader.print_conf_std()
    print("\nResults for each initial states")
    reader.print_domain_results_split()
    print("\nStandard deviation between initial states")
    reader.print_std_split()
    print("\nAutomaton results")
    reader.print_automaton_results()
    print("\nBenchmark info")
    reader.print_benchmark_info()

class Reader:
    def __init__(self, domain, method="amlsi",directory="./"):

        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.metrics_domain = ["fscore",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "time"]
        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression"]

        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]

        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25, 100}:
                self.data_domain[metric][f] = {}
                for n in {0, 20}:
                    self.data_domain[metric][f][n] = {}
                    for it in range(T):
                        self.data_domain[metric][f][n][it+1] = []

        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = {}
            for it in range(T):
                self.data_automaton[metric][it+1] = []

        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []

    def read_log_domain(self):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s%s/%s/IS%d/log.txt" %
                     (self.directory, self.domain, self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            it=1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                        it=0
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise][it].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        self.data_domain["precondition"][fluent][noise][it].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        self.data_domain["effect"][fluent][noise][it].append(line[27:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise][it].append(line[23:])
                        continue
                    elif line[:9] == "Iteration":
                        it+=1
                        continue
                    else:
                        continue

    def read_acc(self):
        file_ = ("%s%s/%s/planning_test_results.txt" %
                 (self.directory, self.domain, self.method))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise][30].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise][30].append(float(tab[0])/n)
                else:
                    continue



    def plot_domain(self):
        self.read_log_domain()
        metrics = ["fscore", "distance"]
        conf = np.array([[100,0],[100,20],[25,0],[25,20]])
        to_plot={}
        for c in conf:
            fluent = c[0]
            noise = c[1]
            if not fluent in to_plot:
                to_plot[fluent]={}
            to_plot[fluent][noise]={}
            for m in metrics:
                y = np.zeros(T)
                for it in range(1,T+1):
                    y[it-1] = mean(np.array([float(x)*100 for x in self.data_domain[m][fluent][noise][it]]))
                to_plot[fluent][noise][m]=y
        colors = {1:"b", 2:"g", 3:"r", 4:"k"}
        markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
        leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
        fig = plt.figure(num=None, figsize=(10, 5), dpi=80, facecolor='w', edgecolor='k')
        gs = gridspec.GridSpec(1,2)
        i=0
        # metric="fscore"
        for metric in metrics:
            plot_ = plt.subplot(gs[0, i])
            legends1 = {metric:[]}
            legends2 = {metric:[]}
            sc = 1
            for fluent in [100,25]:
                for noise in [0,20]:
                    x = np.array(range(1,T+1))
                    l1=plot(plot_, x, to_plot[fluent][noise][metric], \
                             metric,m=markers[sc], c=colors[sc])
                    legends1[metric].append(l1)
                    legends2[metric].append(leg[sc])
                    sc += 1
            plot_.legend(legends1[metric], legends2[metric])
            i=i+1
        #return to_plot

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Iteration")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg
