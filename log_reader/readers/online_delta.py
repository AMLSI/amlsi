#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
@author: Grand Maxence
"""

from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec

def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def mean(l):
    if len(l) == 0:
        return 0
    
    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Delta")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

def read_results(domain, method="amlsi",directory="./"):
    reader= Reader(domain, method, directory)
    print("Overall results")
    reader.print_domain_results()
    print("\nStandard deviation between configurations")
    reader.print_conf_std()
    print("\nResults for each initial states")
    reader.print_domain_results_split()
    print("\nStandard deviation between initial states")
    reader.print_std_split()
    print("\nAutomaton results")
    reader.print_automaton_results()
    print("\nBenchmark info")
    reader.print_benchmark_info()

class Reader:
    def __init__(self, domain, method="amlsi_convergent2",directory="./"):
        
        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20, 20:0}
        self.metrics_domain = ["fscore",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "iteration"]
        
        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]
        
        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        self.clear()
        
    def clear(self):
        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25, 100}:
                self.data_domain[metric][f] = {}
                for n in {0, 20}:
                    self.data_domain[metric][f][n] = []
    def read_log_domain(self, strategy="passive", Delta=5):
        #Read log files
        for initial in [1,2,3]: 
            file_ = ("%s%s_%s/%s/Delta%d/IS%d/log.txt" % \
                     (self.directory, self.domain, self.method,strategy,\
                      Delta,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:23] == "Error Rate Precondition":
                        self.data_domain["precondition"][fluent][noise].append(line[26:])
                        continue
                    elif line[:24] == "Error Rate Postcondition":
                        self.data_domain["effect"][fluent][noise].append(line[27:])
                        continue
                    elif line[:9] == "Iteration":
                        self.data_domain["iteration"][fluent][noise].append(line[9:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue
        file_ = ("%s%s_%s/%s/Delta%d/planning_test_results.txt" % 
                 (self.directory, self.domain, self.method,strategy,Delta))
        raws = extract_line(file_)
        fluent=-1
        noise=-1
        for line in raws:
            if line[:4] == "### ":
                fluent = self.fluent[fluent]
                noise = -1
            elif line[:4] == "*** ":
                noise = self.noise[noise]
            else:
                tab = line.split()
                if(tab[1] == "problems"):
                    n = float(tab[0])
                elif(tab[1] == "solved"):
                    self.data_domain["solve"][fluent][noise].append(float(tab[0])/n)
                elif(tab[1] == "correctly"):
                    self.data_domain["accuracy"][fluent][noise].append(float(tab[0])/n)
                else:
                    continue
    def plot_res(self,domain,repsave="./../plot/online/"):
        colors = {"passive":"b", "UCT2":"g", "exploration":"r", "UCT":"k"}
        markers = {"passive":"o-", "UCT2":"^-", "exploration":"s-", "UCT":"d-"}
        fscore={}
        syntax={}
        epoch={}
        acc={}
        for strat in ["passive", "UCT2"]:
            fscore[strat]={}
            syntax[strat]={}
            epoch[strat]={}
            acc[strat]={}
            fscore[strat][100]={}
            fscore[strat][100][0]=[]
            fscore[strat][100][20]=[]
            fscore[strat][25]={}
            fscore[strat][25][0]=[]
            fscore[strat][25][20]=[]
            epoch[strat][100]={}
            epoch[strat][100][0]=[]
            epoch[strat][100][20]=[]
            epoch[strat][25]={}
            epoch[strat][25][0]=[]
            epoch[strat][25][20]=[]
            acc[strat][100]={}
            acc[strat][100][0]=[]
            acc[strat][100][20]=[]
            acc[strat][25]={}
            acc[strat][25][0]=[]
            acc[strat][25][20]=[]
            syntax[strat][100]={}
            syntax[strat][100][0]=[]
            syntax[strat][100][20]=[]
            syntax[strat][25]={}
            syntax[strat][25][0]=[]
            syntax[strat][25][20]=[]
            for Delta in (2,4,6,8,10):
                self.clear()
                self.read_log_domain(strategy=strat, Delta=Delta)
                for fluent in (100,25):
                    for noise in (0,20):
                        fscore[strat][fluent][noise].append(100*mean(self.data_domain["fscore"][fluent][noise]))
                        syntax[strat][fluent][noise].append(100*mean(self.data_domain["distance"][fluent][noise]))
                        acc[strat][fluent][noise].append(100*mean(self.data_domain["accuracy"][fluent][noise]))
                        epoch[strat][fluent][noise].append(mean(self.data_domain["iteration"][fluent][noise]))
        for fluent in [100,25]:
            for noise in [0,20]:    
                fig = plt.figure(num=None, figsize=(20, 5), dpi=80, facecolor='w', edgecolor='k')
                gs = gridspec.GridSpec(1,4)
                fscoreD = plt.subplot(gs[0, 0])
                distance = plt.subplot(gs[0, 1])
                iteration = plt.subplot(gs[0, 3])
                accuracy = plt.subplot(gs[0, 2])
                legends1 = {"fscore":[], "distance":[], "iteration":[], "accuracy":[]}
                legends2 = {"fscore":[], "distance":[], "iteration":[], "accuracy":[]}
                for strat in ["passive", "UCT2"]:
                    x = np.array((2,4,6,8, 10))
                    l1=plot(fscoreD, x, fscore[strat][fluent][noise], \
                     "FScore",m=markers[strat], c=colors[strat])
                    l2=plot(distance, x, syntax[strat][fluent][noise],\
                     "Syntactical Distance", m=markers[strat], c=colors[strat])
                    l3=plot(iteration, x, epoch[strat][fluent][noise],\
                     "Iterations", m=markers[strat], c=colors[strat])
                    l4=plot(accuracy, x, acc[strat][fluent][noise],\
                     "Accuracy", m=markers[strat], c=colors[strat])
                    legends1["fscore"].append(l1)
                    legends1["distance"].append(l2)
                    legends1["iteration"].append(l3)
                    legends1["accuracy"].append(l4)
                    legends2["fscore"].append(strat)
                    legends2["distance"].append(strat)
                    legends2["iteration"].append(strat)
                    legends2["accuracy"].append(strat)
                fscoreD.legend(legends1["fscore"], legends2["fscore"])
                distance.legend(legends1["distance"], legends2["distance"])
                accuracy.legend(legends1["accuracy"], legends2["accuracy"])
                iteration.legend(legends1["iteration"], legends2["iteration"])
                fig.suptitle("Fluent %d Noise %d" % (fluent, noise), fontsize=20)
                fig.savefig("%sconvergent_%s_%d_%d.pdf" % (repsave, domain, fluent, noise))
                
    def plot_res_passive(self,domain,repsave="./../plot/online/"):
        colors = {1:"b", 2:"g", 3:"r", 4:"k"}
        markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
        leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
        fig = plt.figure(num=None, figsize=(15, 5), dpi=80, facecolor='w', edgecolor='k')
        gs = gridspec.GridSpec(1,4)
        fscoreD = plt.subplot(gs[0, 0])
        distance = plt.subplot(gs[0, 1])
        iteration = plt.subplot(gs[0, 3])
        accuracy = plt.subplot(gs[0, 2])
        legends1 = {"fscore":[], "distance":[], "accuracy":[], "iteration":[]}
        legends2 = {"fscore":[], "distance":[], "accuracy":[], "iteration":[]}
        fscore={}
        syntax={}
        epoch={}
        acc={}
        for strat in ["passive"]:
            fscore[strat]={}
            syntax[strat]={}
            epoch[strat]={}
            acc[strat]={}
            fscore[strat][100]={}
            fscore[strat][100][0]=[]
            fscore[strat][100][20]=[]
            fscore[strat][25]={}
            fscore[strat][25][0]=[]
            fscore[strat][25][20]=[]
            epoch[strat][100]={}
            epoch[strat][100][0]=[]
            epoch[strat][100][20]=[]
            epoch[strat][25]={}
            epoch[strat][25][0]=[]
            epoch[strat][25][20]=[]
            acc[strat][100]={}
            acc[strat][100][0]=[]
            acc[strat][100][20]=[]
            acc[strat][25]={}
            acc[strat][25][0]=[]
            acc[strat][25][20]=[]
            syntax[strat][100]={}
            syntax[strat][100][0]=[]
            syntax[strat][100][20]=[]
            syntax[strat][25]={}
            syntax[strat][25][0]=[]
            syntax[strat][25][20]=[]
            for Delta in (2,4,6,8,10):
                self.clear()
                self.read_log_domain(strategy=strat, Delta=Delta)
                for fluent in (100,25):
                    for noise in (0,20):
                        fscore[strat][fluent][noise].append(100*mean(self.data_domain["fscore"][fluent][noise]))
                        syntax[strat][fluent][noise].append(100*mean(self.data_domain["distance"][fluent][noise]))
                        acc[strat][fluent][noise].append(100*mean(self.data_domain["accuracy"][fluent][noise]))
                        epoch[strat][fluent][noise].append(mean(self.data_domain["iteration"][fluent][noise]))
        sc=1
        for fluent in [100,25]:
            for noise in [0,20]:    
                for strat in ["passive"]:
                    x = np.array((2,4,6,8, 10))
                    l1=plot(fscoreD, x, fscore[strat][fluent][noise], \
                     "FScore",m=markers[sc], c=colors[sc])
                    l2=plot(distance, x, syntax[strat][fluent][noise],\
                     "Syntactical Distance", m=markers[sc], c=colors[sc])
                    l3=plot(iteration, x, epoch[strat][fluent][noise],\
                     "Iterations", m=markers[sc], c=colors[sc])
                    l4=plot(accuracy, x, acc[strat][fluent][noise],\
                     "Accuracy", m=markers[sc], c=colors[sc])
                    legends1["fscore"].append(l1)
                    legends1["distance"].append(l2)
                    legends1["iteration"].append(l3)
                    legends1["accuracy"].append(l4)
                    legends2["fscore"].append(leg[sc])
                    legends2["distance"].append(leg[sc])
                    legends2["iteration"].append(leg[sc])
                    legends2["accuracy"].append(leg[sc])
                fscoreD.legend(legends1["fscore"], legends2["fscore"])
                distance.legend(legends1["distance"], legends2["distance"])
                accuracy.legend(legends1["accuracy"], legends2["accuracy"])
                iteration.legend(legends1["iteration"], legends2["iteration"])
                sc += 1
        fig.suptitle("Fluent %d Noise %d" % (fluent, noise), fontsize=20)
        fig.savefig("%sconvergent_%s_passive.pdf" % (repsave, domain))

    def print_domain_results(self,strat="passive",Delta=2):
        self.clear()
        self.read_log_domain(strategy=strat, Delta=Delta)
        heads = ["Fluents", "Noise","fscore","prec","eff","dist","solved","accuracy", "iteration"]
        res = []
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in self.metrics_domain:
                if(metrics == "time"):
                    continue
                elif(metrics == "iteration"):
                    res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]]))
                else:
#                    print(metrics)
#                    print(self.data_domain[metrics][res_[0]][res_[1]])
#                    print(mean(self.data_domain[metrics][res_[0]][res_[1]])*100)
                    res_.append(mean(self.data_domain[metrics][res_[0]][res_[1]])*100)
            res.append(res_)
        print(tabulate(res, headers=heads))
        
    def save_passive(self, domain, metric="accuracy", rep="../../experiment/", method="amlsi_incr2",\
                 repsave="./../plot/online/"):
        colors = {1:"b", 2:"g", 3:"r", 4:"k"}
        markers = {1:"d-", 2:"o-", 3:"^-", 4:"s-"}
        leg = {1:"(100,0)", 2:"(100,20)", 3:"(25,0)", 4:"(25,20)"}
        fig = plt.figure(num=None, figsize=(5, 5), dpi=80, facecolor='w', edgecolor='k')
        gs = gridspec.GridSpec(1,1)
        graph = plt.subplot(gs[0, 0])
        legends1 = {metric:[]}
        legends2 = {metric:[]}
        score={}
        for strat in ["passive"]:
            score[strat]={}
            score[strat][100]={}
            score[strat][100][0]=[]
            score[strat][100][20]=[]
            score[strat][25]={}
            score[strat][25][0]=[]
            score[strat][25][20]=[]
            for Delta in (2,4,6,8,10):
                self.clear()
                self.read_log_domain(strategy=strat, Delta=Delta)
                for fluent in (100,25):
                    for noise in (0,20):
                        score[strat][fluent][noise].append(100*mean(self.data_domain[metric][fluent][noise]))
        sc=1
        for fluent in [100,25]:
            for noise in [0,20]:    
                for strat in ["passive"]:
                    x = np.array((2,4,6,8, 10))
                    l1=plot(graph, x, score[strat][fluent][noise], \
                     metric,m=markers[sc], c=colors[sc])
                    legends1[metric].append(l1)
                    legends2[metric].append(leg[sc])
                graph.legend(legends1[metric], legends2[metric])
                sc += 1
        fig.savefig("%sconvergent_%s_%s.pdf" % (repsave, domain, metric))
