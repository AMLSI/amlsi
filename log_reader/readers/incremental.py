from tabulate import tabulate
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.gridspec as gridspec
import os, sys, shutil

def read_results(domain, directory="./"):
    print("AMLSI")
    reader= Reader(domain, method="AMLSI", directory=directory)
    reader.print_domain_results()
    print("CONBERGENT")
    reader= Reader(domain, method="Delta/Delta10", directory=directory)
    reader.print_domain_results()

def test_plan(file_plan, problem, domain):
    if(os.path.getsize(file_plan) == 0):
        return [0,0]
    f = open(file_plan, 'r')
    for line in f:
        if(line[0:len(line)-1] == "NO SOLUTION" or line==""):
            return [0,0]
        else:
            break

    command = "../validate -v -t 0.0001 %s %s %s > /dev/null 2> /dev/null" % (domain, problem, file_plan)
    ret =os.system(command)
    # print(command)
    # print(ret)
    if(ret == 0):
        return [1,1]
    else:
        return [1,0]

def accuracy_domain(domain, rep_problem, rep_plans, n):
    s = 0
    a = 0
    instances = []
    file_problems = ("%s/problems.txt" % rep_problem)
    with open(file_problems) as problems:
        for problem in problems:
            problem = problem[:len(problem)-1]
            problem = ("../%s" % problem)
            instances.append(problem)
    for i in range(n):
        nn = i+1
        plan_file = "%s/plan_%d" % (rep_plans, nn)
        problem_file = instances[i]
        score_ = test_plan(plan_file, problem_file, domain)
        s += score_[0]
        a += score_[1]
    return [s/n*100, a/n*100]


def extract_line(file_):
    raws = []
    f = open(file_, "r")
    for line in f:
        if not line == "\n":
            raws.append(line[:-1])
    return raws

def mean(l):
    if len(l) == 0:
        return 0

    ll = []
    for x in l:
        ll.append(float(x))
    N=0
    sum_=0
    for x in ll:
        N+=1
        sum_+=x
    return float(sum_/N) if sum_ > 0 else 0

def plot(subp, x, y, title, m="x-",c="b"):
    leg, = subp.plot(x, y, m, c=c)
    subp.set_xlabel("Delta")
    subp.set_ylabel("Score(%)")
    subp.title.set_text(title)
    return leg

class Reader:
    def __init__(self, domain, method="amlsi_convergent2",directory="./"):

        self.domain = domain
        self.method = method
        self.directory = directory
        self.fluent = {-1:25, 100:25, 25:100}
        self.noise = {-1:0, 0:20,20:0}
        self.metrics_domain = ["fscore",
                               "fscore2",
                               "precondition",
                               "effect",
                               "distance",
                               "solve",
                               "accuracy",
                               "ipc",
                               "iteration",
                               "time"]

        self.metrics_automaton = ["fscore",
                                  "recall",
                                  "precision",
                                  "states",
                                  "observations",
                                  "transitions",
                                  "compression",
                                  "time"]

        self.benchmark_info = ["I+","I-","i+","i-",
                               "E+","E-","e+","e-"]

        self.data_benchmark = {}
        for info in self.benchmark_info:
            self.data_benchmark[info] = []
        self.clear()

    def clear(self):
        self.data_automaton = {}
        for metric in self.metrics_automaton:
            self.data_automaton[metric] = []
        self.data_domain = {}
        for metric in self.metrics_domain:
            self.data_domain[metric] = {}
            for f in {25,100}:
                self.data_domain[metric][f] = {}
                for n in {0, 20}:
                    self.data_domain[metric][f][n] = []

    def read_log_domain_planning(self, Delta=10):
        for initial in [1,2,3]:
            for run in [0,1,2,3,4]:
                for obs in [25,100]:
                    for noise in [0,20]:
                        file_ = ("%s/%s/%s/plans/IS%d/RUN%d/OBS%d/NOISE%d" %
                                 (self.directory, self.domain, self.method, initial, run, obs, noise))
                        domain_file = ("../pddl/%s/domain.pddl" % self.domain)
                        rep_instances = ("../pddl/%s/problem" % self.domain)
                        accSolve = accuracy_domain(domain_file, rep_instances, file_, 20)
                        self.data_domain["solve"][obs][noise].append(accSolve[0])
                        self.data_domain["accuracy"][obs][noise].append(accSolve[1])



    def read_log_domain(self, Delta=10):
        #Read log files
        for initial in [1,2,3]:
            file_ = ("%s/%s/%s/IS%d/log.txt" % \
                     (self.directory, self.domain,self.method,initial))
            raws = extract_line(file_)
            p = False
            fluent = -1
            noise= -1
            for line in raws:
                if line == "############################################":
                    p = True
                if p:
                    if line == "############################################":
                        noise = -1
                        continue
                    elif line[:4] == "### ":
                        fluent = self.fluent[fluent]
                        noise = -1
                    elif line[:4] == "*** ":
                        noise = self.noise[noise]
                    elif line[:6] == "FSCORE":
                        self.data_domain["fscore"][fluent][noise].append(line[9:])
                        continue
                    elif line[:4] == "Time":
                        self.data_domain["time"][fluent][noise].append(line[7:])
                        continue
                    elif line[:9] == "Iteration":
                        self.data_domain["iteration"][fluent][noise].append(line[9:])
                        continue
                    elif line[:20] == "Syntactical distance":
                        self.data_domain["distance"][fluent][noise].append(line[23:])
                        continue
                    else:
                        continue

    def print_domain_results(self):
        self.clear()
        self.read_log_domain()
        self.read_log_domain_planning()
        heads = ["Fluents", "Noise","fscore","dist","accuracy", "time", "Iterations"]
        res = []
        # print(self.data_domain["distance"][100][0])
        for param in [[100,0],[100,20],[25,0],[25,20]]:
            res_ = []
            res_.append(param[0])
            res_.append(param[1])
            for metrics in ["fscore","distance"]:
                x=mean(self.data_domain[metrics][res_[0]][res_[1]])*100
                # print(x)
                # print(round(x,1))
                res_.append(round(x,1))
            for metrics in ["accuracy", "time", "iteration"]:
                x=mean(self.data_domain[metrics][res_[0]][res_[1]])
                res_.append(round(x,1))
            res.append(res_)
        print(tabulate(res, headers=heads))

def plot(domain, rep="./", repsave="./"):
    for m in ["fscore","distance", "accuracy", "time", "iteration"]:
        print(m)
        plot_convergent(domain, metric=m, rep=rep, repsave=repsave)

def plot_convergent(domain, metric="accuracy", rep="./", repsave="./"):
    colors = {1:"b", 2:"g", 3:"r", 4:"k"}
    markers = {1:"d", 2:"o", 3:"^", 4:"s"}
    legBis = {(100,0):1, (100,20):2, (25,0):3, (25,20):4}
    leg = {1:"Scenario 1", 2:"Scenario 2", 3:"Scenario 3", 4:"Scenario 4"}
    fig = plt.figure(num=None, figsize=(5, 5), dpi=80, facecolor='w', edgecolor='k')
    ax = plt.subplot(111)
    score={}
    for fluent in {20,40,60,80,100}:
        score[fluent]={}
        for scenar in ("AMLSI", "POST"):
            score[fluent][scenar]=[]
    logers={}
    for delta in {2,4,6,8,10}:
        scenar = "Delta/Delta%d" % delta
        reader= Reader(domain, scenar, rep)
        reader.clear()
        reader.read_log_domain()
        if(metric == "accuracy"):
            reader.read_log_domain_planning()
        logers[delta]=reader
    for fluent in {100,25}:
        score[fluent]={}
        for noise in {0,20}:
            score[fluent][noise]=[]
            for scenar in {2,4,6,8,10}:
                if(metric == "time" or metric == "accuracy" or metric == "iteration"):
                    score[fluent][noise].append(mean(logers[scenar].data_domain[metric][fluent][noise]))
                else:
                    score[fluent][noise].append(100*mean(logers[scenar].data_domain[metric][fluent][noise]))
    for scenar in ((100,0), (100,20), (25,0), (25,20)):
        x = np.array((2,4,6,8,10))
        y = []
        i = 0
        for sc in score[scenar[0]][scenar[1]]:
            y.append(sc)
        ax.plot(x, y, marker=markers[legBis[scenar]], c=colors[legBis[scenar]], label=leg[legBis[scenar]])

    plt.xlabel("\u0394")
    plt.ylabel("%s " % metric)
    ax.legend(prop={'size': 12})
    fig.savefig("%s/convergent_%s_%s.pdf" % (repsave, domain, metric))
