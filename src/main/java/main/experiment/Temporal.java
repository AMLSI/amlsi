/**
 * 
 */
package main.experiment;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import exception.BlocException;
import fsm.Example;
import fsm.FiniteStateAutomata;
import fsm.Pair;
import fsm.Partition;
import fsm.Sample;
import fsm.Symbol;
import learning.AutomataLearning;
import learning.DomainLearning;
import learning.Generator;
import learning.Individual;
import learning.Mapping;
import learning.Observation;
import learning.ObservedExample;
import learning.temporal.TemporalDomainLearning;
import learning.temporal.TemporalGenerator;
import learning.temporal.TemporalIndividual;
import main.Argument;
import main.Properties;
import simulator.BlackBox;
import simulator.Oracle;
import simulator.temporal.TemporalBlackBox;
import simulator.temporal.TemporalOracle;
import learning.temporal.translator.Translator;

/**
 * @author Maxence Grand
 *
 */
public class Temporal {
	public static final int nbLearn = 1;
	/**
	 * The number of epochs for each tabu search
	 */
	public static final int epochs = 200;
	/**
	 * The number of generation
	 */
	public static final int T = 100;
	/**
	 * The minimal size of generated positive example
	 */
	public static final int minLearn = 10;
	/**
	 * The maximal size of generated positive example
	 */
	public static final int maxLearn = 20;
	/**
	 * The pseudo random number generator used by our example generator
	 */
	private static Random random ;

    /**
     *
     * Run
     */
	public static void run() throws BlocException, CloneNotSupportedException, IOException {
		long[] seeds = Properties.getSeeds();

		String directory = Argument.getDirectory();
		String reference = Argument.getDomain();
		String initialState = Argument.getProblem();
		String name = Argument.getName();
		int T = Argument.getT();
		int Delta = Argument.getDelta();
		boolean twoOp=Argument.isTwoOp();

		/*
		 * The blackbox
		 */
		TemporalOracle tempSim = new TemporalBlackBox(reference, initialState);
		/*
		 * All actions
		 */
		List<Symbol> tempActions = tempSim.getAllActions();
		/*
		 * All predicates
		 */
		List<Symbol> tempPred = new ArrayList<>();
		for(Symbol a : tempSim.getAllPredicates()){
			tempPred.add(a);
		}
		for(Symbol a : tempSim.getPositiveStaticPredicate()){
			tempPred.add(a);
		}

		/**
		 * For logs
		 */
		System.out.println("# actions "+tempSim.getAllActions().size());
		System.out.println("# predicate "+tempPred.size());

		System.out.println("Initial state : "+initialState);
		System.out.println("Delta="+Argument.getDelta());
		System.out.println(reference);

		for(int seed = 0; seed < Argument.getRun() ; seed++) {
			/*
			 * Pseudo random number generator for test set generation
			 */
			random = new Random(seeds[seed]);
			/*
			 * The blackbox
			 */
			File file_temporal = new File(reference);
			if(twoOp) {
				Translator.translate2Op(file_temporal, "classical_"+Argument.getName()+".pddl");
			} else {
				Translator.translate(file_temporal, "classical_"+Argument.getName()+".pddl");
			}
			Oracle sim = new BlackBox("classical_"+Argument.getName()+".pddl", initialState);
			TemporalGenerator generatorTest = new TemporalGenerator(new TemporalBlackBox(reference, initialState),random);
			for(int i = 0; i<100; i++) {
				generatorTest.generate(1, 100, 1000000);
			}
			/*
			 * All actions
			 */
			List<Symbol> actions = sim.getAllActions();
			/*
			 * All predicates
			 */
			List<Symbol> pred = new ArrayList<>();
			for(Symbol a : sim.getAllPredicates()){
				pred.add(a);
			}
			for(Symbol a : sim.getPositiveStaticPredicate()){
				pred.add(a);
			}

			Map<String, Float> durationString = Translator.getDurations(file_temporal);
			Map<Symbol, Float> duration = new HashMap<>();
			for(Symbol action : tempSim.getAllActions()) {
				if(!duration.containsKey(action.generalize())) {
					duration.put(action.generalize(), durationString.get(action.getName()));
				}
			}
			for(float thresh : Properties.getPartial()) {
				System.out.println("############################################"
						+ "\n### Fluent = "+(thresh)+"% ###");
				/*
				 * Set the level of observable fluents
				 */
				Generator.LEVEL = ( (float) thresh / 100);
				for(float noise : Properties.getNoise()){
					Generator.THRESH = ((float)noise / 100);
					System.out.println("\n*** Noise = "+(noise)+"% ***");

					String domainName = directory+"/amlsi_rpnir."
							+((int)thresh)+"."+((int)noise)+"."
							+seed;

					
					/*
					 * Example generator for learning samples
					 */
					Generator generator = new Generator(sim,random);
					TemporalGenerator generatorT = new TemporalGenerator(tempSim,random);
					
					Observation initial = generator.getInitialState();
					TemporalDomainLearning learningModule = new TemporalDomainLearning(
							pred,
							actions,
							directory,
							name,
							reference,
							initialState,
							generator);

					learningModule.setTypes(sim.typeHierarchy());

					AutomataLearning learner;
					learner = new AutomataLearning(
							pred,
							actions,
							generator,
							learningModule);
					long startTime = System.currentTimeMillis(), endTime=0;
					//First iteration
					//System.out.println("Iteration 1");
					Pair<Sample, Sample> samples = generatorT.generate(minLearn, maxLearn, 100);
					Sample pos = samples.getX();
					Sample neg = samples.getY();
					//System.out.println("Iteration 1");


					//Step 1: grammar induction
					//Step 1.1: Adding prefixes
					Sample posPrefixes = pos.clone();
					posPrefixes = learner.decompose(posPrefixes);
					//Step 1.2: pairewise constraints
					for(Example x : pos.getExamples()) {
						learner.removeRules(x);
					}
					for(Symbol a : tempActions) {
						Symbol aStart = a.clone();
						aStart.setName(aStart.getName()+"-start");
						Symbol aInv = a.clone();
						aInv.setName(aInv.getName()+"-inv");
						Symbol aEnd = a.clone();
						aEnd.setName(aEnd.getName()+"-end");
						learner.removeRules(aStart, aEnd);
						//learner.removeRules(aInv, aEnd);
					}
					//learner.printRules();
					//Step 1.3: Automaton learning
					Pair<FiniteStateAutomata, Partition> p =
							learner.RPNI(posPrefixes, neg);
					FiniteStateAutomata A = p.getX();
					A.writeDotFile("tmp.dot");
					/*for(Symbol p1 : pred) {
						for(Symbol p2 : pred) {
							System.out.println(p1+" = "+p2+" ? "+p1.equals(p2));
						}
					}*/
					//Step 2: PDDL Generation
					//Step 2.1: Mapping
					//System.out.println(data);
					Mapping mapAnte = Mapping.getMappingAnte(
							pos,
							p.getX(),
							actions,
							pred);
					Mapping mapPost = Mapping.getMappingPost(
							pos,
							p.getX(),
							actions,
							pred);
					//Step 2.2: Operator Generation
					Individual initialPDDL = learningModule.generatePDDLOperator(
							A, mapAnte, mapPost);
					//System.out.println("Fscore automaton "+learner.test(A));

					//Step 3: Refinement
					//Step 3.1: First prec/post refinement
					Individual refinedPDDL = learningModule.refineOperator(
							initialPDDL.clone(), A, mapAnte, mapPost, pos);
					//Step 3.2: Tabu refinement
					Individual currentPDDL =  refinedPDDL.clone();
					//currentPDDL = TemporalIndividual.convert(currentPDDL.clone(), duration, twoOp);
					float previous = learningModule.fitness(
							currentPDDL, pos, neg,
							generator.getCompressedNegativeExample(),
							initialState);
					float current = previous;
					boolean b = false;
					List<Individual> tabou = new ArrayList<>();
					List<Individual> tabouTemp = new ArrayList<>();
					learningModule.initFitness();
					//long startTimeTabu = System.currentTimeMillis(), endTimeTabu=0;
					do {
						if(b) {
							previous=current;
							currentPDDL = learningModule.refineOperator(
									currentPDDL, A, mapAnte, mapPost, pos);
						}
						//currentPDDL = TemporalIndividual.convert(currentPDDL.clone(), duration, twoOp);
						//System.out.println("Previous = "+previous);
						currentPDDL = learningModule.localSearch(
								currentPDDL,
								pos,
								neg,
								generator.getCompressedNegativeExample(),
								initialState,
								3,
								200,
								tabou);
						b = true;
						
						current = learningModule.fitness(
								currentPDDL, pos, neg,
								generator.getCompressedNegativeExample(),
								initialState);
					}while(current > previous);
					if(currentPDDL.acceptAll(initial, pos) &&
							currentPDDL.rejectAll(initial, neg)) {
						;
					}
					//currentPDDL = TemporalIndividual.convert(currentPDDL.clone(), duration, twoOp);
					//endTimeTabu = System.currentTimeMillis();
					//System.out.println(endTimeTabu-startTimeTabu);
					//System.exit(1);
					//System.out.println("+++++++++++++++++++++++++++++++++++++++++++0");
					//System.out.println(data.keySet().size());
					//System.out.println("+++++++++++++++++++++++++++++++++++++++++++1");
					//Iteration 2:T
					int delta = 0;
					for(int t = 2; t <= T; t++ ) {
						//System.out.println("Iteration "+t+" delta "+delta);
						/*Map<List<Symbol>,List<Observation>> previousData =
								new HashMap<>();
						data.forEach((k,v) -> {
							//
							if(!k.isEmpty()) {
								//System.out.println("youyouyou");
								List<Symbol> kBis = new ArrayList<>();
								List<Observation> vBis = new ArrayList<>();
								//System.out.println(k+" "+v);
								vBis.add(v.get(0).clone());
								for(int i = 0; i < k.size(); i++) {
									vBis.add(v.get(i+1).clone());
									kBis.add(k.get(i).clone());
								}
								previousData.put(kBis, vBis);
							}
						});*/
						//Generate new data
						//Update I+ nand I-
						Sample previousPos = pos.clone();
						Sample previousNeg = neg.clone();
						//generator.clearData();
						samples = generatorT.generate(minLearn, maxLearn, 100);
						//System.out.println(samples.getX());
						//Update R
						//Delete obsolete rules
						for(Example x : samples.getX().getExamples()) {
							learner.removeRules(x);
						}

						//Update A
						//Decompose new positive examples
						Sample tmp = new Sample();
						for(Example xPos : samples.getX().getExamples()) {
							previousPos.addExample(xPos);
							tmp.addExample(xPos);
							/*if(twoOp) {
								generator.updateMapping(xPos);
							} else {
								generator.updateMapping(xPos, Translator.getExtraDumpy(file_temporal));
							}*/
						}
						tmp = learner.decompose(tmp);
						p = learner.RPNI2(previousPos, previousNeg, tmp, samples.getY(), A,
								p.getY());
						A = p.getX();

						pos = previousPos;
						neg = previousNeg;
						A.writeDotFile("tmp.dot");
						//Rebuilt I+
						/*data = generator.getData();
						for(Map.Entry<List<Symbol>,List<Observation>> entry :
								previousData.entrySet()) {
							data.put(entry.getKey(), entry.getValue());
						}
						//generator.testNoiseAndPartiality(data);
						Map<List<Symbol>,List<Observation>> obsPost =
								new HashMap<>();
						Map<List<Symbol>,List<Observation>> obsAnte =
								new HashMap<>();
						Sample posRec = new Sample();
						data.forEach((k,v) -> {
							List<Symbol> ex = new ArrayList<>();
							List<Observation> obsA = new ArrayList<>();
							List<Observation> obsP = new ArrayList<>();
							for(int i = 0; i<k.size(); i++) {
								ex.add(k.get(i));
								obsA.add(v.get(i));
								obsP.add(v.get(i+1));
							}
							obsAnte.put(ex, obsA);
							obsPost.put(ex, obsP);
							posRec.addExample(ex);
						});*/
						
						//Update mapping
						mapAnte = Mapping.getMappingAnte(
								pos,
								p.getX(),
								actions,
								pred);
						mapPost = Mapping.getMappingPost(
								neg,
								p.getX(),
								actions,
								pred);

						//Backtrack
						Individual previousPDDL = currentPDDL.clone();

						Individual tmpPDDL = learningModule.generatePDDLOperator(A, mapAnte, mapPost);
						tmpPDDL = learningModule.refineOperator(tmpPDDL, A, mapAnte, mapPost, pos);

						currentPDDL.backtrackUncompatibleEffects(
								A, mapAnte, mapPost);
						currentPDDL.backtrackUncompatiblePreconditions(
								A, mapAnte, mapPost);
						currentPDDL.backtrackNegativeEffects();
						do {
							while(! currentPDDL.acceptAll(initial, pos)) {
								currentPDDL.backtrackPrecondition(initial, pos);
								currentPDDL.backtrackNegativeEffects();
							};
							currentPDDL.backtrackEffects(initial, pos, neg);
						}while(! currentPDDL.acceptAll(initial, pos));
						currentPDDL = currentPDDL.merge2(tmpPDDL);
						//currentPDDL = tmpPDDL;
						
						//Update D
						//currentPDDL = TemporalIndividual.convert(currentPDDL.clone(), duration, twoOp);
						previous = learningModule.fitness(
								currentPDDL, pos, neg, 
								generator.getCompressedNegativeExample(),
								initialState);
						current = previous;
						tabou = new ArrayList<>();
						tabouTemp = new ArrayList<>();
						learningModule.initFitness();
						do {
							previous=current;
							//System.out.println("Previous = "+previous);
							currentPDDL = learningModule.refineOperator(
									currentPDDL, A, mapAnte, mapPost, pos);
							//currentPDDL = TemporalIndividual.convert(currentPDDL.clone(), duration, twoOp);
							currentPDDL = learningModule.localSearch(
									currentPDDL,
									pos,
									neg,
									generator.getCompressedNegativeExample(),
									initialState,
									3,
									200,
									tabou);
							
							current = learningModule.fitness(
									currentPDDL, pos, neg, 
									generator.getCompressedNegativeExample(),
									initialState);
							
						}while(current > previous);
						if(currentPDDL.acceptAll(initial, pos) &&
								currentPDDL.rejectAll(initial, neg)) {
							
						}
						TemporalIndividual tempCurrent = new TemporalIndividual(currentPDDL, duration, twoOp, pred, tempActions);
						tempCurrent = new TemporalIndividual(currentPDDL, duration, twoOp, pred, tempActions);
						
						
						if(tempCurrent.rejectAll(generatorT.getNegative(),initial) &&
								tempCurrent.acceptAll(generatorT.getPositive(),initial)) {
							if(previousPDDL.equals(currentPDDL)) {
								delta++;
							}else {
								delta=1;
							}
						} else {
							delta=0;
						}

						if(t == T || delta >= Delta) {
							currentPDDL = TemporalIndividual.convert2(currentPDDL.clone(), duration, twoOp, pred, actions, tempActions);
							System.out.println("Iteration "+t);
							System.out.println("Size I+ "+pos.size());
							System.out.println("Size I- "+neg.size());
							endTime= System.currentTimeMillis();
							float time = (float)(endTime-startTime)/1000;
							System.out.println("Time : "+time);
							BufferedWriter bw = new BufferedWriter(
									new FileWriter(domainName+"_intermediate.pddl"));
							Pair<Map<Symbol, Observation>,Map<Symbol, Observation>> pair = currentPDDL.decode();
							Map<Symbol, Observation> preconditions = pair.getX();
							Map<Symbol, Observation> postconditions = pair.getY();
							bw.write(learningModule.generation(
									preconditions, postconditions));
							bw.close();
							File f = new File(domainName+"_intermediate.pddl");
							if(twoOp) {
								Translator.translate2Op(f, Translator.getDurations(file_temporal), domainName+".pddl");
							} else {
								Translator.translate(f, Translator.getDurations(file_temporal), domainName+".pddl");
							}
							Temporal.test(reference, domainName+".pddl");
							List<List<Symbol>> examples = new ArrayList<>();
							Observation initial2 = ((ObservedExample) pos.getExamples().get(0)).getInitialState();
							float fscore = tempCurrent.fscore(generatorTest.getPositive(), generatorTest.getNegative(), initial2);
							System.out.println("FSCORE : "+fscore);
							break;
						}
					}
				}
			}
		}
	}

	/**
	 * Test the learnt domain
	 * @param i The action model
	 * @param posTest The positive test sample
	 * @param negTest The negative test sample
	 * @param ref The reference domain
	 * @param s0 The initial state
	 * @param domainName The domain name
	 * @param learningModule The action model learner
	 * @param actions The action set
	 * @param generatorTest The generator for test samples
	 * @throws IOException
	 */
	private static void test (String ref, String domainName)
					throws IOException {
		try {
			TestMetrics.testTemporal(ref,domainName);
		} catch (BlocException e) {
			// TODO Auto-generated catch block
			System.exit(1);
			e.printStackTrace();
		}
	}
}
