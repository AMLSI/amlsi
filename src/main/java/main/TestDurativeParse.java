/**
 * 
 */
package main;

import java.io.File;
import java.io.IOException;

import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.parser.ErrorManager;
import fr.uga.pddl4j.parser.Exp;
import fr.uga.pddl4j.parser.Op;
import fr.uga.pddl4j.parser.Parser;
import fr.uga.pddl4j.planners.ProblemFactory;
import temporal.planner.CRIKEY;
import fr.uga.pddl4j.parser.Domain;

/**
 * @author Maxence Grand
 *
 */
public class TestDurativeParse {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		//CodedProblem cp;
		String rep ="/home/maxence/Documents/pddl-instances-master/ipc-2011/domains/peg-solitaire-temporal-satisficing/";
		String domain = rep+"domain.pddl";
		String problem = rep+"instances/instance.pddl";
		
		CRIKEY.search(domain, problem);
	}

}
