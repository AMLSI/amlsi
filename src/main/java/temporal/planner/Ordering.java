/**
 * 
 */
package temporal.planner;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fr.uga.pddl4j.problem.Action;
import fr.uga.pddl4j.problem.Problem;

/**
 * @author Maxence Grand
 *
 */
public class Ordering {
	/**
	 * 
	 */
	private String ai;
	/**
	 * 
	 */
	private String aj;
	/**
	 * 
	 */
	private boolean strict;
	
	/**
	 * Getter of strict
	 * @return the strict
	 */
	public boolean isStrict() {
		return strict;
	}
	/**
	 * Setter strict
	 * @param strict the strict to set
	 */
	public void setStrict(boolean strict) {
		this.strict = strict;
	}
	/**
	 * Getter of ai
	 * @return the ai
	 */
	public String getAi() {
		return ai;
	}
	/**
	 * Setter ai
	 * @param ai the ai to set
	 */
	public void setAi(String ai) {
		this.ai = ai;
	}
	/**
	 * Getter of aj
	 * @return the aj
	 */
	public String getAj() {
		return aj;
	}
	/**
	 * Setter aj
	 * @param aj the aj to set
	 */
	public void setAj(String aj) {
		this.aj = aj;
	}

	public String toString() {
		return ai+(strict ? " < " : " <= ")+aj;
	}

	public boolean equals(Object other) {
		if(other instanceof Ordering) {
			Ordering o = (Ordering) other;
			return o.ai.equals(this.ai) && o.aj.equals(this.aj) && o.strict==this.strict;
		}
		return false;
	}
	/**
	 * Constructs 
	 * @param ai
	 * @param aj
	 */
	public Ordering(Action ai, Action aj, Problem pb, boolean strict) {
		this.ai = Translator.getAction(pb, ai);
		this.aj = Translator.getAction(pb, aj);
		this.strict = strict;
		if(Translator.isInvariant(ai) && Translator.isInvariant(aj)) {
			this.ai = Translator.getDurativeActionName(ai)+"-start"+Translator.getParameters(ai, pb);
			this.aj = Translator.getDurativeActionName(aj)+"-end"+Translator.getParameters(aj, pb);
			this.strict = false;
		} else if(Translator.isInvariant(ai)) {
			this.ai = Translator.getDurativeActionName(ai)+"-start"+Translator.getParameters(ai, pb);
			this.strict = false;
		} else if(Translator.isInvariant(aj)) {
			this.aj = Translator.getDurativeActionName(aj)+"-end"+Translator.getParameters(aj, pb);
			this.strict = false;
		}
	}

	/**
	 * Constructs 
	 * @param ai
	 * @param aj
	 */
	public Ordering(Action aj, Problem pb, boolean strict) {
		this.ai = "initial";
		this.aj = Translator.getAction(pb, aj);
		this.strict = strict;
		if(Translator.isInvariant(aj)) {
			this.aj = Translator.getDurativeActionName(aj)+"-end"+Translator.getParameters(aj, pb);
			this.strict = false;
		}
	}

	/**
	 * 
	 * @param PO
	 * @param table
	 * @return
	 */
	public static List<Ordering> TransitiveClosure(List<Ordering> PO, Map<String, Integer> table) {
		//System.out.println(PO);
		int[][] adj = new int[table.keySet().size()][table.keySet().size()];
		for(int i = 1; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				adj[i][j]=0;
			}
		}
		//System.out.println(table.keySet().size());
		for(Ordering o : PO) {
			adj[table.get(o.ai)][table.get(o.aj)] = 1;
		}

		//Compute transitive closure
		for(int i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				if(adj[i][j]==1) {
					for(int k = 0; k<adj.length; k++) {
						if(adj[j][k]==1) {
							adj[i][k]=1;
						}
					}
				}
			}
		}

		//Compute transitive reduction
		for(int i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				if(adj[i][j]==1) {
					for(int k = 0; k<adj.length; k++) {
						if(adj[j][k]==1) {
							adj[i][k]=0;
							//k=0;
						}
					}
				}
			}
		}
		
		List<Ordering> newPO = new ArrayList<>();
		PO.forEach(o -> {
			if(adj[table.get(o.ai)][table.get(o.aj)] == 1) {
				newPO.add(o);
			}
		});
		PO=newPO;
		//newPO.forEach(o -> System.out.println(o));
		return PO;
	}
}
