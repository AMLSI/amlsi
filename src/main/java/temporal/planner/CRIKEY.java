/**
 * 
 */
package temporal.planner;

import java.io.File;
import java.io.FileNotFoundException;

import fr.uga.pddl4j.encoding.CodedProblem;
import fr.uga.pddl4j.heuristics.relaxation.Heuristic;
import fr.uga.pddl4j.parser.Op;
import fr.uga.pddl4j.parser.Parser;
import fr.uga.pddl4j.planners.Planner;
import fr.uga.pddl4j.planners.ProblemFactory;
import fr.uga.pddl4j.planners.statespace.StateSpacePlanner;
import fr.uga.pddl4j.planners.statespace.hsp.HSP;
import fr.uga.pddl4j.util.BitOp;
import fr.uga.pddl4j.util.Plan;
import fr.uga.pddl4j.util.SequentialPlan;

import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Maxence Grand
 *
 */
public class CRIKEY {
	/**
	 * 
	 */
	public static final float TOLERANCE_VALUE = 0.0001f;
	private static final int TIME_OUT = 1200;
	/**
	 * 
	 */
	public static final float MAX_VALUE = Float.MAX_VALUE;
	//public static final float MAX_VALUE = 5f;
	/**
	 * 
	 */
	private static Map<String, Map<String, Float>> minimalConstraint = new HashMap<>();
	/**
	 * 
	 */
	private static Map<String, Map<String, Float>> maximalConstraint  = new HashMap<>();
	private static Map<String, Float> durationOp = new HashMap<>();

	/**
	 * 
	 * @param classicalAction
	 * @param temporalDomain
	 */
	private static void initConstraint(
			List<BitOp> classicalAction,
			String temporalDomain,
			CodedProblem pb) {
		Parser parser = new Parser();
		try {
			parser.parseDomain(temporalDomain);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.exit(1);
		}

		//Get all durative operators
		List<Op> operators = parser.getDomain().getOperators();
		//Get all duration
		operators.forEach(op -> {
			durationOp.put(op.getName().toString(),
					op.getDuration().getChildren().get(1).getValue().floatValue());
		});
		for(int i = 0; i < classicalAction.size(); i++) {
			BitOp opi = classicalAction.get(i);
			CRIKEY.maximalConstraint.put(getAction(pb,opi), new HashMap<>());
			CRIKEY.minimalConstraint.put(getAction(pb,opi), new HashMap<>());
			for(int j = 0; j < classicalAction.size(); j++) {
				if(i == j) {
					continue;
				} else {
					BitOp opj = classicalAction.get(j);
					if(CRIKEY.isInvariant(opi)) {
						continue;
					}
					if(CRIKEY.isInvariant(opj)) {
						continue;
					}
					//start start
					if(isStart(opi) && isStart(opj)) {
						maximalConstraint.get(getAction(pb,opi)).put(
								getAction(pb,opj), MAX_VALUE);
						minimalConstraint.get(getAction(pb,opi)).put(getAction(
								pb,opj), CRIKEY.TOLERANCE_VALUE);
					}
					//end end
					if(isEnd(opi) && isEnd(opj)) {
						maximalConstraint.get(getAction(pb,opi)).put(getAction(
								pb,opj), MAX_VALUE);
						minimalConstraint.get(getAction(pb,opi)).put(
								getAction(pb,opj), CRIKEY.TOLERANCE_VALUE);
					}
					//end start
					if(isEnd(opi) && isStart(opj)) {
						maximalConstraint.get(getAction(pb,opi)).put(
								getAction(pb,opj), MAX_VALUE);
						minimalConstraint.get(getAction(pb,opi)).put(
								getAction(pb,opj), CRIKEY.TOLERANCE_VALUE);
					}
					//start end
					if(isStart(opi) && isEnd(opj)) {
						if(getParameters(opi,pb).equals(getParameters(opj,pb))) {
							float duration = durationOp.get(CRIKEY.getDurativeActionName(opi));
							maximalConstraint.get(getAction(pb,opi)).put(
									getAction(pb,opj), duration);
							minimalConstraint.get(getAction(pb,opi)).put(
									getAction(pb,opj), duration);
						} else {
							maximalConstraint.get(getAction(pb,opi)).put(
									getAction(pb,opj), MAX_VALUE);
							minimalConstraint.get(getAction(pb,opi)).put(
									getAction(pb,opj), CRIKEY.TOLERANCE_VALUE);
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param domain
	 * @param problem
	 */
	public static void search(String domain, String problem) {
		//Read PDDL
		File d = new File(domain);
		File p = new File(problem);

		//Translation
		d = Translator.translate(d, "/home/maxence/Documents/amlsi/tmpClassicalDomain.pddl");

		//Classical Planner
		final ProblemFactory factory = ProblemFactory.getInstance();
		try {
			factory.parse(d, p);
		} catch (Exception e) {
			System.out.println("Unexpected error when parsing the PDDL planning problem description.");
			System.exit(0);
		}
		final CodedProblem pb = factory.encode();
		final HSP planner = new HSP(
				TIME_OUT*100,
				Heuristic.Type.FAST_FORWARD,
				StateSpacePlanner.DEFAULT_WEIGHT,
				Planner.DEFAULT_STATISTICS,
				Planner.DEFAULT_TRACE_LEVEL
				);
		final Plan plan = planner.search(pb);
		if (plan != null) {
			System.out.println("Found plan as follows:"+pb.toString(plan));
			Plan tmp = new SequentialPlan();
			plan.actions().forEach(op -> {
				if(!isInvariant(op)) {
					tmp.add(tmp.size(), op);
				}
			});
			plan.clear();
			tmp.actions().forEach(op -> {
				plan.add(plan.size(), op);
			});
			System.out.println(pb.toString(plan));
		} else {
			System.out.println("No plan found.");
			System.exit(1);
		}

		//Get Map Operator -> positive preconditions
		Map<String,List<String>> preconditionsPos = new HashMap<>();
		//Get Map Operator -> negative preconditions
		Map<String,List<String>> preconditionsNeg = new HashMap<>();
		//Get Map Operator -> positive effects
		Map<String,List<String>> effectPos = new HashMap<>();
		//Get Map Operator -> negative effects
		Map<String,List<String>> effectNeg = new HashMap<>();
		//Get Map Operator -> sucessors
		Map<String,List<String>> sucessor = new HashMap<>();
		Map<String,Integer> idx = new HashMap<>();
		Map<Integer,String> reverseIdx = new HashMap<>();
		for(BitOp op : pb.getOperators()) {
			preconditionsPos.put(getAction(pb,op), new ArrayList<>());
			preconditionsNeg.put(getAction(pb,op), new ArrayList<>());
			effectPos.put(getAction(pb,op), new ArrayList<>());
			effectNeg.put(getAction(pb,op), new ArrayList<>());
			for(int i = 0; i<op.getPreconditions().getPositive().length(); i++) {

				//Get positive preconditions
				if(op.getPreconditions().getPositive().get(i)) {
					preconditionsPos.get(getAction(pb,op)).add(getProposition(pb,i));
				}
			}
			for(int i = 0; i<op.getPreconditions().getNegative().length(); i++) {
				//Get negative preconditions
				if(op.getPreconditions().getNegative().get(i)) {
					preconditionsNeg.get(getAction(pb,op)).add(getProposition(pb,i));
				}
			}
			for(int i = 0; i<op.getUnconditionalEffects().getPositive().length(); i++) {
				//Get positive effects
				if(op.getUnconditionalEffects().getPositive().get(i)) {
					effectPos.get(getAction(pb,op)).add(getProposition(pb,i));
				}
			}
			for(int i = 0; i<op.getUnconditionalEffects().getNegative().length(); i++) {
				//Get negative effects
				if(op.getUnconditionalEffects().getNegative().get(i)) {
					effectNeg.get(getAction(pb,op)).add(getProposition(pb,i));
				}
			}
		}
		//Goal
		List<String> goal = new ArrayList<>();
		for(int i = 0; i < pb.getGoal().getPositive().length();i++) {
			if(pb.getGoal().getPositive().get(i)) {
				goal.add(getProposition(pb,i));
			}
		}
		int i = 0;
		for(BitOp op : plan.actions()) {
			sucessor.put(getAction(pb,op), new ArrayList<>());
			idx.put(getAction(pb,op), i);
			reverseIdx.put(i, getAction(pb,op));
			i++;
		}

		int[][] adj = new int[idx.size()][idx.size()];
		for(i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				adj[i][j]=0;
			}
		}
		//adj[0][1]=1;
		//Get sucessor
		for(i = plan.actions().size()-1; i>=0; i--) {

			BitOp opi = plan.actions().get(i);
			//Preconditions
			for(String prec : preconditionsPos.get(getAction(pb,opi))) {
				for(int j = i-1; j >=0; j--) {
					BitOp opj = plan.actions().get(j);
					if(effectPos.get(getAction(pb,plan.actions().get(j))).contains(prec)) {
						/*if(! sucessor.get(getAction(pb,opj)).contains(getAction(pb,opi))) {
							sucessor.get(getAction(pb,opj)).add(getAction(pb,opi));

						}*/
						adj[idx.get(getAction(pb,opj))][idx.get(getAction(pb,opi))] = 1;
						break;
					}
				}
			}
			//Negative preconditions
			for(String prec : preconditionsNeg.get(getAction(pb,opi))) {
				for(int j = i-1; j >=0; j--) {
					BitOp opj = plan.actions().get(j);
					if(effectNeg.get(getAction(pb,plan.actions().get(j))).contains(prec)) {
						/*if(! sucessor.get(getAction(pb,opj)).contains(getAction(pb,opi))) {
							sucessor.get(getAction(pb,opj)).add(getAction(pb,opi));
						}*/
						adj[idx.get(getAction(pb,opj))][idx.get(getAction(pb,opi))] = 1;
						break;
					}
				}
			}
			//Negative effects
			for(String prec : effectNeg.get(getAction(pb,opi))) {
				for(int j = i-1; j >=0; j--) {
					BitOp opj = plan.actions().get(j);
					if(preconditionsPos.get(getAction(pb,plan.actions().get(j))).contains(prec)) {
						/*if(! sucessor.get(getAction(pb,opj)).contains(getAction(pb,opi))) {
							sucessor.get(getAction(pb,opj)).add(getAction(pb,opi));
						}*/
						adj[idx.get(getAction(pb,opj))][idx.get(getAction(pb,opi))] = 1;
					}
				}
			}
			//Positive effects
			for(String prec : effectPos.get(getAction(pb,opi))) {
				for(int j = i-1; j >=0; j--) {
					BitOp opj = plan.actions().get(j);
					if(preconditionsNeg.get(getAction(pb,plan.actions().get(j))).contains(prec)) {
						/*if(! sucessor.get(getAction(pb,opj)).contains(getAction(pb,opi))) {
							sucessor.get(getAction(pb,opj)).add(getAction(pb,opi));
						}*/
						adj[idx.get(getAction(pb,opj))][idx.get(getAction(pb,opi))] = 1;
					}
				}
			}
			//Primary add
			for(String prec : effectPos.get(getAction(pb,opi))) {
				if(goal.contains(prec)) {
					for(int j = i-1; j >=0; j--) {
						BitOp opj = plan.actions().get(j);
						if(effectNeg.get(getAction(pb,plan.actions().get(j))).contains(prec)) {
							/*if(! sucessor.get(getAction(pb,opj)).contains(getAction(pb,opi))) {
								sucessor.get(getAction(pb,opj)).add(getAction(pb,opi));
							}*/
							adj[idx.get(getAction(pb,opj))][idx.get(getAction(pb,opi))] = 1;
						}
					}
				}
			}

		}

		System.out.println("Index table");
		System.out.println("*************");
		for(int e: reverseIdx.keySet()) {
			System.out.println(e+" "+reverseIdx.get(e));
		}
		/*System.out.println("*************\n");
		System.out.println("Adjency matrix");
		System.out.println("*************");
		for(i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				System.out.print(adj[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("*************\n");*/

		//Compute transitive closure
		for(i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				if(adj[i][j]==1) {
					for(int k = 0; k<adj.length; k++) {
						if(adj[j][k]==1) {
							adj[i][k]=1;
						}
					}
				}
			}
		}

		//Compute transitive reduction
		for(i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				if(adj[i][j]==1) {
					for(int k = 0; k<adj.length; k++) {
						if(adj[j][k]==1) {
							adj[i][k]=0;
						}
					}
				}
			}
		}
		System.out.println("Adjency matrix - Transitively reduced matrix");
		System.out.println("*************");
		for(i = 0; i<adj.length; i++) {
			for(int j = 0; j<adj.length; j++) {
				System.out.print(adj[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("*************\n");

		//Compute constraint matrix
		CRIKEY.initConstraint(plan.actions(), domain, pb);
		float[][] constraintMatrix = new float[adj.length][adj.length];
		for(i = 0; i<adj.length; i++) {
			for(int j = i; j<adj.length; j++) {
				if(i==j) {
					constraintMatrix[i][j]=0;
				}/* else if(i==0 && j==1) {
					constraintMatrix[i][j]=0;
				}*/ else {		
					if(adj[i][j] == 1) {
						float[] constraint = constraint(
								reverseIdx.get(i),
								reverseIdx.get(j));
						constraintMatrix[i][j]=constraint[0];
						constraintMatrix[j][i]=constraint[1] == Float.MAX_VALUE ? Float.MAX_VALUE : -constraint[1];
					} else {
						constraintMatrix[i][j]=MAX_VALUE;
						constraintMatrix[j][i]=MAX_VALUE;
					}
				}
			}
		}

		/*System.out.println("Contraint Matrix");
		System.out.println("*************");
		for(i = 0; i<constraintMatrix.length; i++) {
			for(int j = 0; j<constraintMatrix.length; j++) {
				System.out.print(constraintMatrix[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("*********");*/
		//Floyd Warshall Algorithm
		for(int k = 0 ; k < constraintMatrix.length; k++) {
			for(i=0; i<constraintMatrix.length; i++) {
				for(int j = 0; j<constraintMatrix.length; j++) {
					constraintMatrix[i][j] = Math.min(
							constraintMatrix[i][j],
							constraintMatrix[i][k]+constraintMatrix[k][j]);
				}
			}
			/*System.out.println("Contraint Matrix k="+k);
			System.out.println("*************");
			for(i = 0; i<constraintMatrix.length; i++) {
				for(int j = 0; j<constraintMatrix.length; j++) {
					System.out.print(constraintMatrix[i][j]+" ");
				}
				System.out.println();
			}
			System.out.println("*********");*/
		}

		System.out.println("Contraint Matrix");
		System.out.println("*************");
		for(i = 0; i<constraintMatrix.length; i++) {
			for(int j = 0; j<constraintMatrix.length; j++) {
				System.out.print(constraintMatrix[i][j]+" ");
			}
			System.out.println();
		}
		System.out.println("*************\n");
		
		StringBuilder buildPlan = new StringBuilder();
		for(i=0; i<constraintMatrix.length; i++) {
			//System.out.println(Arrays.toString(reverseIdx.get(i).split("\\(")));
			String act = reverseIdx.get(i).split("\\(")[0];
			if(act.split("-")[1].equals("start")) {
				String[] tab = act.split("-");
				act = act.substring(0, act.length()-tab[tab.length-1].length()-1);
				buildPlan.append("[").append(constraintMatrix[0][i]).append("] ");
				buildPlan.append(act+"("+reverseIdx.get(i).split("\\(")[1]);
				buildPlan.append("[").append(durationOp.get(act)).append("] ");
				buildPlan.append("\n");
			}
		}
		System.out.println(buildPlan.toString());
	}

	/**
	 * 
	 * @param pb
	 * @param op
	 * @return
	 */
	private static String getAction(CodedProblem pb, BitOp op) {
		StringBuilder str = new StringBuilder();
		str.append(op.getName());
		str.append("(");
		for(int i = 0; i < op.getArity(); i++) {
			str.append(" "+pb.getConstants().get(op.getValueOfParameter(i)));
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * 
	 * @param pb
	 * @param idx
	 * @return
	 */
	private static String getProposition(CodedProblem pb, int idx) {
		StringBuilder str = new StringBuilder();
		int idxPred = pb.getRelevantFacts().get(idx).getPredicate();
		int[] idxParam = pb.getRelevantFacts().get(idx).getArguments();
		str.append("(").append(pb.getPredicates().get(idxPred));
		for(int param : idxParam) {
			str.append(" ").append(pb.getConstants().get(param));
		}
		str.append(")");
		return str.toString();
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	private static boolean isInvariant(BitOp op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("inv")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	private static boolean isStart(BitOp op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("start")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param op
	 * @return
	 */
	private static boolean isEnd(BitOp op) {
		String str = op.getName();
		String[] str2 = str.split("-");
		if(str2[str2.length-1].equals("end")) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	private static String getParameters(BitOp a, CodedProblem pb) {
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < a.getArity(); i++) {
			str.append(" "+pb.getConstants().get(a.getValueOfParameter(i)));
		}
		return str.toString();
	}

	/**
	 * 
	 * @param a
	 * @return
	 */
	private static String getDurativeActionName(BitOp a) {
		String res = "";
		String t[] = a.getName().split("-");
		if(t.length < 2) {
			return a.getName();
		}
		res=a.getName();
		res=res.substring(0, res.length() - (t[t.length-1].length()+1));
		return res;
	}

	/**
	 * 
	 * @param s1
	 * @param s2
	 * @return
	 */
	private static float[] constraint(String s1, String s2) {
		float[] constraint = new float[2];
		//System.out.println(s1+" "+s2);
		constraint[0] = minimalConstraint.get(s1).get(s2);
		constraint[1] = maximalConstraint.get(s1).get(s2);
		return constraint;
	}
}
