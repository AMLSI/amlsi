package learning;

import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import fsm.Symbol;

/**
 * This class represent an individual's chromosome
 * @see learning.Individual
 * @author Maxence Grand
 */
public class Chromosome {
    /**
     * The ordered set of genes
     */
    private List<Gene> genes;
    /**
     * Mapping between predicate and idex of each predicate in the set of genes
     */
    private Map<Symbol, Integer> predicateIdx;

    /**
     * Constructs the chromosome
     */
    public Chromosome() {
        this.genes = new ArrayList<>();
        this.predicateIdx = new HashMap<>();
    }

    /**
     * Constructs a chromosome corresponding to an observation
     * @param obs An observation
     */
    public Chromosome(Observation obs){
        this();
        int idx = 0;
        for(Symbol pred : obs.getPredicatesSymbols()) {
            this.genes.add(new Gene(obs.getValue(pred)));
            this.predicateIdx.put(pred, idx);
            idx++;
        }
    }

    /**
     * Constructs a chromosome from another chromosome
     * @param other A chromosome
     */
    public Chromosome(Chromosome other) {
        this(other.decode());
    }

    /**
     * Clone the chromosome
     * @return A chromosome
     */
    public Chromosome clone(){
        return new Chromosome(this);
    }

    /**
     * Decode a chromosome ie translate a chromosome into an observation
     * @return The corresponding observation
     */
    public Observation decode() {
        Observation obs = new Observation();
        for(Map.Entry<Symbol, Integer> entry : this.predicateIdx.entrySet()) {
            obs.addObservation
                (entry.getKey(), this.genes.get(entry.getValue()).getValue());
        }
        return obs;
    }

    /**
     * The hashcode
     * @return A hashcode
     */
    public int hashCode(){
        int hash = 1;
        for(Gene g : this.genes){
            hash*=g.hashCode();
        }
        return hash;
    }

    /**
     * Equality test
     * @param other A chromosome
     * @return True if the chromosomes are equals
     */
    public boolean equals(Object other){
        if(! (other instanceof Chromosome)) {
            return false;
        }

        Chromosome ch = (Chromosome) other;
        if(this.genes.size() != ch.genes.size()) {
            return false;
        }

        boolean b = true;
        for(Map.Entry<Symbol, Integer> entry : this.predicateIdx.entrySet()) {
            b &= (ch.predicateIdx.containsKey(entry.getKey()) &&
                  this.genes.get(entry.getValue()).equals
                  (ch.genes.get(ch.predicateIdx.get(entry.getKey())))
                  );
            if(!b) {
            	return b;
            }
        }
        return b;
    }

    /**
     * Mutate a given gene
     * @param idx The gene's index to mutate
     */
    public void mutation(int idx){
        if(idx < 0 || idx >= size()) {
            throw new IllegalArgumentException();
        }

        int idxGene = idx / 2;
        int idxBit = idx % 2;

        this.genes.get(idxGene).setBit(idxBit,! this.genes.get(idxGene).
                                       getBits()[idxBit]);
    }

    /**
     * Mutate a given gene with a given value
     * @param idxGene The gene's index to mutate
     * @param v The new value
     */
    public void mutation(int idxGene, Observation.Value v) {
        this.genes.get(idxGene).setValue(v);
    }

    /**
     * Set a gene with a given value
     * @param idx The gene's index to modify
     * @param value The new value
     */
    public void setGene(int idx, boolean value){
        if(idx < 0 || idx >= size()) {
            throw new IllegalArgumentException();
        }

        int idxGene = idx / 2;
        int idxBit = idx % 2;

        this.genes.get(idxGene).setBit(idxBit, value);
    }

    /**
     * Getter of a given gene
     * @param idx Gene's index
     * @return The gene
     */
    public boolean getGene(int idx){
        if(idx < 0 || idx >= size()) {
            throw new IllegalArgumentException();
        }

        int idxGene = idx / 2;
        int idxBit = idx % 2;

        return this.genes.get(idxGene).getBits()[idxBit];
    }

    /**
     * Getter of a given gene's value
     * @param idx Gene's index
     * @return Gene's index
     */
    public Observation.Value getGeneValue(int idx){
        if(idx < 0 || idx >= size()) {
            throw new IllegalArgumentException();
        }

        int idxGene = idx;

        return this.genes.get(idxGene).getValue();
    }

    /**
     * The size of the chromosome ie the number times the number of bit in each
     * genes
     *
     * @return Chromosome's size
     */
    public int size() {
        return this.genes.size() * Gene.NB_BITS;
    }

    /**
     * The number of genes
     * @return The number of genes
     */
    public int nbGenes() {
        return this.genes.size();
    }


    /**
     * Check if two chromosome check is consistant with the ARMS' aassumptions
     *
     * @param ch A chromosome
     * @return True if the two chromosme are consistant with the ARMS'
     * aassumptions
     */
    public boolean isConsistant(Chromosome ch) {
        for(int i = 0; i < genes.size(); i++) {
            switch(genes.get(i).getValue()) {
            case TRUE:
                switch(ch.genes.get(i).getValue()) {
                case TRUE:
                    return false;
                case FALSE:
                    break;
                case ANY:
                    break;
                default:
                    break;
                }
                break;
            case FALSE:
                switch(ch.genes.get(i).getValue()) {
                case TRUE:
                    break;
                case FALSE:
                    return false;
                case ANY:
                    break;
                default:
                    break;
                }
                break;
            default:
                switch(ch.genes.get(i).getValue()) {
                case TRUE:
                    break;
                case FALSE:
                    return false;
                case ANY:
                    break;
                default:
                    break;
                }
                break;
            }
        }
        return true;
    }

    /**
     * Get the predicate corresponding to a given gene
     * @param idx Gene's index
     * @return A predicate
     */
    public Symbol getPredicate(int idx) {
        for(Map.Entry<Symbol, Integer> entry: predicateIdx.entrySet()) {
            if(entry.getValue() == idx) {
                return entry.getKey();
            }
        }
        return null;
    }

    /**
     * String representation of a chromosome
     * @return A string
     */
    public String toString() {
        return genes+" "+predicateIdx;
    }

    /**
     * Get the index of a predicate p
     * @param p A predicate
     * @return p's index
     */
    public int getIndex(Symbol p) {
    	return this.predicateIdx.get(p);
    }

    /**
     * Remove all genes without some specified
     * @param toNotRemove Specified genes
     */
    public void removeAllExcept(List<Symbol> toNotRemove) {
    	this.predicateIdx.forEach((p,idx) -> {
    		if(! toNotRemove.contains(p) && this.getGeneValue(idx) != Observation.Value.ANY) {
    			this.mutation(idx, Observation.Value.ANY);
    		}
    	});
    }

    /**
     * All predicates
     * @return all predicates
     */
    public List<Symbol> allPredicates() {
    	return new ArrayList<>(this.predicateIdx.keySet());
    }

	/**
	 * Getter of predicateIdx
	 * @return the predicateIdx
	 */
	public Map<Symbol, Integer> getPredicateIdx() {
		return predicateIdx;
	}
}
