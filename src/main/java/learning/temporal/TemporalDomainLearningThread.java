/**
 * 
 */
package learning.temporal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import fsm.Symbol;
import learning.Individual;
import learning.Observation;
import simulator.temporal.TemporalExample;

/**
 * @author Maxence Grand
 *
 */
public class TemporalDomainLearningThread extends Thread{
	/**
	 * 
	 */
	private TemporalDomainLearning learner;
	/**
	 * 
	 */
	private List<Individual> indiv;
	/**
	 * 
	 */
	private Map<List<Symbol>,List<Observation>> data;
	/**
	 * 
	 */
	private List<TemporalExample> pos;
	/**
	 * 
	 */
	private List<TemporalExample> neg;
	/**
	 * 
	 */
	private List<CompressedTemporalNegativeExample> compressed;
	/**
	 * 
	 */
	private String is;
	/**
	 * 
	 */
	private Map<Symbol,Float> duration;
	/**
	 * 
	 */
	private boolean twoOp;
	/**
	 * 
	 */
	private BufferTemporal buffer;
	int nb;
	
	/**
	 * Constructs 
	 * @param learner
	 * @param indiv
	 * @param data
	 * @param pos
	 * @param neg
	 * @param compressed
	 * @param is
	 * @param duration
	 * @param twoOp
	 */
	public TemporalDomainLearningThread(TemporalDomainLearning learner, List<Individual> indiv,
			Map<List<Symbol>, List<Observation>> data, List<TemporalExample> pos, List<TemporalExample> neg,
			List<CompressedTemporalNegativeExample> compressed, String is, Map<Symbol, Float> duration, boolean twoOp,
			BufferTemporal buffer, int nb) {
		this.learner = learner;
		this.indiv = indiv;
		this.data = data;
		this.pos = pos;
		this.neg = neg;
		this.compressed = compressed;
		this.is = is;
		this.duration = duration;
		this.twoOp = twoOp;
		this.buffer = buffer;
		this.nb=nb;
	}


	/**
	 * 
	 * @return
	 */
	public float fitness(Individual i) {

		float fit=0f;

		try {
			//Individual indiv2 = TemporalIndividual.convert(i.clone(), duration, twoOp);
			/*TemporalIndividual tempIndiv = new TemporalIndividual(i, duration, twoOp);
			List<List<Symbol>> examples = new ArrayList<>();
			examples.addAll(data.keySet());
			Observation initial = new Observation(data.get(examples.get(0)).get(0));
			float fitPos = learner.fitPositive(tempIndiv, pos, initial);
			float fitNeg = learner.fitNegative(tempIndiv, neg, compressed, initial);

			/*float fitPrec = learner.fitPrecondition(i, data);
			float fitPost = learner.fitPostcondition(i, data);
			fit = (fitPos  + fitNeg /*+ fitPrec + fitPost);*/
			return fit;
		} catch(Exception ioe) {
			return Float.MIN_VALUE;
		}
	}

	/**
	 * 
	 */
	@Override
	public void run() {
		// TODO Auto-generated method stub
		//System.out.println(indiv);
		
		for(int i = 0; i<indiv.size(); i++) {
			//System.out.println("Test "+i+' '+nb);
			if(this.buffer.indivWating(indiv.get(i))) {
				//System.out.println("Begin Test "+i+' '+nb);
				float fit = this.fitness(indiv.get(i));
				this.learner.addFitness(indiv.get(i), fit);
				//System.out.println("End Test "+i);
			}
		}
		//System.out.println("Finish "+this.learner.hasIndividual(indiv)+" "+this.learner.sizeFitness()+" "+indiv.hashCode());
	}
}
