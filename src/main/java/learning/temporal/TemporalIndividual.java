/**
 * 
 */
package learning.temporal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fsm.Pair;
import fsm.Symbol;
import learning.Individual;
import learning.IndividualInstantiation;
import learning.Observation;
import simulator.temporal.TemporalExample;
/**
 * @author Maxence Grand
 *
 */
public class TemporalIndividual {
	/**
	 * 
	 */
	private Individual start;
	/**
	 * 
	 */
	private Individual invariant;
	/**
	 * 
	 */
	private Individual end;
	/**
	 * 
	 */
	private Map<Symbol, Float> durations;
	/**
	 * 
	 */
	private static Map<Individual, Individual> previousConversion = new HashMap<>();
	/**
	 * 
	 */
	private boolean twoOp;
	
	/**
	 * 
	 * Constructs 
	 * @param classical
	 * @param durations
	 */
	public TemporalIndividual(Individual classical, Map<Symbol, Float> durations, boolean twoOp, List<Symbol> P, List<Symbol> A) {
		this(durations);
		this.twoOp = twoOp;
		List<Symbol> operators = new ArrayList<>(this.durations.keySet());
		/*this.start.setP(P);
		this.invariant.setP(P);
		this.end.setP(P);
		this.start.setA(A);
		this.invariant.setA(A);
		this.end.setA(A);*/
		if(twoOp) {
			operators.forEach(operator -> {
				//System.out.println(operator+" "+classical.getDomain().keySet());
				Symbol s = operator.clone();
				s.setName(s.getName()+"-start");
				Symbol e = operator.clone();
				e.setName(e.getName()+"-end");
				Observation startClassicalPrecondition = classical.getPrecond(s);
				Observation startClassicalEffect = classical.getPostcond(s);
				Observation endClassicalPrecondition = classical.getPrecond(e);
				Observation endClassicalEffect = classical.getPostcond(e);
				List<Symbol> propositions = startClassicalPrecondition.getPredicatesSymbols();
				this.start.addOperator(operator, propositions);
				this.invariant.addOperator(operator, propositions);
				this.end.addOperator(operator, propositions);

				//Ad start effect
				startClassicalEffect.getPredicates().forEach((k,v) -> {
					this.start.getPostcond(operator).addObservation(k, v);
				});
				//Ad end effect
				endClassicalEffect.getPredicates().forEach((k,v) -> {
					this.end.getPostcond(operator).addObservation(k, v);
				});
				List<Symbol> inv = new ArrayList<>();
				//System.out.println(classical.getDomain());
				//System.out.println(operator+" "+s+" "+startClassicalPrecondition);
				startClassicalPrecondition.getPredicates().forEach((k,v) -> {
					switch(v) {
					case ANY:
						break;
					case MISSED:
						break;
					default:
						if(endClassicalPrecondition.getValue(k) == v) {
							inv.add(k);
							this.invariant.getPrecond(operator).addObservation(k, v);
						} else {
							this.start.getPrecond(operator).addObservation(k, v);
						}
						break;
					}
				});
				endClassicalPrecondition.getPredicates().forEach((k,v) -> {
					if(!inv.contains(k)) {
						/*if(v == Observation.Value.TRUE) {
							this.end.getPrecond(operator).addObservation(k, v);
						} else if(v == Observation.Value.FALSE && !startClassicalPrecondition.containsPredicate(k)) {
							this.end.getPrecond(operator).addObservation(k, v);
						} else if(v == Observation.Value.FALSE && startClassicalEffect.getValue(k)!=v) {
							this.end.getPrecond(operator).addObservation(k, v);
						} */
						this.end.getPrecond(operator).addObservation(k, v);
					}
				});
			});
		} else {
			operators.forEach(operator -> {
				Symbol s = operator.clone();
				s.setName(s.getName()+"-start");
				Symbol e = operator.clone();
				e.setName(e.getName()+"-end");
				Symbol i = operator.clone();
				i.setName(i.getName()+"-inv");
				Observation startClassicalPrecondition = classical.getPrecond(s);
				Observation startClassicalEffect = classical.getPostcond(s);
				Observation endClassicalPrecondition = classical.getPrecond(e);
				Observation endClassicalEffect = classical.getPostcond(e);
				Observation invClassicalPrecondition = classical.getPrecond(i);
				Observation invClassicalEffect = classical.getPostcond(i);
				List<Symbol> propositions = startClassicalPrecondition.getPredicatesSymbols();
				this.start.addOperator(operator, propositions);
				this.invariant.addOperator(operator, propositions);
				this.end.addOperator(operator, propositions);

				//Add start effect
				startClassicalEffect.getPredicates().forEach((k,v) -> {
					if(! isDumpy(operator,k)) {
						this.start.getPostcond(operator).addObservation(k, v);
					}
				});
				//Add end effect
				endClassicalEffect.getPredicates().forEach((k,v) -> {
					if(! isDumpy(operator,k)) {
						this.end.getPostcond(operator).addObservation(k, v);
					}
				});

				//Add start precondition
				startClassicalPrecondition.getPredicates().forEach((k,v) -> {
					if(! isDumpy(operator,k)) {
						this.start.getPrecond(operator).addObservation(k, v);
					}
				});
				//Add end precondition
				endClassicalPrecondition.getPredicates().forEach((k,v) -> {
					if(! isDumpy(operator,k)) {
						this.end.getPrecond(operator).addObservation(k, v);
					}
				});
				//Add end precondition
				invClassicalPrecondition.getPredicates().forEach((k,v) -> {
					if(! isDumpy(operator,k)) {
						this.invariant.getPrecond(operator).addObservation(k, v);
					}
				});
			});
		}


	}

	/**
	 * 
	 * Constructs 
	 * @param classical
	 * @param durations
	 */
	public TemporalIndividual(Map<Symbol, Float> durations) {
		this.durations = durations;
		this.start = new Individual();
		this.invariant = new Individual();
		this.end = new Individual();
	}

	/**
	 * 
	 * @param example
	 * @param is
	 * @return
	 */
	public boolean accept(TemporalExample example, Observation is,
			IndividualInstantiation startI, IndividualInstantiation invI,
			IndividualInstantiation endI) {
		Map<Float, Symbol> seq = example.getTemporalSequence();
		Map<Float, Symbol> nextEnd = new HashMap<>();
		List<Float> startTime = new ArrayList<>(seq.keySet());
		Collections.sort(startTime);
		Observation current = is.clone();
		for(float f : startTime) {
			//Apply effect of previous end
			List<Float> toRemove = new ArrayList<>();
			for(float f1 : beforeStart(nextEnd.keySet(), f)) {
				current = endI.apply(nextEnd.get(f1), current);
				toRemove.add(f1);
			}
			for(float f1 : toRemove) {
				nextEnd.remove(f1);
			}

			Symbol newAct = seq.get(f);
			float endNewAction = f+this.durations.get(newAct.generalize());
			List<Float> currentActions = beforeStart(nextEnd.keySet(),
					endNewAction);

			if(isApplicable(startI, invI, endI, newAct, current, nextEnd,
					endNewAction, currentActions)) {
				current = startI.apply(newAct, current);
				nextEnd.put(endNewAction, newAct);
			} else {
				return false;
			}

		}
		return true;
	}

	/**
	 * 
	 * @param startI
	 * @param invI
	 * @param endI
	 * @param newAct
	 * @param current
	 * @param nextEnd
	 * @param endNewAction
	 * @param currentActions
	 * @return
	 */
	public boolean isApplicable(IndividualInstantiation startI,
			IndividualInstantiation invI, IndividualInstantiation endI,
			Symbol newAct, Observation current, Map<Float, Symbol> nextEnd,
			float endNewAction, List<Float> currentActions) {
		//Check at start precondition for the new action
		//System.out.println();
		if(!current.contains(startI.getPrecond(newAct)) ||
				!current.contains(invI.getPrecond(newAct))) {
			//System.out.println(f+" start ");
			return false;
		}

		//Check invariant for current actions
		Observation clone = current.clone();
		clone = startI.apply(newAct, clone);
		for(float f1 : nextEnd.keySet()) {
			if(!clone.contains(invI.getPrecond(nextEnd.get(f1)))) {
				//System.out.println(f+" invariant current"+f1);
				return false;
			}
		}
		Observation clone2 = clone.clone();
		clone2 = endI.apply(newAct, clone2);
		for(float f1 : nextEnd.keySet()) {
			if(f1 > endNewAction) {
				if(!clone2.contains(invI.getPrecond(nextEnd.get(f1)))) {
					//System.out.println(f+" invariant next"+f1);
					return false;
				}
			}
		}
		//Check invariant for new action
		for(float f1 : currentActions) {
			clone = endI.apply(nextEnd.get(f1), clone);
			if(!clone.contains(invI.getPrecond(newAct))) {
				//System.out.println(f+" invariant new"+f1);
				return false;
			}
		}
		//Check at end new actions
		if(!clone.contains(endI.getPrecond(newAct))) {
			//System.out.println(f+" at end new");
			return false;
		}
		//check at end all Action
		clone = current.clone();
		clone = startI.apply(newAct, clone);
		for(float f1 : currentActions) {
			if(clone.contains(endI.getPrecond(nextEnd.get(f1)))) {
				clone = endI.apply(nextEnd.get(f1), clone);
			} else {
				//System.out.println(f+" end"+f1);
				return false;
			}
		}
		clone = endI.apply(newAct, clone);
		for(float f1 : nextEnd.keySet()) {
			if(f1 > endNewAction) {
				if(clone.contains(endI.getPrecond(nextEnd.get(f1)))) {
					clone = endI.apply(nextEnd.get(f1), clone);
				} else {
					//System.out.println(f+" end"+f1);
					return false;
				}
			}
		}

		return true;
	}

	/**
	 * 
	 * @param temp
	 * @param is
	 * @return
	 */
	public boolean acceptAll(List<TemporalExample> temp, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		//System.out.println(startI);
		int i =0;
		for(TemporalExample ex : temp) {
			i++;
			if(!this.accept(ex,is,startI,invI,endI)) {
				return false;
			}
		}
		return true;
	}

	public float rateOfAccepted(List<TemporalExample> temp, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		//System.out.println(startI);
		float N=0, acc=0;
		for(TemporalExample ex : temp) {
			N++;
			if(this.accept(ex,is,startI,invI,endI)) {
				acc++;
			}
		}
		return acc/N;
	}
	
	public void printAccepted(List<TemporalExample> temp, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		for(TemporalExample ex : temp) {
			if(this.accept(ex,is,startI,invI,endI)) {
				System.out.println(ex);;
			}
		}
	}
	
	public void printRejected(List<TemporalExample> temp, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		for(TemporalExample ex : temp) {
			if(!this.accept(ex,is,startI,invI,endI)) {
				System.out.println(ex);;
			}
		}
	}
	/**
	 * 
	 * @param temp
	 * @param is
	 * @return
	 */
	public boolean rejectAll(List<TemporalExample> temp, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		int i = 0;
		for(TemporalExample ex : temp) {
			i++;
			if(this.accept(ex,is,startI,invI,endI)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param temp
	 * @param is
	 * @return
	 */
	public float scorePos(List<TemporalExample> temp, Observation is) {
		float res = 0f;
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		for(TemporalExample ex : temp) {
			if(this.accept(ex,is,startI,invI,endI)) {
				res += ex.getTemporalSequence().size();
			}
		}
		return res;
	}

	/**
	 * 
	 * @param compressed
	 * @param temp
	 * @param is
	 * @return
	 */
	public float scoreNeg(List<CompressedTemporalNegativeExample> compressed, 
			List<TemporalExample> temp, Observation is) {
		float res = 0f;
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(temp));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(temp));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(temp));
		for(CompressedTemporalNegativeExample ex : compressed) {
			Map<Float, Symbol> seq = ex.getPrefixe();
			Map<Float, List<Symbol>> neg = ex.getNegatives();
			Map<Float, Symbol> nextEnd = new HashMap<>();
			List<Float> startTime = new ArrayList<>(seq.keySet());
			Collections.sort(startTime);
			Observation current = is.clone();
			for(float f : startTime) {
				List<Float> negTime = new ArrayList<>(neg.keySet());
				Collections.sort(negTime);
				int itNeg=0;
				while(itNeg < negTime.size() && negTime.get(itNeg) <= f) {
					List<Float> toRemove = new ArrayList<>();
					for(float f1 : beforeStart(nextEnd.keySet(), f)) {
						current = endI.apply(nextEnd.get(f1), current);
						toRemove.add(f1);
					}
					for(float f1 : toRemove) {
						nextEnd.remove(f1);
					}

					for(Symbol newAct : neg.get(negTime.get(itNeg))) {
						float endNewAction = f+this.durations.get(newAct.generalize());
						List<Float> currentActions = beforeStart(nextEnd.keySet(),
								endNewAction);

						if(!isApplicable(startI, invI, endI, newAct, current, nextEnd,
								endNewAction, currentActions)) {
							res++;
						}
					}
					itNeg++;
				}
				List<Float> toRemove = new ArrayList<>();
				for(float f1 : beforeStart(nextEnd.keySet(), f)) {
					current = endI.apply(nextEnd.get(f1), current);
					toRemove.add(f1);
				}
				for(float f1 : toRemove) {
					nextEnd.remove(f1);
				}

				Symbol newAct = seq.get(f);
				float endNewAction = f+this.durations.get(newAct.generalize());
				List<Float> currentActions = beforeStart(nextEnd.keySet(),
						endNewAction);

				if(isApplicable(startI, invI, endI, newAct, current, nextEnd,
						endNewAction, currentActions)) {
					current = startI.apply(newAct, current);
					nextEnd.put(endNewAction, newAct);
				} else {
					break;
				}
			}
		}
		return res;
	}

	/**
	 * 
	 * @param s1
	 * @param s2
	 */
	private static boolean isDumpy(Symbol s1, Symbol s2) {
		String opName = s1.getName();
		String propName = s2.getName();
		if(propName.equals(opName+"-inv")) {
			return true;
		}
		if(propName.equals("i"+opName+"-inv")) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param l
	 * @param f
	 * @return
	 */
	private static List<Float> beforeStart(Collection<Float> l, float f) {
		List<Float> r = new ArrayList<>();
		for(float x : l) {
			if(x <= f) {
				r.add(x);
			}
		}
		return r;
	}

	/**
	 * 
	 * @param temp
	 * @return
	 */
	private List<Symbol> getAllActions(List<TemporalExample> temp) {
		List<Symbol> actions = new ArrayList<>();
		for(TemporalExample ex : temp) {
			for(Symbol a : ex.getTemporalSequence().values()) {
				if(!actions.contains(a)) {
					actions.add(a);
				}
			}
		}
		return actions;
	}

	/**
	 * 
	 * @param l
	 * @return
	 */
	private static float getLast(Collection<Float> l) {
		float f = -1f;
		for(float ff : l) {
			if(ff > f) {
				f = ff;
			}
		}
		return f;
	}

	/**
	 * 
	 */
	public String toString() {
		return "At Start\n"+this.start+"\nOverAll\n"+this.invariant+"\nAt End\n"+this.end+"\n"+this.durations;
	}

	/**
	 * 
	 * @param temp
	 * @param is
	 * @return
	 */
	public float fscore(List<TemporalExample> pos,List<TemporalExample> neg, Observation is) {
		IndividualInstantiation startI = 
				new IndividualInstantiation(this.start, getAllActions(pos));
		IndividualInstantiation invI = 
				new IndividualInstantiation(this.invariant, getAllActions(pos));
		IndividualInstantiation endI = 
				new IndividualInstantiation(this.end, getAllActions(pos));
		//System.out.println(startI);
		int iPos =0;
		for(TemporalExample ex : pos) {
			if(this.accept(ex,is,startI,invI,endI)) {
				iPos++;
			} else {
				//System.out.println(ex);
			}
		}
		startI = new IndividualInstantiation(this.start, getAllActions(neg));
		invI = new IndividualInstantiation(this.invariant, getAllActions(neg));
		endI = new IndividualInstantiation(this.end, getAllActions(neg));
		int iNeg =0;
		for(TemporalExample ex : neg) {
			if(this.accept(ex,is,startI,invI,endI)) {
				iNeg++;
			}
		}
		float R = learning.Measure.R(iPos, pos.size());
		float P = learning.Measure.P(iPos, iNeg);
		System.out.println(R+" "+P);
		return learning.Measure.FScore(R, P);
	}

	/**
	 * 
	 * @param l
	 * @param durations
	 * @param twoOp
	 * @return
	 */
	public static List<Individual> neighbors(List<Individual> l, 
			Map<Symbol, Float> durations, boolean twoOp, List<Symbol> P, 
			List<Symbol> A, List<Symbol> Atemp) {
		List<Individual> res = new ArrayList<>();
		l.forEach(indiv -> {
			Individual indiv2 = TemporalIndividual.convert(indiv, durations, twoOp, P, A, Atemp);
			if(indiv2!=null && !res.contains(indiv)) {
				res.add(indiv);
			}
		});
		return res;
	}

	/**
	 * 
	 * @return
	 */
	public Individual convert(List<Symbol> P, List<Symbol> A) {
		Individual indiv = new Individual();
		indiv.setP(P);
		indiv.setA(A);
		Map<Symbol, Pair<Observation, Observation>> domainStart = 
				this.start.getDomain();
		Map<Symbol, Pair<Observation, Observation>> domainEnd = 
				this.end.getDomain();
		Map<Symbol, Pair<Observation, Observation>> domainInv = 
				this.invariant.getDomain();
		List<Symbol> operators = new ArrayList<>(this.durations.keySet());
		operators.forEach(op -> {
			Symbol opStart = op.clone();
			opStart.setName(opStart.getName()+"-start");
			Symbol opEnd = op.clone();
			opEnd.setName(opEnd.getName()+"-end");
			indiv.addOperator(opStart, this.start.getP());
			indiv.addOperator(opEnd, this.start.getP());
			//Start
			domainStart.get(op).getX().getPredicates().forEach((k,v) -> {
				//System.out.println(opStart+" "+k+" "+v);
				indiv.mutation(true, opStart, k, v);
			});
			domainStart.get(op).getY().getPredicates().forEach((k,v) -> {
				indiv.mutation(false, opStart, k, v);
			});
			domainInv.get(op).getX().getPredicates().forEach((k,v) -> {
				if(v == Observation.Value.TRUE || v == Observation.Value.FALSE) {
					indiv.mutation(true, opStart, k, v);
				}
			});
			//End
			domainEnd.get(op).getX().getPredicates().forEach((k,v) -> {
				indiv.mutation(true, opEnd, k, v);
			});
			domainEnd.get(op).getY().getPredicates().forEach((k,v) -> {
				indiv.mutation(false, opEnd, k, v);
			});
			domainInv.get(op).getX().getPredicates().forEach((k,v) -> {
				if(v == Observation.Value.TRUE || v == Observation.Value.FALSE) {
					indiv.mutation(true, opEnd, k, v);
				}
			});
			
			//Remove identical effect
			indiv.getDomain().get(opStart).getY().getPredicates().forEach((k,v) -> {
				if(indiv.getDomain().get(opEnd).getY().getValue(k) == v) {
					indiv.mutation(false, opStart, k, Observation.Value.ANY);
					indiv.mutation(false, opEnd, k, Observation.Value.ANY);
				}
			});
			//Remove inconsistant effect p in s and !p in end or inv
			indiv.getDomain().get(opStart).getY().getPredicates().forEach((k,v) -> {
				switch(v) {
				case FALSE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case TRUE:
						indiv.mutation(false, opStart, k, Observation.Value.ANY);
						indiv.mutation(true, opEnd, k, Observation.Value.ANY);
						break;
					default:
						break;
					}
					break;
				case TRUE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case FALSE:
						indiv.mutation(false, opStart, k, Observation.Value.ANY);
						indiv.mutation(true, opEnd, k, Observation.Value.ANY);
						break;
					default:
						break;
					}
					break;
				default:
					break;

				}
			});
			//If end effect in overall change overall to start preconditions
			indiv.getDomain().get(opStart).getX().getPredicates().forEach((k,v) -> {
				switch(v) {
				case FALSE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case FALSE:
						switch(indiv.getDomain().get(opEnd).getY().getValue(k)) {
						case TRUE:
							indiv.mutation(true, opEnd, k, Observation.Value.ANY);
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
					break;
				case TRUE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case TRUE:
						switch(indiv.getDomain().get(opEnd).getY().getValue(k)) {
						case FALSE:
							indiv.mutation(true, opEnd, k, Observation.Value.ANY);
							break;
						default:
							break;
						}
						break;
					default:
						break;
					}
					break;
				default:
					break;

				}
			});
		});
		return indiv;
	}

	public boolean isCOnsistant() {
		Individual indiv = new Individual();
		indiv.setConsistant(true);
		Map<Symbol, Pair<Observation, Observation>> domainStart = 
				this.start.getDomain();
		Map<Symbol, Pair<Observation, Observation>> domainEnd = 
				this.end.getDomain();
		Map<Symbol, Pair<Observation, Observation>> domainInv = 
				this.invariant.getDomain();
		List<Symbol> operators = new ArrayList<>(this.durations.keySet());
		operators.forEach(op -> {
			Symbol opStart = op.clone();
			opStart.setName(opStart.getName()+"-start");
			Symbol opEnd = op.clone();
			opEnd.setName(opEnd.getName()+"-end");
			indiv.addOperator(opStart, this.start.getP());
			indiv.addOperator(opEnd, this.start.getP());
			//Start
			domainStart.get(op).getX().getPredicates().forEach((k,v) -> {
				indiv.mutation(true, opStart, k, v);
			});
			domainStart.get(op).getY().getPredicates().forEach((k,v) -> {
				indiv.mutation(false, opStart, k, v);
			});
			domainInv.get(op).getX().getPredicates().forEach((k,v) -> {
				if(v == Observation.Value.TRUE || v == Observation.Value.FALSE) {
					indiv.mutation(true, opStart, k, v);
				}
			});
			//End
			domainEnd.get(op).getX().getPredicates().forEach((k,v) -> {
				indiv.mutation(true, opEnd, k, v);
			});
			domainEnd.get(op).getY().getPredicates().forEach((k,v) -> {
				indiv.mutation(false, opEnd, k, v);
			});
			domainInv.get(op).getX().getPredicates().forEach((k,v) -> {
				if(v == Observation.Value.TRUE || v == Observation.Value.FALSE) {
					indiv.mutation(true, opEnd, k, v);
				}
			});

			//Remove identical effect
			indiv.getDomain().get(opStart).getY().getPredicates().forEach((k,v) -> {
				if(indiv.getDomain().get(opEnd).getY().getValue(k) == v && v != Observation.Value.ANY) {
					indiv.setConsistant(false);
				}
			});
			//Remove inconsistant effect p in s and !p in end or inv
			indiv.getDomain().get(opStart).getY().getPredicates().forEach((k,v) -> {
				switch(v) {
				case FALSE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case TRUE:
						indiv.setConsistant(false);
						break;
					default:
						break;
					}
					break;
				case TRUE:
					switch(indiv.getDomain().get(opEnd).getX().getValue(k)) {
					case FALSE:
						indiv.setConsistant(false);
						break;
					default:
						break;
					}
					break;
				default:
					break;

				}
			});
		});
		return indiv.getConsistant();
	}
	/**
	 * 
	 * @param indiv
	 * @param durations
	 * @param twoOp
	 * @return
	 */
	public static Individual convert(Individual indiv, 
			Map<Symbol, Float> durations, boolean twoOp, List<Symbol> P,
			List<Symbol> A, List<Symbol> Atemp) {
		/*if(TemporalIndividual.previousConversion.containsKey(indiv)) {
			return TemporalIndividual.previousConversion.get(indiv);
		}*/
		TemporalIndividual temp = new TemporalIndividual(indiv, durations, twoOp, P, Atemp);
		if(!temp.isCOnsistant()) {
			return null;
		}
		//System.out.println(temp);
		Individual res = temp.convert(P, A);
		TemporalIndividual.previousConversion.put(indiv, res);
		return res;
	}
	
	public static Individual convert2(Individual indiv,  
			Map<Symbol, Float> durations, boolean twoOp, List<Symbol> P,
			List<Symbol> A, List<Symbol> Atemp) {
		/*if(TemporalIndividual.previousConversion.containsKey(indiv)) {
			return TemporalIndividual.previousConversion.get(indiv);
		}*/
		//System.out.println(indiv);
		TemporalIndividual temp = new TemporalIndividual(indiv, durations, twoOp, P, Atemp);
		//System.out.println(temp);
		Individual res = temp.convert(P, A);
		TemporalIndividual.previousConversion.put(indiv, res);
		return res;
	}
}
