package learning;

import simulator.Oracle;
import java.util.List;
import fsm.Symbol;

/**
 * This class contains all static methods use to test a PDDL domain
 * @author Maxence Grand
 */
public class TestPlanner {

	/**
	 * An oracle
	 */
	private static Oracle simulator;

	/**
	 * Initialize the oracle
	 * @param sim An oracle
	 */
	static void init(Oracle sim) {
		simulator = sim;
	}

	/**
	 * 
	 * @param domain
	 * @param initialState
	 * @param example
	 * @return
	 * @throws InstantiationException
	 */
	public static float negFit(String domain, String initialState, List<Symbol> example) throws InstantiationException {
		simulator.reInit();
		for(int i = 0; i< example.size(); i++) {
			if(simulator.getAllActions().contains(example.get(i))) {
				if(simulator.accept(example.get(i))) {
					//System.out.println(i+" "+example.get(i));
					/*Node currentState = new Node(simulator.getCurrentState());
            			Observation obs = new Observation(simulator.getAllPredicates());
            			for(Symbol s : simulator.getSymbolsState(currentState)){
            				obs.addTrueObservation(s);
            			}
            			for(Symbol s : simulator.getPositiveStaticPredicate()){
            				obs.addTrueObservation(s);
            			}
            			System.out.println(obs);*/
					continue;
				} else {
					//System.out.println("you");
					if(i < example.size()-1) {
						return 0f;
					} else {
						return 1f;
					}
				}
			} else {
				throw new InstantiationException();
			}
		}

		return 0f;
	}
}
