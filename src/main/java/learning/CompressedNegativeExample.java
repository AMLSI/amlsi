/**
 * 
 */
package learning;

import java.util.ArrayList;
import java.util.List;

import fsm.Example;
import fsm.Symbol;

/**
 * @author Maxence Grand
 *
 */
public class CompressedNegativeExample {
	/**
	 * 
	 */
	private List<Symbol> prefix;
	/**
	 * 
	 */
	private List<List<Symbol>> negatives;
	
	/**
	 * Constructs 
	 * @param prefix
	 * @param negatives
	 */
	public CompressedNegativeExample(Example prefix, List<Example> negatives) {
		this.prefix = prefix.getActionSequences();
		this.negatives = new ArrayList<>();
		for(int i = 0; i < prefix.size(); i++) {
			List<Symbol> currentNeg = new ArrayList<>();
			for(Example ex : negatives) {
				if(ex.size() == i+1) {
					currentNeg.add(ex.get(i));
				}
			}
			this.negatives.add(currentNeg);
		}
		List<Symbol> currentNeg = new ArrayList<>();
		for(Example ex : negatives) {
			if(ex.size() == prefix.size()+1) {
				currentNeg.add(ex.get(prefix.size()));
			}
		}
		this.negatives.add(currentNeg);
		//negatives.forEach( n -> this.negatives.add(n.getActionSequences()));
	}
	
	/**
	 * Constructs 
	 * @param prefix
	 * @param negatives
	 */
	public CompressedNegativeExample(List<Symbol> prefix, List<List<Symbol>> negatives) {
		this.prefix = prefix;
		this.negatives = negatives;
	}

	/**
	 * Getter of prefix
	 * @return the prefix
	 */
	public List<Symbol> getPrefix() {
		return prefix;
	}

	/**
	 * Setter prefix
	 * @param prefix the prefix to set
	 */
	public void setPrefix(List<Symbol> prefix) {
		this.prefix = prefix;
	}

	/**
	 * Getter of negatives
	 * @return the negatives
	 */
	public List<List<Symbol>> getNegatives() {
		return negatives;
	}

	/**
	 * Setter negatives
	 * @param negatives the negatives to set
	 */
	public void setNegatives(List<List<Symbol>> negatives) {
		this.negatives = negatives;
	}
	
	
}
