/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package learning;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;

import exception.PlanException;
import fr.uga.pddl4j.planners.statespace.search.Node;
import fsm.Sample;
import fsm.Symbol;
import fsm.Example;
import fsm.Pair;
import simulator.Oracle;
import simulator.temporal.TemporalOracle;

/**
 * This class implements the generator.
 *
 * @author Maxence Grand
 * @version 1.0
 * @since   10-2019
 */
public class Generator{
	/**
	 * The level of noise to simulate noise. 0% if we doesn't want simulate noise
	 */
	public static float THRESH = (float) 0.0;
	/**
	 * The level of observable informations.
	 */
	public static float LEVEL = (float) 0.25;
	/**
	 * The pseudo random number generator
	 */
	private Random random;
	/**
	 * The oracle
	 */
	private Oracle blackbox;
	/**
	 * The mapping ante
	 */
	//private Map<List<Symbol>,List<Observation>> mapping;
	/**
	 * The mapping post
	 */
	//private Map<List<Symbol>,List<Observation>> mappingPost;
	/**
	 * The observations
	 */
	//public Map<List<Symbol>,List<Observation>> data;
	/**
	 * 
	 */
	private List<CompressedNegativeExample> compressedNegativeExample;
	/**
	 * The constructor of the object Generator
	 *
	 * @param sim The oracle
	 * @param r The pseudo random number generator
	 */
	public Generator(Oracle sim, Random r){
		blackbox = sim;
		//mappingPost=new HashMap<>();
		//mapping=new HashMap<>();
		random = r;
		//data = new HashMap<>();
		compressedNegativeExample=new ArrayList<>();
	}


	/**
	 * Getter of compressedNegativeExample
	 * @return the compressedNegativeExample
	 */
	public List<CompressedNegativeExample> getCompressedNegativeExample() {
		return compressedNegativeExample;
	}


	/**
	 * Getter of data generated with intermediate observation
	 * @return The observations
	 */
	/*public Map<List<Symbol>,List<Observation>> getData(){
		return data;
	}*/

	/**
	 * Get mapping
	 * @return The mapping ante
	 */
	/*public Map<List<Symbol>, List<Observation>> getMappingSeqObs(){
		return mapping;
	}*/

	/**
	 * Get mapping
	 * @return The mapping post
	 */
	/*public Map<List<Symbol>, List<Observation>> getMappingPostSeqObs(){
		return mappingPost;
	}*/

	/**
	 * Map Observation with actions
	 *
	 * @param pos Positive sample
	 */
	public Sample map(Sample pos) {
		Sample res = new Sample();
		for(Example example : pos.getExamples()) {
			List<Observation> observations = play(example);
			res.addExample(new ObservedExample(example.getActionSequences(),
					observations));
		}
		return res;
	}

	public Sample map(Sample pos, TemporalOracle simT) {
		Sample res = new Sample();
		for(Example example : pos.getExamples()) {
			List<Observation> observations = play(example, simT);
			res.addExample(new ObservedExample(example.getActionSequences(),
					observations));
		}
		return res;
	}
	/**
	 * Map Observation with actions
	 *
	 * @param pos Positive sample
	 */
	/*public void map(Sample pos, List<String> extraDumpy) {
		mappingPost=new HashMap<>();
		mapping=new HashMap<>();
		data=new HashMap<>();
		for(List<Symbol> example : pos.getSymbols()) {
			//System.out.println("Yo !!!!");
			List<Observation> observations = play(example, extraDumpy);
			data.put(example, observations);
			List<Observation> observationsPost = new ArrayList<>();
			List<Observation> observationsAnte= new ArrayList<>();

			observations = clean(observations);

			//Ante state
			for(int i = 0; i< observations.size()-1; i++) {
				observationsAnte.add(observations.get(i));
			}

			//Next state
			for(int i = 1; i< observations.size(); i++) {
				observationsPost.add(observations.get(i));

			}
			mapping.put(example, observationsAnte);
			mappingPost.put(example, observationsPost);
		}
	}*/

	/**
	 * Update the mapping
	 * @param example The new example
	 */
	public Example map(Example example) {
		List<Observation> observations = play(example);
		return new ObservedExample(example.getActionSequences(), observations);

	}

	/**
	 * Update the mapping
	 * @param example The new example
	 */
	/*public void updateMapping(List<Symbol> example, List<String> extraDumpy) {
		List<Observation> observations = play(example, extraDumpy);
		data.put(example, observations);
		List<Observation> observationsPost = new ArrayList<>();
		List<Observation> observationsAnte= new ArrayList<>();

		observations = clean(observations);

		//Ante state
		for(int i = 0; i< observations.size()-1; i++) {
			observationsAnte.add(observations.get(i));
		}

		//Next state
		for(int i = 1; i< observations.size(); i++) {
			observationsPost.add(observations.get(i));

		}
		mapping.put(example, observationsAnte);
		mappingPost.put(example, observationsPost);

	}*/

	/**
	 * Interaction to generate samples
	 *
	 * @param M The size of the positive sample
	 * @param min Minimal size of positive example
	 * @param max Maximal size of positive example
	 * @return I+,I-
	 */
	public Pair<Sample, Sample> interactPartial(int M, int min, int max) {
		List<Example> pos = new ArrayList<>();
		List<Example> neg = new ArrayList<>();
		List<Symbol> ops = blackbox.getAllActions();
		for(int m=0; m<M; m++) {
			blackbox.reInit();
			int n = random.nextInt(max-min+1)+min;
			List<Symbol> seq = new ArrayList<>();
			List<List<Symbol>> compressed = new ArrayList<>();
			for(int i=0; i<n; i++){
				//Copy action list
				List<Symbol> tmp = new ArrayList<>();
				for(Symbol a : ops) {
					tmp.add(a);
				}
				Symbol op = tmp.get(random.nextInt(tmp.size()));
				List<Symbol> negative = new ArrayList<>();
				while(! blackbox.isApplicable(op) && tmp.size() > 0) {
					negative.add(op);
					List<Symbol> tmp2 = new ArrayList<>();
					for(Symbol s : seq) {
						tmp2.add(s);
					}
					tmp2.add(op);
					neg.add(new Example(tmp2));
					tmp.remove(op);
					if(tmp.size() <= 0) {
						break;
					}
					op = tmp.get(random.nextInt(tmp.size()));
				}
				compressed.add(negative);
				if(tmp.size() <= 0) {
					break;
				}
				if(blackbox.isApplicable(op)){
					blackbox.isApplicable(op);
					try {
						blackbox.apply();
					} catch (PlanException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					seq.add(op);
				} else {
					break;
				}

			}
			this.compressedNegativeExample.add(
					new CompressedNegativeExample(seq, compressed));
			blackbox.reInit();
			pos.add(new Example(seq));
		}
		return new Pair<>(new Sample(pos), new Sample(neg));
	}

	/*public Pair<Sample, Sample> interactPartialWhithoutMap(int M, int min, int max) {
		List<Example> pos = new ArrayList<>();
		List<Example> neg = new ArrayList<>();
		List<Symbol> ops = blackbox.getAllActions();
		for(int m=0; m<M; m++) {
			blackbox.reInit();
			int n = random.nextInt(max-min+1)+min;
			List<Symbol> seq = new ArrayList<>();
			List<List<Symbol>> compressed = new ArrayList<>();
			for(int i=0; i<n; i++){
				//Copy action list
				List<Symbol> tmp = new ArrayList<>();
				for(Symbol a : ops) {
					tmp.add(a);
				}
				Symbol op = tmp.get(random.nextInt(tmp.size()));
				List<Symbol> negative = new ArrayList<>();
				while(! blackbox.isApplicable(op) && tmp.size() > 0) {
					negative.add(op);
					List<Symbol> tmp2 = new ArrayList<>();
					for(Symbol s : seq) {
						tmp2.add(s);
					}
					tmp2.add(op);
					neg.add(new Example(tmp2));
					tmp.remove(op);
					if(tmp.size() <= 0) {
						break;
					}
					op = tmp.get(random.nextInt(tmp.size()));
				}
				compressed.add(negative);
				if(tmp.size() <= 0) {
					break;
				}
				if(blackbox.isApplicable(op)){
					blackbox.isApplicable(op);
					blackbox.apply();
					seq.add(op);
				} else {
					break;
				}

			}
			this.compressedNegativeExample.add(
					new CompressedNegativeExample(seq, compressed));
			blackbox.reInit();
			pos.add(new Example(seq));
		}
		return new Pair<>(new Sample(pos), new Sample(neg));
	}*/
	/**
	 * Play action to map action/observations
	 *
	 * @param example The example
	 * @return  Observations
	 */
	public List<Observation> play(Example example) {
		//System.out.println("PASS");
		blackbox.reInit();
		List<Observation> observations = new ArrayList<>();
		for(int i = 0; i<example.size(); i++) {
			Symbol op = example.get(i);
			if(i == 0) {
				Node currentState = new Node(blackbox.getCurrentState());
				Observation obs = new Observation(blackbox.getAllPredicates());
				for(Symbol s : blackbox.getSymbolsState(currentState)){
					obs.addTrueObservation(s);
				}
				for(Symbol s : blackbox.getPositiveStaticPredicate()){
					obs.addTrueObservation(s);
				}
				observations.add(obs);
			}

			blackbox.apply(op);
			Node currentState = new Node(blackbox.getCurrentState());
			Observation obs = new Observation(blackbox.getAllPredicates());
			for(Symbol s : blackbox.getSymbolsState(currentState)){
				obs.addTrueObservation(s);
			}
			for(Symbol s : blackbox.getPositiveStaticPredicate()){
				obs.addTrueObservation(s);
			}
			observations.add(obs);
		}

		boolean first = true;
		//Add missing attribute
		for(Observation obs : observations) {
			for(Symbol pred : this.blackbox.getAllPredicates() ) {
				float prob = this.random.nextFloat();
				if(prob > LEVEL && !first) {
					obs.missingPredicate(pred);
				}
			}
			first =false;
		}

		first = true;
		//Add noise
		for(Observation obs : observations) {
			for(Symbol pred : this.blackbox.getAllPredicates() ) {
				if(obs.isPresent(pred)){
					float prob = this.random.nextFloat();
					if(prob < THRESH && !first) {
						obs.addNoise(pred);
					}
				}
			}
			first = false;
		}

		return observations;
	}

	public List<Observation> play(Example example, TemporalOracle simT) {
		//System.out.println("PASS");
		simT.reInit();
		List<Observation> observations = new ArrayList<>();
		for(int i = 0; i<example.size(); i++) {
			Symbol op = example.get(i);
			if(i == 0) {
				Node currentState = new Node(simT.getCurrentState());
				Observation obs = new Observation(simT.getAllPredicates());
				//System.err.println(i+" "+currentState);
				for(Symbol s : simT.getSymbolsState(currentState)){
					obs.addTrueObservation(s);
				}
				for(Symbol s : simT.getPositiveStaticPredicate()){
					obs.addTrueObservation(s);
				}
				observations.add(obs);
				//System.err.println(obs);
				//System.exit(1);
			}

			simT.apply_sequential(op);
			//System.out.println(op);
			Node currentState = new Node(simT.getCurrentState());
			Observation obs = new Observation(simT.getAllPredicates());
			for(Symbol s : simT.getSymbolsState(currentState)){
				obs.addTrueObservation(s);
			}
			for(Symbol s : simT.getPositiveStaticPredicate()){
				obs.addTrueObservation(s);
			}
			observations.add(obs);
			//System.err.println(obs);
			//System.exit(1);
		}

		boolean first = true;
		//Add missing attribute
		for(Observation obs : observations) {
			for(Symbol pred : simT.getAllPredicates() ) {
				float prob = this.random.nextFloat();
				if(prob > LEVEL && !first) {
					obs.missingPredicate(pred);
				}
			}
			first =false;
		}

		first = true;
		//Add noise
		for(Observation obs : observations) {
			for(Symbol pred : simT.getAllPredicates() ) {
				if(obs.isPresent(pred)){
					float prob = this.random.nextFloat();
					if(prob < THRESH && !first) {
						obs.addNoise(pred);
					}
				}
			}
			first = false;
		}

		return observations;
	}
	
	/**
	 * Play action to map action/observations
	 *
	 * @param example The example
	 * @return  Observations
	 */
	public List<Observation> play(List<Symbol> example, List<String> extraDumpy) {
		blackbox.reInit();
		List<Observation> observations = new ArrayList<>();
		for(int i = 0; i<example.size(); i++) {
			Symbol op = example.get(i);
			if(i == 0) {
				Node currentState = new Node(blackbox.getCurrentState());
				Observation obs = new Observation(blackbox.getAllPredicates());
				for(Symbol s : blackbox.getSymbolsState(currentState)){
					obs.addTrueObservation(s);
				}
				for(Symbol s : blackbox.getPositiveStaticPredicate()){
					obs.addTrueObservation(s);
				}
				observations.add(obs);
			}
			blackbox.apply(op);
			Node currentState = new Node(blackbox.getCurrentState());
			Observation obs = new Observation(blackbox.getAllPredicates());
			for(Symbol s : blackbox.getSymbolsState(currentState)){
				obs.addTrueObservation(s);
			}
			for(Symbol s : blackbox.getPositiveStaticPredicate()){
				obs.addTrueObservation(s);
			}
			observations.add(obs);
		}

		boolean first = true;
		//Add missing attribute
		for(Observation obs : observations) {
			for(Symbol pred : this.blackbox.getAllPredicates() ) {
				float prob = this.random.nextFloat();
				if(prob > LEVEL && !first && !extraDumpy.contains(pred.getName())) {
					obs.missingPredicate(pred);
				}
			}
			first =false;
		}

		first = true;
		//Add noise
		for(Observation obs : observations) {
			for(Symbol pred : this.blackbox.getAllPredicates() ) {
				if(obs.isPresent(pred)){
					float prob = this.random.nextFloat();
					if(prob < THRESH && !first && !extraDumpy.contains(pred.getName())) {
						obs.addNoise(pred);
					}
				}
			}
			first = false;
		}

		return observations;
	}

	/**
	 * Clean observations
	 * @param obs Observations to clean
	 * @return Cleaned observations
	 */
	public List<Observation> clean(List<Observation> obs) {
		return obs;
	}

	/**
	 * Return the initial state
	 * @return An observation
	 */
	public Observation getInitialState() {
		Observation res = new Observation(blackbox.getAllPredicates());
		for(Symbol s : blackbox.getSymbolsState(blackbox.getInitialState())){
			res.addTrueObservation(s);
		}
		for(Symbol s : blackbox.getPositiveStaticPredicate()){
			res.addTrueObservation(s);
		}
		return res;
	}
	/**
	 * Play action to map action/observations without simulate noise
	 *
	 * @param example The example
	 * @return  Observations
	 */
	public List<Observation> playWithoutNoise(Example example) {
		blackbox.reInit();
		List<Observation> observations = new ArrayList<>();
		for(int i = 0; i<example.size(); i++) {
			Symbol op = example.get(i);
			if(i == 0) {
				Node currentState = new Node(blackbox.getCurrentState());
				Observation obs = new Observation(blackbox.getAllPredicates());
				for(Symbol s : blackbox.getSymbolsState(currentState)){
					obs.addTrueObservation(s);
				}
				for(Symbol s : blackbox.getPositiveStaticPredicate()){
					obs.addTrueObservation(s);
				}
				observations.add(obs);
			}

			blackbox.apply(op);
			Node currentState = new Node(blackbox.getCurrentState());
			Observation obs = new Observation(blackbox.getAllPredicates());
			for(Symbol s : blackbox.getSymbolsState(currentState)){
				obs.addTrueObservation(s);
			}
			for(Symbol s : blackbox.getPositiveStaticPredicate()){
				obs.addTrueObservation(s);
			}
			observations.add(obs);
		}
		return observations;
	}

	/**
	 * Clear data
	 */
	/*public void clearData() {
		data.clear();
	}*/
}
