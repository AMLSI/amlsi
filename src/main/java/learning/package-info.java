/**
 * 
 * This package contains all classes and interfaces used to implelent
 * learning modules
 * 
 * @author Maxence Grand
 *
 */
package learning;
