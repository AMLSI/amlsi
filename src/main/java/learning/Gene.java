package learning;

/**
 * This class represents a gene 
 * @author Maxence Grand
 */
public class Gene {
    /**
     * The number of bits of a gene
     */
    public static int NB_BITS = 2;
    /**
     * The bit's array
     */
    private boolean[] bits;

    /**
     * Constructs a gene with all bits set to 0
     */
    public Gene(){
        bits = new boolean[NB_BITS];
    }

    /**
     * Constructs a gene from a given value
     * @param value The value
     * @see learning.Observation.Value
     */
    public Gene(Observation.Value value){
        this();
        switch(value) {
        case TRUE:
            bits[0] = true;
            bits[1] = true;
            break;
        case FALSE:
            bits[0] = false;
            bits[1] = false;
            break;
        default:
            bits[0] = true;
            bits[1] = false;
            break;
        }
    }

    /**
     * Set the value of the gene
     * @param value The value
     * @see learning.Observation.Value
     */
    public void setValue (Observation.Value value){
        switch(value) {
        case TRUE:
            bits[0] = true;
            bits[1] = true;
            break;
        case FALSE:
            bits[0] = false;
            bits[1] = false;
            break;
        default:
            bits[0] = true;
            bits[1] = false;
            break;
        }
    }
    
    /**
     * Constructs a gene from a given array of bits
     * @param bits The array of bits
     */
    public Gene(boolean[] bits){
        this();
        if(bits.length != NB_BITS) {
            throw new IllegalArgumentException();
        }

        this.bits[0] = bits[0];
        this.bits[1] = bits[1];
    }

    /**
     * Constructs a gene from another gene
     * @param other A gene
     */
    public Gene(Gene other){
        this(other.bits);
    }

    /**
     * Clone the gene
     * @return A gene
     */
    public Gene clone() {
        return new Gene(this);
    }

    /**
     * Getter of the array of bits
     * @return An array of bits
     */
    public boolean[] getBits(){
        return bits;
    }

    /**
     * Set a given bit
     * @param idx Bit's index
     * @param bool The bit value
     */
    public void setBit(int idx, boolean bool) {
        if(idx < 0 || idx > NB_BITS) {
            throw new IllegalArgumentException();
        }

        this.bits[idx] = bool;
    }

    /**
     * Get the corresponding value of the gene
     * @return A value
     * @see learning.Observation.Value
     */
    public Observation.Value getValue(){
        if(this.bits[0] && this.bits[1]){
            return Observation.Value.TRUE;
        }else if(!this.bits[0] && !this.bits[1]){
            return Observation.Value.FALSE;
        }else{
            return Observation.Value.ANY;
        }
    }

    /**
     * Equality test
     * @param other A gene
     * @return True if genes are equal
     */
    @Override
    public boolean equals(Object other) {
        if(! (other instanceof Gene)) {
            return false;
        }

        Gene g = (Gene) other;
        switch(this.getValue()) {
        case TRUE:
        	switch(g.getValue()) {
            case TRUE:
            	return true;
            default:
            	return false;
            }
        case FALSE:
        	switch(g.getValue()) {
            case FALSE:
            	return true;
            default:
            	return false;
            }
        default:
        	switch(g.getValue()) {
        	case TRUE:
            	return false;
        	case FALSE:
            	return false;
            default:
            	return true;
            }
        }
        //return this.getValue() == g.getValue();
    }

    /**
     * The hashcode
     * @return The hashcode
     */
    @Override
    public int hashCode() {
        switch(getValue()) {
        case TRUE:
            return 1;
        case FALSE:
            return 2;
        default:
            return 3;
        }
    }

    /**
     * String representation of a bit
     * @return A string
     */
    @Override
    public String toString() {
        String res = "[ ";

        for(int i = 0; i < bits.length; i++) {
            if(bits[i]) {
                res += "1 ";
            }else {
                res += "0 ";
            }
        }
        res += "]";
        return res;
    }
}
