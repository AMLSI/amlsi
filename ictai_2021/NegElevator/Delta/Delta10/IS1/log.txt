# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial1.pddl
Delta=10
pddl/NegElevator/domain.pddl
E+ size : 100
E- size : 13821
e+ mean size : 52.99
e- mean size : 34.13248
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 413
Time : 22.157
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 413
Time : 22.046
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 366
Time : 7.985
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 385
Time : 20.833
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12315
e+ mean size : 47.98
e- mean size : 34.52075
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 297
Time : 15.466
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 35
Size I+ 35
Size I- 828
Time : 138.492
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 297
Time : 7.969
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 297
Time : 11.878
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12076
e+ mean size : 47.19
e- mean size : 34.12852
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 450
Time : 18.208
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 450
Time : 19.422
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 342
Time : 8.477
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 441
Time : 23.76
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13375
e+ mean size : 51.54
e- mean size : 36.472973
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 326
Time : 12.968
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 378
Time : 17.234
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 334
Time : 7.056
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 513
Time : 33.282
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13207
e+ mean size : 50.94
e- mean size : 35.884457
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 469
Time : 31.316
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 41
Size I+ 41
Size I- 1032
Time : 250.94
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 405
Time : 16.55
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 405
Time : 21.32
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
