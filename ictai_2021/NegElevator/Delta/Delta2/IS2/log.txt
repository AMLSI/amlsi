# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial2.pddl
Delta=2
pddl/NegElevator/domain.pddl
E+ size : 100
E- size : 13233
e+ mean size : 51.68
e- mean size : 36.82491
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 199
Time : 5.734
false 
Syntactical distance : 0.033333335
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.017879948
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 153
Time : 10.344
false 
Syntactical distance : 0.116666675
Error Rate Precondition : 0.01981948
Error Rate Postcondition : 0.033910174
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 114
Time : 1.375
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 114
Time : 2.788
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12995
e+ mean size : 50.49
e- mean size : 35.05333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 129
Time : 3.266
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 129
Time : 3.352
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 90
Time : 0.934
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 167
Time : 4.002
false 
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13148
e+ mean size : 51.15
e- mean size : 35.614616
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 188
Time : 4.489
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 217
Time : 7.338
false 
Syntactical distance : 0.016666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 64
Time : 0.899
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 87
Time : 1.929
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13577
e+ mean size : 52.17
e- mean size : 36.00648
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 119
Time : 1.909
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 146
Time : 3.089
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 95
Time : 0.761
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 95
Time : 1.829
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13918
e+ mean size : 53.62
e- mean size : 36.94561
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 166
Time : 2.849
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 166
Time : 3.801
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 87
Time : 0.785
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 87
Time : 1.397
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
