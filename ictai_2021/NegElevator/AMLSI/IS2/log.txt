test [board( f0 p0), board( f0 p1), depart( f2 p0), depart( f2 p1), up( f0 f1), up( f1 f2), down( f1 f0), down( f2 f1)]
# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial2.pddl
pddl/NegElevator/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1024
x+ mean size : 14.766666
x- mean size : 8.427734
E+ size : 100
E- size : 13233
e+ mean size : 51.68
e- mean size : 36.82491
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 24
#Transitions : 44
Compression level : 18.458334
PDDL Generation
Time : 16.141
61 520
61 520
Recall = 0.61
Precision = 0.10499139
Fscore automaton 0.17914832
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 24
#Transitions : 44
Compression level : 18.458334
PDDL Generation
Time : 19.91
61 520
61 520
Recall = 0.61
Precision = 0.10499139
Fscore automaton 0.17914832
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 24
#Transitions : 44
Compression level : 18.458334
PDDL Generation
Time : 9.847
61 520
61 520
Recall = 0.61
Precision = 0.10499139
Fscore automaton 0.17914832
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 24
#Transitions : 44
Compression level : 18.458334
PDDL Generation
Time : 20.298
61 520
61 520
Recall = 0.61
Precision = 0.10499139
Fscore automaton 0.17914832
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1082
x+ mean size : 15.033334
x- mean size : 8.731977
E+ size : 100
E- size : 12995
e+ mean size : 50.49
e- mean size : 35.05333
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 451.0
#States : 20
#Transitions : 36
Compression level : 22.55
PDDL Generation
Time : 14.103
98 696
98 696
Recall = 0.98
Precision = 0.12342569
Fscore automaton 0.21923937
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 451.0
#States : 20
#Transitions : 36
Compression level : 22.55
PDDL Generation
Time : 21.784
98 696
98 696
Recall = 0.98
Precision = 0.12342569
Fscore automaton 0.21923937
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 451.0
#States : 20
#Transitions : 36
Compression level : 22.55
PDDL Generation
Time : 9.045
98 696
98 696
Recall = 0.98
Precision = 0.12342569
Fscore automaton 0.21923937
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 451.0
#States : 20
#Transitions : 36
Compression level : 22.55
PDDL Generation
Time : 22.665
98 696
98 696
Recall = 0.98
Precision = 0.12342569
Fscore automaton 0.21923937
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 997
x+ mean size : 14.933333
x- mean size : 8.414243
E+ size : 100
E- size : 13148
e+ mean size : 51.15
e- mean size : 35.614616
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 27
#Transitions : 50
Compression level : 16.592592
PDDL Generation
Time : 11.98
65 487
65 487
Recall = 0.65
Precision = 0.117753625
Fscore automaton 0.1993865
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 27
#Transitions : 50
Compression level : 16.592592
PDDL Generation
Time : 22.834
65 487
65 487
Recall = 0.65
Precision = 0.117753625
Fscore automaton 0.1993865
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 27
#Transitions : 50
Compression level : 16.592592
PDDL Generation
Time : 11.963
65 487
65 487
Recall = 0.65
Precision = 0.117753625
Fscore automaton 0.1993865
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 27
#Transitions : 50
Compression level : 16.592592
PDDL Generation
Time : 22.307
65 487
65 487
Recall = 0.65
Precision = 0.117753625
Fscore automaton 0.1993865
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 968
x+ mean size : 14.566667
x- mean size : 8.089876
E+ size : 100
E- size : 13577
e+ mean size : 52.17
e- mean size : 36.00648
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 437.0
#States : 19
#Transitions : 34
Compression level : 23.0
PDDL Generation
Time : 13.056
66 436
66 436
Recall = 0.66
Precision = 0.13147411
Fscore automaton 0.21926911
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 437.0
#States : 19
#Transitions : 34
Compression level : 23.0
PDDL Generation
Time : 20.547
66 436
66 436
Recall = 0.66
Precision = 0.13147411
Fscore automaton 0.21926911
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 437.0
#States : 19
#Transitions : 34
Compression level : 23.0
PDDL Generation
Time : 7.953
66 436
66 436
Recall = 0.66
Precision = 0.13147411
Fscore automaton 0.21926911
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 437.0
#States : 19
#Transitions : 34
Compression level : 23.0
PDDL Generation
Time : 18.341
66 436
66 436
Recall = 0.66
Precision = 0.13147411
Fscore automaton 0.21926911
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1107
x+ mean size : 15.566667
x- mean size : 8.885276
E+ size : 100
E- size : 13918
e+ mean size : 53.62
e- mean size : 36.94561
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 25
#Transitions : 45
Compression level : 18.68
PDDL Generation
Time : 15.36
73 115
73 115
Recall = 0.73
Precision = 0.3882979
Fscore automaton 0.5069444
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 25
#Transitions : 45
Compression level : 18.68
PDDL Generation
Time : 23.086
73 115
73 115
Recall = 0.73
Precision = 0.3882979
Fscore automaton 0.5069444
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 467.0
#States : 25
#Transitions : 45
Compression level : 18.68
PDDL Generation
Time : 10.884
73 115
73 115
Recall = 0.73
Precision = 0.3882979
Fscore automaton 0.5069444
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 467.0
#States : 25
#Transitions : 45
Compression level : 18.68
PDDL Generation
Time : 22.668
73 115
73 115
Recall = 0.73
Precision = 0.3882979
Fscore automaton 0.5069444
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
