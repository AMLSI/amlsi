test [board( f0 p0), board( f2 p1), depart( f1 p0), depart( f1 p1), up( f0 f1), up( f1 f2), down( f1 f0), down( f2 f1)]
# actions 8
# predicate 13
Initial state : pddl/NegElevator/initial_states/initial3.pddl
pddl/NegElevator/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1049
x+ mean size : 15.5
x- mean size : 8.769304
E+ size : 100
E- size : 13146
e+ mean size : 52.3
e- mean size : 35.592422
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 465.0
#States : 31
#Transitions : 55
Compression level : 15.0
PDDL Generation
Time : 20.39
42 283
42 283
Recall = 0.42
Precision = 0.12923077
Fscore automaton 0.19764705
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 465.0
#States : 31
#Transitions : 55
Compression level : 15.0
PDDL Generation
Time : 25.0
42 283
42 283
Recall = 0.42
Precision = 0.12923077
Fscore automaton 0.19764705
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 465.0
#States : 31
#Transitions : 55
Compression level : 15.0
PDDL Generation
Time : 14.209
42 283
42 283
Recall = 0.42
Precision = 0.12923077
Fscore automaton 0.19764705
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 465.0
#States : 31
#Transitions : 55
Compression level : 15.0
PDDL Generation
Time : 25.952
42 283
42 283
Recall = 0.42
Precision = 0.12923077
Fscore automaton 0.19764705
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1031
x+ mean size : 15.333333
x- mean size : 8.636275
E+ size : 100
E- size : 11995
e+ mean size : 47.51
e- mean size : 35.519382
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 33
#Transitions : 62
Compression level : 13.939394
PDDL Generation
Time : 19.964
49 286
49 286
Recall = 0.49
Precision = 0.14626865
Fscore automaton 0.22528735
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 33
#Transitions : 62
Compression level : 13.939394
PDDL Generation
Time : 25.775
49 286
49 286
Recall = 0.49
Precision = 0.14626865
Fscore automaton 0.22528735
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 460.0
#States : 33
#Transitions : 62
Compression level : 13.939394
PDDL Generation
Time : 15.402
49 286
49 286
Recall = 0.49
Precision = 0.14626865
Fscore automaton 0.22528735
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 460.0
#States : 33
#Transitions : 62
Compression level : 13.939394
PDDL Generation
Time : 28.652
49 286
49 286
Recall = 0.49
Precision = 0.14626865
Fscore automaton 0.22528735
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1022
x+ mean size : 14.6
x- mean size : 8.460861
E+ size : 100
E- size : 13935
e+ mean size : 54.49
e- mean size : 38.91353
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 438.0
#States : 24
#Transitions : 47
Compression level : 18.25
PDDL Generation
Time : 14.789
69 525
69 525
Recall = 0.69
Precision = 0.116161615
Fscore automaton 0.19884725
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 438.0
#States : 24
#Transitions : 47
Compression level : 18.25
PDDL Generation
Time : 21.795
69 525
69 525
Recall = 0.69
Precision = 0.116161615
Fscore automaton 0.19884725
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 438.0
#States : 24
#Transitions : 47
Compression level : 18.25
PDDL Generation
Time : 10.127
69 525
69 525
Recall = 0.69
Precision = 0.116161615
Fscore automaton 0.19884725
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 438.0
#States : 24
#Transitions : 47
Compression level : 18.25
PDDL Generation
Time : 20.102
69 525
69 525
Recall = 0.69
Precision = 0.116161615
Fscore automaton 0.19884725
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1086
x+ mean size : 15.0
x- mean size : 8.776243
E+ size : 100
E- size : 13148
e+ mean size : 50.71
e- mean size : 35.437557
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 25
#Transitions : 44
Compression level : 18.0
PDDL Generation
Time : 16.721
60 354
60 354
Recall = 0.6
Precision = 0.14492753
Fscore automaton 0.23346305
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 25
#Transitions : 44
Compression level : 18.0
PDDL Generation
Time : 22.511
60 354
60 354
Recall = 0.6
Precision = 0.14492753
Fscore automaton 0.23346305
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 25
#Transitions : 44
Compression level : 18.0
PDDL Generation
Time : 11.799
60 354
60 354
Recall = 0.6
Precision = 0.14492753
Fscore automaton 0.23346305
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 25
#Transitions : 44
Compression level : 18.0
PDDL Generation
Time : 20.166
60 354
60 354
Recall = 0.6
Precision = 0.14492753
Fscore automaton 0.23346305
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1014
x+ mean size : 14.833333
x- mean size : 8.325444
E+ size : 100
E- size : 12410
e+ mean size : 49.6
e- mean size : 34.17325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 25
#Transitions : 42
Compression level : 17.8
PDDL Generation
Time : 14.566
64 246
64 246
Recall = 0.64
Precision = 0.20645161
Fscore automaton 0.31219512
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 25
#Transitions : 42
Compression level : 17.8
PDDL Generation
Time : 18.694
64 246
64 246
Recall = 0.64
Precision = 0.20645161
Fscore automaton 0.31219512
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 445.0
#States : 25
#Transitions : 42
Compression level : 17.8
PDDL Generation
Time : 9.703
64 246
64 246
Recall = 0.64
Precision = 0.20645161
Fscore automaton 0.31219512
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 445.0
#States : 25
#Transitions : 42
Compression level : 17.8
PDDL Generation
Time : 18.179
64 246
64 246
Recall = 0.64
Precision = 0.20645161
Fscore automaton 0.31219512
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
