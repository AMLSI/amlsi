test [move( rooma roomb), move( roomb rooma), pick( ball0 rooma left), pick( ball0 rooma right), pick( ball0 roomb left), pick( ball0 roomb right), drop( ball0 rooma left), drop( ball0 rooma right), drop( ball0 roomb left), drop( ball0 roomb right)]
# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial1.pddl
pddl/gripper/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1034
x+ mean size : 14.2
x- mean size : 7.9700193
E+ size : 100
E- size : 13100
e+ mean size : 50.86
e- mean size : 34.45649
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 426.0
#States : 8
#Transitions : 16
Compression level : 53.25
PDDL Generation
Time : 6.517
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 426.0
#States : 8
#Transitions : 16
Compression level : 53.25
PDDL Generation
Time : 14.199
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 426.0
#States : 8
#Transitions : 16
Compression level : 53.25
PDDL Generation
Time : 5.297
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 426.0
#States : 8
#Transitions : 16
Compression level : 53.25
PDDL Generation
Time : 13.674
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1089
x+ mean size : 14.766666
x- mean size : 8.185492
E+ size : 100
E- size : 12472
e+ mean size : 49.79
e- mean size : 33.434895
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 8
#Transitions : 16
Compression level : 55.375
PDDL Generation
Time : 5.793
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 8
#Transitions : 16
Compression level : 55.375
PDDL Generation
Time : 11.472
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 8
#Transitions : 16
Compression level : 55.375
PDDL Generation
Time : 6.072
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 8
#Transitions : 16
Compression level : 55.375
PDDL Generation
Time : 18.169
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1333
x+ mean size : 16.366667
x- mean size : 9.185296
E+ size : 100
E- size : 12226
e+ mean size : 48.27
e- mean size : 34.716095
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 491.0
#States : 8
#Transitions : 16
Compression level : 61.375
PDDL Generation
Time : 6.514
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 491.0
#States : 8
#Transitions : 16
Compression level : 61.375
PDDL Generation
Time : 12.992
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 491.0
#States : 8
#Transitions : 16
Compression level : 61.375
PDDL Generation
Time : 6.671
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 491.0
#States : 8
#Transitions : 16
Compression level : 61.375
PDDL Generation
Time : 16.722
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1120
x+ mean size : 14.233334
x- mean size : 8.339286
E+ size : 100
E- size : 12988
e+ mean size : 52.01
e- mean size : 34.179474
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 427.0
#States : 8
#Transitions : 16
Compression level : 53.375
PDDL Generation
Time : 5.057
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 427.0
#States : 8
#Transitions : 16
Compression level : 53.375
PDDL Generation
Time : 11.39
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 427.0
#States : 8
#Transitions : 16
Compression level : 53.375
PDDL Generation
Time : 5.054
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 427.0
#States : 8
#Transitions : 16
Compression level : 53.375
PDDL Generation
Time : 13.308
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1232
x+ mean size : 15.866667
x- mean size : 8.847403
E+ size : 100
E- size : 13188
e+ mean size : 50.96
e- mean size : 33.273506
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 476.0
#States : 8
#Transitions : 16
Compression level : 59.5
PDDL Generation
Time : 5.819
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 476.0
#States : 8
#Transitions : 16
Compression level : 59.5
PDDL Generation
Time : 13.73
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 476.0
#States : 8
#Transitions : 16
Compression level : 59.5
PDDL Generation
Time : 6.027
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 476.0
#States : 8
#Transitions : 16
Compression level : 59.5
PDDL Generation
Time : 15.97
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
