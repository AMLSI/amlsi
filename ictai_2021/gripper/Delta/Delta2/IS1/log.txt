# actions 10
# predicate 8
Initial state : pddl/gripper/initial_states/initial1.pddl
Delta=2
pddl/gripper/domain.pddl
E+ size : 100
E- size : 13100
e+ mean size : 50.86
e- mean size : 34.45649
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 111
Time : 0.994
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 111
Time : 1.367
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 96
Time : 0.426
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 96
Time : 1.174
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12472
e+ mean size : 49.79
e- mean size : 33.434895
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 112
Time : 0.962
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 112
Time : 0.983
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 76
Time : 0.437
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 76
Time : 0.821
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12226
e+ mean size : 48.27
e- mean size : 34.716095
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 97
Time : 0.513
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 119
Time : 1.309
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 109
Time : 0.499
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 109
Time : 0.79
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12988
e+ mean size : 52.01
e- mean size : 34.179474
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 92
Time : 0.692
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 227
Time : 4.182
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 132
Time : 0.566
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 132
Time : 1.139
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 13188
e+ mean size : 50.96
e- mean size : 33.273506
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 103
Time : 0.904
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 134
Time : 2.143
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 71
Time : 0.488
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 71
Time : 0.64
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
