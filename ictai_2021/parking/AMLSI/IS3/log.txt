test [move-curb-to-curb( car_00 curb_0 curb_1), move-curb-to-curb( car_00 curb_0 curb_2), move-curb-to-curb( car_00 curb_1 curb_0), move-curb-to-curb( car_00 curb_1 curb_2), move-curb-to-curb( car_00 curb_2 curb_0), move-curb-to-curb( car_00 curb_2 curb_1), move-curb-to-curb( car_01 curb_0 curb_1), move-curb-to-curb( car_01 curb_0 curb_2), move-curb-to-curb( car_01 curb_1 curb_0), move-curb-to-curb( car_01 curb_1 curb_2), move-curb-to-curb( car_01 curb_2 curb_0), move-curb-to-curb( car_01 curb_2 curb_1), move-curb-to-curb( car_02 curb_0 curb_1), move-curb-to-curb( car_02 curb_0 curb_2), move-curb-to-curb( car_02 curb_1 curb_0), move-curb-to-curb( car_02 curb_1 curb_2), move-curb-to-curb( car_02 curb_2 curb_0), move-curb-to-curb( car_02 curb_2 curb_1), move-curb-to-car( car_00 curb_0 car_01), move-curb-to-car( car_00 curb_0 car_02), move-curb-to-car( car_00 curb_1 car_01), move-curb-to-car( car_00 curb_1 car_02), move-curb-to-car( car_00 curb_2 car_01), move-curb-to-car( car_00 curb_2 car_02), move-curb-to-car( car_01 curb_0 car_00), move-curb-to-car( car_01 curb_0 car_02), move-curb-to-car( car_01 curb_1 car_00), move-curb-to-car( car_01 curb_1 car_02), move-curb-to-car( car_01 curb_2 car_00), move-curb-to-car( car_01 curb_2 car_02), move-curb-to-car( car_02 curb_0 car_00), move-curb-to-car( car_02 curb_0 car_01), move-curb-to-car( car_02 curb_1 car_00), move-curb-to-car( car_02 curb_1 car_01), move-curb-to-car( car_02 curb_2 car_00), move-curb-to-car( car_02 curb_2 car_01), move-car-to-curb( car_00 car_01 curb_0), move-car-to-curb( car_00 car_01 curb_1), move-car-to-curb( car_00 car_01 curb_2), move-car-to-curb( car_00 car_02 curb_0), move-car-to-curb( car_00 car_02 curb_1), move-car-to-curb( car_00 car_02 curb_2), move-car-to-curb( car_01 car_00 curb_0), move-car-to-curb( car_01 car_00 curb_1), move-car-to-curb( car_01 car_00 curb_2), move-car-to-curb( car_01 car_02 curb_0), move-car-to-curb( car_01 car_02 curb_1), move-car-to-curb( car_01 car_02 curb_2), move-car-to-curb( car_02 car_00 curb_0), move-car-to-curb( car_02 car_00 curb_1), move-car-to-curb( car_02 car_00 curb_2), move-car-to-curb( car_02 car_01 curb_0), move-car-to-curb( car_02 car_01 curb_1), move-car-to-curb( car_02 car_01 curb_2), move-car-to-car( car_00 car_01 car_02), move-car-to-car( car_00 car_02 car_01), move-car-to-car( car_01 car_00 car_02), move-car-to-car( car_01 car_02 car_00), move-car-to-car( car_02 car_00 car_01), move-car-to-car( car_02 car_01 car_00)]
# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial3.pddl
pddl/parking/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 5428
x+ mean size : 15.0
x- mean size : 8.623434
E+ size : 100
E- size : 64266
e+ mean size : 51.12
e- mean size : 32.577053
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 95
#Transitions : 206
Compression level : 4.736842
PDDL Generation
Time : 283.137
4 19
4 19
Recall = 0.04
Precision = 0.17391305
Fscore automaton 0.06504065
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 95
#Transitions : 206
Compression level : 4.736842
PDDL Generation
Time : 2021.367
4 19
4 19
Recall = 0.04
Precision = 0.17391305
Fscore automaton 0.06504065
Syntactical distance : 0.07175926
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 95
#Transitions : 206
Compression level : 4.736842
PDDL Generation
Time : 265.689
4 19
4 19
Recall = 0.04
Precision = 0.17391305
Fscore automaton 0.06504065
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 95
#Transitions : 206
Compression level : 4.736842
PDDL Generation
Time : 1884.378
4 19
4 19
Recall = 0.04
Precision = 0.17391305
Fscore automaton 0.06504065
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 6304
x+ mean size : 15.933333
x- mean size : 9.000634
E+ size : 100
E- size : 62039
e+ mean size : 48.87
e- mean size : 34.41809
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 478.0
#States : 100
#Transitions : 216
Compression level : 4.78
PDDL Generation
Time : 344.884
10 15
10 15
Recall = 0.1
Precision = 0.4
Fscore automaton 0.16000001
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 478.0
#States : 100
#Transitions : 216
Compression level : 4.78
PDDL Generation
Time : 2618.053
10 15
10 15
Recall = 0.1
Precision = 0.4
Fscore automaton 0.16000001
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 478.0
#States : 100
#Transitions : 216
Compression level : 4.78
PDDL Generation
Time : 346.006
10 15
10 15
Recall = 0.1
Precision = 0.4
Fscore automaton 0.16000001
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 478.0
#States : 100
#Transitions : 216
Compression level : 4.78
PDDL Generation
Time : 2423.125
10 15
10 15
Recall = 0.1
Precision = 0.4
Fscore automaton 0.16000001
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 5919
x+ mean size : 15.466666
x- mean size : 8.870079
E+ size : 100
E- size : 59254
e+ mean size : 46.13
e- mean size : 31.88551
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 94
#Transitions : 215
Compression level : 4.93617
PDDL Generation
Time : 325.396
4 23
4 23
Recall = 0.04
Precision = 0.14814815
Fscore automaton 0.062992126
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 94
#Transitions : 215
Compression level : 4.93617
PDDL Generation
Time : 2501.492
4 23
4 23
Recall = 0.04
Precision = 0.14814815
Fscore automaton 0.062992126
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 464.0
#States : 94
#Transitions : 215
Compression level : 4.93617
PDDL Generation
Time : 329.481
4 23
4 23
Recall = 0.04
Precision = 0.14814815
Fscore automaton 0.062992126
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 464.0
#States : 94
#Transitions : 215
Compression level : 4.93617
PDDL Generation
Time : 2517.697
4 23
4 23
Recall = 0.04
Precision = 0.14814815
Fscore automaton 0.062992126
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 5609
x+ mean size : 15.166667
x- mean size : 8.671956
E+ size : 100
E- size : 65462
e+ mean size : 52.31
e- mean size : 35.93804
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 77
#Transitions : 168
Compression level : 5.909091
PDDL Generation
Time : 282.613
8 25
8 25
Recall = 0.08
Precision = 0.24242425
Fscore automaton 0.120300755
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 77
#Transitions : 168
Compression level : 5.909091
PDDL Generation
Time : 2290.372
8 25
8 25
Recall = 0.08
Precision = 0.24242425
Fscore automaton 0.120300755
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 77
#Transitions : 168
Compression level : 5.909091
PDDL Generation
Time : 282.528
8 25
8 25
Recall = 0.08
Precision = 0.24242425
Fscore automaton 0.120300755
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 77
#Transitions : 168
Compression level : 5.909091
PDDL Generation
Time : 2400.095
8 25
8 25
Recall = 0.08
Precision = 0.24242425
Fscore automaton 0.120300755
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 5851
x+ mean size : 15.266666
x- mean size : 8.665357
E+ size : 100
E- size : 67512
e+ mean size : 53.57
e- mean size : 35.69034
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 85
#Transitions : 193
Compression level : 5.388235
PDDL Generation
Time : 609.391
7 12
7 12
Recall = 0.07
Precision = 0.36842105
Fscore automaton 0.11764706
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 85
#Transitions : 193
Compression level : 5.388235
PDDL Generation
Time : 2854.957
7 12
7 12
Recall = 0.07
Precision = 0.36842105
Fscore automaton 0.11764706
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 85
#Transitions : 193
Compression level : 5.388235
PDDL Generation
Time : 305.188
7 12
7 12
Recall = 0.07
Precision = 0.36842105
Fscore automaton 0.11764706
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 85
#Transitions : 193
Compression level : 5.388235
PDDL Generation
Time : 2592.247
7 12
7 12
Recall = 0.07
Precision = 0.36842105
Fscore automaton 0.11764706
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
