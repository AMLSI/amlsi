# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial3.pddl
Delta=8
pddl/parking/domain.pddl
E+ size : 100
E- size : 64266
e+ mean size : 51.12
e- mean size : 32.577053
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 1996
Time : 250.14
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 28
Size I+ 28
Size I- 4923
Time : 1830.985
false 
Syntactical distance : 0.053240746
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1348
Time : 50.367
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 1980
Time : 156.326
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 62039
e+ mean size : 48.87
e- mean size : 34.41809
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 43
Size I+ 43
Size I- 7168
Time : 4629.139
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 48
Size I+ 48
Size I- 7849
Time : 6483.376
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1667
Time : 56.617
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1905
Time : 102.36
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 59254
e+ mean size : 46.13
e- mean size : 31.88551
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 24
Size I+ 24
Size I- 3659
Time : 625.275
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 3995
Time : 1111.454
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1550
Time : 56.589
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 2246
Time : 140.797
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 65462
e+ mean size : 52.31
e- mean size : 35.93804
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 3271
Time : 438.52
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 85
Size I+ 85
Size I- 13823
Time : 26684.324
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1577
Time : 57.233
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 2707
Time : 235.014
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 67512
e+ mean size : 53.57
e- mean size : 35.69034
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 2329
Time : 178.84
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 33
Size I+ 33
Size I- 5785
Time : 2753.88
false 
Syntactical distance : 0.05787037
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1606
Time : 62.005
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1831
Time : 104.353
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
