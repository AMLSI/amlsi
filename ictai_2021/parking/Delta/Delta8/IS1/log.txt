# actions 60
# predicate 24
Initial state : pddl/parking/initial_states/initial1.pddl
Delta=8
pddl/parking/domain.pddl
E+ size : 100
E- size : 69720
e+ mean size : 52.15
e- mean size : 33.316868
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 10
Size I+ 10
Size I- 1773
Time : 101.796
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 4287
Time : 727.059
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1853
Time : 80.3
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 1853
Time : 85.78
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 73782
e+ mean size : 54.99
e- mean size : 35.674988
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 20
Size I+ 20
Size I- 3008
Time : 398.456
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 48
Size I+ 48
Size I- 7794
Time : 4253.053
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1635
Time : 69.759
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1782
Time : 82.12
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 62917
e+ mean size : 46.68
e- mean size : 32.31421
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 31
Size I+ 31
Size I- 5151
Time : 1586.564
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 40
Size I+ 40
Size I- 6583
Time : 3256.835
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1666
Time : 48.18
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 2131
Time : 107.211
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 70826
e+ mean size : 52.64
e- mean size : 34.61439
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2141
Time : 143.556
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 33
Size I+ 33
Size I- 5666
Time : 1421.679
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1510
Time : 52.618
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1754
Time : 88.603
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 63547
e+ mean size : 47.59
e- mean size : 32.401325
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1808
Time : 113.503
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 20
Size I+ 20
Size I- 4146
Time : 893.738
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1611
Time : 86.006
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1923
Time : 108.887
false 
Syntactical distance : 0.039351854
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
