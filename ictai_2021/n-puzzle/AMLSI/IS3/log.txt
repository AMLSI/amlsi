test [move( t_1 p_1_1 p_1_2), move( t_1 p_1_1 p_2_1), move( t_1 p_1_2 p_1_1), move( t_1 p_1_2 p_2_2), move( t_1 p_2_1 p_1_1), move( t_1 p_2_1 p_2_2), move( t_1 p_2_2 p_1_2), move( t_1 p_2_2 p_2_1), move( t_2 p_1_1 p_1_2), move( t_2 p_1_1 p_2_1), move( t_2 p_1_2 p_1_1), move( t_2 p_1_2 p_2_2), move( t_2 p_2_1 p_1_1), move( t_2 p_2_1 p_2_2), move( t_2 p_2_2 p_1_2), move( t_2 p_2_2 p_2_1), move( t_3 p_1_1 p_1_2), move( t_3 p_1_1 p_2_1), move( t_3 p_1_2 p_1_1), move( t_3 p_1_2 p_2_2), move( t_3 p_2_1 p_1_1), move( t_3 p_2_1 p_2_2), move( t_3 p_2_2 p_1_2), move( t_3 p_2_2 p_2_1)]
# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial3.pddl
pddl/n-puzzle/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3220
x+ mean size : 14.933333
x- mean size : 8.297205
E+ size : 100
E- size : 37252
e+ mean size : 49.84
e- mean size : 34.824387
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 5.231
44 0
44 0
Recall = 0.44
Precision = 1.0
Fscore automaton 0.6111111
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 12.016
44 0
44 0
Recall = 0.44
Precision = 1.0
Fscore automaton 0.6111111
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 5.802
44 0
44 0
Recall = 0.44
Precision = 1.0
Fscore automaton 0.6111111
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 448.0
#States : 14
#Transitions : 26
Compression level : 32.0
PDDL Generation
Time : 10.773
44 0
44 0
Recall = 0.44
Precision = 1.0
Fscore automaton 0.6111111
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 3343
x+ mean size : 14.8
x- mean size : 8.064612
E+ size : 100
E- size : 37556
e+ mean size : 51.6
e- mean size : 34.7062
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 13
#Transitions : 25
Compression level : 34.153847
PDDL Generation
Time : 4.411
48 0
48 0
Recall = 0.48
Precision = 1.0
Fscore automaton 0.6486486
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 13
#Transitions : 25
Compression level : 34.153847
PDDL Generation
Time : 11.627
48 0
48 0
Recall = 0.48
Precision = 1.0
Fscore automaton 0.6486486
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 444.0
#States : 13
#Transitions : 25
Compression level : 34.153847
PDDL Generation
Time : 4.497
48 0
48 0
Recall = 0.48
Precision = 1.0
Fscore automaton 0.6486486
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 444.0
#States : 13
#Transitions : 25
Compression level : 34.153847
PDDL Generation
Time : 11.956
48 0
48 0
Recall = 0.48
Precision = 1.0
Fscore automaton 0.6486486
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 3541
x+ mean size : 15.7
x- mean size : 8.488845
E+ size : 100
E- size : 37363
e+ mean size : 50.91
e- mean size : 34.296764
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 471.0
#States : 13
#Transitions : 25
Compression level : 36.23077
PDDL Generation
Time : 4.526
61 0
61 0
Recall = 0.61
Precision = 1.0
Fscore automaton 0.757764
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 471.0
#States : 13
#Transitions : 25
Compression level : 36.23077
PDDL Generation
Time : 8.668
61 0
61 0
Recall = 0.61
Precision = 1.0
Fscore automaton 0.757764
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 471.0
#States : 13
#Transitions : 25
Compression level : 36.23077
PDDL Generation
Time : 4.46
61 0
61 0
Recall = 0.61
Precision = 1.0
Fscore automaton 0.757764
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 471.0
#States : 13
#Transitions : 25
Compression level : 36.23077
PDDL Generation
Time : 9.668
61 0
61 0
Recall = 0.61
Precision = 1.0
Fscore automaton 0.757764
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 3287
x+ mean size : 15.166667
x- mean size : 8.607545
E+ size : 100
E- size : 38355
e+ mean size : 52.48
e- mean size : 35.88656
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 4.461
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 8.634
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 4.454
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 455.0
#States : 12
#Transitions : 24
Compression level : 37.916668
PDDL Generation
Time : 8.692
100 0
100 0
Recall = 1.0
Precision = 1.0
Fscore automaton 1.0
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 3424
x+ mean size : 15.633333
x- mean size : 8.646612
E+ size : 100
E- size : 36211
e+ mean size : 48.92
e- mean size : 34.156445
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 469.0
#States : 13
#Transitions : 25
Compression level : 36.076923
PDDL Generation
Time : 4.923
60 0
60 0
Recall = 0.6
Precision = 1.0
Fscore automaton 0.75
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 469.0
#States : 13
#Transitions : 25
Compression level : 36.076923
PDDL Generation
Time : 12.18
60 0
60 0
Recall = 0.6
Precision = 1.0
Fscore automaton 0.75
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 469.0
#States : 13
#Transitions : 25
Compression level : 36.076923
PDDL Generation
Time : 4.98
60 0
60 0
Recall = 0.6
Precision = 1.0
Fscore automaton 0.75
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 469.0
#States : 13
#Transitions : 25
Compression level : 36.076923
PDDL Generation
Time : 12.672
60 0
60 0
Recall = 0.6
Precision = 1.0
Fscore automaton 0.75
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
