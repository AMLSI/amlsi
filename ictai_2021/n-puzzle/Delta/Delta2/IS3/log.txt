# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial3.pddl
Delta=2
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 37252
e+ mean size : 49.84
e- mean size : 34.824387
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 298
Time : 0.994
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 298
Time : 2.479
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 211
Time : 0.504
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 211
Time : 0.717
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 37556
e+ mean size : 51.6
e- mean size : 34.7062
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 264
Time : 0.669
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 264
Time : 0.643
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 249
Time : 0.518
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 249
Time : 0.868
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 37363
e+ mean size : 50.91
e- mean size : 34.296764
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 329
Time : 0.79
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 329
Time : 1.037
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 267
Time : 0.591
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 267
Time : 0.851
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 38355
e+ mean size : 52.48
e- mean size : 35.88656
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 416
Time : 0.893
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 416
Time : 1.268
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 387
Time : 0.821
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 387
Time : 1.018
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 36211
e+ mean size : 48.92
e- mean size : 34.156445
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 273
Time : 0.688
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 273
Time : 1.051
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 360
Time : 0.705
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 360
Time : 1.058
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
