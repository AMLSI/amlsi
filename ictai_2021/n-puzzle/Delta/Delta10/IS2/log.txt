# actions 24
# predicate 24
Initial state : pddl/n-puzzle/initial_states/initial2.pddl
Delta=10
pddl/n-puzzle/domain.pddl
E+ size : 100
E- size : 39394
e+ mean size : 53.79
e- mean size : 35.195488
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1062
Time : 7.388
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1062
Time : 6.085
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1060
Time : 6.215
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1060
Time : 6.484
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 36245
e+ mean size : 49.32
e- mean size : 33.418705
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1176
Time : 6.611
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1176
Time : 6.735
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1083
Time : 6.47
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1083
Time : 6.625
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 34172
e+ mean size : 46.09
e- mean size : 32.290882
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1047
Time : 6.081
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1047
Time : 6.972
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1218
Time : 6.72
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1218
Time : 12.239
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 36102
e+ mean size : 49.0
e- mean size : 33.54753
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 955
Time : 8.091
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 955
Time : 8.851
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1098
Time : 8.014
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1098
Time : 8.168
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 33544
e+ mean size : 46.18
e- mean size : 32.959007
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 982
Time : 5.855
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 982
Time : 6.047
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1052
Time : 6.483
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1052
Time : 6.671
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
