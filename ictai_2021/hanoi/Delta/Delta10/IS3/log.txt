# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial3.pddl
Delta=10
pddl/hanoi/domain.pddl
E+ size : 100
E- size : 33107
e+ mean size : 49.13
e- mean size : 32.896305
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 1194
Time : 50.772
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 1319
Time : 90.665
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1195
Time : 28.35
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 17
Size I+ 17
Size I- 1510
Time : 75.294
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 37215
e+ mean size : 55.09
e- mean size : 35.114094
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 17
Size I+ 17
Size I- 1310
Time : 81.337
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 44
Size I+ 44
Size I- 3412
Time : 685.386
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 884
Time : 22.889
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 884
Time : 39.86
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 34186
e+ mean size : 50.21
e- mean size : 33.80232
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 12
Size I+ 12
Size I- 1084
Time : 29.943
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 1585
Time : 137.711
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 900
Time : 26.418
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 1172
Time : 42.477
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 33142
e+ mean size : 48.15
e- mean size : 33.94388
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 1226
Time : 37.015
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 19
Size I+ 19
Size I- 1595
Time : 179.369
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1301
Time : 25.07
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 15
Size I+ 15
Size I- 1585
Time : 130.499
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 33944
e+ mean size : 49.84
e- mean size : 32.84831
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 16
Size I+ 16
Size I- 1560
Time : 61.437
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 39
Size I+ 39
Size I- 3209
Time : 490.21
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1150
Time : 20.836
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1150
Time : 59.523
false 
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
