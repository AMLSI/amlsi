test [pick_up( d1 c1), pick_up( d1 c2), pick_up( d1 c3), pick_up( d2 c1), pick_up( d2 c2), pick_up( d2 c3), pick_up( d3 c1), pick_up( d3 c2), pick_up( d3 c3), unstack( d2 d1), unstack( d3 d1), unstack( d3 d2), put_down( d1 c1), put_down( d1 c2), put_down( d1 c3), put_down( d2 c1), put_down( d2 c2), put_down( d2 c3), put_down( d3 c1), put_down( d3 c2), put_down( d3 c3), stack( d2 d1), stack( d3 d1), stack( d3 d2)]
# actions 24
# predicate 25
Initial state : pddl/hanoi/initial_states/initial3.pddl
pddl/hanoi/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 3014
x+ mean size : 14.866667
x- mean size : 8.545786
E+ size : 100
E- size : 33107
e+ mean size : 49.13
e- mean size : 32.896305
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 446.0
#States : 32
#Transitions : 57
Compression level : 13.9375
PDDL Generation
Time : 34.369
28 37
28 37
Recall = 0.28
Precision = 0.43076923
Fscore automaton 0.33939394
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 446.0
#States : 32
#Transitions : 57
Compression level : 13.9375
PDDL Generation
Time : 192.348
28 37
28 37
Recall = 0.28
Precision = 0.43076923
Fscore automaton 0.33939394
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 446.0
#States : 32
#Transitions : 57
Compression level : 13.9375
PDDL Generation
Time : 33.194
28 37
28 37
Recall = 0.28
Precision = 0.43076923
Fscore automaton 0.33939394
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 446.0
#States : 32
#Transitions : 57
Compression level : 13.9375
PDDL Generation
Time : 240.818
28 37
28 37
Recall = 0.28
Precision = 0.43076923
Fscore automaton 0.33939394
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 2974
x+ mean size : 14.333333
x- mean size : 8.115333
E+ size : 100
E- size : 37215
e+ mean size : 55.09
e- mean size : 35.114094
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 430.0
#States : 27
#Transitions : 48
Compression level : 15.925926
PDDL Generation
Time : 32.761
16 22
16 22
Recall = 0.16
Precision = 0.42105263
Fscore automaton 0.23188403
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 430.0
#States : 27
#Transitions : 48
Compression level : 15.925926
PDDL Generation
Time : 210.527
16 22
16 22
Recall = 0.16
Precision = 0.42105263
Fscore automaton 0.23188403
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 430.0
#States : 27
#Transitions : 48
Compression level : 15.925926
PDDL Generation
Time : 34.024
16 22
16 22
Recall = 0.16
Precision = 0.42105263
Fscore automaton 0.23188403
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 430.0
#States : 27
#Transitions : 48
Compression level : 15.925926
PDDL Generation
Time : 220.512
16 22
16 22
Recall = 0.16
Precision = 0.42105263
Fscore automaton 0.23188403
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 3145
x+ mean size : 15.066667
x- mean size : 8.776789
E+ size : 100
E- size : 34186
e+ mean size : 50.21
e- mean size : 33.80232
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 28
#Transitions : 50
Compression level : 16.142857
PDDL Generation
Time : 33.323
40 28
40 28
Recall = 0.4
Precision = 0.5882353
Fscore automaton 0.47619048
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 28
#Transitions : 50
Compression level : 16.142857
PDDL Generation
Time : 253.502
40 28
40 28
Recall = 0.4
Precision = 0.5882353
Fscore automaton 0.47619048
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 452.0
#States : 28
#Transitions : 50
Compression level : 16.142857
PDDL Generation
Time : 34.276
40 28
40 28
Recall = 0.4
Precision = 0.5882353
Fscore automaton 0.47619048
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 452.0
#States : 28
#Transitions : 50
Compression level : 16.142857
PDDL Generation
Time : 252.997
40 28
40 28
Recall = 0.4
Precision = 0.5882353
Fscore automaton 0.47619048
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 2749
x+ mean size : 14.066667
x- mean size : 8.328847
E+ size : 100
E- size : 33142
e+ mean size : 48.15
e- mean size : 33.94388
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 422.0
#States : 37
#Transitions : 66
Compression level : 11.405405
PDDL Generation
Time : 34.521
38 13
38 13
Recall = 0.38
Precision = 0.74509805
Fscore automaton 0.5033113
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 422.0
#States : 37
#Transitions : 66
Compression level : 11.405405
PDDL Generation
Time : 255.197
38 13
38 13
Recall = 0.38
Precision = 0.74509805
Fscore automaton 0.5033113
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 422.0
#States : 37
#Transitions : 66
Compression level : 11.405405
PDDL Generation
Time : 34.251
38 13
38 13
Recall = 0.38
Precision = 0.74509805
Fscore automaton 0.5033113
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 422.0
#States : 37
#Transitions : 66
Compression level : 11.405405
PDDL Generation
Time : 199.636
38 13
38 13
Recall = 0.38
Precision = 0.74509805
Fscore automaton 0.5033113
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 2667
x+ mean size : 13.8
x- mean size : 8.049494
E+ size : 100
E- size : 33944
e+ mean size : 49.84
e- mean size : 32.84831
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 414.0
#States : 31
#Transitions : 54
Compression level : 13.354838
PDDL Generation
Time : 27.198
25 9
25 9
Recall = 0.25
Precision = 0.7352941
Fscore automaton 0.37313432
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 414.0
#States : 31
#Transitions : 54
Compression level : 13.354838
PDDL Generation
Time : 160.589
25 9
25 9
Recall = 0.25
Precision = 0.7352941
Fscore automaton 0.37313432
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 414.0
#States : 31
#Transitions : 54
Compression level : 13.354838
PDDL Generation
Time : 27.632
25 9
25 9
Recall = 0.25
Precision = 0.7352941
Fscore automaton 0.37313432
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 414.0
#States : 31
#Transitions : 54
Compression level : 13.354838
PDDL Generation
Time : 165.921
25 9
25 9
Recall = 0.25
Precision = 0.7352941
Fscore automaton 0.37313432
Syntactical distance : 0.009259259
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
