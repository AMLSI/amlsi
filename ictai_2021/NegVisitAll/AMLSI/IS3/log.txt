test [move( n0 n1), move( n0 n3), move( n1 n3), move( n2 n0), move( n3 n2), visit( n0), visit( n1), visit( n2), visit( n3)]
# actions 9
# predicate 13
Initial state : pddl/NegVisitAll/initial_states/initial3.pddl
pddl/NegVisitAll/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 1274
x+ mean size : 15.0
x- mean size : 8.771585
E+ size : 100
E- size : 16565
e+ mean size : 50.28
e- mean size : 35.80543
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 19.722
69 510
69 510
Recall = 0.69
Precision = 0.119170986
Fscore automaton 0.20324007
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 17.862
69 510
69 510
Recall = 0.69
Precision = 0.119170986
Fscore automaton 0.20324007
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 18.42
69 510
69 510
Recall = 0.69
Precision = 0.119170986
Fscore automaton 0.20324007
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 450.0
#States : 43
#Transitions : 73
Compression level : 10.4651165
PDDL Generation
Time : 18.001
69 510
69 510
Recall = 0.69
Precision = 0.119170986
Fscore automaton 0.20324007
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 1329
x+ mean size : 15.266666
x- mean size : 9.3927765
E+ size : 100
E- size : 16588
e+ mean size : 49.09
e- mean size : 34.914818
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 46
#Transitions : 75
Compression level : 9.956522
PDDL Generation
Time : 19.268
48 449
48 449
Recall = 0.48
Precision = 0.09657948
Fscore automaton 0.16080403
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 46
#Transitions : 75
Compression level : 9.956522
PDDL Generation
Time : 20.366
48 449
48 449
Recall = 0.48
Precision = 0.09657948
Fscore automaton 0.16080403
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 458.0
#States : 46
#Transitions : 75
Compression level : 9.956522
PDDL Generation
Time : 20.573
48 449
48 449
Recall = 0.48
Precision = 0.09657948
Fscore automaton 0.16080403
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 458.0
#States : 46
#Transitions : 75
Compression level : 9.956522
PDDL Generation
Time : 20.876
48 449
48 449
Recall = 0.48
Precision = 0.09657948
Fscore automaton 0.16080403
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 1354
x+ mean size : 15.4
x- mean size : 9.19424
E+ size : 100
E- size : 15488
e+ mean size : 47.66
e- mean size : 35.090843
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 49
#Transitions : 82
Compression level : 9.428572
PDDL Generation
Time : 22.458
54 383
54 383
Recall = 0.54
Precision = 0.123569794
Fscore automaton 0.20111732
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 49
#Transitions : 82
Compression level : 9.428572
PDDL Generation
Time : 23.178
54 383
54 383
Recall = 0.54
Precision = 0.123569794
Fscore automaton 0.20111732
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 462.0
#States : 49
#Transitions : 82
Compression level : 9.428572
PDDL Generation
Time : 22.594
54 383
54 383
Recall = 0.54
Precision = 0.123569794
Fscore automaton 0.20111732
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 462.0
#States : 49
#Transitions : 82
Compression level : 9.428572
PDDL Generation
Time : 23.356
54 383
54 383
Recall = 0.54
Precision = 0.123569794
Fscore automaton 0.20111732
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 1390
x+ mean size : 15.433333
x- mean size : 9.5474825
E+ size : 100
E- size : 16307
e+ mean size : 49.86
e- mean size : 36.10063
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 48
#Transitions : 83
Compression level : 9.645833
PDDL Generation
Time : 22.615
32 345
32 345
Recall = 0.32
Precision = 0.084880635
Fscore automaton 0.1341719
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 48
#Transitions : 83
Compression level : 9.645833
PDDL Generation
Time : 22.768
32 345
32 345
Recall = 0.32
Precision = 0.084880635
Fscore automaton 0.1341719
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 463.0
#States : 48
#Transitions : 83
Compression level : 9.645833
PDDL Generation
Time : 22.585
32 345
32 345
Recall = 0.32
Precision = 0.084880635
Fscore automaton 0.1341719
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 463.0
#States : 48
#Transitions : 83
Compression level : 9.645833
PDDL Generation
Time : 23.555
32 345
32 345
Recall = 0.32
Precision = 0.084880635
Fscore automaton 0.1341719
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 1268
x+ mean size : 14.766666
x- mean size : 8.644321
E+ size : 100
E- size : 17160
e+ mean size : 51.96
e- mean size : 36.774475
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 45
#Transitions : 77
Compression level : 9.844444
PDDL Generation
Time : 19.797
31 216
31 216
Recall = 0.31
Precision = 0.12550607
Fscore automaton 0.17867436
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 45
#Transitions : 77
Compression level : 9.844444
PDDL Generation
Time : 19.035
31 216
31 216
Recall = 0.31
Precision = 0.12550607
Fscore automaton 0.17867436
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 443.0
#States : 45
#Transitions : 77
Compression level : 9.844444
PDDL Generation
Time : 15.945
31 216
31 216
Recall = 0.31
Precision = 0.12550607
Fscore automaton 0.17867436
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 443.0
#States : 45
#Transitions : 77
Compression level : 9.844444
PDDL Generation
Time : 18.39
31 216
31 216
Recall = 0.31
Precision = 0.12550607
Fscore automaton 0.17867436
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
