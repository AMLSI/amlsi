# actions 6
# predicate 9
Initial state : pddl/NegVisitAll/initial_states/initial2.pddl
Delta=6
pddl/NegVisitAll/domain.pddl
E+ size : 100
E- size : 13047
e+ mean size : 54.2
e- mean size : 35.577526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 197
Time : 2.373
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 210
Time : 3.7
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 179
Time : 2.795
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 211
Time : 4.308
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 11545
e+ mean size : 49.27
e- mean size : 34.478302
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 204
Time : 2.017
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 204
Time : 2.173
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 190
Time : 3.76
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 190
Time : 2.813
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12597
e+ mean size : 52.18
e- mean size : 34.76288
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 160
Time : 2.235
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 160
Time : 2.379
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 214
Time : 2.575
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 214
Time : 2.722
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12390
e+ mean size : 52.3
e- mean size : 35.445522
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 197
Time : 4.208
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 197
Time : 3.304
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 207
Time : 2.531
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 207
Time : 2.494
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10665
e+ mean size : 46.65
e- mean size : 33.12958
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 222
Time : 1.732
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 222
Time : 2.126
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 203
Time : 1.423
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 203
Time : 1.477
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
