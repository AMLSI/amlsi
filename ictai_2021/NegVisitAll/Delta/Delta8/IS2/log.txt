# actions 6
# predicate 9
Initial state : pddl/NegVisitAll/initial_states/initial2.pddl
Delta=8
pddl/NegVisitAll/domain.pddl
E+ size : 100
E- size : 13047
e+ mean size : 54.2
e- mean size : 35.577526
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 217
Time : 3.917
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 231
Time : 4.559
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 233
Time : 5.386
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 242
Time : 5.031
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 11545
e+ mean size : 49.27
e- mean size : 34.478302
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 260
Time : 4.832
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 260
Time : 4.547
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 219
Time : 7.003
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 219
Time : 5.426
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12597
e+ mean size : 52.18
e- mean size : 34.76288
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 221
Time : 4.582
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 221
Time : 4.846
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 236
Time : 4.301
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 236
Time : 4.015
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 12390
e+ mean size : 52.3
e- mean size : 35.445522
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 270
Time : 8.068
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 270
Time : 6.558
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 248
Time : 4.085
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 248
Time : 4.088
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10665
e+ mean size : 46.65
e- mean size : 33.12958
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 267
Time : 2.99
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 267
Time : 3.372
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 236
Time : 2.319
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 236
Time : 2.327
false 
Syntactical distance : 0.0
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
