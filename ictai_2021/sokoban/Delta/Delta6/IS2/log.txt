# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
Delta=6
pddl/sokoban/domain.pddl
E+ size : 100
E- size : 36679
e+ mean size : 49.78
e- mean size : 34.099567
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 851
Time : 16.132
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 888
Time : 38.92
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 698
Time : 7.621
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 837
Time : 33.034
false 
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010018128
FSCORE : 1.0
E+ size : 100
E- size : 38391
e+ mean size : 53.03
e- mean size : 35.329636
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1351
Time : 32.813
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 1528
Time : 78.495
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 675
Time : 8.014
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1208
Time : 53.064
false 
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.88888896
E+ size : 100
E- size : 37988
e+ mean size : 52.54
e- mean size : 34.1864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 828
Time : 15.523
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 7846
Time : 4114.99
false 
Syntactical distance : 0.10555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0066691367
FSCORE : 0.6389776
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 778
Time : 9.036
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 856
Time : 15.78
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 38594
e+ mean size : 53.41
e- mean size : 35.54537
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1010
Time : 29.376
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 40
Size I+ 40
Size I- 3231
Time : 619.335
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 611
Time : 8.232
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 16
Size I+ 16
Size I- 1356
Time : 56.777
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 39317
e+ mean size : 54.09
e- mean size : 35.744335
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 1383
Time : 37.225
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 21
Size I+ 21
Size I- 2076
Time : 153.183
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 642
Time : 8.31
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 862
Time : 31.491
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
