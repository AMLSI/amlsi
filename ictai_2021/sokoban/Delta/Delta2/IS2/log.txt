# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial2.pddl
Delta=2
pddl/sokoban/domain.pddl
E+ size : 100
E- size : 36679
e+ mean size : 49.78
e- mean size : 34.099567
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 438
Time : 7.315
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 543
Time : 15.099
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 326
Time : 2.209
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 437
Time : 11.22
false 
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010018128
FSCORE : 1.0
E+ size : 100
E- size : 38391
e+ mean size : 53.03
e- mean size : 35.329636
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 484
Time : 6.056
false 
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.88888896

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 619
Time : 8.414
false 
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.010678105
FSCORE : 0.88888896
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 245
Time : 1.723
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 582
Time : 16.996
false 
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.88888896
E+ size : 100
E- size : 37988
e+ mean size : 52.54
e- mean size : 34.1864
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 487
Time : 6.796
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 7846
Time : 3977.718
false 
Syntactical distance : 0.10555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0066691367
FSCORE : 0.6389776
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 380
Time : 2.214
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 495
Time : 7.693
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 38594
e+ mean size : 53.41
e- mean size : 35.54537
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 322
Time : 2.902
false 
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.9189189

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 466
Time : 9.745
false 
Syntactical distance : 0.061111115
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.009890308
FSCORE : 0.9189189
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 327
Time : 1.955
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 327
Time : 4.204
false 
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.009890308
FSCORE : 0.9189189
E+ size : 100
E- size : 39317
e+ mean size : 54.09
e- mean size : 35.744335
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 511
Time : 7.998
false 
Syntactical distance : 0.05
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.90710384

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 758
Time : 24.235
false 
Syntactical distance : 0.072222225
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.90710384
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 334
Time : 2.668
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 517
Time : 12.606
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
