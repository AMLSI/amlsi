# actions 25
# predicate 42
Initial state : pddl/sokoban/initial_states/initial1.pddl
Delta=4
pddl/sokoban/domain.pddl
E+ size : 100
E- size : 45992
e+ mean size : 50.64
e- mean size : 33.55131
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 586
Time : 14.278
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 13
Size I+ 13
Size I- 1390
Time : 49.515
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 709
Time : 4.556
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 794
Time : 10.821
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 45719
e+ mean size : 49.53
e- mean size : 32.957634
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 1267
Time : 38.035
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 23
Size I+ 23
Size I- 1797
Time : 160.369
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 604
Time : 3.598
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 12
Size I+ 12
Size I- 1213
Time : 46.726
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 49679
e+ mean size : 53.87
e- mean size : 34.829586
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 764
Time : 12.876
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 73
Size I+ 73
Size I- 5484
Time : 1942.834
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 557
Time : 3.804
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 557
Time : 6.396
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 42718
e+ mean size : 47.37
e- mean size : 33.856102
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 853
Time : 14.324
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 28
Size I+ 28
Size I- 2134
Time : 218.898
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 600
Time : 3.972
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 5
Size I+ 5
Size I- 600
Time : 12.546
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 48496
e+ mean size : 53.82
e- mean size : 33.019382
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 619
Time : 10.183
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 974
Time : 29.165
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 650
Time : 4.498
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 868
Time : 23.099
false 
Syntactical distance : 0.03888889
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
