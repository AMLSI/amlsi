# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial2.pddl
Delta=2
pddl/peg/domain.pddl
E+ size : 100
E- size : 10705
e+ mean size : 5.69
e- mean size : 4.5860815
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 281
Time : 6.256
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.0047984645
Error Rate Postcondition : 0.0
FSCORE : 0.88888896

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 281
Time : 5.231
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.004821601
Error Rate Postcondition : 0.0
FSCORE : 0.88888896
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 380
Time : 4.522
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.004821601
Error Rate Postcondition : 0.0
FSCORE : 0.88888896

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 380
Time : 6.739
false 
Syntactical distance : 0.1875
Error Rate Precondition : 0.0756129
Error Rate Postcondition : 0.0
FSCORE : 0.88888896
E+ size : 100
E- size : 10225
e+ mean size : 5.47
e- mean size : 4.4870415
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 447
Time : 9.069
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.0073158424
Error Rate Postcondition : 0.0
FSCORE : 0.8304093

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 669
Time : 83.639
false 
Syntactical distance : 0.16666667
Error Rate Precondition : 0.07981698
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 186
Time : 4.31
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.0073697586
Error Rate Postcondition : 0.0
FSCORE : 0.8304093

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 602
Time : 33.455
false 
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10626
e+ mean size : 5.6
e- mean size : 4.4980235
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 519
Time : 25.417
false 
Syntactical distance : 0.125
Error Rate Precondition : 0.0059142434
Error Rate Postcondition : 0.0
FSCORE : 0.8636364

*** Noise = 20.0% ***
Iteration 8
Size I+ 8
Size I- 519
Time : 29.606
false 
Syntactical distance : 0.1875
Error Rate Precondition : 0.07146378
Error Rate Postcondition : 0.0
FSCORE : 0.8636364
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 4
Size I+ 4
Size I- 293
Time : 7.329
false 
Syntactical distance : 0.13194443
Error Rate Precondition : 0.011758942
Error Rate Postcondition : 0.0
FSCORE : 0.8636364

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 697
Time : 49.81
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10512
e+ mean size : 5.61
e- mean size : 4.513889
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 393
Time : 9.98
false 
Syntactical distance : 0.111111104
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.05603736
FSCORE : 0.93457943

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 539
Time : 32.241
false 
Syntactical distance : 0.083333336
Error Rate Precondition : 0.062028304
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 286
Time : 2.943
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 3
Size I+ 3
Size I- 286
Time : 5.665
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10803
e+ mean size : 5.75
e- mean size : 4.5760436
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 6
Size I+ 6
Size I- 465
Time : 13.213
false 
Syntactical distance : 0.1388889
Error Rate Precondition : 0.010989011
Error Rate Postcondition : 0.0
FSCORE : 0.8700565

*** Noise = 20.0% ***
Iteration 9
Size I+ 9
Size I- 553
Time : 42.035
false 
Syntactical distance : 0.14583333
Error Rate Precondition : 0.068571426
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 3
Size I+ 3
Size I- 235
Time : 4.092
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 4
Size I+ 4
Size I- 243
Time : 11.735
false 
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0078125
FSCORE : 1.0
