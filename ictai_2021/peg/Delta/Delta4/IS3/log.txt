# actions 52
# predicate 57
Initial state : pddl/peg/initial_states/initial3.pddl
Delta=4
pddl/peg/domain.pddl
E+ size : 100
E- size : 19977
e+ mean size : 7.92
e- mean size : 6.0419483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 9
Size I+ 9
Size I- 1685
Time : 59.33
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 6
Size I+ 6
Size I- 1104
Time : 46.012
false 
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.011175899
FSCORE : 0.9107982
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 1071
Time : 10.946
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 10
Size I+ 10
Size I- 1990
Time : 59.582
false 
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 20531
e+ mean size : 7.86
e- mean size : 6.0698943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 1986
Time : 117.92
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 1348
Time : 61.9
false 
Syntactical distance : 0.111111104
Error Rate Precondition : 0.05733375
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 7
Size I+ 7
Size I- 1334
Time : 19.364
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 14
Size I+ 14
Size I- 2502
Time : 156.724
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 20292
e+ mean size : 7.81
e- mean size : 6.057609
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 2017
Time : 85.183
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 18
Size I+ 18
Size I- 3193
Time : 335.255
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 1041
Time : 10.855
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 11
Size I+ 11
Size I- 1945
Time : 87.341
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 20344
e+ mean size : 7.68
e- mean size : 6.0107155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 888
Time : 25.888
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.9124424

*** Noise = 20.0% ***
Iteration 34
Size I+ 34
Size I- 5233
Time : 1218.345
false 
Syntactical distance : 0.111111104
Error Rate Precondition : 0.060687795
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 886
Time : 9.909
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 1349
Time : 22.815
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 22230
e+ mean size : 8.17
e- mean size : 6.113045
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 8
Size I+ 8
Size I- 1451
Time : 59.458
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 30
Size I+ 30
Size I- 4085
Time : 779.919
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 5
Size I+ 5
Size I- 837
Time : 10.469
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 7
Size I+ 7
Size I- 1166
Time : 25.345
false 
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.99
