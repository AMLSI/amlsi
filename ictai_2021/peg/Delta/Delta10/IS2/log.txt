# actions 32
# predicate 39
Initial state : pddl/peg/initial_states/initial2.pddl
Delta=10
pddl/peg/domain.pddl
E+ size : 100
E- size : 10705
e+ mean size : 5.69
e- mean size : 4.5860815
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 19
Size I+ 19
Size I- 774
Time : 101.971
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 69
Size I+ 69
Size I- 900
Time : 1090.022
false 
Syntactical distance : 0.111111104
Error Rate Precondition : 0.06278749
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 13
Size I+ 13
Size I- 746
Time : 25.343
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 72
Size I+ 72
Size I- 900
Time : 1692.749
false 
Syntactical distance : 0.20138888
Error Rate Precondition : 0.07190184
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10225
e+ mean size : 5.47
e- mean size : 4.4870415
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 18
Size I+ 18
Size I- 753
Time : 147.957
false 
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 47
Size I+ 47
Size I- 874
Time : 784.881
false 
Syntactical distance : 0.055555556
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 15
Size I+ 15
Size I- 744
Time : 30.573
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 27
Size I+ 27
Size I- 852
Time : 246.471
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10626
e+ mean size : 5.6
e- mean size : 4.4980235
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 27
Size I+ 27
Size I- 878
Time : 188.3
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 83
Size I+ 83
Size I- 900
Time : 1697.543
false 
Syntactical distance : 0.06944445
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.007677543
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 14
Size I+ 14
Size I- 723
Time : 29.45
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 28
Size I+ 28
Size I- 892
Time : 268.671
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10512
e+ mean size : 5.61
e- mean size : 4.513889
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 24
Size I+ 24
Size I- 851
Time : 136.858
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 26
Size I+ 26
Size I- 852
Time : 225.9
false 
Syntactical distance : 0.0625
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 525
Time : 17.731
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 25
Size I+ 25
Size I- 830
Time : 151.594
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
E+ size : 100
E- size : 10803
e+ mean size : 5.75
e- mean size : 4.5760436
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
Iteration 36
Size I+ 36
Size I- 823
Time : 231.492
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 100
Size I+ 100
Size I- 899
Time : 2789.748
false 
Syntactical distance : 0.09027778
Error Rate Precondition : 0.016666668
Error Rate Postcondition : 0.0070422534
FSCORE : 0.8700565
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
Iteration 11
Size I+ 11
Size I- 652
Time : 19.293
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
Iteration 29
Size I+ 29
Size I- 793
Time : 294.62
false 
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
