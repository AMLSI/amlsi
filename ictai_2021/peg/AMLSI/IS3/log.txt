test [jump-new-move( pos-0-0 pos-0-1 pos-0-2), jump-new-move( pos-0-0 pos-1-0 pos-2-0), jump-new-move( pos-0-1 pos-1-1 pos-2-1), jump-new-move( pos-0-2 pos-0-1 pos-0-0), jump-new-move( pos-0-2 pos-1-2 pos-2-2), jump-new-move( pos-1-0 pos-1-1 pos-1-2), jump-new-move( pos-1-0 pos-2-0 pos-3-0), jump-new-move( pos-1-1 pos-2-1 pos-3-1), jump-new-move( pos-1-2 pos-1-1 pos-1-0), jump-new-move( pos-1-2 pos-2-2 pos-3-2), jump-new-move( pos-2-0 pos-1-0 pos-0-0), jump-new-move( pos-2-0 pos-2-1 pos-2-2), jump-new-move( pos-2-1 pos-1-1 pos-0-1), jump-new-move( pos-2-2 pos-1-2 pos-0-2), jump-new-move( pos-2-2 pos-2-1 pos-2-0), jump-new-move( pos-3-0 pos-2-0 pos-1-0), jump-new-move( pos-3-0 pos-3-1 pos-3-2), jump-new-move( pos-3-1 pos-2-1 pos-1-1), jump-new-move( pos-3-2 pos-2-2 pos-1-2), jump-new-move( pos-3-2 pos-3-1 pos-3-0), jump-continue-move( pos-0-0 pos-0-1 pos-0-2), jump-continue-move( pos-0-0 pos-1-0 pos-2-0), jump-continue-move( pos-0-1 pos-1-1 pos-2-1), jump-continue-move( pos-0-2 pos-0-1 pos-0-0), jump-continue-move( pos-0-2 pos-1-2 pos-2-2), jump-continue-move( pos-1-0 pos-1-1 pos-1-2), jump-continue-move( pos-1-0 pos-2-0 pos-3-0), jump-continue-move( pos-1-1 pos-2-1 pos-3-1), jump-continue-move( pos-1-2 pos-1-1 pos-1-0), jump-continue-move( pos-1-2 pos-2-2 pos-3-2), jump-continue-move( pos-2-0 pos-1-0 pos-0-0), jump-continue-move( pos-2-0 pos-2-1 pos-2-2), jump-continue-move( pos-2-1 pos-1-1 pos-0-1), jump-continue-move( pos-2-2 pos-1-2 pos-0-2), jump-continue-move( pos-2-2 pos-2-1 pos-2-0), jump-continue-move( pos-3-0 pos-2-0 pos-1-0), jump-continue-move( pos-3-0 pos-3-1 pos-3-2), jump-continue-move( pos-3-1 pos-2-1 pos-1-1), jump-continue-move( pos-3-2 pos-2-2 pos-1-2), jump-continue-move( pos-3-2 pos-3-1 pos-3-0), end-move( pos-0-0), end-move( pos-0-1), end-move( pos-0-2), end-move( pos-1-0), end-move( pos-1-1), end-move( pos-1-2), end-move( pos-2-0), end-move( pos-2-1), end-move( pos-2-2), end-move( pos-3-0), end-move( pos-3-1), end-move( pos-3-2)]
# actions 52
# predicate 57
Initial state : pddl/peg/initial_states/initial3.pddl
pddl/peg/domain.pddl

RPNIR run : 0
I+ size : 30
I- size : 6241
x+ mean size : 8.166667
x- mean size : 6.232655
E+ size : 100
E- size : 19977
e+ mean size : 7.92
e- mean size : 6.0419483
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 245.00002
#States : 57
#Transitions : 77
Compression level : 4.298246
PDDL Generation
Time : 202.742
47 5
47 5
Recall = 0.47
Precision = 0.90384614
Fscore automaton 0.6184211
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 245.00002
#States : 57
#Transitions : 77
Compression level : 4.298246
PDDL Generation
Time : 571.757
47 5
47 5
Recall = 0.47
Precision = 0.90384614
Fscore automaton 0.6184211
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.9847716
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 245.00002
#States : 57
#Transitions : 77
Compression level : 4.298246
PDDL Generation
Time : 73.124
47 5
47 5
Recall = 0.47
Precision = 0.90384614
Fscore automaton 0.6184211
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 245.00002
#States : 57
#Transitions : 77
Compression level : 4.298246
PDDL Generation
Time : 552.175
47 5
47 5
Recall = 0.47
Precision = 0.90384614
Fscore automaton 0.6184211
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 1
I+ size : 30
I- size : 6589
x+ mean size : 8.0
x- mean size : 6.0122933
E+ size : 100
E- size : 20531
e+ mean size : 7.86
e- mean size : 6.0698943
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 240.0
#States : 70
#Transitions : 94
Compression level : 3.4285715
PDDL Generation
Time : 163.923
61 28
61 28
Recall = 0.61
Precision = 0.6853933
Fscore automaton 0.6455027
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 240.0
#States : 70
#Transitions : 94
Compression level : 3.4285715
PDDL Generation
Time : 344.666
61 28
61 28
Recall = 0.61
Precision = 0.6853933
Fscore automaton 0.6455027
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 240.0
#States : 70
#Transitions : 94
Compression level : 3.4285715
PDDL Generation
Time : 60.849
61 28
61 28
Recall = 0.61
Precision = 0.6853933
Fscore automaton 0.6455027
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 240.0
#States : 70
#Transitions : 94
Compression level : 3.4285715
PDDL Generation
Time : 492.71
61 28
61 28
Recall = 0.61
Precision = 0.6853933
Fscore automaton 0.6455027
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 2
I+ size : 30
I- size : 6222
x+ mean size : 8.033334
x- mean size : 6.220186
E+ size : 100
E- size : 20292
e+ mean size : 7.81
e- mean size : 6.057609
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 241.00002
#States : 71
#Transitions : 101
Compression level : 3.3943665
PDDL Generation
Time : 205.418
51 5
51 5
Recall = 0.51
Precision = 0.91071427
Fscore automaton 0.65384614
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 241.00002
#States : 71
#Transitions : 101
Compression level : 3.3943665
PDDL Generation
Time : 512.644
51 5
51 5
Recall = 0.51
Precision = 0.91071427
Fscore automaton 0.65384614
Syntactical distance : 0.048611116
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 0.99497485
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 241.00002
#States : 71
#Transitions : 101
Compression level : 3.3943665
PDDL Generation
Time : 78.517
51 5
51 5
Recall = 0.51
Precision = 0.91071427
Fscore automaton 0.65384614
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 241.00002
#States : 71
#Transitions : 101
Compression level : 3.3943665
PDDL Generation
Time : 575.09
51 5
51 5
Recall = 0.51
Precision = 0.91071427
Fscore automaton 0.65384614
Syntactical distance : 0.111111104
Error Rate Precondition : 0.05719644
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 3
I+ size : 30
I- size : 5920
x+ mean size : 8.033334
x- mean size : 5.9567566
E+ size : 100
E- size : 20344
e+ mean size : 7.68
e- mean size : 6.0107155
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 241.00002
#States : 65
#Transitions : 94
Compression level : 3.7076926
PDDL Generation
Time : 148.514
60 24
60 24
Recall = 0.6
Precision = 0.71428573
Fscore automaton 0.65217394
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 241.00002
#States : 65
#Transitions : 94
Compression level : 3.7076926
PDDL Generation
Time : 439.659
60 24
60 24
Recall = 0.6
Precision = 0.71428573
Fscore automaton 0.65217394
Syntactical distance : 0.11805555
Error Rate Precondition : 0.060687795
Error Rate Postcondition : 0.0
FSCORE : 0.9847716
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 241.00002
#States : 65
#Transitions : 94
Compression level : 3.7076926
PDDL Generation
Time : 57.091
60 24
60 24
Recall = 0.6
Precision = 0.71428573
Fscore automaton 0.65217394
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 241.00002
#States : 65
#Transitions : 94
Compression level : 3.7076926
PDDL Generation
Time : 467.394
60 24
60 24
Recall = 0.6
Precision = 0.71428573
Fscore automaton 0.65217394
Syntactical distance : 0.111111104
Error Rate Precondition : 0.05721551
Error Rate Postcondition : 0.0
FSCORE : 1.0

RPNIR run : 4
I+ size : 30
I- size : 6260
x+ mean size : 8.1
x- mean size : 6.080671
E+ size : 100
E- size : 22230
e+ mean size : 8.17
e- mean size : 6.113045
############################################
### Fluent = 25.0% ###

*** Noise = 0.0% ***
#Observed states : 243.00002
#States : 64
#Transitions : 93
Compression level : 3.7968752
PDDL Generation
Time : 130.543
49 32
49 32
Recall = 0.49
Precision = 0.60493827
Fscore automaton 0.5414365
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 243.00002
#States : 64
#Transitions : 93
Compression level : 3.7968752
PDDL Generation
Time : 423.219
49 32
49 32
Recall = 0.49
Precision = 0.60493827
Fscore automaton 0.5414365
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
############################################
### Fluent = 100.0% ###

*** Noise = 0.0% ***
#Observed states : 243.00002
#States : 64
#Transitions : 93
Compression level : 3.7968752
PDDL Generation
Time : 49.132
49 32
49 32
Recall = 0.49
Precision = 0.60493827
Fscore automaton 0.5414365
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0

*** Noise = 20.0% ***
#Observed states : 243.00002
#States : 64
#Transitions : 93
Compression level : 3.7968752
PDDL Generation
Time : 468.354
49 32
49 32
Recall = 0.49
Precision = 0.60493827
Fscore automaton 0.5414365
Syntactical distance : 0.041666668
Error Rate Precondition : 0.0
Error Rate Postcondition : 0.0
FSCORE : 1.0
